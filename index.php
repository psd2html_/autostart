<?php
// change the following paths if necessary
$yii=dirname(__FILE__).'/protected/vendor/yii/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
if (!defined('YII_DEBUG'))
{
    define('YII_DEBUG', in_array($_SERVER['REMOTE_ADDR'], array(
        '109.87.73.72',// Artem's IP
        '192.168.1.215',
        '93.153.230.53',
        '80.70.228.66'
    )));
}

defined('YII_DEBUG') or define('YII_DEBUG',false);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);

//do not run app before register YiiExcel autoload
$app = Yii::createWebApplication($config);

//Yii::import('ext.yiiexcel.YiiExcel', true);
//Yii::registerAutoloader(array('YiiExcel', 'autoload'), true);

// Optional:
//  As we always try to run the autoloader before anything else, we can use it to do a few
//      simple checks and initialisations
//PHPExcel_Shared_ZipStreamWrapper::register();

/*if (ini_get('mbstring.func_overload') & 2) {
    throw new Exception('Multibyte function overloading in PHP must be disabled for string functions (2).');
}
PHPExcel_Shared_String::buildCharacterSets();
*/
//Now you can run application
$app->run();