<?php
class BackendModule extends CWebModule
{
	public $debug = false;
    public $sidebar = array();
    
	public function init()
	{
		// Set required classes for import.
		$this->setImport(array(
            'backend.controllers.*',
			'backend.models.*',
		));
	}

	/**
	* @return the current version.
	*/
	public function getVersion()
	{
		return '1.0.0';
	}
}
