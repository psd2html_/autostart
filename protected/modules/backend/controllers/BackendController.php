<?php

class BackendController extends Controller
{
	public $layout='//layouts/inner';

	public function actionIndex()
	{                   
        if(Yii::app()->user->isAdmin())
        {
            $this->render('index');
        }
        else
        {
            $this->redirect(Yii::app()->user->loginUrl);
        }
	}
}
