<?php

class BodyBackendController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/inner';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Body;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

        if(isset($_POST['Body']))
        {
            $oldFileName = $model->image;
            $model->attributes=$_POST['Body'];
            
            $model->image=CUploadedFile::getInstance($model,'image');
            
            if(!is_null($model->image))
            {
                $name = $model->image->getName();
                $newname = uniqid().'.'.end(explode('.',$name));
                $path=Yii::getPathOfAlias(Yii::app()->getModule('car')->assetsBodyPath).'/';
                //echo $path;exit();
                $model->image->saveAs($path.$newname);
                $thumb=Yii::app()->phpThumb->create($path.$newname);
                $thumb->resize(250,188);
                $thumb->save($path."250x188_".$newname);
                $thumb->resize(100,100);
                $thumb->save($path."100x100_".$newname);
                
                $model->image = $newname;
            }
            else
            {
                $model->image = $oldFileName;
            }
            if($model->save())
                $this->redirect(array('view','id'=>$model->id_car_body));
        }

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);



        if(isset($_POST['Body']) || isset($_FILES['Body']))
        {
            if(isset($_POST['Body']))
            {
                $model->attributes=$_POST['Body'];
            }
            if(isset($_FILES['Body']))
            {
                $filename = $_FILES['Body']['name']['image'];
                if(!empty($filename))
                {
                    $newname = uniqid().'.'.end(explode('.',$filename));
                    $path=Yii::getPathOfAlias(Yii::app()->getModule('car')->assetsBodyPath).'/';

                    $filepath = $_FILES['Body']['tmp_name']['image'];

                    copy($filepath, $path.$newname);
                    $thumb=Yii::app()->phpThumb->create($path.$newname);
                    $thumb->resize(250,250);
                    $thumb->save($path."250x250_".$newname);

                    $thumb->resize(100,100);
                    $thumb->save($path."100x100_".$newname);

                    $thumb->resize(80,60);
                    $thumb->save($path."80x60_".$newname);

                    $model->image=$newname;
                }
            }



            if($model->save())
                $this->redirect(array('view','id'=>$model->id_car_body));
        }

        $this->render('update',array(
            'model'=>$model,
        ));



	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $model=new Body('search');
        
        $model->unsetAttributes(); // clear any default values

        $model->setAttributes(
            Yii::app()->getRequest()->getParam(
                'Body',
                array()
            )
        );


        
        $this->render('index',array(
            'model'=>$model,
        ));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Body the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Body::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Body $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='body-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
