<?php

class ComplectationBackendController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/inner';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'deleteAll'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($carModelId)
	{
        $model=new Complectation;

        
        
        $carModel = CarModel::model()->findByPk($carModelId);

        $model->id_car_model = $carModel->id_car_model;

        $post = Yii::app()->getRequest()->getPost('generationMods');
        
        if(Yii::app()->getRequest()->getIsPostRequest() 
            && Yii::app()->getRequest()->getPost('Complectation') 
            && !empty($post))
        {
            $model->attributes = Yii::app()->getRequest()->getPost('Complectation');
            
            if($model->save())
            {
                foreach(Yii::app()->getRequest()->getPost('generationMods') as $genMod)
                {
                    $rel = new ComplectationGenerationModification;
                    $rel->id_car_generation_modification = $genMod;
                    $rel->id_car_complectation = $model->id_car_complectation;
                    $rel->price_min = 0;
                    
                    $rel->save();
                }
                
                $this->redirect(array('update','id'=>$model->id_car_complectation));
            }
        }
     
		$this->render('create',array(
			'model'=>$model,'carModel'=>$carModel
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(Yii::app()->getRequest()->getIsPostRequest() 
            && Yii::app()->getRequest()->getPost('Complectation'))
		{
			$model->attributes=Yii::app()->getRequest()->getPost('Complectation');
            
            $genMods = Yii::app()->getRequest()->getPost('generationMods');
            if(count($genMods)>0)
            {
                foreach($model->genModRel as $rel)
                {
                    if(!in_array($rel->id_car_generation_modification,$genMods))
                    {
                        $rel->delete();
                    } 
                }
                foreach($genMods as $rel)
                {
                    if(!$model->inGenerationMods($rel))
                    {
                        $newGen = new ComplectationGenerationModification;
                        $newGen->id_car_complectation = $model->id_car_complectation;
                        $newGen->id_car_generation_modification = $rel;
                        $newGen->price_min = 0;
                        $newGen->save();
                    }
                }
            }
            else
            {
                foreach($model->genModRel as $rel)
                {
                    $rel->delete();
                }
            }



            if(Yii::app()->getRequest()->getPost('GenMod'))
            {
                foreach(Yii::app()->getRequest()->getPost('GenMod') as $key=>$val)
                {

                    $rel = ComplectationGenerationModification::model()->findByPk($key);


                    if($rel)
                    {
                        $rel->price_min = $val;
                        $rel->save();
                    }
                }
                        
            }
            
            if(isset($_POST['Complectation']['chars']))
            {
                foreach($model->characteristicValues as $char)
                {
                    if(isset($_POST['Complectation']['chars'][$char->id_car_characteristic_value]))
                    {
                        $char->value=1;    
                    }
                    else
                    {
                        $char->value=0;
                    }
                    $char->save();
                }
                //var_dump($_POST['Complectation']['chars']);exit();
            }
            if($model->save())
            {
                $this->redirect(array('update','id'=>$model->id_car_complectation));
            }		
        }

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $model = Complectation::model()->findByPk($id);
        if($model)
        {
            ComplectationCharacteristicValue::model()->deleteAll('id_car_complectation=:id_car_complectation',array(':id_car_complectation' => $id));
            
            ComplectationGenerationModification::model()->deleteAll('id_car_complectation=:id_car_complectation',array(':id_car_complectation' => $id));
            
            $model->delete();
        }
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

    public function actionDeleteAll()
    {
        $idList = Yii::app()->request->getParam('car_complectation_ids');
        foreach ($idList as $id) {
            $model = Complectation::model()->findByPk($id);
            if($model)
            {
                ComplectationCharacteristicValue::model()->deleteAll('id_car_complectation=:id_car_complectation',array(':id_car_complectation' => $id));

                ComplectationGenerationModification::model()->deleteAll('id_car_complectation=:id_car_complectation',array(':id_car_complectation' => $id));

                $model->delete();
            }
        }
    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $model=new Complectation('search');
        
        $model->unsetAttributes(); // clear any default values

        $model->setAttributes(
            Yii::app()->getRequest()->getParam(
                'Complectation',
                array()
            )
        );

  
        
        $this->render('index',array(
            'model'=>$model,
        ));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Complectation('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Complectation']))
			$model->attributes=$_GET['Complectation'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Complectation the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Complectation::model()->findByPk($id);
        $model->loadChars();

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Complectation $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='complectation-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
