<?php

class TypeBackendController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/inner';

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Type');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
}
