<?php

class CatalogController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/main';
    public $defaultAction = 'search';

    public $minPriceSelect;

    public $keywords = 'официальные дилеры в москве, автосалоны, купить машину, новые автомобили, комплектации и цены';
    public $description = 'Новые машины у официальных дилеров Москвы на одной платформе. Удобный подбор марок и моделей по ценам, техническим характеристикам, фото и обзорам';
    public $title = 'Автомобили в наличии у официальных дилеров | Dilerity';
    public $seoText;
    public $seoPage;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(
                    'index', 'car', 'indexAdd', 'view', 'search', 'single', 'favorites',
                    'compare', 'ajaxmod', 'character', 'getmodellist', 'getmodificationlist',
                    'getgenerationlist', 'getbodylist','getmodlist', 'livesearch', 'livesearchin',
                    'dealers', 'profile', 'agroupseopage', 'cgroupseopage', 'dgroupseopage', 'egroupseopage',
                    'sitemap', 'OfferFromDealer', 'SingleModal'
                ),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionDealers()
    {
        $this->render('dealers');
    }


    public function actionProfile()
    {
        $this->render('profile');
    }

    public function actionSearch()
    {

        if (Yii::app()->request->getQuery('slug'))
        {
            $mark = Mark::model()->find('alias=:alias',array('alias'=>Yii::app()->request->getQuery('mark')));
            $seoPage = SeoPage::model()->find('slug=:slug and id_car_mark=:id_car_mark',array('slug'=>Yii::app()->request->getQuery('slug'), 'id_car_mark' => $mark->id_car_mark));
        }

        if (Yii::app()->request->isAjaxRequest && isset($_POST['page']))
            $_GET['page'] = Yii::app()->request->getPost('page');

        $marks = Yii::app()->request->getQuery('marks');
        $markAlias = Yii::app()->request->getQuery('mark');

        $bodies = Yii::app()->request->getQuery('bodies');
        $bodyAlias = Yii::app()->request->getQuery('body');

        if(Yii::app()->request->getQuery('mark') && !empty($markAlias))
        {
            $mark = Mark::model()->find('alias=:alias',array('alias'=>Yii::app()->request->getQuery('mark')));

            if($mark)
            {
                $marks[$mark->id_car_mark]=$mark->id_car_mark;
                $_GET['marks']=array($mark->id_car_mark);
                $seoPage = SeoPage::model()->find('slug=:slug and id_car_mark=:id_car_mark',array('slug'=>Yii::app()->request->getQuery('slug'), 'id_car_mark' => $mark->id_car_mark));
            }
            else
            {
                throw new CHttpException(404,'The requested page does not exist.');
            }
        }

        if(Yii::app()->request->getQuery('body') && !empty($bodyAlias))
        {
            $body = Body::model()->find('alias=:alias',array('alias'=>Yii::app()->request->getQuery('body')));
            $url = Yii::app()->createUrl('/car/catalog',array('body'=>$body->alias));

            if($body)
            {
                if (Yii::app()->request->getUrl() != $url) {
                    $this->redirect($url, 301);
                }

                $bodies[$body->id_car_body]=$body->id_car_body;
                $_GET['bodies']=array($body->id_car_body);
            }
            else
            {
                throw new CHttpException(404,'The requested page does not exist.');
            }
        }

        $pricesort = Yii::app()->request->getQuery('pricesort');

        $searchText = Yii::app()->request->getQuery('search');

        $bodies = Yii::app()->request->getQuery('bodies');

        $privods = Yii::app()->request->getQuery('privod');
        $transmissions = Yii::app()->request->getQuery('transmission');

        if (Yii::app()->request->getQuery('slug')==='diesel') {
            $fuelTypes[] = 'Дизель';
        } else {
            $fuelTypes = Yii::app()->request->getQuery('fuelType');
        }

        $priceMin = Yii::app()->request->getQuery('price-min');
        $priceMax = Yii::app()->request->getQuery('price-max');

        $powerMin = Yii::app()->request->getQuery('power-from');
        //$powerMin = !empty($powerMin)?$powerMin:0;
        $powerMax = Yii::app()->request->getQuery('power-to');
        //$powerMax = !empty($powerMax)?$powerMax:100000;

        $speedMin = Yii::app()->request->getQuery('speed-from');
        //$speedMin = !empty($speedMin)?$speedMin:0;
        $speedMax = Yii::app()->request->getQuery('speed-to');
        //$speedMax = !empty($speedMax)?$speedMax:100000;

        $rashodMin = Yii::app()->request->getQuery('rashod-from');
        //$rashodMin = !empty($rashodMin)?$rashodMin:0;
        $rashodMax = Yii::app()->request->getQuery('rashod-to');
        //$rashodMax = !empty($rashodMax)?$rashodMax:100000;

        /*var_dump($marks);
        exit();
                 */
        $criteria = new CDbCriteria;
        $criteria->select = 't.id, generation.name as GenerationName, generation.year_begin as YearBegin, mods.id_car_body as bodySelect, min(complectationRel.price_min) as minPriceSelect ';


        $criteria->with = array('mark','model','generation','mods','complectationRel'=>array('together'=>true));
        //$criteria->condition = 'generation.id_car_generation in (83)'; //Hyundai Solaris
        $criteria->group = 'generation.name, generation.year_begin, mods.id_car_body ';
        //$criteria->condition = 'generation.id_car_generation is not null and mark.is_visible=1 and t.is_visible=1 and model.is_visible=1 and generation.is_visible=1 and mainBody.id_car_body is not null';

        $criteria->together = false;
        if($marks)
        {
            $criteria->addInCondition('mark.id_car_mark',$marks,'AND');
            $criteria->limit = '100000';
        }
        //$res = GenerationModification::model()->findAll($criteria);
        //echo '<pre>'; var_dump($res); echo '</pre>'; exit;
        if($bodies)
        {
            $criteria->addInCondition('mods.id_car_body',$bodies,'AND');
            //$criteria->addInCondition('mods.alias',$bodies,'AND');
            $criteria->limit = '100000';

        }
        if($privods)
        {
            array_push($criteria->with,'privod');
            $criteria->addInCondition('privod.value',$privods,'AND');
            $criteria->limit = '100000';
            //echo "test";exit();
        }
        if($transmissions)
        {
            array_push($criteria->with,'transmission');
            $criteria->addInCondition('transmission.value',$transmissions,'AND');
            $criteria->limit = '100000';

        }
        if($fuelTypes)
        {
            array_push($criteria->with,'fuel');
            $criteria->addInCondition('fuel.value',$fuelTypes,'AND');
            $criteria->limit = '100000';

        }
        if($priceMin)
        {
            $criteria->addCondition('complectationRel.price_min>='.$priceMin.' or complectationRel.price_min=0');
            $criteria->limit = '100000';

        }
        if($priceMax)
        {
            $criteria->addCondition('complectationRel.price_min<='.$priceMax.' or complectationRel.price_min = 0');
            $criteria->limit = '100000';

        }

        if($powerMin || $powerMax)
        {
            array_push($criteria->with,'power');
            $criteria->addCondition('CAST(REPLACE(power.value, \',\', \'.\') AS DECIMAL(12,2))>=:powerMin AND CAST(REPLACE(power.value, \',\', \'.\') AS DECIMAL(12,2))<=:powerMax');
            $criteria->params[':powerMin']=str_replace(',','.',!empty($powerMin)?$powerMin:0);
            $criteria->params[':powerMax']=str_replace(',','.',!empty($powerMax)?$powerMax:100000);
            $criteria->limit = '100000';

        }

        if($speedMin || $speedMax)
        {
            array_push($criteria->with,'speed');
            $criteria->addCondition('CAST(REPLACE(speed.value, \',\', \'.\') AS DECIMAL(12,2))>=:speedMin AND CAST(REPLACE(speed.value, \',\', \'.\') AS DECIMAL(12,2))<=:speedMax');
            $criteria->params[':speedMin']=str_replace(',','.',!empty($speedMin)?$speedMin:0);
            $criteria->params[':speedMax']=str_replace(',','.',!empty($speedMax)?$speedMax:100000);
            $criteria->limit = '100000';

        }

        if($rashodMin || $rashodMax)
        {
            array_push($criteria->with,'rashod');
            $criteria->addCondition('CAST(REPLACE(rashod.value, \',\', \'.\') AS DECIMAL(12,2))>=:rashodMin AND CAST(REPLACE(rashod.value, \',\', \'.\') AS DECIMAL(12,2))<=:rashodMax');
            $criteria->params[':rashodMin']=str_replace(',','.',!empty($rashodMin)?$rashodMin:0);
            $criteria->params[':rashodMax']=str_replace(',','.',!empty($rashodMax)?$rashodMax:100000);

            $criteria->limit = '100000';
        }

        if($searchText)
        {
            $arr = explode(' ',$searchText);
            foreach($arr as $a)
            {
                if(mb_strlen($a)>2)
                {
                    array_push($criteria->with,'generation');
                    $criteria->addSearchCondition('mark.name',$a,true,'AND');
                    $criteria->addSearchCondition('mark.name_rus',$a,true,'OR');
                    $criteria->addSearchCondition('model.name',$a,true,'OR');
                    $criteria->addSearchCondition('model.name_rus',$a,true,'OR');
                    $criteria->addSearchCondition('generation.name',$a,true,'OR');
                    //$criteria->addSearchCondition('mark.name_rus',$a,true,'AND');
                }
            }
        }
        /*$criteria->addCondition('mark.is_visible=1');
        $criteria->addCondition('model.is_visible=1');
        $criteria->addCondition('generation.is_visible=1');
        $criteria->addCondition('t.is_visible=1');
        */
        $criteria->order = 'generation.year_begin DESC';
        /*if($pricesort)
        {
            $criteria->order = 'cast(minprice.value as unsigned) '.$pricesort;
        }*/
        $markImg = Yii::app()->getModule('car')->defaultCategoryImage;
        $models = 0;
        $mods = 0;
        $mark = "";
        if(count($marks)==1)
        {
            $mark = array_shift($marks);
            $mark = Mark::model()->findByPk($mark);
            if($mark->image)
            {
                if($mark->image)
                    $markImg = '/upload/car/mark/'.$mark->image;
            }
            $models = count($mark->models);
            $mods = count($mark->mods);

        if (isset($seoPage) && $seoPage->group_id == 5) {
            $this->title = $seoPage->title;
            $this->description = $seoPage->description;
            $this->seoPage = $seoPage;
        } else {
            $this->title = (!empty($mark->title)) ? $mark->title : $mark->name_rus . ' / ' . $mark->name . ' — купить по выгодной цене | Все комплектации авто с фото | Гараж — сайт про автомобили';
            $this->description = (!empty($mark->description)) ? $mark->description : 'Онлайн сервис покупки новых машин. Получите предложения на ' . $mark->name . ' до визита в автосалон. Только  официальные дилеры.';
        }

        $this->keywords = $mark->name.', '.$mark->name_rus.', '.$mark->name_rus.' официальный дилер, купить '.$mark->name_rus.', '.$mark->name_rus.' цена, новый '.$mark->name_rus;
            //$this->title='Новые '.$mark->name.' - в наличии у официальных дилеров | Dilerity';
		    //this->description='Предложения официальных дилеров '.$mark->name.' в Москве на одной платформе. Удобный подбор моделей по ценам, техническим характеристикам, фото и обзорам';
        }


        $dataProvider=new CActiveDataProvider('GenerationModification', array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>8,
                'pageVar' =>'page',
            ),
        ));
        //$dataProvider->setPagination(true);
        //var_dump($dataProvider);exit();

        if (Yii::app()->request->isAjaxRequest){
            $this->renderPartial('_loop_search', array(
                'models'=>$dataProvider,
            ));
            Yii::app()->end();
        } else {
            $this->render('search',array(
                'markmodels'=>$models,
                'mods'=>$mods,
                'markImg'=>$markImg,
                'mark'=>$mark,
                'models'=>$dataProvider,
            ));
        }
    }

    public function actionFavorites()
    {
        $favs = array();
        if(isset(Yii::app()->request->cookies['favorites']))
        {
            $cookie = Yii::app()->request->cookies['favorites']->value;
            $favs = json_decode($cookie);
        }

        $arr = array();
        $arrC = array();
        foreach($favs as $fav)
        {
            if($fav->compl==0)
            {
                array_push($arr,$fav->modify);
            }
            else
            {
                array_push($arrC,$fav->compl);
            }
        }
        $criteria = new CDbCriteria;
        $criteria->addInCondition('t.id',$arr,'AND');

        $model = GenerationModification::model()->findAll($criteria);

        $criteria2 = new CDbCriteria;
        $criteria2->addInCondition('t.id_car_complectation_generation_modification',$arrC,'AND');

        $modelC = ComplectationGenerationModification::model()->findAll($criteria2);
        $this->render('favorites',array(
            'models'=>$model,'compls'=>$modelC
        ));
    }


    public function actionSingle($id=null)
    {
        $this->layout='//layouts/single';
        $modelAlias = Yii::app()->request->getQuery('model');
        $model = CarModel::model()->find('alias=:alias',array('alias'=>$modelAlias));

        if (empty($id)) {
            $modification = Modification::model()->find('id_car_model=:id_car_model',array('id_car_model'=>$model->id_car_model));
            $genMod = GenerationModification::model()->find('id_car_modification=:id_car_modification',array('id_car_modification'=>$modification->id_car_modification));
        } else {
            $genMod = GenerationModification::model()->findByPk($id);
        }

        $genMod->generation->loadChars();

        $this->title = $genMod->mark->name . ' ' . $genMod->model->name . ' ' . ucfirst($genMod->body->alias) . ' / '.$genMod->mark->name_rus . ' ' . $genMod->model->name_rus . ' '. $genMod->body->name .' — комплектации и цены | Купить авто от официального дилера (салона) | Фото';
        $this->description = 'Онлайн сервис покупки новых машин. Получите предложения на ' . $genMod->mark->name . ' ' . $genMod->model->name . ' до визита в автосалон. Бесплатно. Только  официальные дилеры.';
        $this->keywords=$genMod->generation->name.', '.$genMod->mark->name_rus.' '.$genMod->model->name_rus.', '.$genMod->mark->name_rus.' '.$genMod->model->name_rus.' цена, '.$genMod->mark->name_rus.' '.$genMod->model->name_rus.' фото, '.$genMod->model->name_rus.' '.$genMod->mods->body->name.', '.$genMod->model->name_rus.', '.$genMod->model->name;


        $this->render('single',array(
            'genMod'=>$genMod
        ));

    }

    public function actionSingleModal()
    {
        if(Yii::app()->request->isPostRequest) {
            $id = Yii::app()->request->getPost('id');


            $model = GenerationModification::model()->findByPk($id);

            return $this->renderPartial('ajax_modal', array(
                'model' => $model
            ));
        }

    }


    public function actionAgroupseopage($slug)
    {
        $this->layout='//layouts/single';

        $slug_explode = explode('/', $slug);
        $id = end($slug_explode);

        $markAlias = Yii::app()->request->getQuery('mark');
        $mark = Mark::model()->find('alias=:alias',array('alias'=>$markAlias));


        $modelAlias = Yii::app()->request->getQuery('model');
        $model = CarModel::model()->find('alias=:alias and id_car_mark=:id_car_mark',array('alias'=>$modelAlias, 'id_car_mark'=>$mark->id_car_mark));

        if (empty($id)) {
            $modification = Modification::model()->find('id_car_model=:id_car_model',array('id_car_model'=>$model->id_car_model));
            $genMod = GenerationModification::model()->find('id_car_modification=:id_car_modification',array('id_car_modification'=>$modification->id_car_modification));
        } else {
            $genMod = GenerationModification::model()->findByPk($id);
        }

        if ($genMod == NULL) {
            return $this->render('single_empty');
        }
        $criteria = new CDbCriteria;
        $criteria->select = 't.id, generation.name as GenerationName, generation.year_begin as YearBegin, mods.id_car_body as bodySelect, min(complectationRel.price_min) as minPriceSelect ';
        $criteria->with = array('mark','model','generation','mods','complectationRel'=>array('together'=>true));
        $criteria->condition = 'generation.id_car_generation='. $genMod->id_car_generation;
        $criteria->addCondition('mark.id_car_mark =' .$mark->id_car_mark);
        $criteria->group = 'generation.name, generation.year_begin, mods.id_car_body ';
        $criteria->together = false;
        $res = GenerationModification::model()->find($criteria);
        $genMod->minPriceSelect = $res->minPriceSelect;

        $genMod->generation->loadChars();
        $seoPage = SeoPage::model()->find('slug=:slug and id_car_model=:id_car_model',array('slug'=>$slug, 'id_car_model'=>$model->id_car_model));
        $this->seoPage = $seoPage;

        if ($seoPage) {
            $this->title = $seoPage->title;
            $this->description = $seoPage->description;
            $this->seoPage = $seoPage;
        } else {
            $this->title = $genMod->mark->name . ' ' . $genMod->model->name . ' ' . ucfirst($genMod->body->alias) . ' / '.$genMod->mark->name_rus . ' ' . $genMod->model->name_rus . ' '. $genMod->body->name .' — комплектации и цены | Купить авто от официального дилера (салона) | Фото';
            $this->description = 'Онлайн сервис покупки новых машин. Получите предложения на ' . $genMod->mark->name . ' ' . $genMod->model->name . ' до визита в автосалон. Бесплатно. Только  официальные дилеры.';
        }

        $this->keywords = $genMod->generation->name.', '.$genMod->mark->name_rus.' '.$genMod->model->name_rus.', '.$genMod->mark->name_rus.' '.$genMod->model->name_rus.' цена, '.$genMod->mark->name_rus.' '.$genMod->model->name_rus.' фото, '.$genMod->model->name_rus.' '.$genMod->mods->body->name.', '.$genMod->model->name_rus.', '.$genMod->model->name;

        if ($seoPage) {
            if ($seoPage->group_block_sort == 2) {
                $this->render('single_photo', array(
                    'genMod' => $genMod
                ));
            } else if ($seoPage->group_block_sort == 3) {
                $this->render('single_tests', array(
                    'genMod' => $genMod
                ));
            } else if ($seoPage->group_block_sort == 4) {
                $this->render('single_review', array(
                    'genMod' => $genMod
                ));
            } else {
                $this->render('single', array(
                    'genMod' => $genMod
                ));
            }
        } else {
            $this->render('single', array(
                'genMod' => $genMod
            ));
        }

    }


    public function actionDgroupseopage($slug, $compl=null)
    {

        $this->layout='//layouts/character';

        $slug_explode = explode('/', $slug);
        $id = end($slug_explode);

        $modelAlias = Yii::app()->request->getQuery('model');
        $model = CarModel::model()->find('alias=:alias',array('alias'=>$modelAlias));

        if ($compl)
        {
            $complGenMod = ComplectationGenerationModification::model()->findByPk($compl);
            $genMod = GenerationModification::model()->find('id=:id',array('id'=>$complGenMod->id_car_generation_modification));
            $seoPage = SeoPage::model()->find('slug=:slug and id_car_model=:id_car_model',array('slug'=>Yii::app()->request->getQuery('slug') . '/' . $compl, 'id_car_model'=>$model->id_car_model));
        } else {
            if (empty($id))
            {
                $modification = Modification::model()->find('id_car_model=:id_car_model',array('id_car_model'=>$model->id_car_model));
                $genMod = GenerationModification::model()->find('id_car_modification=:id_car_modification',array('id_car_modification'=>$modification->id_car_modification));
            } else {
                $genMod = GenerationModification::model()->findByPk($id);
            }
            $complGenMod = ComplectationGenerationModification::model()->find('id_car_generation_modification=:id_car_generation_modification',array('id_car_generation_modification'=>$genMod->id));
            $seoPage = SeoPage::model()->find('slug=:slug and id_car_model=:id_car_model',array('slug'=>$slug, 'id_car_model'=>$model->id_car_model));
        }

        $genMod->generation->loadChars();

        $this->seoPage = $seoPage;

        if ($seoPage && $seoPage->group_id==4) {
            $this->title = $seoPage->title;
            $this->description = $seoPage->description;
            $this->seoText = $seoPage->body;
        } else {
            $this->title = $genMod->mark->name . ' ' . $genMod->model->name . ' ' . ucfirst($genMod->body->alias) . ' / '.$genMod->mark->name_rus . ' ' . $genMod->model->name_rus . ' '. $genMod->body->name .' — комплектации и цены | Купить авто от официального дилера (салона) | Фото';
            $this->description = 'Онлайн сервис покупки новых машин. Получите предложения на ' . $genMod->mark->name . ' ' . $genMod->model->name . ' до визита в автосалон. Бесплатно. Только  официальные дилеры.';
        }

        $this->keywords = $genMod->generation->name.', '.$genMod->mark->name_rus.' '.$genMod->model->name_rus.', '.$genMod->mark->name_rus.' '.$genMod->model->name_rus.' цена, '.$genMod->mark->name_rus.' '.$genMod->model->name_rus.' фото, '.$genMod->model->name_rus.' '.$genMod->mods->body->name.', '.$genMod->model->name_rus.', '.$genMod->model->name;

        $this->render('character',array(
            'genModRel' => $genMod,
            'complRel' => $complGenMod
        ));

    }

    public function actionCharacter()
    {
        $this->layout='//layouts/character';

        $compl = Yii::app()->request->getQuery('compl');
        $mod = Yii::app()->request->getQuery('mod');

        if ($mod)
        {
            $genModRel = GenerationModification::model()->findByPk(Yii::app()->request->getQuery('id'));
            $genModRel->generation->loadChars();
            $genModRel->mods->loadChars();
            $complRel = $genModRel->complectationRel[0];

            $this->title = $genModRel->mark->name_rus . ' '. $genModRel->model->name_rus . ' ' . $genModRel->body->name . ' ' . $mod . ' / ' . $genModRel->mark->name . ' '. $genModRel->model->name . ' ' . $genModRel->body->alias . ' ' . $mod . ' - двигатель, характеристики';
        } else {
            $complRel = ComplectationGenerationModification::model()->findByPk($compl);
            $genModRel = GenerationModification::model()->findByPk($complRel->id_car_generation_modification);
            $genModRel->generation->loadChars();
            $genModRel->mods->loadChars();

            $volume = $genModRel->volume->value/1000;
            $modName = str_replace(".", "-",number_format($volume, 1, '.', ' '));
            $complectation = Complectation::model()->findByPk($complRel->id_car_complectation);

            $this->title = $genModRel->mark->name_rus . ' '. $genModRel->model->name_rus . ' ' . $genModRel->body->name . ' ' .  $modName . ' ' . $complectation->name . ' / ' . $genModRel->mark->name . ' ' . $genModRel->model->name . ' ' . ucfirst($genModRel->body->alias) . ' ' . $modName . ' ' . $complectation->name . ' - комплектация и опция';
        }

        $this->description = 'Онлайн сервис покупки новых машин. Получите предложения на ' . $genModRel->mark->name . ' ' . $genModRel->model->name . ' до визита в автосалон. Только официальные дилеры.';
        $this->keywords = $genModRel->generation->name.', '.$genModRel->mark->name_rus.' '.$genModRel->model->name_rus.', '.$genModRel->mark->name_rus.' '.$genModRel->model->name_rus.' цена, '.$genModRel->mark->name_rus.' '.$genModRel->model->name_rus.' фото, '.$genModRel->model->name_rus.' '.$genModRel->mods->body->name.', '.$genModRel->model->name_rus.', '.$genModRel->model->name;

        $this->render('character',array(
            'genModRel'=>$genModRel,'complRel'=>$complRel
        ));
    }



    public function actionOfferFromDealer()
    {

        $error = array();

        $email_support = Helpers::getSettings('email_support');


        if(Yii::app()->request->isPostRequest)
        {
            $order = time();
            //$post['compls'] = implode(",", array(1121, 1128, 1136));
            $post['compls'] = implode(",", Yii::app()->request->getPost('compls'));
            //$post['colorsChar'] = Yii::app()->request->getPost('colorsChar');
            $post['inputColorCustom'] = Yii::app()->request->getPost('inputColorCustom');
            $post['inputColorAny'] = Yii::app()->request->getPost('inputColorAny');
            $post['inputAddress'] = Yii::app()->request->getPost('inputAddress');
            $post['inputAddressOther'] = Yii::app()->request->getPost('inputAddressOther');
            $post['inputMail'] = Yii::app()->request->getPost('inputMail');
            $post['inputName'] = Yii::app()->request->getPost('inputName');
            $post['inputPhone'] = Yii::app()->request->getPost('inputPhone');
            $post['inputPassword'] = Yii::app()->request->getPost('inputPassword');
            $post['inputCarTrade'] = Yii::app()->request->getPost('inputCarTrade');
            $post['inputCarCredit'] = Yii::app()->request->getPost('inputCarCredit');
            $post['inputComment'] = Yii::app()->request->getPost('inputComment');

            $error = array();

            if(!$post['inputAddress'])
            {
                $error['inputAddress'] = 'Выберите один из пунктов';
            } elseif ($post['inputAddress']==2 && !$post['inputAddressOther'])
            {
                $error['inputAddressOther'] = 'Поле Адресс должно быть заполнено';
            }
            if(!$post['inputMail'])
            {
                $error['inputMail'] = 'Поле Email должно быть заполнено';
            }
            if(!$post['inputPhone'])
            {
                $error['inputPhone'] = 'Поле Телеофон должно быть заполнено';
            }
            if(!$post['inputName'])
            {
                $error['inputName'] = 'Поле ФИО должно быть заполнено';
            }
            if(!$post['compls'])
            {
                $error['compls'] = 'Выберите хотя бы одну комплектацию';
            } else {
                $criteria = new CDbCriteria;
                $criteria->select = '*, mark.name as markName, model.name as modelName';
                $criteria->with = array('mark', 'model', 'mods', 'complectation', 'complectationRel', 'generation');
                $criteria->condition = 'complectationRel.id_car_complectation_generation_modification in (' . $post['compls'] . ')';
                $genMod = GenerationModification::model()->findAll($criteria);

                if ($genMod) {
                    $markName = $genMod[0]['markName'];
                    $modelName = $genMod[0]['modelName'];
                    $body = Body::model()->findByPk($genMod[0]->mods['id_car_body']);
                    $generation_year_begin = '';
                    if (isset($genMod[0]->generation->year_begin)) {
                        $generation_year_begin =  $genMod[0]->generation->year_begin . ' - н.в.';
                    }

                    $modCompls = array();
                    foreach ($genMod as $mod)
                    {
                        $complete_add = array();
                        if (isset($mod->mods->fuel) and !empty($mod->mods->fuel->value)) {
                            $complete_add[] = $mod->mods->fuel->value;
                        }
                        if (isset($mod->mods->privod) and !empty($mod->mods->privod->value)) {
                            $complete_add[] = $mod->mods->privod->value . ' привод';
                        }
                        $complete_add_str = implode(', ', $complete_add);

                        foreach ($mod->complectation as $compl)
                        {
                            $modCompls[] = $mod->mods->name . ' - ' . $compl->name . ' ' . $complete_add_str;
                        }

                    }

                    $compls = implode(", ", $modCompls);

                } else {
                    $error['compls']='Что-то пошло не так. Комплектации не найдены!';
                }

            }
            /*if(!$post['colorsChar'] && $post['inputColorCustom'] && $post['inputColorAny'])
            {
                $error['colorsChar']='Выберите цвет';
            }*/

           /* $user = User::model()->find('email=:email',array('email'=>$post['inputMail']));
            if($user)
            {
                $error['inputMail'] = 'Пользователь с такой почтой уже существует!';
            }*/

            $message = count($error)>0?'Сообщение не отправлено':'Сообщение успешно отправлено';

            if(count($error)==0)
            {
                /*$color = '';
                if (count($post['colorsChar']) > 0) {
                    $color = implode(", ", $post['colorsChar']);
                } else if ($post['inputColorCustom']) {
                    $color = $post['inputColorCustom'];
                } else  if ( $post['inputColorAny']) {
                    $color = $post['inputColorAny'];
                }*/

                if ($post['inputAddress']==2) {
                    $address = $post['inputAddressOther'];
                } else {
                    $address = 'Я из Москвы или области';
                }

                $to = Helpers::getSettings('email');

                //$subject = "Заявка с сайта " . Yii::app()->getBaseUrl(true);
                $subject = "Заявка с сайта garage.am - заявка #'.$order";
                $headers = "From: " . $email_support . "\r\nReply-To: " . $email_support;

                mail($to, $subject,
                    "Заявка на получение предложеня от дилеров" .
                    "\r\nЗаявка #" .$order .
                    "\r\nМарка: " . $markName .
                    "\r\nМодель: " . $modelName . ' '. $generation_year_begin .
                    "\r\nКузов: " . $body['name'] .
                    "\r\nКомплектации: " . $compls .
                    "\r\nФИО: " . $post['inputName'] .
                    "\r\nАдрес: " . $address .
                    "\r\nEmail: " . $post['inputMail'] .
                    "\r\nТелефон: " . $post['inputPhone'] .
                    "\r\n" . $post['inputCarTrade'] .
                    "\r\nДругие пожeлания: ".$post['inputComment']."\r\n" .
                    "\r\n" . $post['inputCarCredit'] . "\r\n",$headers);

                $email = $post['inputMail'];
                $user = User::model()->find('email=:email',array('email'=>$email));

                if(!$user)
                {
                    //register new user
                    $model = new User;
                    $profile = new Profile;

                    $userName = explode("@", $post['inputMail']);
                    $username = mb_strtolower($userName[0]);
                    $password = Yii::app()->getModule('user')->generateUserPassword(10);
                    //$password = $post['inputPassword'];

                    $model->attributes = array('username' => $username);
                    $model->attributes = array('email' => $email);
                    $model->attributes = array('password' => $password);
                    $model->activkey = Yii::app()->getModule('user')->encrypting(microtime().$model->password);

                    $profile->user_id=0;
                    $profile->attributes = array('firstname' => $username);
                    $profile->attributes = array('lastname' => $username);

                    if($model->validate() && $profile->validate()) {

                        $model->password = Yii::app()->getModule('user')->encrypting('admin11');
                        if($model->save()) {
                            $profile->user_id = $model->id;
                            $profile->save();
                        }
                    }
                }

                //send user notification
               /* $toUserMessage = "Добрый день! Заявка с сайта garage.am - получена! Наши мененджеры свяжутся с Вами!" .
                "\r\nДля входа в личный кабинет используйте: " .
                "\r\nЛогин: " . $username .
                "\r\nПароль: " . $password . "\r\n";*/

                

                //Yii::app()->getModule('user')->sendMail($email, 'garage.am - Заявка получена!', $toUserMessage);
                $userName = explode(" ", $post['inputName']);
                $userName = mb_convert_case($userName[0], MB_CASE_TITLE, "UTF-8");

                Yii::app()->session->add("first_name", $userName);

                Yii::app()->session->add("email", $post['inputMail']);
                Yii::app()->session->add("order_id", $order);

                $sku =  "Модель: " . $modelName;



                $sku_page =  "<table class='table table-responsive table-bordered table-striped'>
                    <thead>
                        <tr>
                            <th>
                                Марка
                            </th>
                            <th>
                                Модель
                            </th>
                            <th>
                                Кузов
                            </th>
                            <th>
                                Комплектации
                            </th>
                            <th>
                                Адрес
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Телефон
                            </th>
                            <th>
                                Другие пожелания
                            </th>
                            <th>
                                Другие
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                " . $markName . "
                            </td>
                            <td>
                                " . $modelName . "
                            </td>
                            <td>
                                " . $body['name'] . "
                            </td>
                            <td>
                                " . $compls . "
                            </td>
                            <td>
                                " . $address . "
                            </td>
                            <td>
                                " . $post['inputMail'] . "
                            </td>
                            <td>    
                                " . $post['inputPhone'] . "
                            </td>
                            <td>
                                " . $post['inputComment'] . "
                            </td>
                            <td>
                                " . $post['inputCarTrade'] . " <br><br>
                                " . $post['inputCarCredit'] . "
                            </td>
                        </tr>
                    </tbody>
                </table>";

                Yii::app()->session->add("sku_page", $sku_page);
                Yii::app()->session->add("sku", $sku);

                $toUserMessage = "Добрый день! Заявка с сайта garage.am - получена! Наши мененджеры свяжутся с Вами!" . "\r\n";

                $headers .= "\r\nContent-type: text/html; charset=utf-8\r\n"; 

                $toUserMessage = $toUserMessage . "<br>" . $sku_page;

                mail($email, 'Garage.am - заявка #'.$order, $toUserMessage, $headers);

            }
            echo json_encode(array('message'=>$message,'errors'=>$error));
        }
        else
        {
            throw new CHttpException(404,'Страница не найдена');
        }

    }

    public function actionCompare()
    {
        $this->layout='//layouts/compare';

        $favs = array();

        $mods = Yii::app()->request->getParam('mods')?:array();
        $compls = Yii::app()->request->getParam('compls')?:array();
        $models = array();
        if(count($mods)>0 || count($compls)>0)
        {
            foreach($mods as $mod)
            {
                $m = GenerationModification::model()->findByPk($mod);
                if($m)
                    array_push($models, array('mod'=>$m,'compl'=>null));
            }
            foreach($compls as $compl)
            {
                $c = ComplectationGenerationModification::model()->findByPk($compl);
                if($c)
                    array_push($models, array('mod'=>$c->genModRel,'compl'=>$c));
            }
        }
        else
        {
            if(isset(Yii::app()->request->cookies['favorites']))
            {
                $cookie = Yii::app()->request->cookies['favorites']->value;
                $favs = json_decode($cookie);
            }


            foreach($favs as $fav)
            {
                $mod = GenerationModification::model()->findByPk($fav->modify);
                if($mod)
                {
                    $compl = null;
                    if($fav->compl>0)
                    {
                        $compl = ComplectationGenerationModification::model()->findByPk($fav->compl);
                    }
                    else if(count($mod->complectationRel)>0)
                    {
                        $compl = $mod->complectationRel[0];
                    }
                    array_push($models, array('mod'=>$mod,'compl'=>$compl));
                }
            }
        }
        if(count($models)>0)
        {
            $params = Characteristic::model()->findAll();
            $addParams = AddCharacteristic::getList();

            $complParams = ComplectationCharacteristic::model()->findAll(array('condition'=>'id_parent is not null','order'=>'id_parent'));

            $this->render('compare',array(
                'models'=>$models, 'params'=>$params, 'addParams'=>$addParams, 'complParams'=>$complParams
            ));
        }
        else
        {
            $this->layout='//layouts/main';

            $this->render('compare_empty');
        }
    }

    public function actionAjaxmod()
    {
        if(Yii::app()->request->isPostRequest)
        {
            $mod = Yii::app()->request->getPost('mod');
            $compl = Yii::app()->request->getPost('compl');
            $idx = Yii::app()->request->getPost('idx');

            $mod = GenerationModification::model()->findByPk($mod);
            $compl = ComplectationGenerationModification::model()->findByPk($compl);
            $params = Characteristic::model()->findAll();
            $addParams = AddCharacteristic::getList();
            $complParams = ComplectationCharacteristic::model()->findAll(array('condition'=>'id_parent is not null','order'=>'id_parent'));
            $head = $this->renderPartial('_compare_head',array('mod'=>$mod,'compl'=>$compl,'idx'=>$idx),true);
            $content = $this->renderPartial('_compare_content',array('mod'=>$mod,'compl'=>$compl, 'params'=>$params, 'addParams'=>$addParams, 'complParams'=>$complParams,'idx'=>$idx),true);

            echo json_encode(array('head'=>$head,'content'=>$content,'idx'=>$idx));
        }
    }

    public function actionGetmodellist($dropdown=false)
    {
        if(Yii::app()->request->isPostRequest)
        {
            $mark = Yii::app()->request->getPost('mark');

            $sel = Mark::model()->findByPk($mark);
            $models = CHtml::listData($sel->models,'id_car_model','name');
            if(!$dropdown)
            {
                echo json_encode($models);
            }
            else
            {
                echo CHtml::tag('option', array('value'=>''),'',true);

                foreach($models as $value=>$name)
                {
                    echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
                }
            }
        }
    }
    public function actionGetgenerationlist($dropdown=false)
    {
        if(Yii::app()->request->isPostRequest)
        {
            $model = Yii::app()->request->getPost('model');

            $sel = CarModel::model()->findByPk($model);
            $gens = $sel->getGenerationList();
            if(!$dropdown)
            {
                echo json_encode($gens);
            }
            else
            {
                foreach($gens as $value=>$name)
                {
                    echo CHtml::tag('option',
                               array('value'=>$value),CHtml::encode($name),true);
                }
            }
        }
    }
    public function actionGetbodylist()
    {
        if(Yii::app()->request->isPostRequest)
        {
            $model = Yii::app()->request->getPost('gen');

            $sel = Generation::model()->findByPk($model);
            $bodies = CHtml::listData($sel->getBodies(),'id_car_body','name');
            echo json_encode($bodies);
        }
    }

    public function actionGetmodlist()
    {
        if(Yii::app()->request->isPostRequest)
        {
            $gen = Yii::app()->request->getPost('gen');
            $body = Yii::app()->request->getPost('body');

            $criteria = new CDbCriteria;
            $criteria->select = 't.id_car_modification, mods.name as modName';
            $criteria->with = array('generation','mods');
            $criteria->condition = 'generation.id_car_generation=:id_car_generation and mods.id_car_body=:id_car_body';
            $criteria->params = array(':id_car_generation'=>$gen,':id_car_body'=>$body);
            $criteria->order = 'modName';

            $mods = GenerationModification::model()->findAll($criteria);

            $res = CHtml::listData($mods,'id','modName');
            echo json_encode($res);
        }
    }

    public function actionGetmodificationlist($dropdown=false)
    {
        if(Yii::app()->request->isPostRequest)
        {
            $model = Yii::app()->request->getPost('model');

            $criteria = new CDbCriteria;

            $criteria->select = 'id_car_modification as id, name';
            $criteria->with = array('model');
            $criteria->condition = 'model.id_car_model=:id_car_model';
            $criteria->params = array(':id_car_model'=>$model);
            $criteria->order = 'id_car_modification';
            $mods = Modification::model()->findAll($criteria);

            $res = CHtml::listData($mods,'id_car_modification','name');

            if ($res) {
                if (!$dropdown) {
                    echo json_encode($res);
                } else {
                    echo CHtml::tag('option', array('value' => ''), '', true);
                    foreach ($res as $value => $name) {
                        echo CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
                    }
                }
            } else {
                echo CHtml::tag('option', array('value' => ''), '', true);
            }
        }
    }

    public function actionLivesearch()
    {
        $mainIds = array(0);

        if(Yii::app()->request->isPostRequest)
        {
            $text = trim(Yii::app()->request->getPost('s'));

            if(mb_strlen($text)<2)
            {
                throw new CHttpException(404,'Страница не найдена');
            }
        }
        else
        {
            throw new CHttpException(404,'Страница не найдена');
        }
        $q = explode(' ',$text);

        if(mb_strlen($text)>2)
        {
            $criteria = new CDbCriteria;
            $criteria->addSearchCondition('name',$text,true,'OR');
            $criteria->addSearchCondition('name_rus',$text,true,'OR');
            $criteria->addCondition('is_visible=1');
            $criteria->limit = 10;
            $resMarkFirst = Mark::model()->findAll($criteria);
        }
        $criteria = new CDbCriteria;

        $n = false;
        foreach($q as $i)
        {
            if(mb_strlen($i)>2)
            {
                $criteria->addSearchCondition('name',$i,true,'OR');
                $criteria->addSearchCondition('name_rus',$i,true,'OR');
                $criteria->addCondition('is_visible=1');
                $n = true;
            }
        }
        if($n)
        {
            $criteria->limit = 10;
            $resMark = Mark::model()->findAll($criteria);
        }
        else
        {
            $resMark = array();
        }

        $criteria = new CDbCriteria;
        $criteria->addSearchCondition('name',$text,true,'OR');
        $criteria->limit = 10;
        $resBody = Body::model()->findAll($criteria);

        /*$criteria = new CDbCriteria;
        $criteria->with = array('mark');
        $criteria->addSearchCondition('t.name',$text,true,'OR');
        $criteria->addSearchCondition('t.name_rus',$text,true,'OR');
        $criteria->addCondition('t.is_visible=1 and mark.is_visible=1');
        $criteria->limit = 10;
        $resModel = CarModel::model()->findAll($criteria);*/

        $resModel = array();

        $arr = array();
        $arr['mark']=array();
        $arr['body']=array();
        $arr['model']=array();

        foreach($resMarkFirst as $mark)
        {
            $new = array(
                'icon'=>((isset($mark->icon)&&!empty($mark->icon))?CHtml::image("/upload/car/mark/80x60_".$mark->icon):""),
                'name'=>$mark->name,
                'url'=>Yii::app()->createUrl('/car/catalog',array('mark'=>$mark->alias))
            );
            if(!isset($arr['mark'][$mark->id_car_mark]))
            {
                $arr['mark'][$mark->id_car_mark]=$new;
            }
        }
        foreach($resMark as $mark)
        {
            $new = array(
                'icon'=>((isset($mark->icon)&&!empty($mark->icon))?CHtml::image("/upload/car/mark/80x60_".$mark->icon):""),
                'name'=>$mark->name,
                'url'=>Yii::app()->createUrl('/car/catalog',array('mark'=>$mark->alias))
            );
            if(!isset($arr['mark'][$mark->id_car_mark]))
            {
                $arr['mark'][$mark->id_car_mark]=$new;
            }
        }


        foreach($resBody as $body)
        {
            $arr['body'][$body->id_car_body]=array(
                'icon'=>$body->icon,
                'name'=>$body->name,
                'url'=>Yii::app()->createUrl('/car/catalog',array('bodies'=>array($body->id_car_body)))
            );
        }

        if(count($resMark)>0)
        {
            if(count(explode(' ',$text))>1)
            {
                if(mb_strlen($text)>2)
                {
                    $criteria = new CDbCriteria;
                    $criteria->addSearchCondition('t.name',$text,true,'OR');
                    $criteria->limit=20;
                    $criteria->with= array('mark', 'model');
                    $criteria->addCondition('t.is_visible=1');
                    $criteria->addCondition('mark.is_visible=1');
                    $criteria->addCondition('model.is_visible=1');

                    //$criteria->addSearchCondition('t.name_rus',$text,true,'OR');
                    $resModel = Generation::model()->findAll($criteria);
                    //var_dump($resModel);

                }
                foreach($resModel as $model)
                {
                        if($model->modsRel)
                        {
                                            $arr['model'][$model->model->id_car_model]=array(
                        'icon'=>((isset($model->model->icon)&&!empty($model->model->icon))?CHtml::image("/upload/car/model/80x60_".$model->model->icon):""),
                        'name'=>$model->name,
                        'url'=>Yii::app()->createUrl('/car/catalog/single',array('id'=>$model->modsRel[0]->id,'mark'=>$model->mark->alias,'model'=>$model->alias)));
                        }
                }

            }
            else
            {
                foreach($resMark as $mark)
                {
                    $resModel = CarModel::model()->findAll('id_car_mark='.$mark->id_car_mark.' and is_visible=1');
                    foreach($resModel as $model)
                    {
                        if($model->generationMods)
                        {
                        $arr['model'][$model->id_car_model]=array(
                            'icon'=>((isset($model->icon)&&!empty($model->icon))?CHtml::image("/upload/car/model/80x60_".$model->icon):""),
                            'name'=>$model->mark->name." ".$model->name,
                            'url'=>Yii::app()->createUrl('/car/catalog/single',array('id'=>$model->generationMods[0]->id,'mark'=>$model->mark->alias,'model'=>$model->alias)));
                        }
                    }
                }
            }
        }
        else
        {
            if(mb_strlen($text)>2)
            {
                $criteria = new CDbCriteria;
                $criteria->addSearchCondition('t.name',$text,true,'OR');
                $criteria->limit=20;
                //$criteria->addSearchCondition('t.name_rus',$text,true,'OR');
                    $criteria->with= array('mark', 'model');
                    $criteria->addCondition('t.is_visible=1');
                    $criteria->addCondition('mark.is_visible=1');
                    $criteria->addCondition('model.is_visible=1');
                $resModel = Generation::model()->findAll($criteria);
                //var_dump($resModel);

            }
            foreach($resModel as $model)
            {
                        if($model->modsRel)
                        {
                $arr['model'][$model->id_car_generation]=array(
                    'icon'=>((isset($model->model->icon)&&!empty($model->model->icon))?CHtml::image("/upload/car/model/80x60_".$model->model->icon):""),
                    'name'=>$model->name,
                    'url'=>Yii::app()->createUrl('/car/catalog/single',array('id'=>$model->modsRel[0]->id,'mark'=>$model->mark->alias,'model'=>$model->model->alias)));
                        }

            }
        }
        /*if((count($arr['mark'])+count($arr['body'])+count($arr['model']))<10)
        {
            $q = explode(' ',$text);
            if(count($q)>1)
            {
                $criteria = new CDbCriteria;
                foreach($q as $i)
                {
                    if(mb_strlen($i)>2)
                    {
                        $criteria->addSearchCondition('name',$i,true,'OR');
                        $criteria->addSearchCondition('name_rus',$i,true,'OR');
                        $criteria->addCondition('is_visible=1');
                    }
                }
                $criteria->limit = 10;
                $resMark = Mark::model()->findAll($criteria);


                $criteria = new CDbCriteria;

                if(mb_strlen($text)>2)
                {
                    $criteria->with = array('mark','model');
                    $criteria->addCondition('t.is_visible=1 and mark.is_visible=1 and model.is_visible=1');
                    $criteria->addSearchCondition('t.name',$text,true,'AND');
                }

                $criteria->limit = 10;
                $resGen = Generation::model()->findAll($criteria);

                foreach($resMark as $mark)
                {
                    $new = array(
                        'icon'=>((isset($mark->image->icon)&&!empty($mark->image->icon))?CHtml::image("/upload/car/generation/80x60_".$mark->image->icon):""),
                        'name'=>$mark->name,
                        'url'=>Yii::app()->createUrl('/car/catalog',array('marks'=>array($mark->id_car_mark)))
                    );
                    if(!isset($arr['mark'][$mark->id_car_mark]))
                    {
                        $arr['mark'][$mark->id_car_mark]=$new;
                    }
                }

                foreach($resBody as $body)
                {
                    $new = array(
                        'icon'=>$body->icon,
                        'name'=>$body->name,
                        'url'=>Yii::app()->createUrl('/car/catalog',array('bodies'=>array($body->id_car_body)))
                    );
                    if(!isset($arr['mark'][$body->id_car_body]))
                    {
                        $arr['mark'][$body->id_car_body] = $new;
                    }
                }


                foreach($resGen as $gen)
                {
                    $new = array(
                        'icon'=>((isset($gen->model->image->icon)&&!empty($gen->model->image->icon))?CHtml::image("/upload/car/model/80x60_".$gen->model->image->icon):""),
                        'name'=>$gen->name,
                        'url'=>Yii::app()->createUrl('/car/catalog/single',array('id'=>$gen->mods[0]->id_car_modification)));

                    $arr['model'][$gen->mods[0]->id_car_modification]=$new;

                }

            }
        }*/

        $result = array();
        $result['mark']=array();
        $result['body']=array();
        $result['model']=array();


        foreach($arr['mark'] as $elem)
        {
            array_push($result['mark'],$elem);
        }
        foreach($arr['body'] as $elem)
        {
            array_push($result['body'],$elem);
        }
        foreach($arr['model'] as $elem)
        {
            array_push($result['model'],$elem);
        }

        //var_dump($arr);
        echo json_encode($result);
        exit();
    }
    public function actionLivesearchin()
    {
        if(Yii::app()->request->isPostRequest)
        {
            $text = trim(Yii::app()->request->getPost('s'));

            if(mb_strlen($text)<2)
            {
                throw new CHttpException(404,'Страница не найдена');
            }
        }
        else
        {
            throw new CHttpException(404,'Страница не найдена');
        }

        $arr = array();

        $mainIds = array(0);
        $q = explode(' ',$text);

        if(mb_strlen($text)>1)
        {
            $criteria = new CDbCriteria;
            $criteria->addSearchCondition('t.name',$text,true,'OR');
            $criteria->limit=20;
            //$criteria->addSearchCondition('t.name_rus',$text,true,'OR');
            $resModel = Generation::model()->findAll($criteria);
        }
        /*if(count($q)>=1)
        {
            $criteria = new CDbCriteria;
            foreach($q as $i)
            {
                if(mb_strlen($i)>2)
                {
                    $criteria->addSearchCondition('t.name',$i,true,'OR');
                    $criteria->addSearchCondition('t.name_rus',$i,true,'OR');
                }
             }
            $criteria->limit = 10;
            $resModel = CarModel::model()->findAll($criteria);

            foreach($resModel as $model)
            {
                array_push($arr,array(
                    'id'=>$model->mods[0]->id_car_modification,
                    'text'=>$model->mark->name." ".$model->name)
                );
                $mainIds[] = $model->id_car_model;
            }

            $criteria = new CDbCriteria;
            foreach($q as $i)
            {
                if(mb_strlen($i)>2)
                {
                    $criteria->addSearchCondition('name',$i,true,'OR');
                    $criteria->addSearchCondition('name_rus',$i,true,'OR');
                    $criteria->addCondition('is_visible=1');
                }
            }
            $criteria->limit = 10;
            $resMark = Mark::model()->findAll($criteria);

            foreach($resMark as $mark)
            {
                $resModelAdd = array();
                $criteria = new CDbCriteria;
                foreach($q as $i)
                {
                    $criteria->with = array('mark');
                    $criteria->addSearchCondition('t.name',$i,true,'OR');
                    $criteria->addSearchCondition('t.name_rus',$i,true,'OR');
                    $criteria->addCondition('t.is_visible=1 and mark.is_visible=1 and mark.id_car_mark='.$mark->id_car_mark.' and t.id_car_model not in ('.implode(',',$mainIds).')');
                }
                $criteria->limit = 10;
                $resModelFirst = CarModel::model()->findAll($criteria);

                if(count($resModelFirst)==0)
                {
                    $resModelFirst = CarModel::model()->findAll('id_car_mark='.$mark->id_car_mark);
                }
                else
                {
                    /*$ids = array();
                    foreach($resModelFirst as $m)
                    {
                        array_push($ids,$m->id_car_model);
                    }
                    $ids = CMap::mergeArray($ids, $mainIds);
                    $resModelAdd = CarModel::model()->findAll('id_car_model not in ('.implode(',',$mainIds).') and id_car_mark='.$mark->id_car_mark);

                }
                foreach($resModelFirst as $model)
                {
                    array_push($arr,array(
                        'id'=>$model->mods[0]->id_car_modification,
                        'text'=>$model->mark->name." ".$model->name)
                    );
                }
                foreach($resModelAdd as $model)
                {
                    array_push($arr,array(
                        'id'=>$model->mods[0]->id_car_modification,
                        'text'=>$model->mark->name." ".$model->name)
                    );
                }
            }


        } */

        foreach($resModel as $model)
        {
            $body = '';
            foreach($model->modsRel as $modsRel)
            {
                if($body != $modsRel->mods->body->id_car_body)
                {
                    array_push($arr,array(
                        'id'=>$modsRel->id,
                        'text'=>$modsRel->generation->name." (".$modsRel->generation->year_begin.($modsRel->generation->year_end?(" - ".$modsRel->generation->year_end):" - н.д.")."), ".$modsRel->mods->body->name)
                    );
                    $body = $modsRel->mods->body->id_car_body;
                    //$mainIds[] = $model->id_car_model;
                }
            }
        }
        //var_dump($arr);
        echo json_encode($arr);
        exit();
    }
}
