<?php

class GenerationBackendController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/inner';

	/**
	 * @return array action filters
	 */

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Lists all models.
	 */
    public function actionIndex()
    {
        $model=new Generation('search');
        
        $model->unsetAttributes(); // clear any default values

        $model->setAttributes(
            Yii::app()->getRequest()->getParam(
                'Generation',
                array()
            )
        );

        $this->render('index',array(
            'model'=>$model,
        ));
    
    }
    public function actionCreate()
    {
        $model=new Generation;

        if(isset($_POST['CarGeneration']) || isset($_POST['Generation']))
        {
            $model->attributes=$_POST['Generation'];
            
            if($model->save())
            {
                $model->loadChars();
                if(isset($_POST['CarGeneration']))
                {
                    $model->attributes=$_POST['CarGeneration'];
                    
                    $model->introtext->value = $_POST['CarGeneration']['introtext'];   
                    $model->introtext->save();
                    
                    $model->description->value = $_POST['CarGeneration']['description'];   
                    $model->description->save();

                    $model->review->value = $_POST['CarGeneration']['review'];
                    $model->review->save();
                }
            //$oldFileName = $model->image;
                $this->redirect(array('update','id'=>$model->id_car_generation));
            }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

	public function actionUpdate($id)
	{

        $model=$this->loadModel($id);
        $model->loadChars();
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        //var_dump($_POST);
        //var_dump($_FILES);  exit();
        //$imgs=CUploadedFile::getInstancesByName('Modification');
        //var_dump($imgs);
        //var_dump($_FILES['CarGeneration']); exit();
        if(isset($_POST['CarGeneration']) || isset($_FILES['CarGeneration']) || isset($_POST['Generation']) || isset($_POST['compRel']))
        {
            //$oldFileName = $model->image;
            if(isset($_POST['CarGeneration']) || isset($_POST['Generation']) || isset($_POST['compRel']))
            {
                $model->attributes=$_POST['Generation'];
                
                $model->introtext->value = $_POST['CarGeneration']['introtext'];   
                $model->introtext->save();
                
                $model->description->value = $_POST['CarGeneration']['description'];   
                $model->description->save();

                $model->review->value = $_POST['CarGeneration']['review'];
                $model->review->save();

                $similars = isset($_POST['CarGeneration']['similar'])?$_POST['CarGeneration']['similar']:array();
                foreach($similars as $body=>$sim)
                {
                    $simArr = explode(',',$sim);
                    foreach($model->similar as $similar)
                    {
                        if($similar->id_car_body==$body && !in_array($similar->id_car_generation_similar,$simArr))
                        {
                            $mods = $similar->generation->modsRel;
                            $arr = array();
                            foreach($mods as $mod)
                            {
                                if($mod->mods->id_car_body==$similar->id_car_body)
                                {
                                    $arr[]=$mod->id;
                                }
                            }

                            $criteria = new CDbCriteria;
                            $criteria->addCondition('id_car_generation=:id_car_generation');
                            $criteria->addCondition('id_car_body=:id_car_body');
                            $criteria->addCondition('id_car_similar<>:id_car_similar');
                            $criteria->addInCondition('id_car_generation_similar',$arr,'AND');
                            
                            $criteria->params[':id_car_similar'] = $similar->id_car_similar;
                            $criteria->params[':id_car_generation'] = $similar->mods->id_car_generation;
                            $criteria->params[':id_car_body'] = $similar->mods->mods->id_car_body;
                            
                            $genSim = GenerationSimilar::model()->find($criteria);

                            if($genSim)
                            {
                                $genSim->delete();
                            }
                            $similar->delete();
                        }
                        
                    }
                    

                    foreach($simArr as $i)
                    {
                        if(!empty($i))
                        {
                            $s = GenerationSimilar::model()->find('id_car_generation_similar=:sim and id_car_body=:body and id_car_generation=:id_gen',array(':sim'=>$i, ':body'=>$body, ':id_gen'=>$model->id_car_generation));
                            if(!$s)
                            {
                                $s = new GenerationSimilar;
                                $s->id_car_generation = $model->id_car_generation;
                                $s->id_car_body = $body;
                                $s->id_car_generation_similar = $i;
                                $s->save();
                                
                                $mods = $s->generation->modsRel;
                                $selMod = false;
                                
                                foreach($mods as $mod)
                                {
                                    if($mod->mods->id_car_body==$body)
                                    {
                                        $selMod = $mod->id;
                                        break;
                                    }
                                }
                                //var_dump($selMod);exit();
                                if($selMod)
                                {
                                    $genSim = new GenerationSimilar;
                                    $genSim->id_car_generation = $s->mods->id_car_generation;
                                    $genSim->id_car_body = $s->mods->mods->id_car_body;
                                    $genSim->id_car_generation_similar = $selMod;
                                    $genSim->save();
                                }
                            }
                        }

                    }
                }

                if(isset($_POST['CarGeneration']['colorDelete']))
                {
                    foreach($_POST['CarGeneration']['colorDelete'] as $del=>$val)
                    {
                        if($color = GenerationCharacteristicValue::model()->findByPk($del))
                        {
                            $color->delete();
                        }
                        
                    }
                }

                if(isset($_POST['CarGeneration']['galleryDelete']))
                {
                    foreach($_POST['CarGeneration']['galleryDelete'] as $del=>$val)
                    {
                        if($gal = GenerationImage::model()->findByPk($del))
                        {
                            $gal->delete();
                        }
                        
                    }
                }

                if(isset($_POST['CarGeneration']['galleryChar']))
                {
                    foreach($_POST['CarGeneration']['galleryChar'] as $char=>$val)
                    {
                        $img = GenerationImage::model()->findByPk($char);

                        if($img = GenerationImage::model()->findByPk($char))
                        {
                            $img->id_car_add_characteristic = $val;
                            $img->save();
                        }
                        
                    }
                }

                if(isset($_POST['compRel']))
                {
                    foreach($_POST['compRel'] as $mod => $compls)
                    {
                        foreach($compls as $compl=>$val)
                        {
                            $c = ComplectationGenerationModification::model()->find('id_car_generation_modification=:id_gen and id_car_complectation=:id_compl',array(':id_gen'=>$mod,':id_compl'=>$compl));
                            
                            if((empty($val) || $val==0) && $c)
                            {
                                $c->delete();
                            }
                            else if(!(empty($val) || $val==0) && $c)
                            {
                                $c->price_min = $val;
                                $c->save();
                            }
                            else if(!(empty($val) || $val==0) && !$c)
                            {
                                $c = new ComplectationGenerationModification;
                                $c->id_car_generation_modification = $mod;
                                $c->id_car_complectation = $compl;
                                $c->price_min = $val;
                                $c->save();
                            }
                                                        
                        }
                    }
                }
            }

            if(isset($_FILES['CarGeneration']))
            {

                $filename = $_FILES['CarGeneration']['name']['image'];


                if(!empty($filename))
                {

                    foreach($_FILES['CarGeneration']['name']['image'] as $key=>$val)
                    {
                        $filename = $_FILES['CarGeneration']['name']['image'][$key];
                        if(!empty($filename))
                        {
                            $img = GenerationCharacteristicValue::model()->findByPk($key);
                            
                            $newname = uniqid().'.'.end(explode('.',$filename));    
                            $path=Yii::getPathOfAlias(Yii::app()->getModule('car')->assetsGenPath).'/'; 

                            $filepath = $_FILES['CarGeneration']['tmp_name']['image'][$key];
                            
                            copy($filepath, $path.$newname);
                            $thumb=Yii::app()->phpThumb->create($path.$newname);
                            $thumb->resize(350,260);
                            $thumb->save($path."350x260_".$newname);
                            $thumb->resize(200,150);
                            $thumb->save($path."200x150_".$newname);
                            $thumb->resize(100,100);
                            $thumb->save($path."100x100_".$newname);
                           
                            $img->image=$newname;
                            $img->save();
                        }
                    }
                }
                $filename = $_FILES['CarGeneration']['name']['image_head'];


                if(!empty($filename))
                {
                    foreach($_FILES['CarGeneration']['name']['image_head'] as $key=>$val)
                    {
                        $filename = $_FILES['CarGeneration']['name']['image_head'][$key];
                        if(!empty($filename))
                        {
                            $img = GenerationCharacteristicValue::model()->findByPk($key);
                            
                            $newname = uniqid().'.'.end(explode('.',$filename));    
                            $path=Yii::getPathOfAlias(Yii::app()->getModule('car')->assetsGenPath).'/'; 

                            $filepath = $_FILES['CarGeneration']['tmp_name']['image_head'][$key];
                            
                            copy($filepath, $path.$newname);
                            $thumb=Yii::app()->phpThumb->create($path.$newname);
                            $thumb->resize(350,260);
                            $thumb->save($path."350x260_".$newname);
                            $thumb->resize(200,150);
                            $thumb->save($path."200x150_".$newname);
                            $thumb->resize(100,100);
                            $thumb->save($path."100x100_".$newname);
                           
                            $img->image=$newname;
                            $img->save();
                        }
                    }
                }

                foreach($model->colors as $color)
                {
                    $filename = isset($_FILES['CarGeneration']['name']['colors'][$color->id_car_characteristic_value])?$_FILES['CarGeneration']['name']['colors'][$color->id_car_characteristic_value]:'';
                    //var_dump($_FILES['CarGeneration']['name']['colors']);exit();

                    if(!empty($filename))
                    {
                        $newname = uniqid().'.'.end(explode('.',$filename));    
                        $path=Yii::getPathOfAlias(Yii::app()->getModule('car')->assetsGenPath).'/'; 

                        $filepath = $_FILES['CarGeneration']['tmp_name']['colors'][$color->id_car_characteristic_value];
                        
                        copy($filepath, $path.$newname);
                        $thumb=Yii::app()->phpThumb->create($path.$newname);
                        $thumb->resize(890,350);
                        $thumb->save($path."890x350_".$newname);
                        $thumb->resize(200,150);
                        $thumb->save($path."200x150_".$newname);
                        $thumb->resize(100,100);
                        $thumb->save($path."100x100_".$newname);
                        
                        $color->image = $newname;
                        if(!$color->save())
                        {
                            var_dump($color->getErrors());    
                            var_dump($color);    
                            exit();
                        }
                    }
                    
                }

                foreach($_FILES['CarGeneration']['name']['gallery'] as $keyBody => $body)
                {


                    foreach($body as $key => $img)
                    {   

                        $filename = $_FILES['CarGeneration']['name']['gallery'][$keyBody][$key];
                        
                        if(!empty($filename))
                        {
                            $newname = uniqid().'.'.end(explode('.',$filename));    

                            $path=Yii::getPathOfAlias(Yii::app()->getModule('car')->assetsGenPath).'/'; 


                            $filepath = $_FILES['CarGeneration']['tmp_name']['gallery'][$keyBody][$key];
                            
                            copy($filepath, $path.$newname);

                            $thumb=Yii::app()->phpThumb->create($path.$newname);
                            $thumb->resize(890,350);
                            $thumb->save($path."890x350_".$newname);
                            $thumb->resize(200,150);
                            $thumb->save($path."200x150_".$newname);
                            $thumb->resize(100,100);
                            $thumb->save($path."100x100_".$newname);

                            $imgGal = new GenerationImage();
                            $imgGal->id_car_generation = $model->id_car_generation;
                            $imgGal->id_car_add_characteristic = 2;
                            $imgGal->image = $newname;
                            $imgGal->id_car_body = $keyBody;
                            $imgGal->save();
                        }
                    }
                }
            }

            if($model->save())
                $this->redirect(array('update','id'=>$model->id_car_generation));
            else
            {
                var_dump($model->getErrors());exit();
            }
        }

        $this->render('update',array(
            'model'=>$model/*,'charlist'=>Generation::getAddChars()*/
        ));
    }
    
    public function actionDelete($id)
    {
        $model = Generation::model()->findByPk($id);
        if($model)
        {
            $query = "delete c.* from `car_complectation_generation_modification` as c
                inner join `car_generation_modification` as m on c.id_car_generation_modification = m.id
            where m.id_car_generation=:id_car_generation";
            $command = Yii::app()->db->createCommand($query);
            var_dump($command->execute(array(':id_car_generation' => $id)));

            GenerationModification::model()->deleteAll('id_car_generation=:id_car_generation',array(':id_car_generation' => $id));
            
            GenerationCharacteristicValue::model()->deleteAll('id_car_generation=:id_car_generation',array(':id_car_generation' => $id));
            
            GenerationImage::model()->deleteAll('id_car_generation=:id_car_generation',array(':id_car_generation' => $id));
            
            GenerationSimilar::model()->deleteAll('id_car_generation=:id_car_generation',array(':id_car_generation' => $id));
            
            $model->delete();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }
        
    public function loadModel($id)
    {
        $model=Generation::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        $model->loadChars();
        return $model;
    }
    
}
