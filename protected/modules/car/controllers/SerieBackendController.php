<?php

class SerieBackendController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/inner';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $model=new Serie('search');
        
        $model->unsetAttributes(); // clear any default values

        $model->setAttributes(
            Yii::app()->getRequest()->getParam(
                'Serie',
                array()
            )
        );

        $this->render('index',array(
            'model'=>$model,
        ));

	}

    public function actionCreate($generation)
    {
        $model=new Serie;
        
        $model->id_car_generation=$generation;
        $gen = Generation::model()->findByPk($generation);
        $model->id_car_model = $gen->id_car_model;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Serie']))
        {
            $model->attributes=$_POST['Serie'];
            if($model->save())
            {            
                $this->redirect(array('update','id'=>$model->id_car_serie));
            }
        }

        $this->render('create',array(
            'model'=>$model,'generation'=>$gen
        ));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Serie']))
        {
            $model->attributes=$_POST['Serie'];
            if($model->save())
            {            
                $this->redirect(array('update','id'=>$model->id_car_serie));
            }
        }

        $this->render('update',array(
            'model'=>$model,'generation'=>$model->generation
        ));
    }

	public function loadModel($id)
	{
		$model=Serie::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

       public function actionDelete($id)
    {
        //Удаляем характеристики комплектаций
        $query = "delete cv.* from `car_complectation_characteristic_value` as cv
                    inner join `car_complectation` as c on cv.id_car_complectation = c.id_car_complectation
                    inner join `car_modification` as cm on c.id_car_modification = cm.id_car_modification
                    inner join `car_serie` as cs on cm.id_car_serie = cs.id_car_serie
                where cs.id_car_serie=:id_car_serie";
        $command = Yii::app()->db->createCommand($query);
        var_dump($command->execute(array(':id_car_serie' => $id)));
        
        //Удаляем характеристики модификаций
        $query = "delete cv.* from `car_characteristic_value` as cv
                    inner join `car_modification` as cm on cv.id_car_modification = cm.id_car_modification
                    inner join `car_serie` as cs on cm.id_car_serie = cs.id_car_serie
                where cs.id_car_serie=:id_car_serie";
        $command = Yii::app()->db->createCommand($query);
        var_dump($command->execute(array(':id_car_serie' => $id)));
        
        //Удаляем доп.характеристики модификаций
        $query = "delete cv.* from `car_add_characteristic_value` as cv
                    inner join `car_modification` as cm on cv.id_car_modification = cm.id_car_modification
                    inner join `car_serie` as cs on cm.id_car_serie = cs.id_car_serie
                where cs.id_car_serie=:id_car_serie";
        $command = Yii::app()->db->createCommand($query);
        var_dump($command->execute(array(':id_car_serie' => $id)));
        
        //Удаляем комплектации
        $query = "delete c.* from `car_complectation` as c 
                    inner join `car_modification` as cm on c.id_car_modification = cm.id_car_modification
                    inner join `car_serie` as cs on cm.id_car_serie = cs.id_car_serie
                where cs.id_car_serie=:id_car_serie";
        $command = Yii::app()->db->createCommand($query);
        var_dump($command->execute(array(':id_car_serie' => $id)));
        
        //Удаляем модификации
        $query = "delete cm.* from `car_modification` as cm
                    inner join `car_serie` as cs on cm.id_car_serie = cs.id_car_serie
                where cs.id_car_serie=:id_car_serie";
        $command = Yii::app()->db->createCommand($query);
        var_dump($command->execute(array(':id_car_serie' => $id)));
        
        //Удаляем серии
        $query = "delete cs.* from `car_serie` as cs 
                where cs.id_car_serie=:id_car_serie";
        $command = Yii::app()->db->createCommand($query);
        var_dump($command->execute(array(':id_car_serie' => $id)));
                
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

	/**
	 * Performs the AJAX validation.
	 * @param Serie $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='serie-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
