<?php

class SeoPageBackendController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/inner';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view', 'generate'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete','deleteAll','generateXml'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
     * generarate xml
     */
    public function actionGenerateXml()
    {

        $criteria = new CDbCriteria;

        $criteria->select = '*, date(t.update_time) as updateTime';
        $criteria->with = array('mark', 'model');
        $criteria->group = 't.id';

        $seo_pages = SeoPage::model()->findAll($criteria);
        $marks = Mark::model()->findAll(array('condition'=>'is_visible=1'));
        $bodies = Body::model()->findAll(array('order'=>'id_car_body', 'condition'=>'id_car_body<>8'));

        $criteria = new CDbCriteria;
        $criteria->select = 'mark.id_car_mark as markID, model.id_car_model as modelID, mark.name as markName, model.name as modelName, generation.name as GenerationName, generation.year_begin as YearBegin, mods.name as modName';

        $criteria->with = array('mark', 'model', 'generation', 'mods', 'body');
        $criteria->condition = 'mark.is_visible=1 and model.is_visible=1 and generation.is_visible=1 and mods.is_visible=1';
        $criteria->group = 'generation.name, generation.year_begin, mods.id_car_body';
        $criteria->together = false;
        $criteria->order = 'generation.year_begin DESC';

        $genMod = GenerationModification::model()->findAll($criteria);

        $xmlData = array();

        foreach ($seo_pages as $key => $page){

            if ($page->model) {
                $xmlData[$key]['url'] = Yii::app()->createUrl('/car/catalog/single',array('mark'=>$page->mark->alias,'model'=>$page->model->alias)) . '/' . $page->slug;
                $xmlData[$key]['lastmod'] = $page->updateTime;
            } else {
                $xmlData[$key]['url'] = Yii::app()->createUrl('/car/catalog',array('mark'=>$page->mark->alias,'slug'=>$page->slug));
                $xmlData[$key]['lastmod'] = $page->updateTime;
            }
        }

        $host = Yii::app()->request->hostInfo;

        $xml = "";

        $xml .=  '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';


        $xml .='<url><loc>' . Yii::app()->getBaseUrl(true) . DIRECTORY_SEPARATOR . 'how_it_works</loc></url>';
        $xml .='<url><loc>' . Yii::app()->getBaseUrl(true) . DIRECTORY_SEPARATOR . 'dealers</loc></url>';
        $xml .='<url><loc>' . Yii::app()->getBaseUrl(true) . DIRECTORY_SEPARATOR . 'for_dealers</loc></url>';
        $xml .='<url><loc>' . Yii::app()->getBaseUrl(true) . DIRECTORY_SEPARATOR . 'about</loc></url>';
        $xml .='<url><loc>' . Yii::app()->getBaseUrl(true) . DIRECTORY_SEPARATOR . 'contacts</loc></url>';

        $xml .='<url><loc>' . Yii::app()->getBaseUrl(true) . Yii::app()->createUrl('/car/catalog',array('price'=>'do-600000-rub')) . '</loc></url>';
        $xml .='<url><loc>' . Yii::app()->getBaseUrl(true) . Yii::app()->createUrl('/car/catalog',array('price'=>'ot-600000-do-1000000-rub')) . '</loc></url>';
        $xml .='<url><loc>' . Yii::app()->getBaseUrl(true) . Yii::app()->createUrl('/car/catalog',array('price'=>'ot-1000000-do-1500000-rub')) . '</loc></url>';
        $xml .='<url><loc>' . Yii::app()->getBaseUrl(true) . Yii::app()->createUrl('/car/catalog',array('price'=>'ot-1500000-do-2000000-rub')) . '</loc></url>';
        $xml .='<url><loc>' . Yii::app()->getBaseUrl(true) . Yii::app()->createUrl('/car/catalog',array('price'=>'ot-2000000-rub')) . '</loc></url>';

        foreach($bodies as $body)
        {
            $xml .='<url><loc>' . Yii::app()->getBaseUrl(true) . Yii::app()->createUrl('/car/catalog',array('body'=>$body->alias)) . '</loc></url>';
        }

        foreach ($marks as $mark)
        {
            $xml .='<url><loc>' . Yii::app()->getBaseUrl(true)  . Yii::app()->createUrl('/car/catalog',array('mark'=>$mark->alias)) . '</loc></url>';
        }

        foreach ($genMod as $val)
        {
            $val->generation->loadChars();

            $xml .='<url><loc>' . Yii::app()->getBaseUrl(true) . Yii::app()->createUrl('/car/catalog/single',array('mark'=>$val->mark->alias,'model'=>$val->model->alias,'id'=>$val->id)) . '</loc></url>';
        }

        foreach ($xmlData as $val)
        {
            $xml .='<url><loc>' . $host . $val['url'] . '</loc><lastmod>' . $val['lastmod'] . '</lastmod></url>';
        }


        $xml .= '</urlset>';

        file_put_contents(dirname(Yii::app()->basePath) . DIRECTORY_SEPARATOR . 'sitemap.xml', $xml);

        $this->redirect(array('index'));

    }

    /**
     * BUILD "A" GROUP PAGES
     */
    public function actionGenerateA($insertData, $genMod)
    {

        $car_model = CarModel::model()->findByPk($genMod[0]->modelID);
        $car_mark = Mark::model()->findByPk($genMod[0]->markID);

        $currentYear = date("Y");
        $nextYear = $currentYear+1;

        $insertRows = array();

        foreach ($genMod as $key => $mod) {

            $insertData['group_id'] = 1;
            $insertData['id_car_modification'] = $mod->mods->id_car_modification;
            $insertData['id_car_body'] = $mod->body->id_car_body;

            $bodyAlias = ($mod->body->alias=='suv') ? strtoupper($mod->body->alias) : ucfirst($mod->body->alias);

            //main url
            $insertData['title'] = $car_mark->name_rus . ' ' . $car_model->name_rus . ' ' . $mod->body->name . ' / ' . $car_mark->name . ' ' . $car_model->name . ' ' . $bodyAlias . ' - комплектации и цены | Купить авто от официального дилера (салона) | Фото';
            $insertData['slug'] = $mod->id;
            $insertRows[] = $insertData;

            //new url
            $insertData['title'] = 'Новый ' . $car_mark->name_rus . ' ' . $car_model->name_rus . ' ' . $mod->body->name . ' / ' . $car_mark->name . ' ' . $car_model->name . ' ' . $bodyAlias . ' New в новом кузове — комплектации и цены нового авто с фото';
            $insertData['slug'] = 'new/' . $mod->id;
            $insertRows[] = $insertData;

            //current year url
            $insertData['title'] = $car_mark->name_rus . ' ' . $car_model->name_rus . ' ' . $mod->body->name . ' ' . $currentYear . ' / ' . $car_mark->name . ' ' . $car_model->name . ' ' . $bodyAlias . ' ' . $currentYear . ' года — купить в новом кузове по выгодной цене | Комплектации и цены';
            $insertData['slug'] = $currentYear . '/' . $mod->id;
            $insertRows[] = $insertData;

            //next year url
            $insertData['title'] = $car_mark->name_rus . ' ' . $car_model->name_rus . ' ' . $mod->body->name . ' ' . $nextYear . ' / ' . $car_mark->name . ' ' . $car_model->name . ' ' . $bodyAlias . ' ' . $nextYear . ' года — купить в новом кузове по выгодной цене | Комплектации и цены';
            $insertData['slug'] = $nextYear . '/' . $mod->id;
            $insertRows[] = $insertData;
        }

        return $insertRows;
    }

    /**
     * BUILD "B" GROUP PAGES
     */
    public function actionGenerateB($insertData, $genMod)
    {

        $car_model = CarModel::model()->findByPk($genMod[0]->modelID);
        $car_mark = Mark::model()->findByPk($genMod[0]->markID);

        $currentYear = date("Y");
        $nextYear = $currentYear+1;

        $insertRows = array();

        foreach ($genMod as $key => $gmod) {

            $insertData['group_id'] = 2;
            $insertData['id_car_modification'] = $gmod->mods->id_car_modification;
            $insertData['id_car_body'] = $gmod->body->id_car_body;
            $bodyAlias = ($gmod->body->alias=='suv') ? strtoupper($gmod->body->alias) : ucfirst($gmod->body->alias);

            foreach ($gmod->generation->getVolume() as $key => $volume) {

                $modName = str_replace(".", "-", number_format(floatval($volume), 1));

                /*$insertData['title'] = (!empty($_POST['SeoPage']['title'])) ? $_POST['SeoPage']['title'] : $car_mark->name_rus . ' '. $car_model->name_rus . ' ' . $gmod->body->name . ' ' . $modName . ' / ' . $car_mark->name . ' '. $car_model->name . ' ' . $bodyAlias . ' ' . $modName . ' - двигатель, характеристики';
                $insertData['slug'] = $modName . '/' . $gmod->id;*/
                $insertData['car_volume_value'] = $volume;
                /*$insertRows[] = $insertData;*/

                foreach ($gmod->generation->getTransmission() as $key => $trans) {

                    if ($trans=='механика')
                    {
                        $insertData['title'] = (!empty($_POST['SeoPage']['title'])) ? $_POST['SeoPage']['title'] : $car_mark->name_rus . ' '. $car_model->name_rus . ' ' . $gmod->body->name . ' ' . $modName . ' Механика / ' . $car_mark->name . ' '. $car_model->name . ' ' . $bodyAlias . ' ' . $modName . ' MT';
                        $insertData['slug'] = $modName . '/mt/' . $gmod->id;
                        $insertData['id_car_transmission'] = 'Механическая';
                    } else {
                        $insertData['title'] = (!empty($_POST['SeoPage']['title'])) ? $_POST['SeoPage']['title'] : $car_mark->name_rus . ' '. $car_model->name_rus . ' ' . $gmod->body->name . ' ' . $modName . ' Автомат / ' . $car_mark->name . ' '. $car_model->name . ' ' . $bodyAlias . ' ' . $modName . ' AT';
                        $insertData['slug'] = $modName . '/at/' . $gmod->id;
                        $insertData['id_car_transmission'] = 'Автоматическая';
                    }
                    $insertRows[] = $insertData;
                }
                unset($insertData['id_car_transmission']);

                foreach($gmod->generation->getFuel() as $key => $fuel) {

                    if ($fuel == 'Дизель')
                    {
                        //fuel url
                        $insertData['title'] = (!empty($_POST['SeoPage']['title'])) ? $_POST['SeoPage']['title'] : $car_mark->name_rus . ' '. $car_model->name_rus . ' ' . $gmod->body->name . ' ' . $modName . ' ' . $fuel . ' / ' . $car_mark->name . ' '. $car_model->name . ' ' . $bodyAlias . ' ' . $modName . ' Diesel';
                        $insertData['slug'] = $modName . '/diesel/' . $gmod->id;
                        $insertData['id_car_fuel'] = $fuel;
                        $insertRows[] = $insertData;
                    }

                }
                unset($insertData['id_car_fuel']);
                unset($insertData['car_volume_value']);

            }

            foreach ($gmod->generation->getTransmission() as $key => $trans) {
                //transmission url
                $insertData['id_car_modification'] = 0;
                if ($trans=='механика')
                {
                    $insertData['title'] = (!empty($_POST['SeoPage']['title'])) ? $_POST['SeoPage']['title'] : $car_mark->name_rus . ' '. $car_model->name_rus . ' ' . $gmod->body->name . ' Механика / ' . $car_mark->name . ' '. $car_model->name . ' ' . $bodyAlias . ' MT - комплектации и цены';
                    $insertData['slug'] = 'mt/' . $gmod->id;
                    $insertData['id_car_transmission'] = 'Механическая';
                } else {
                    $insertData['title'] = (!empty($_POST['SeoPage']['title'])) ? $_POST['SeoPage']['title'] : $car_mark->name_rus . ' '. $car_model->name_rus . ' ' . $gmod->body->name . ' Автомат / ' . $car_mark->name . ' '. $car_model->name . ' ' . $bodyAlias . ' AT - комплектации и цены';
                    $insertData['slug'] = 'at/' . $gmod->id;
                    $insertData['id_car_transmission'] = 'Автоматическая';
                }
                unset($insertData['id_car_fuel']);
                $insertData['id_car_modification'] = $gmod->mods->id_car_modification;

                $insertRows[] = $insertData;
            }
            unset($insertData['id_car_transmission']);

            foreach($gmod->generation->getFuel() as $key => $fuel) {

                //fuel without modification name url
                if ($fuel == 'Дизель')
                {
                    $insertData['title'] = (!empty($_POST['SeoPage']['title'])) ? $_POST['SeoPage']['title'] : $car_mark->name_rus . ' '. $car_model->name_rus . ' ' . $gmod->body->name . ' ' . $fuel . ' / ' . $car_mark->name . ' '. $car_model->name . ' ' . $bodyAlias . ' Diesel';
                    $insertData['slug'] = 'diesel/' . $gmod->id;
                    $insertData['id_car_fuel'] = $fuel;
                    $insertData['id_car_modification'] = 0;
                    $insertRows[] = $insertData;
                    unset($insertData['id_car_fuel']);
                    $insertData['id_car_modification'] = $gmod->mods->id_car_modification;
                }

            }
            unset($insertData['id_car_fuel']);

            foreach($gmod->generation->getPrivod() as $key => $privod) {

                if ($privod == 'Полный')
                {
                    //privod url
                    $insertData['title'] = (!empty($_POST['SeoPage']['title'])) ? $_POST['SeoPage']['title'] : $car_mark->name_rus . ' ' . $car_model->name_rus . ' ' . $gmod->body->name . ' полный привод 4x4 / ' . $car_mark->name . ' ' . $car_model->name . ' ' . $bodyAlias . ' 4wd — новые авто, комплектации и цены, фото';
                    $insertData['slug'] = 'awd/' . $gmod->id;
                    $insertData['id_car_privod'] = $privod;
                    $insertData['id_car_modification'] = 0;
                    $insertRows[] = $insertData;
                    unset($insertData['id_car_privod']);
                    $insertData['id_car_modification'] = $gmod->mods->id_car_modification;
                }

            }
        }

        return $insertRows;
    }

    /**
     * BUILD "C" GROUP PAGES
     */
    public function actionGenerateC($insertData, $genMod, $gblock_sort = 0)
    {

        $car_model = CarModel::model()->findByPk($genMod[0]->modelID);
        $car_mark = Mark::model()->findByPk($genMod[0]->markID);

        $currentYear = date("Y");
        $nextYear = $currentYear+1;

        $insertRows = array();

        foreach ($genMod as $key => $mod) {

            $insertData['group_id'] = 3;
            $insertData['id_car_modification'] = $mod->mods->id_car_modification;
            $insertData['id_car_body'] = $mod->body->id_car_body;
            $bodyAlias = ($mod->body->alias=='suv') ? strtoupper($mod->body->alias) : ucfirst($mod->body->alias);

            if ($gblock_sort == 3 || !$gblock_sort)
            {
                $insertData['group_block_sort'] = 3;

                //tests url
                $insertData['title'] = 'Тест-драйв, видео и обзор - ' . $car_mark->name_rus . ' ' . $car_model->name_rus . ' ' . $mod->body->name . ' / ' . $car_mark->name . ' ' . $car_model->name . ' ' . $bodyAlias;
                $insertData['slug'] = 'tests/' . $mod->id;
                $insertRows[] = $insertData;

                //tests current year url
                $insertData['title'] = 'Тест-драйв, видео и обзор - ' . $car_mark->name_rus . ' ' . $car_model->name_rus . ' ' . $mod->body->name . ' ' . $currentYear . ' / ' . $car_mark->name . ' ' . $car_model->name . ' ' . $bodyAlias . ' ' . $currentYear;
                $insertData['slug'] = $currentYear . '/tests/' . $mod->id;
                $insertRows[] = $insertData;

                //tests next year url
                $insertData['title'] = 'Тест-драйв, видео и обзор - ' . $car_mark->name_rus . ' ' . $car_model->name_rus . ' ' . $mod->body->name . ' ' . $nextYear . ' / ' . $car_mark->name . ' ' . $car_model->name . ' ' . $bodyAlias . ' ' . $nextYear;
                $insertData['slug'] = $nextYear . '/tests/' . $mod->id;
                $insertRows[] = $insertData;

                //new tests url
                $insertData['title'] = 'Тест-драйв, видео и обзор - Новый ' . $car_mark->name_rus . ' ' . $car_model->name_rus . ' ' . $mod->body->name . ' / ' . $car_mark->name . ' ' . $car_model->name . ' ' . $bodyAlias . ' New';
                $insertData['slug'] = 'new/tests/' . $mod->id;
                $insertRows[] = $insertData;
            }

            if ($gblock_sort == 4 || !$gblock_sort) {
                $insertData['group_block_sort'] = 4;

                //review url
                $insertData['title'] = 'Отзывы владельцев - ' . $car_mark->name_rus . ' ' . $car_model->name_rus . ' ' . $mod->body->name . ' / ' . $car_mark->name . ' ' . $car_model->name . ' ' . $bodyAlias;
                $insertData['slug'] = 'review/' . $mod->id;
                $insertRows[] = $insertData;

                //review current year url
                $insertData['title'] = 'Отзывы владельцев - ' . $car_mark->name_rus . ' ' . $car_model->name_rus . ' ' . $mod->body->name . ' ' . $currentYear . ' / ' . $car_mark->name . ' ' . $car_model->name . ' ' . $bodyAlias . ' ' . $currentYear;
                $insertData['slug'] = $currentYear . '/review/' . $mod->id;
                $insertRows[] = $insertData;

                //review next year url
                $insertData['title'] = 'Отзывы владельцев - ' . $car_mark->name_rus . ' ' . $car_model->name_rus . ' ' . $mod->body->name . ' ' . $nextYear . ' / ' . $car_mark->name . ' ' . $car_model->name . ' ' . $bodyAlias . ' ' . $nextYear;
                $insertData['slug'] = $nextYear . '/review/' . $mod->id;
                $insertRows[] = $insertData;

                //new review url
                $insertData['title'] = 'Тест-драйв, видео и обзор - Новый ' . $car_mark->name_rus . ' ' . $car_model->name_rus . ' ' . $mod->body->name . ' / ' . $car_mark->name . ' ' . $car_model->name . ' ' . $bodyAlias . ' New';
                $insertData['slug'] = 'new/review/' . $mod->id;
                $insertRows[] = $insertData;
            }
        }

        return $insertRows;
    }

    /**
     * BUILD "D" GROUP PAGES
     */
    public function actionGenerateD($insertData, $genMod)
    {

        $id_car_mark = $genMod[0]->markID;
        $id_car_model = $genMod[0]->modelID;

        $car_mark = Mark::model()->findByPk($id_car_mark);
        $car_model = CarModel::model()->findByPk($id_car_model);

       /* $criteria = new CDbCriteria;

        $criteria->select = '*';
        $criteria->with = array('mods', 'body', 'complectation');
        $criteria->condition = 'mods.id_car_model=' . $id_car_model;

        $genModCompl = ComplectationGenerationModification::model()->findAll($criteria);*/

        $currentYear = date("Y");
        $nextYear = $currentYear + 1;

        $insertRows = array();

        foreach ($genMod as $key => $mod) {

            $modName = str_replace(".", "-", number_format(floatval($mod->mods->name), 1));
            $bodyAlias = ($mod->body->alias=='suv') ? strtoupper($mod->body->alias) : ucfirst($mod->body->alias);

            $insertData['group_id'] = 4;
            $insertData['id_car_modification'] = $mod->mods->id_car_modification;
            $insertData['id_car_body'] = $mod->body->id_car_body;

            //ttx main url
            $insertData['title'] = $car_mark->name_rus . ' ' . $car_model->name_rus . ' ' . $mod->body->name . ' / ' . $car_mark->name . ' ' . $car_model->name . ' ' . $bodyAlias . ' - технические характеристики';
            $insertData['slug'] = 'ttx/' . $mod->id;
            $insertRows[] = $insertData;

            //ttx current year url
            $insertData['title'] = $car_mark->name_rus . ' ' . $car_model->name_rus . ' ' . $mod->body->name . ' ' . $currentYear . ' / ' . $car_mark->name . ' ' . $car_model->name . ' ' . $bodyAlias . ' ' . $currentYear . ' - технические характеристики';
            $insertData['slug'] = $currentYear . '/ttx/' . $mod->id;
            $insertRows[] = $insertData;

            //ttx next year url
            $insertData['title'] = $car_mark->name_rus . ' ' . $car_model->name_rus . ' ' . $mod->body->name . ' ' . $nextYear . ' / ' . $car_mark->name . ' ' . $car_model->name . ' ' . $bodyAlias . ' ' . $nextYear . ' - технические характеристики';
            $insertData['slug'] = $nextYear . '/ttx/' . $mod->id;
            $insertRows[] = $insertData;
        }

        /*foreach ($genModCompl as $key => $compl) {

            $insertData['id_car_body'] = $compl->body->id_car_body;
            $insertData['id_car_modification'] = $compl->mods->id_car_modification;

            $modName = str_replace(".", "-", number_format(floatval($compl->mods->name), 1));

            //complectation url
            $insertData['id_car_modification'] = $compl->mods->id_car_modification;
            $insertData['title'] = $car_mark->name_rus . ' '. $car_model->name_rus . ' ' . $compl->body->name . ' ' .  $modName . ' ' . $compl->complectation->name . ' / ' . $car_mark->name . ' ' . $car_model->name . ' ' . ucfirst($compl->body->alias) . ' ' . $modName . ' ' . $compl->complectation->name . ' - комплектация и опция';
            $insertData['slug'] = 'options/' . $compl->id_car_complectation_generation_modification;
            $insertRows[] = $insertData;
        }*/

        return $insertRows;
    }

    /**
     * Generate mark list of pages.
     */
    public function actionGenerateE($id_car_mark)
    {

        $car_mark = Mark::model()->findByPk($id_car_mark);

        $insertData = array();
        $insertData['id_car_mark'] = $id_car_mark;
        $insertData['group_id'] = 5;
        $insertData['create_time'] = date('Y-m-d H:i:s');
        $insertData['update_time'] = date('Y-m-d H:i:s');
        $insertData['user_id'] = Yii::app()->user->id;
        $insertData['change_user_id'] = Yii::app()->user->id;
        $insertData['body'] = 'Онлайн сервис покупки новых машин. Получите предложения на ' . $car_mark->name . ' до визита в автосалон. Только официальные дилеры.';
        $insertData['description'] = 'Онлайн сервис покупки новых машин. Получите предложения на ' . $car_mark->name . ' до визита в автосалон. Только официальные дилеры.';

        $currentYear = date("Y");
        $nextYear = $currentYear+1;

        $insertRows = array();

        //mark current year url
        $insertData['title'] =  $car_mark->name . ' ' . $currentYear . ' — цены на новые авто этого года | ' . $car_mark->name_rus . ' в наличии | Комплактации ' . $currentYear . ' с фото';
        $insertData['slug'] = $currentYear;
        $insertRows[] = $insertData;

        //mark next year url
        $insertData['title'] =  $car_mark->name . ' ' . $nextYear . ' — цены на новые авто этого года | ' . $car_mark->name_rus . ' в наличии | Комплактации ' . $nextYear . ' с фото';
        $insertData['slug'] = $nextYear;
        $insertRows[] = $insertData;

        //mark dealers url
        $insertData['title'] =  $car_mark->name_rus . ' / ' . $car_mark->name . ' от официального дилера | Модельный ряд и цены | Гараж — сайт по поиску официального дилера, автосалона или центра';
        $insertData['slug'] = 'dealers';
        $insertRows[] = $insertData;

        //mark diesel url
        $insertData['title'] =  $car_mark->name_rus . ' / ' . $car_mark->name . ' - дизель';
        $insertData['slug'] = 'diesel';
        $insertData['id_car_fuel'] = 'Дизель';
        $insertRows[] = $insertData;
        unset($insertData['id_car_fuel']);

        //mark new url
        $insertData['title'] =  'Новый ' . $car_mark->name_rus . ' / ' . $car_mark->name . ' - комплектации и цены | ' . $car_mark->name_rus . ' в новом кузове, фото';
        $insertData['slug'] = 'new';
        $insertRows[] = $insertData;

        //mark tests url
        $insertData['title'] = 'Тест драйв ' . $car_mark->name_rus . ' — видео';
        $insertData['slug'] = 'tests';
        $insertRows[] = $insertData;

        //mark review url
        $insertData['title'] = $car_mark->name_rus . ' / ' . $car_mark->name . ' - отзывы владельцев';
        $insertData['slug'] = 'review';
        $insertRows[] = $insertData;

        //mark review url
        $insertData['title'] = $car_mark->name_rus . ' / ' . $car_mark->name . ' - технические характеристики';
        $insertData['slug'] = 'ttx';
        $insertRows[] = $insertData;

        $builder = Yii::app()->db->schema->commandBuilder;
        $command = $builder->createMultipleInsertCommand('car_seo_page',$insertRows);
        $command->execute();

        $this->redirect(array('index'));
    }

    public function actionCreate()
    {
        $model = new SeoPage;

        if(isset($_POST['SeoPage']))
        {

            $id_car_mark = $_POST['id_car_mark'];
            $car_mark = Mark::model()->findByPk($id_car_mark);

            if (empty($car_mark) || ($_POST['SeoPage']['group_id']!=5 && empty($_POST['SeoPage']['id_car_model'])) )
            {
                $model->addError('id_car_mark', 'Выберите марку и модель авто!');
                $this->render('create',array(
                    'model'=>$model,
                ));
                return;
            }

            if ($_POST['SeoPage']['group_id']==5)
            {

                if (empty($_POST['SeoPage']['id_car_model']))
                {
                    return self::actionGenerateE($id_car_mark);
                } else {
                    $model->addError('id_car_model', 'Для генерации страниц группы "E" - поле "Модель" оставьте пустым!');
                    $this->render('create',array(
                        'model'=>$model,
                    ));
                    return;
                }

            }

            $id_car_model = $_POST['SeoPage']['id_car_model'];
            $car_model = CarModel::model()->findByPk($id_car_model);

            $criteria = new CDbCriteria;
            $criteria->select = 'mark.id_car_mark as markID, model.id_car_model as modelID, mark.name as markName, model.name as modelName, generation.name as GenerationName, generation.year_begin as YearBegin, mods.name as modName, min(complectationRel.price_min) as minPriceSelect ';

            $criteria->with = array('mark', 'model', 'volume', 'generation', 'mods', 'body', 'complectationRel' => array('together' => true));
            $criteria->group = 'generation.name, generation.year_begin, mods.id_car_body ';
            $criteria->together = false;
            $criteria->condition = 'model.id_car_model=' . $id_car_model . ' and  mark.is_visible=1 and model.is_visible=1 and generation.is_visible=1 and mods.is_visible=1';
            $criteria->order = 'generation.year_begin DESC';

            $genMod = GenerationModification::model()->findAll($criteria);

            $insertData = array();
            $insertData['id_car_mark'] = $id_car_mark;
            $insertData['id_car_model'] = $id_car_model;
            $insertData['create_time'] = date('Y-m-d H:i:s');
            $insertData['update_time'] = date('Y-m-d H:i:s');
            $insertData['user_id'] = Yii::app()->user->id;
            $insertData['change_user_id'] = Yii::app()->user->id;

            if (!empty($_POST['SeoPage']['description']))
            {
                $insertData['description'] = $_POST['SeoPage']['description'];
            } else {
                $insertData['description'] = 'Онлайн сервис покупки новых машин. Получите предложения на ' . $car_mark->name . ' ' . $car_model->name . ' до визита в автосалон. Только официальные дилеры.';
            }

            if (!empty($_POST['SeoPage']['body'])) {
                $insertData['body'] = $_POST['SeoPage']['body'];
            } else {
                $insertData['body'] = 'Онлайн сервис покупки новых машин. Получите предложения на ' . $car_mark->name . ' ' . $car_model->name . ' до визита в автосалон. Только официальные дилеры.';
            }

            if ($_POST['SeoPage']['group_id']==1) {

                $insertRows = self::actionGenerateA($insertData, $genMod);

            } else if ($_POST['SeoPage']['group_id'] == 2) {

                $insertRows = self::actionGenerateB($insertData, $genMod);

            } else if ($_POST['SeoPage']['group_id']==3) {

                $insertRows = self::actionGenerateC($insertData, $genMod, $_POST['SeoPage']['group_block_sort']);

            } else if ($_POST['SeoPage']['group_id']==4) {

                $insertRows = self::actionGenerateD($insertData, $genMod);

            }

            $builder = Yii::app()->db->schema->commandBuilder;
            $command = $builder->createMultipleInsertCommand('car_seo_page',$insertRows);
            $command->execute();

            $this->redirect(array('index'));

        } else {
            $this->render('create',array(
                'model'=>$model,
            ));
        }
    }

    /**
     * Generate list of pages.
     */
    public function actionGenerate()
    {

        $model = new SeoPage;

        if(isset($_POST['SeoPage']))
        {

            $id_car_mark = $_POST['id_car_mark'];
            $car_mark = Mark::model()->findByPk($id_car_mark);

            if (empty($_POST['SeoPage']['id_car_model']))
            {
                return self::actionGenerateE($id_car_mark);
            }

            $id_car_model = $_POST['SeoPage']['id_car_model'];
            $car_model = CarModel::model()->findByPk($id_car_model);

            $criteria = new CDbCriteria;
            $criteria->select = 'mark.id_car_mark as markID, model.id_car_model as modelID, mark.name as markName, model.name as modelName, generation.name as GenerationName, generation.year_begin as YearBegin, mods.name as modName, min(complectationRel.price_min) as minPriceSelect ';

            $criteria->with = array('mark', 'model', 'volume', 'generation', 'mods', 'body', 'complectationRel' => array('together' => true));
            $criteria->group = 'generation.name, generation.year_begin, mods.id_car_body ';
            $criteria->together = false;
            $criteria->condition = 'model.id_car_model=' . $id_car_model . ' and  mark.is_visible=1 and model.is_visible=1 and generation.is_visible=1 and mods.is_visible=1';
            $criteria->order = 'generation.year_begin DESC';

            $genMod = GenerationModification::model()->findAll($criteria);

            $criteria->with = array('mark', 'model', 'generation', 'mods', 'body', 'transmission', 'fuel', 'privod', 'complectationRel' => array('together' => true));
            $criteria->group = 'mods.id_car_modification';
            $criteria->condition = 'model.id_car_model=' . $id_car_model . ' and  mark.is_visible=1 and model.is_visible=1 and generation.is_visible=1 and mods.is_visible=1';
            $mods = GenerationModification::model()->findAll($criteria);

            $insertData = array();

            $insertData['id_car_mark'] = $id_car_mark;
            $insertData['id_car_model'] = $id_car_model;
            $insertData['create_time'] = date('Y-m-d H:i:s');
            $insertData['update_time'] = date('Y-m-d H:i:s');
            $insertData['user_id'] = Yii::app()->user->id;
            $insertData['change_user_id'] = Yii::app()->user->id;
            $insertData['body'] = 'Онлайн сервис покупки новых машин. Получите предложения на ' . $car_mark->name . ' ' . $car_model->name . ' до визита в автосалон. Только официальные дилеры.';
            $insertData['description'] = 'Онлайн сервис покупки новых машин. Получите предложения на ' . $car_mark->name . ' ' . $car_model->name . ' до визита в автосалон. Только официальные дилеры.';


            $insertRowsA = self::actionGenerateA($insertData, $genMod);
            $insertRowsB = self::actionGenerateB($insertData, $genMod);
            $insertRowsC = self::actionGenerateC($insertData, $genMod);
            $insertRowsD = self::actionGenerateD($insertData, $genMod);

            $insertRows = array_merge($insertRowsA, $insertRowsB, $insertRowsC, $insertRowsD);

            $builder = Yii::app()->db->schema->commandBuilder;
            $command = $builder->createMultipleInsertCommand('car_seo_page',$insertRows);
            $command->execute();

            $this->redirect(array('index'));

        } else {

            $this->render('generate',array(
                'model'=>$model,
            ));
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['SeoPage']))
        {
            $model->attributes=$_POST['SeoPage'];

            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Delete All fields.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     */
    public function actionDeleteAll()
    {

        $model = new SeoPage();

        SeoPage::model()->deleteAll();

        $this->redirect(array('index'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {

        $model = new SeoPage('search');

        $model->unsetAttributes(); // clear any default values

        $model->setAttributes(
            Yii::app()->getRequest()->getParam(
                'SeoPage',
                array()
            )
        );

        $this->render('index',array(
            'model'=>$model,
        ));

    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new SeoPage('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['SeoPage']))
            $model->attributes=$_GET['SeoPage'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return SeoPage the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {

        $model=SeoPage::model()->findByPk($id);
        $model->loadChars();

        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        $model->loadChars();
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param SeoPage $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='seo-page-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
