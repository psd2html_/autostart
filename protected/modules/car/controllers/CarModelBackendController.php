<?php

class CarModelBackendController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/inner';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','deleteHidden','translitAll','importModel','importImagesModel','updatePrices','upload'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{

        $model=new CarModel('search');
        
        $model->unsetAttributes(); // clear any default values

        $model->setAttributes(
            Yii::app()->getRequest()->getParam(
                'CarModel',
                array()
            )
        );

        $this->render('index',array(
            'model'=>$model,
        ));
	}

    public function actionCreate()
    {
        $model=new CarModel;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['CarModel']))
        {
            $model->attributes=$_POST['CarModel'];
            if(isset($_FILES['CarModel']))
            {
                $filename = $_FILES['CarModel']['name']['icon'];
                if(!empty($filename))
                {
                    $newname = uniqid().'.'.end(explode('.',$filename));    
                    $path=Yii::getPathOfAlias(Yii::app()->getModule('car')->assetsModelPath).'/'; 

                    $filepath = $_FILES['CarModel']['tmp_name']['icon'];
                    
                    copy($filepath, $path.$newname);
                    $thumb=Yii::app()->phpThumb->create($path.$newname);
                    $thumb->resize(250,250);
                    $thumb->save($path."250x250_".$newname);

                    $thumb->resize(100,100);
                    $thumb->save($path."100x100_".$newname);
                   
                    $thumb->resize(80,60);
                    $thumb->save($path."80x60_".$newname);
                   
                    $model->icon=$newname;
                }
            }           
            if($model->save())
            {
                $this->redirect(array('update','id'=>$model->id_car_model));
            }
        }
        $this->render('create',array(
            'model'=>$model,
        ));
    }    
    
    public function actionUpdate($id)
    {
        $model = CarModel::model()->findByPk($id);
        
        if(isset($_POST['CarModel']) || isset($_FILES['CarModel']))
        {
            if(isset($_POST['CarModel']))
            {
                $model->attributes=$_POST['CarModel'];
            }
            if(isset($_FILES['CarModel']))
            {
                $filename = $_FILES['CarModel']['name']['icon'];
                if(!empty($filename))
                {
                    $newname = uniqid().'.'.end(explode('.',$filename));    
                    $path=Yii::getPathOfAlias(Yii::app()->getModule('car')->assetsModelPath).'/'; 

                    $filepath = $_FILES['CarModel']['tmp_name']['icon'];
                    
                    copy($filepath, $path.$newname);
                    $thumb=Yii::app()->phpThumb->create($path.$newname);
                    $thumb->resize(250,250);
                    $thumb->save($path."250x250_".$newname);

                    $thumb->resize(100,100);
                    $thumb->save($path."100x100_".$newname);
                   
                    $thumb->resize(80,60);
                    $thumb->save($path."80x60_".$newname);
                   
                    $model->icon=$newname;
                }
            }           
            if($model->save())
                $this->redirect(array('update','id'=>$model->id_car_model));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }
    
    
    public function actionDelete($id)
    {
        $model = CarModel::model()->findByPk($id);
        if($model)
        {
            $model->deleteModel();
                   
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }
    public function actionDeleteHidden()
    {
        $models = CarModel::model()->findAll('is_visible=0');
        
        foreach($models as $model)
        {
            $model->deleteModel();
        }
        
        $this->redirect(array('index'));

    }
    
    public function actionTranslitAll()
    {
        $models = CarModel::model()->findAll('t.alias=""');
        
        foreach($models as $model)
        {
            $model->alias = Yii::app()->cars->translit($model->name);    
            $model->save();
        }
        
        $this->redirect(array('index'));
 
    }
    
    public function loadModel($id)
    {
        $model=CarModel::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
    
    public function actionUpload()
    {
        $this->render('upload');
    }
    
    public function actionImportImagesModel()
    {
        if(isset($_FILES['importImages']))
        {
            $filename = $_FILES['importImages']['tmp_name'];
            $genPath = Yii::getPathOfAlias(Yii::app()->getModule('car')->assetsGenPath).'/';        
        
            Yii::app()->cars->importImagesModels($filename,$genPath);
            Yii::app()->user->setFlash('success', "Импорт изображений завершен");
        }
        $this->redirect(array('/car/carModelBackend/upload'));
    }
    public function actionImportModel()
    {
        if(isset($_FILES['importModel']))
        {
            $filename = $_FILES['importModel']['tmp_name'];
        
            Yii::app()->cars->importModels($filename);
            Yii::app()->user->setFlash('success', "Импорт моделей завершен");
        }
        $this->redirect(array('/car/carModelBackend/upload'));
    }
    public function actionUpdatePrices()
    {
        if(isset($_FILES['updatePrices']))
        {
            $filename = $_FILES['updatePrices']['tmp_name'];
            
            Yii::app()->cars->updatePrices($filename);
            Yii::app()->user->setFlash('success', "Обновление цен завершено");
        }
        $this->redirect(array('/car/carModelBackend/upload'));
    }
}
