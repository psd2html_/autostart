<?php

class MarkBackendController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/inner';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','deleteHidden','translitAll'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Mark('search');
        $model->unsetAttributes(); // clear any default values

        $model->setAttributes(
            Yii::app()->getRequest()->getParam(
                'Mark',
                array()
            )
        );


        
		$this->render('index',array(
			'model'=>$model,
		));
	}

    public function actionCreate()
    {
        $model=new Mark;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Mark']))
        {
            $model->attributes=$_POST['Mark'];
            if(isset($_FILES['CarMark']))
            {
                $filename = $_FILES['CarMark']['name']['image'];
                if(!empty($filename))
                {
                    $newname = uniqid().'.'.end(explode('.',$filename));    
                    $path=Yii::getPathOfAlias(Yii::app()->getModule('car')->assetsGenPath).'/'; 

                    $filepath = $_FILES['CarMark']['tmp_name']['image'];
                    
                    copy($filepath, $path.$newname);
                    $thumb=Yii::app()->phpThumb->create($path.$newname);
                    $thumb->resize(890,350);
                    $thumb->save($path."890x350_".$newname);
                    $thumb->resize(200,150);
                    $thumb->save($path."200x150_".$newname);
                    $thumb->resize(100,100);
                    $thumb->save($path."100x100_".$newname);
                   
                    $model->image = $newname;

                }
                $filename = $_FILES['CarMark']['name']['icon'];
                if(!empty($filename))
                {
                    $newname = uniqid().'.'.end(explode('.',$filename));    
                    $path=Yii::getPathOfAlias(Yii::app()->getModule('car')->assetsMarkPath).'/'; 

                    $filepath = $_FILES['CarMark']['tmp_name']['icon'];
                    
                    copy($filepath, $path.$newname);
                    $thumb=Yii::app()->phpThumb->create($path.$newname);
                    $thumb->resize(250,250);
                    $thumb->save($path."250x250_".$newname);
                    $thumb->resize(100,100);
                    $thumb->save($path."100x100_".$newname);
                    $thumb->resize(80,60);
                    $thumb->save($path."80x60_".$newname);
                   
                    $model->icon = $newname;

                }
            }           
            if($model->save())
            {
                $this->redirect(array('update','id'=>$model->id_car_mark));
            }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }
    
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        //var_dump($_POST);
        //var_dump($_FILES);  exit();
        //$imgs=CUploadedFile::getInstancesByName('Modification');
        //var_dump($imgs);
        //var_dump($_FILES['CarGeneration']); exit();
        if(isset($_POST['CarMark']) || isset($_FILES['CarMark']))
        {
            $model->attributes=$_POST['Mark'];

            if(isset($_FILES['CarMark']))
            {
                $filename = $_FILES['CarMark']['name']['image'];
                if(!empty($filename))
                {
                    $newname = uniqid().'.'.end(explode('.',$filename));    
                    $path=Yii::getPathOfAlias(Yii::app()->getModule('car')->assetsMarkPath).'/'; 

                    $filepath = $_FILES['CarMark']['tmp_name']['image'];
                    
                    copy($filepath, $path.$newname);
                    $thumb=Yii::app()->phpThumb->create($path.$newname);
                    $thumb->resize(890,350);
                    $thumb->save($path."890x350_".$newname);
                    $thumb->resize(200,150);
                    $thumb->save($path."200x150_".$newname);
                    $thumb->resize(100,100);
                    $thumb->save($path."100x100_".$newname);
                   
                    $model->image = $newname;

                }
                $filename = $_FILES['CarMark']['name']['icon'];
                if(!empty($filename))
                {
                    $newname = uniqid().'.'.end(explode('.',$filename));    
                    $path=Yii::getPathOfAlias(Yii::app()->getModule('car')->assetsMarkPath).'/'; 

                    $filepath = $_FILES['CarMark']['tmp_name']['icon'];
                    
                    copy($filepath, $path.$newname);
                    $thumb=Yii::app()->phpThumb->create($path.$newname);
                    $thumb->resize(250,250);
                    $thumb->save($path."250x250_".$newname);
                    $thumb->resize(100,100);
                    $thumb->save($path."100x100_".$newname);
                    $thumb->resize(80,60);
                    $thumb->save($path."80x60_".$newname);
                   
                    $model->icon = $newname;

                }
            }           
            if($model->save())
                $this->redirect(array('update','id'=>$model->id_car_mark));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }
    
    public function actionDelete($id)
    {       
        $model = Mark::model()->findByPk($id);
        if($model)
        {
            foreach($model->allmodels as $carModel)
            {
                $carModel->deleteModel();    
            }
            $model->delete();
                   
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }    
    }
    
    public function actionDeleteHidden()
    {
        $models = Mark::model()->findAll('is_visible=0');
        
        foreach($models as $model)
        {
            foreach($model->allmodels as $carModel)
            {
                $carModel->deleteModel();    
            }
            $model->delete();
        }
        
        $this->redirect(array('index'));

    }
    
    public function actionTranslitAll()
    {
        $models = Mark::model()->findAll('t.alias=""');
        
        foreach($models as $model)
        {
            $model->alias = Yii::app()->cars->translit($model->name);    
            $model->save();
        }
        
        $this->redirect(array('index'));
 
    }
    
	public function loadModel($id)
	{
		$model=Mark::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Mark $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='mark-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
