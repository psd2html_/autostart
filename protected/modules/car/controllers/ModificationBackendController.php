<?php

class ModificationBackendController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/inner';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','indexAdd','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'deleteAll'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

	/**
	 * Lists all models.
	 */
    public function actionIndex()
    {
        $model=new Modification('search');

        
        $model->unsetAttributes(); // clear any default values

        $model->setAttributes(
            Yii::app()->getRequest()->getParam(
                'Modification',
                array()
            )
        );

        $this->render('index',array(
            'model'=>$model,
        ));
    }
    public function actionIndexAdd()
    {
        $model=new Modification('search');
        
        $model->unsetAttributes(); // clear any default values

        $model->setAttributes(
            Yii::app()->getRequest()->getParam(
                'Modification',
                array()
            )
        );

        $this->render('indexAdd',array(
            'model'=>$model,
        ));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        //$model->loadChars();
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        //var_dump($_POST);
        //var_dump($_FILES);  exit();
        //$imgs=CUploadedFile::getInstancesByName('Modification');

        if(Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('Modification') && Yii::app()->getRequest()->getPost('generations'))
        {
            $attr = Yii::app()->getRequest()->getPost('Modification');
            
            $model->attributes = $attr;
            
            //$oldFileName = $model->image;
            if(isset($attr['char']))
            {
                foreach($attr['char'] as $char_id=>$val)
                {
                    $charValue = CharacteristicValue::model()->findByPk($char_id);
                    $charValue->value = $val;
                    if(isset($attr['charUnit'][$char_id]))
                    {
                        $charValue->unit = $attr['charUnit'][$char_id];
                    }
                    $charValue->save();
                }
            }
                 
            if($model->save())
            {
                $gens = Yii::app()->getRequest()->getPost('generations');
                foreach($model->generationRel as $gen)
                {
                    if(!in_array($gen->id_car_generation,$gens))
                    {
                        $gen->delete();
                    } 
                }
                
                foreach($gens as $gen)
                {
                    if(!$model->inGeneration($gen))
                    {
                        $newGen = new GenerationModification;
                        $newGen->id_car_generation = $gen;
                        $newGen->id_car_modification = $model->id_car_modification;
                        $newGen->save();
                    }
                }
                $this->redirect(array('update','id'=>$model->id_car_modification));
            }
        }

        $this->render('update',array(
            'model'=>$model, 'carModel'=>$model->model
        ));
    }

	public function actionCreate($carModelId)
	{
        $model=new Modification;
        
        $carModel = CarModel::model()->findByPk($carModelId);

        $model->id_car_model = $carModel->id_car_model;
        $post = Yii::app()->getRequest()->getPost('generations');
        
        if(Yii::app()->getRequest()->getIsPostRequest() 
            && Yii::app()->getRequest()->getPost('Modification') 
            && !empty($post))
        {
            $model->attributes = Yii::app()->getRequest()->getPost('Modification');
            
            if($model->save())
            {
                foreach(Yii::app()->getRequest()->getPost('generations') as $gen)
                {
                    $rel = new GenerationModification;
                    $rel->id_car_generation = $gen;
                    $rel->id_car_modification = $model->id_car_modification;
                    
                    $rel->save();
                }
                
                $this->redirect(array('update','id'=>$model->id_car_modification));
            }
        }

        $this->render('create',array(
            'model'=>$model,'carModel'=>$carModel
        ));
	}
    public function actionDelete($id)
    {

        $model = $this->loadModel($id);
        $model->is_visible = 0;
        $model->save();

    }

    public function actionDeleteAll()
    {
        $idList = Yii::app()->request->getParam('car_modification_ids');
        foreach ($idList as $id) {
            $model = $this->loadModel($id);
            $model->is_visible = 0;
            $model->save();
        }
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Modification the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Modification::model()->findByPk($id);
        $model->loadChars();
        
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}
