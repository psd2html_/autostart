<?php
class CarModule extends CWebModule
{
	public $debug = false;
    
    public $defaultImage = '/images/nophoto.jpg';
    public $defaultCategoryImage = '/images/bgg.jpg';
    public $defaultCategoryText = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit consectetur excepturi eveniet veniam qui, repellat iure dolores, maxime placeat iusto delectus quod. Aliquam eveniet quis alias tempore officiis adipisci vel nihil mollitia natus obcaecati unde cupiditate sint iure quo non in dolorem repudiandae, recusandae provident nostrum minus minima fuga molestiae eaque. Veniam reiciendis, assumenda nulla?';
    
    public $assetsBodyPath = 'application.modules.car.views.assets.body';
    public $assetsModelPath = 'application.modules.car.views';
    public $assetsCharPath = 'application.modules.car.views.assets.chars';
    public $assetsGenPath = 'application.modules.car.views.assets.generation';
    public $assetsMarkPath = 'application.modules.car.views.assets.mark';
    public $urlMarkPath = '/upload/car/mark/';
    public $urlBodyPath = '/upload/car/body/';
    public $urlModelPath = '/upload/car/views/';
    public $urlCharPath = '/upload/car/chars/';
    public $urlGenPath = '/upload/car/generation/';
    
    public $importPath = '/upload/import/';
    
    public $colors = array(
        '13'=>'ff0000',
        '14'=>'f0f0f0',
        '15'=>'000000',
        '16'=>'0000ff',
        '17'=>'aaaaaa',
    );

	public function init()
	{
		// Set required classes for import.
		$this->setImport(array(
            'car.controllers.*',
			'car.components.*',
			'car.models.*',
            'settings.models.*',
		));
	}

	/**
	* @return the current version.
	*/
	public function getVersion()
	{
		return '1.0.0';
	}
}
