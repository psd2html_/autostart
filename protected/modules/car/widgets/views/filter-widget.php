<div class="rightFilters whiteSpace">
    <?php
        $form = $this->beginWidget(
            'booster.widgets.TbActiveForm',
            array(
                'id' => 'verticalForm',
                'htmlOptions' => array('class' => ''), // for inset effect
                'method' => 'get',
                'action' => Yii::app()->createUrl('/car/catalog/search')
            )
        );
    ?>
        
    <p class="firstInFilters"><strong>Цена</strong></p>
    <div class="row">
        <div class="col-xs-6">
            <input id="downPrice" type="text" class="form-control input-sm" name="price-min" placeholder="от" value="<?=number_format((int)$filters['priceMin'], 0, ',', ' ')?>">
        </div>
        <div class="col-xs-6">
            <input id="topPrice" type="text" class="form-control input-sm" name="price-max" placeholder="до" value="<?=number_format((int)$filters['priceMax'], 0, ',', ' ')?>">
        </div>
    </div>
    <div id="slider-range" data-min="<?=$priceMin?>" data-max="<?=$priceMax?>" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 1.23457%; width: 86.4198%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 1.23457%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 87.6543%;"></span></div>
    <div class="panel-group" id="filters">
        <div class="panel panel-default">
            <div class="panel-heading posRel">
                <a class="filterGroupTitle" data-toggle="collapse" data-parent="#filters" href="#collapseTwo" aria-expanded="true">
                    Кузов
                    <span class="glyphicon glyphicon-chevron-down downAccordion"></span>
                </a>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse in">
                <div class="panel-body">
                    <?php
                        foreach($bodies as $body)
                        {
                            ?>
                                <label class="oneFilterCarcase">
                                    <input type="checkbox" name="bodies[<?=$body->id_car_body?>]" <?=in_array($body->id_car_body,$filters['bodies'])?"checked":""?> value="<?=$body->id_car_body?>">
                                    <?=$body->icon?>
                                    <div><?=$body->name?></div>
                                </label>
                            <?php
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading posRel">
                <a class="filterGroupTitle" data-toggle="collapse" data-parent="#filters" href="#collapseThree">
                    Марка
                    <span class="glyphicon glyphicon-chevron-down downAccordion"></span>
                </a>
            </div>
            <div id="collapseThree" class="panel-collapse collapse markList">
                <div class="panel-body">
                    <div class="btn-group btn-group-sm markFilter" role="group" aria-label="left">
                        <div class="btn btn-default active" data-on-other="false">Популярные</div>
                        <div class="btn btn-default" data-on-other="true">Все марки</div>
                    </div>
                    <div class="checkbox row">
                            <?php
                                foreach($marks as $mark)
                                {
                                    echo '<div class="col-xs-6" '.((!$mark->is_fav && !in_array($mark->id_car_mark,$filters['marks']))?'data-other="true" style="display:none;"':"").'><p ><label><input type="checkbox" name="marks['.$mark->id_car_mark.']" value="'.$mark->id_car_mark.'" '.(in_array($mark->id_car_mark,$filters['marks'])?"checked":"").'>'.$mark->name.'</label></p></div>';
                                }
                            ?>                                        

                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading posRel">
                <a class="filterGroupTitle" data-toggle="collapse" data-parent="#filters" href="#collapseFour">
                    Двигатель
                    <span class="glyphicon glyphicon-chevron-down downAccordion"></span>
                </a>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <p class="filterTitle">Привод</p>
                    <div class="checkbox row">
                            <?php
                                foreach($privods as $privod)
                                {
                                    echo '<div class="col-xs-6"><p><label><input type="checkbox" name="privod[]" value="'.$privod.'" '.(in_array($privod,$filters['privod'])?"checked":"").'>'.$privod.'</label></p></div>';
                                }
                            ?>   
                    </div>
                    <p class="filterTitle">Трансмиссия</p>
                    <div class="checkbox row">
                           <?php
                                foreach($transmissions as $transmission)
                                {
                                    echo '<div class="col-xs-6"><p><label><input type="checkbox" name="transmission[]" value="'.$transmission.'" '.(in_array($transmission,$filters['transmissions'])?"checked":"").'>'.$transmission.'</label></p></div>';
                                }
                            ?>   

                    </div>
                    <p class="filterTitle">Тип топлива</p>
                    <div class="checkbox row">
                           <?php
                                foreach($fuelTypes as $fuelType)
                                {
                                    echo '<div class="col-xs-6"><p><label><input type="checkbox" name="fuelType[]" value="'.$fuelType.'" '.(in_array($fuelType,$filters['fuelTypes'])?"checked":"").'>'.$fuelType.'</label></p></div>';
                                }
                            ?> 
                    </div>
                    <p class="filterTitle">Мощность</p>
                    <div class="row">
                        <div class="col-xs-6">
                            <input type="text" class="form-control input-sm" name="power-from" placeholder="от" value="<?=$this->filters['powerMin']>0?$this->filters['powerMin']:""?>">
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control input-sm" name="power-to" placeholder="до" value="<?=$this->filters['powerMax']<10000?$this->filters['powerMax']:""?>">
                        </div>
                    </div>
                    <p class="filterTitle">Разгон до 100 км/ч</p>
                    <div class="row">
                        <div class="col-xs-6">
                            <input type="text" class="form-control input-sm" name="speed-from" placeholder="от" value="<?=$this->filters['speedMin']>0?$this->filters['speedMin']:""?>">
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control input-sm" name="speed-to" placeholder="до" value="<?=$this->filters['speedMax']<10000?$this->filters['speedMax']:""?>">
                        </div>
                    </div>
                    <p class="filterTitle">Расход топлива</p>
                    <div class="row">
                        <div class="col-xs-6">
                            <input type="text" class="form-control input-sm" name="rashod-from" placeholder="от" value="<?=$this->filters['rashodMin']>0?$this->filters['rashodMin']:""?>">
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control input-sm" name="rashod-to" placeholder="до" value="<?=$this->filters['rashodMax']<10000?$this->filters['rashodMax']:""?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        $this->widget(
            'booster.widgets.TbButton',
            array('buttonType' => 'submit', 'label' => 'Отфильтровать', 'htmlOptions'=> array('class' => 'btn btn-success btn-md', 'id'=>'filtered'))
        );
    ?>
    <?php
        $this->endWidget();
        unset($form);
    ?>
</div>