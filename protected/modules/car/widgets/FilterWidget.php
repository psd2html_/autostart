<?php

class FilterWidget extends CWidget
{
    public $view = 'filter-widget';
    

    public $htmlOptions = array();
    
    public $filters = array();

    public function run()
    {

        $bodies = Body::model()->findAll(array('order'=>'id_car_body'));
        $marks_col1 = array();
        $marks_col2 = array();
        $marks= Mark::model()->findAll(array('condition'=>'is_visible=1','order'=>'name'));
        
        /*$col = true;
        foreach($marks as $mark)
        {
            if($col)
            {
                array_push($marks_col1,$mark);
            }
            else
            {
                array_push($marks_col2,$mark);
            }
            $col = !$col;
        }*/
        
        $this->filters['bodies'] = Yii::app()->request->getQuery('bodies')?:array();
        $this->filters['marks'] = Yii::app()->request->getQuery('marks')?:array();
        $this->filters['privod'] = Yii::app()->request->getQuery('privod')?:array();
        $this->filters['transmissions'] = Yii::app()->request->getQuery('transmission')?:array();

        if (Yii::app()->request->getQuery('slug')==='diesel')
        {
            $this->filters['fuelTypes'] = array('Дизель');
        } else {
            $this->filters['fuelTypes'] = Yii::app()->request->getQuery('fuelType')?:array();
        }

        $this->filters['priceMin'] = Yii::app()->request->getQuery('price-min')?:ComplectationGenerationModification::getMinPrice();
        $this->filters['priceMax'] = Yii::app()->request->getQuery('price-max')?:ComplectationGenerationModification::getMaxPrice();
        
        $this->filters['powerMin'] = Yii::app()->request->getQuery('power-from')?:0;
        $this->filters['powerMax'] = Yii::app()->request->getQuery('power-to')?:100000;
        
        $this->filters['speedMin'] = Yii::app()->request->getQuery('speed-from')?:0;
        $this->filters['speedMax'] = Yii::app()->request->getQuery('speed-to')?:100000;
        
        $this->filters['rashodMin'] = Yii::app()->request->getQuery('rashod-from')?:0;
        $this->filters['rashodMax'] = Yii::app()->request->getQuery('rashod-to')?:100000;
        
        //$privods = Yii::app()->cars->getPrivods();
        $privods = Yii::app()->cars->getPrivods();
        $transmissions = Yii::app()->cars->getTransmissions();
        $fuelTypes = Yii::app()->cars->getFuelTypes();
        
        
        
        $this->render($this->view,  array(
                'bodies' => $bodies,
                'marks' => $marks,
                'privods' => $privods,
                'transmissions' => $transmissions,
                'priceMin' => ComplectationGenerationModification::getMinPrice(),
                'priceMax' => ComplectationGenerationModification::getMaxPrice(),
                'fuelTypes' => $fuelTypes,
                'filters' => $this->filters,
                'htmlOptions' => $this->htmlOptions
            )
        );
    }
}
