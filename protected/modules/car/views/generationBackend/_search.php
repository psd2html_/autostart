<?php
/* @var $this GenerationController */
/* @var $model Generation */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_car_generation'); ?>
		<?php echo $form->textField($model,'id_car_generation'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_car_model'); ?>
		<?php echo $form->textField($model,'id_car_model'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'year_begin'); ?>
		<?php echo $form->textField($model,'year_begin',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'year_end'); ?>
		<?php echo $form->textField($model,'year_end',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_visible'); ?>
		<?php echo $form->textField($model,'is_visible'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_car_type'); ?>
		<?php echo $form->textField($model,'id_car_type'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->