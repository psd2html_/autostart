<?php
/* @var $this GenerationController */
/* @var $model Generation */

$this->breadcrumbs=array(
	'Generations'=>array('index'),
	$model->name=>array('view','id'=>$model->id_car_generation),
	'Update',
);

$this->menu=array(
	array('label'=>'List Generation', 'url'=>array('index')),
	array('label'=>'Create Generation', 'url'=>array('create')),
	array('label'=>'View Generation', 'url'=>array('view', 'id'=>$model->id_car_generation)),
	array('label'=>'Manage Generation', 'url'=>array('admin')),
);
?>

<h1>Update Generation <?php echo $model->id_car_generation; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>