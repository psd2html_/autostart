<?php
/* @var $this GenerationController */
/* @var $model Generation */

$this->breadcrumbs=array(
	'Generations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Generation', 'url'=>array('index')),
	array('label'=>'Manage Generation', 'url'=>array('admin')),
);
?>

<h1>Create Generation</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>