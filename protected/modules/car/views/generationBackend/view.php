<?php
/* @var $this GenerationController */
/* @var $model Generation */

$this->breadcrumbs=array(
	'Generations'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Generation', 'url'=>array('index')),
	array('label'=>'Create Generation', 'url'=>array('create')),
	array('label'=>'Update Generation', 'url'=>array('update', 'id'=>$model->id_car_generation)),
	array('label'=>'Delete Generation', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_car_generation),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Generation', 'url'=>array('admin')),
);
?>

<h1>View Generation #<?php echo $model->id_car_generation; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_car_generation',
		'name',
		'id_car_model',
		'year_begin',
		'year_end',
		'is_visible',
		'id_car_type',
	),
)); ?>
