<?php
/* @var $this GenerationController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Generations',
);

$this->menu=array(
	array('label'=>'Create Generation', 'url'=>array('create')),
	array('label'=>'Manage Generation', 'url'=>array('admin')),
);
?>

<h1>Generations</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
