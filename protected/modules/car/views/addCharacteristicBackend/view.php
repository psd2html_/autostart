<?php
/* @var $this AddCharacteristicController */
/* @var $model AddCharacteristic */

$this->breadcrumbs=array(
	'Add Characteristics'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List AddCharacteristic', 'url'=>array('index')),
	array('label'=>'Create AddCharacteristic', 'url'=>array('create')),
	array('label'=>'Update AddCharacteristic', 'url'=>array('update', 'id'=>$model->id_car_characteristic)),
	array('label'=>'Delete AddCharacteristic', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_car_characteristic),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AddCharacteristic', 'url'=>array('admin')),
);
?>

<h1>View AddCharacteristic #<?php echo $model->id_car_characteristic; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_car_characteristic',
		'name',
		'id_parent',
		'id_car_type',
	),
)); ?>
