<?php
/* @var $this AddCharacteristicController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Add Characteristics',
);

$this->menu=array(
	array('label'=>'Create AddCharacteristic', 'url'=>array('create')),
	array('label'=>'Manage AddCharacteristic', 'url'=>array('admin')),
);
?>

<h1>Add Characteristics</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
