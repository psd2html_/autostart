<?php
/* @var $this AddCharacteristicController */
/* @var $model AddCharacteristic */

$this->breadcrumbs=array(
	'Add Characteristics'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AddCharacteristic', 'url'=>array('index')),
	array('label'=>'Manage AddCharacteristic', 'url'=>array('admin')),
);
?>

<h1>Create AddCharacteristic</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>