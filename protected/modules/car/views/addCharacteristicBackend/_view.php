<?php
/* @var $this AddCharacteristicController */
/* @var $data AddCharacteristic */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_characteristic')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_car_characteristic), array('view', 'id'=>$data->id_car_characteristic)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_parent')); ?>:</b>
	<?php echo CHtml::encode($data->id_parent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_type')); ?>:</b>
	<?php echo CHtml::encode($data->id_car_type); ?>
	<br />


</div>