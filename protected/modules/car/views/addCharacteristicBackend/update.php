<?php
/* @var $this AddCharacteristicController */
/* @var $model AddCharacteristic */

$this->breadcrumbs=array(
	'Add Characteristics'=>array('index'),
	$model->name=>array('view','id'=>$model->id_car_characteristic),
	'Update',
);

$this->menu=array(
	array('label'=>'List AddCharacteristic', 'url'=>array('index')),
	array('label'=>'Create AddCharacteristic', 'url'=>array('create')),
	array('label'=>'View AddCharacteristic', 'url'=>array('view', 'id'=>$model->id_car_characteristic)),
	array('label'=>'Manage AddCharacteristic', 'url'=>array('admin')),
);
?>

<h1>Update AddCharacteristic <?php echo $model->id_car_characteristic; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>