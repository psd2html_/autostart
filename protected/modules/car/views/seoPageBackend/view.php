<?php
/* @var $this SeoPageBackendController */
/* @var $model SeoPage */

$this->breadcrumbs=array(
	'Seo Pages'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List SeoPage', 'url'=>array('index')),
	array('label'=>'Create SeoPage', 'url'=>array('create')),
	array('label'=>'Update SeoPage', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SeoPage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SeoPage', 'url'=>array('admin')),
);
?>

<h1>View SeoPage #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'category_id',
		'lang',
		'parent_id',
		'create_time',
		'update_time',
		'user_id',
		'change_user_id',
		'title_short',
		'title',
		'slug',
		'body',
		'keywords',
		'description',
		'status',
		'is_protected',
		'layout',
		'view',
	),
)); ?>
