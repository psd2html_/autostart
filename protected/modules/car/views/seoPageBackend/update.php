<?php
/* @var $this SeoPageBackendController */
/* @var $model SeoPage */

$this->breadcrumbs=array(
	'Seo Pages'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SeoPage', 'url'=>array('index')),
	array('label'=>'Create SeoPage', 'url'=>array('create')),
	array('label'=>'View SeoPage', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SeoPage', 'url'=>array('admin')),
);
?>

<h1>Update SeoPage <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>