<?php
/* @var $this SeoPageBackendController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Seo Pages',
);

$this->menu=array(
	array('label'=>'Create SeoPage', 'url'=>array('create')),
	array('label'=>'Manage SeoPage', 'url'=>array('admin')),
);
?>

<h1>Seo Pages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
