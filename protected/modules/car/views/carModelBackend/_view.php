<?php
/* @var $this CarModelController */
/* @var $data CarModel */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_model')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_car_model), array('view', 'id'=>$data->id_car_model)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_mark')); ?>:</b>
	<?php echo CHtml::encode($data->id_car_mark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_visible')); ?>:</b>
	<?php echo CHtml::encode($data->is_visible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_type')); ?>:</b>
	<?php echo CHtml::encode($data->id_car_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_rus')); ?>:</b>
	<?php echo CHtml::encode($data->name_rus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_error_ignore')); ?>:</b>
	<?php echo CHtml::encode($data->is_error_ignore); ?>
	<br />


</div>