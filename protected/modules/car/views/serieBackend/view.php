<?php
/* @var $this SerieController */
/* @var $model Serie */

$this->breadcrumbs=array(
	'Series'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Serie', 'url'=>array('index')),
	array('label'=>'Create Serie', 'url'=>array('create')),
	array('label'=>'Update Serie', 'url'=>array('update', 'id'=>$model->id_car_serie)),
	array('label'=>'Delete Serie', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_car_serie),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Serie', 'url'=>array('admin')),
);
?>

<h1>View Serie #<?php echo $model->id_car_serie; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_car_serie',
		'id_car_model',
		'name',
		'is_visible',
		'id_car_generation',
		'id_car_type',
	),
)); ?>
