<?php
/* @var $this SerieController */
/* @var $data Serie */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_serie')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_car_serie), array('view', 'id'=>$data->id_car_serie)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_model')); ?>:</b>
	<?php echo CHtml::encode($data->id_car_model); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_visible')); ?>:</b>
	<?php echo CHtml::encode($data->is_visible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_generation')); ?>:</b>
	<?php echo CHtml::encode($data->id_car_generation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_type')); ?>:</b>
	<?php echo CHtml::encode($data->id_car_type); ?>
	<br />


</div>