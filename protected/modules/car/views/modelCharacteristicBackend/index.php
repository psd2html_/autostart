<?php
/* @var $this ModelCharacteristicController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Model Characteristics',
);

$this->menu=array(
	array('label'=>'Create ModelCharacteristic', 'url'=>array('create')),
	array('label'=>'Manage ModelCharacteristic', 'url'=>array('admin')),
);
?>

<h1>Model Characteristics</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
