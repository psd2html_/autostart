<?php
/* @var $this ModelCharacteristicController */
/* @var $model ModelCharacteristic */

$this->breadcrumbs=array(
	'Model Characteristics'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ModelCharacteristic', 'url'=>array('index')),
	array('label'=>'Manage ModelCharacteristic', 'url'=>array('admin')),
);
?>

<h1>Create ModelCharacteristic</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>