<?php
/* @var $this ModelCharacteristicController */
/* @var $model ModelCharacteristic */

$this->breadcrumbs=array(
	'Model Characteristics'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List ModelCharacteristic', 'url'=>array('index')),
	array('label'=>'Create ModelCharacteristic', 'url'=>array('create')),
	array('label'=>'Update ModelCharacteristic', 'url'=>array('update', 'id'=>$model->id_car_characteristic)),
	array('label'=>'Delete ModelCharacteristic', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_car_characteristic),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ModelCharacteristic', 'url'=>array('admin')),
);
?>

<h1>View ModelCharacteristic #<?php echo $model->id_car_characteristic; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_car_characteristic',
		'name',
		'id_parent',
		'id_car_type',
	),
)); ?>
