<?php
/* @var $this ModelCharacteristicController */
/* @var $model ModelCharacteristic */

$this->breadcrumbs=array(
	'Model Characteristics'=>array('index'),
	$model->name=>array('view','id'=>$model->id_car_characteristic),
	'Update',
);

$this->menu=array(
	array('label'=>'List ModelCharacteristic', 'url'=>array('index')),
	array('label'=>'Create ModelCharacteristic', 'url'=>array('create')),
	array('label'=>'View ModelCharacteristic', 'url'=>array('view', 'id'=>$model->id_car_characteristic)),
	array('label'=>'Manage ModelCharacteristic', 'url'=>array('admin')),
);
?>

<h1>Update ModelCharacteristic <?php echo $model->id_car_characteristic; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>