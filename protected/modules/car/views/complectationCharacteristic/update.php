<?php
/* @var $this ComplectationCharacteristicController */
/* @var $model ComplectationCharacteristic */

$this->breadcrumbs=array(
	'Complectation Characteristics'=>array('index'),
	$model->name=>array('view','id'=>$model->id_car_characteristic),
	'Update',
);

$this->menu=array(
	array('label'=>'List ComplectationCharacteristic', 'url'=>array('index')),
	array('label'=>'Create ComplectationCharacteristic', 'url'=>array('create')),
	array('label'=>'View ComplectationCharacteristic', 'url'=>array('view', 'id'=>$model->id_car_characteristic)),
	array('label'=>'Manage ComplectationCharacteristic', 'url'=>array('admin')),
);
?>

<h1>Update ComplectationCharacteristic <?php echo $model->id_car_characteristic; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>