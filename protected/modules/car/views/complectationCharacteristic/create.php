<?php
/* @var $this ComplectationCharacteristicController */
/* @var $model ComplectationCharacteristic */

$this->breadcrumbs=array(
	'Complectation Characteristics'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ComplectationCharacteristic', 'url'=>array('index')),
	array('label'=>'Manage ComplectationCharacteristic', 'url'=>array('admin')),
);
?>

<h1>Create ComplectationCharacteristic</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>