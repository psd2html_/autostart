<?php
/* @var $this ComplectationCharacteristicController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Complectation Characteristics',
);

$this->menu=array(
	array('label'=>'Create ComplectationCharacteristic', 'url'=>array('create')),
	array('label'=>'Manage ComplectationCharacteristic', 'url'=>array('admin')),
);
?>

<h1>Complectation Characteristics</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
