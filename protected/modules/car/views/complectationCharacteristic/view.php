<?php
/* @var $this ComplectationCharacteristicController */
/* @var $model ComplectationCharacteristic */

$this->breadcrumbs=array(
	'Complectation Characteristics'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List ComplectationCharacteristic', 'url'=>array('index')),
	array('label'=>'Create ComplectationCharacteristic', 'url'=>array('create')),
	array('label'=>'Update ComplectationCharacteristic', 'url'=>array('update', 'id'=>$model->id_car_characteristic)),
	array('label'=>'Delete ComplectationCharacteristic', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_car_characteristic),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ComplectationCharacteristic', 'url'=>array('admin')),
);
?>

<h1>View ComplectationCharacteristic #<?php echo $model->id_car_characteristic; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_car_characteristic',
		'name',
		'id_parent',
		'id_car_type',
	),
)); ?>
