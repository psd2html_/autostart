<?php
/* @var $this ComplectationController */
/* @var $model Complectation */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'complectation-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_car_modification'); ?>
		<?php echo $form->textField($model,'id_car_modification'); ?>
		<?php echo $form->error($model,'id_car_modification'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_visible'); ?>
		<?php echo $form->textField($model,'is_visible'); ?>
		<?php echo $form->error($model,'is_visible'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'price_min'); ?>
		<?php echo $form->textField($model,'price_min'); ?>
		<?php echo $form->error($model,'price_min'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'price_max'); ?>
		<?php echo $form->textField($model,'price_max'); ?>
		<?php echo $form->error($model,'price_max'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->