<?php
/* @var $this ComplectationController */
/* @var $model Complectation */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_car_complectation'); ?>
		<?php echo $form->textField($model,'id_car_complectation'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_car_modification'); ?>
		<?php echo $form->textField($model,'id_car_modification'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_visible'); ?>
		<?php echo $form->textField($model,'is_visible'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'price_min'); ?>
		<?php echo $form->textField($model,'price_min'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'price_max'); ?>
		<?php echo $form->textField($model,'price_max'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->