<?php
/* @var $this ComplectationController */
/* @var $model Complectation */

$this->breadcrumbs=array(
	'Complectations'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Complectation', 'url'=>array('index')),
	array('label'=>'Create Complectation', 'url'=>array('create')),
	array('label'=>'Update Complectation', 'url'=>array('update', 'id'=>$model->id_car_complectation)),
	array('label'=>'Delete Complectation', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_car_complectation),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Complectation', 'url'=>array('admin')),
);
?>

<h1>View Complectation #<?php echo $model->id_car_complectation; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_car_complectation',
		'id_car_modification',
		'name',
		'is_visible',
		'price_min',
		'price_max',
	),
)); ?>
