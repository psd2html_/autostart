<?php
/* @var $this ComplectationController */
/* @var $model Complectation */

$this->breadcrumbs=array(
	'Complectations'=>array('index'),
	$model->name=>array('view','id'=>$model->id_car_complectation),
	'Update',
);

$this->menu=array(
	array('label'=>'List Complectation', 'url'=>array('index')),
	array('label'=>'Create Complectation', 'url'=>array('create')),
	array('label'=>'View Complectation', 'url'=>array('view', 'id'=>$model->id_car_complectation)),
	array('label'=>'Manage Complectation', 'url'=>array('admin')),
);
?>

<h1>Update Complectation <?php echo $model->id_car_complectation; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>