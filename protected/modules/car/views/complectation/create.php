<?php
/* @var $this ComplectationController */
/* @var $model Complectation */

$this->breadcrumbs=array(
	'Complectations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Complectation', 'url'=>array('index')),
	array('label'=>'Manage Complectation', 'url'=>array('admin')),
);
?>

<h1>Create Complectation</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>