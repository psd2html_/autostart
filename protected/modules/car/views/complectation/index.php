<?php
/* @var $this ComplectationController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Complectations',
);

$this->menu=array(
	array('label'=>'Create Complectation', 'url'=>array('create')),
	array('label'=>'Manage Complectation', 'url'=>array('admin')),
);
?>

<h1>Complectations</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
