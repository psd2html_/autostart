<?php
/* @var $this ComplectationController */
/* @var $data Complectation */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_complectation')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_car_complectation), array('view', 'id'=>$data->id_car_complectation)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_modification')); ?>:</b>
	<?php echo CHtml::encode($data->id_car_modification); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_visible')); ?>:</b>
	<?php echo CHtml::encode($data->is_visible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price_min')); ?>:</b>
	<?php echo CHtml::encode($data->price_min); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price_max')); ?>:</b>
	<?php echo CHtml::encode($data->price_max); ?>
	<br />


</div>