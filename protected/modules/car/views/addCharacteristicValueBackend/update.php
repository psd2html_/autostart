<?php
/* @var $this AddCharacteristicValueController */
/* @var $model AddCharacteristicValue */

$this->breadcrumbs=array(
	'Add Characteristic Values'=>array('index'),
	$model->id_car_characteristic_value=>array('view','id'=>$model->id_car_characteristic_value),
	'Update',
);

$this->menu=array(
	array('label'=>'List AddCharacteristicValue', 'url'=>array('index')),
	array('label'=>'Create AddCharacteristicValue', 'url'=>array('create')),
	array('label'=>'View AddCharacteristicValue', 'url'=>array('view', 'id'=>$model->id_car_characteristic_value)),
	array('label'=>'Manage AddCharacteristicValue', 'url'=>array('admin')),
);
?>

<h1>Update AddCharacteristicValue <?php echo $model->id_car_characteristic_value; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>