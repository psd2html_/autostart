<?php
/* @var $this AddCharacteristicValueController */
/* @var $model AddCharacteristicValue */

$this->breadcrumbs=array(
	'Add Characteristic Values'=>array('index'),
	$model->id_car_characteristic_value,
);

$this->menu=array(
	array('label'=>'List AddCharacteristicValue', 'url'=>array('index')),
	array('label'=>'Create AddCharacteristicValue', 'url'=>array('create')),
	array('label'=>'Update AddCharacteristicValue', 'url'=>array('update', 'id'=>$model->id_car_characteristic_value)),
	array('label'=>'Delete AddCharacteristicValue', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_car_characteristic_value),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AddCharacteristicValue', 'url'=>array('admin')),
);
?>

<h1>View AddCharacteristicValue #<?php echo $model->id_car_characteristic_value; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_car_characteristic_value',
		'value',
		'image',
		'unit',
		'id_car_add_characteristic',
		'id_car_modification',
		'is_visible',
		'id_car_type',
	),
)); ?>
