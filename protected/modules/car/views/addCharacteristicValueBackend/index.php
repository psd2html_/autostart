<?php
/* @var $this AddCharacteristicValueController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Add Characteristic Values',
);

$this->menu=array(
	array('label'=>'Create AddCharacteristicValue', 'url'=>array('create')),
	array('label'=>'Manage AddCharacteristicValue', 'url'=>array('admin')),
);
?>

<h1>Add Characteristic Values</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
