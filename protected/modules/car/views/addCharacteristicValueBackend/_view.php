<?php
/* @var $this AddCharacteristicValueController */
/* @var $data AddCharacteristicValue */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_characteristic_value')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_car_characteristic_value), array('view', 'id'=>$data->id_car_characteristic_value)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('value')); ?>:</b>
	<?php echo CHtml::encode($data->value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image')); ?>:</b>
	<?php echo CHtml::encode($data->image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unit')); ?>:</b>
	<?php echo CHtml::encode($data->unit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_add_characteristic')); ?>:</b>
	<?php echo CHtml::encode($data->id_car_add_characteristic); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_modification')); ?>:</b>
	<?php echo CHtml::encode($data->id_car_modification); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_visible')); ?>:</b>
	<?php echo CHtml::encode($data->is_visible); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_type')); ?>:</b>
	<?php echo CHtml::encode($data->id_car_type); ?>
	<br />

	*/ ?>

</div>