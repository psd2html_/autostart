<?php
/* @var $this AddCharacteristicValueController */
/* @var $model AddCharacteristicValue */

$this->breadcrumbs=array(
	'Add Characteristic Values'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AddCharacteristicValue', 'url'=>array('index')),
	array('label'=>'Manage AddCharacteristicValue', 'url'=>array('admin')),
);
?>

<h1>Create AddCharacteristicValue</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>