<?php
/* @var $this ModelImageController */
/* @var $model ModelImage */

$this->breadcrumbs=array(
	'Model Images'=>array('index'),
	$model->id_car_model_image=>array('view','id'=>$model->id_car_model_image),
	'Update',
);

$this->menu=array(
	array('label'=>'List ModelImage', 'url'=>array('index')),
	array('label'=>'Create ModelImage', 'url'=>array('create')),
	array('label'=>'View ModelImage', 'url'=>array('view', 'id'=>$model->id_car_model_image)),
	array('label'=>'Manage ModelImage', 'url'=>array('admin')),
);
?>

<h1>Update ModelImage <?php echo $model->id_car_model_image; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>