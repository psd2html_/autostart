<?php
/* @var $this ModelImageController */
/* @var $model ModelImage */

$this->breadcrumbs=array(
	'Model Images'=>array('index'),
	$model->id_car_model_image,
);

$this->menu=array(
	array('label'=>'List ModelImage', 'url'=>array('index')),
	array('label'=>'Create ModelImage', 'url'=>array('create')),
	array('label'=>'Update ModelImage', 'url'=>array('update', 'id'=>$model->id_car_model_image)),
	array('label'=>'Delete ModelImage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_car_model_image),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ModelImage', 'url'=>array('admin')),
);
?>

<h1>View ModelImage #<?php echo $model->id_car_model_image; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_car_model_image',
		'image',
		'id_car_model',
	),
)); ?>
