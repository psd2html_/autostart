<?php
/* @var $this ModelImageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Model Images',
);

$this->menu=array(
	array('label'=>'Create ModelImage', 'url'=>array('create')),
	array('label'=>'Manage ModelImage', 'url'=>array('admin')),
);
?>

<h1>Model Images</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
