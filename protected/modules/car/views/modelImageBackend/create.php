<?php
/* @var $this ModelImageController */
/* @var $model ModelImage */

$this->breadcrumbs=array(
	'Model Images'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ModelImage', 'url'=>array('index')),
	array('label'=>'Manage ModelImage', 'url'=>array('admin')),
);
?>

<h1>Create ModelImage</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>