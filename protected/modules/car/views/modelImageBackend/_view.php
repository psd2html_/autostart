<?php
/* @var $this ModelImageController */
/* @var $data ModelImage */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_model_image')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_car_model_image), array('view', 'id'=>$data->id_car_model_image)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image')); ?>:</b>
	<?php echo CHtml::encode($data->image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_model')); ?>:</b>
	<?php echo CHtml::encode($data->id_car_model); ?>
	<br />


</div>