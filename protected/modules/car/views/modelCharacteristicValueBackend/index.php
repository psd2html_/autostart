<?php
/* @var $this ModelCharacteristicValueController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Model Characteristic Values',
);

$this->menu=array(
	array('label'=>'Create ModelCharacteristicValue', 'url'=>array('create')),
	array('label'=>'Manage ModelCharacteristicValue', 'url'=>array('admin')),
);
?>

<h1>Model Characteristic Values</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
