<?php
/* @var $this ModelCharacteristicValueController */
/* @var $model ModelCharacteristicValue */

$this->breadcrumbs=array(
	'Model Characteristic Values'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ModelCharacteristicValue', 'url'=>array('index')),
	array('label'=>'Manage ModelCharacteristicValue', 'url'=>array('admin')),
);
?>

<h1>Create ModelCharacteristicValue</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>