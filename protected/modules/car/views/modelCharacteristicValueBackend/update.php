<?php
/* @var $this ModelCharacteristicValueController */
/* @var $model ModelCharacteristicValue */

$this->breadcrumbs=array(
	'Model Characteristic Values'=>array('index'),
	$model->id_car_characteristic_value=>array('view','id'=>$model->id_car_characteristic_value),
	'Update',
);

$this->menu=array(
	array('label'=>'List ModelCharacteristicValue', 'url'=>array('index')),
	array('label'=>'Create ModelCharacteristicValue', 'url'=>array('create')),
	array('label'=>'View ModelCharacteristicValue', 'url'=>array('view', 'id'=>$model->id_car_characteristic_value)),
	array('label'=>'Manage ModelCharacteristicValue', 'url'=>array('admin')),
);
?>

<h1>Update ModelCharacteristicValue <?php echo $model->id_car_characteristic_value; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>