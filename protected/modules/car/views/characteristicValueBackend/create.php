<?php
/* @var $this CharacteristicValueController */
/* @var $model CharacteristicValue */

$this->breadcrumbs=array(
	'Characteristic Values'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CharacteristicValue', 'url'=>array('index')),
	array('label'=>'Manage CharacteristicValue', 'url'=>array('admin')),
);
?>

<h1>Create CharacteristicValue</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>