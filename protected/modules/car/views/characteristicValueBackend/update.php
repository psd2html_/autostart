<?php
/* @var $this CharacteristicValueController */
/* @var $model CharacteristicValue */

$this->breadcrumbs=array(
	'Characteristic Values'=>array('index'),
	$model->id_car_characteristic_value=>array('view','id'=>$model->id_car_characteristic_value),
	'Update',
);

$this->menu=array(
	array('label'=>'List CharacteristicValue', 'url'=>array('index')),
	array('label'=>'Create CharacteristicValue', 'url'=>array('create')),
	array('label'=>'View CharacteristicValue', 'url'=>array('view', 'id'=>$model->id_car_characteristic_value)),
	array('label'=>'Manage CharacteristicValue', 'url'=>array('admin')),
);
?>

<h1>Update CharacteristicValue <?php echo $model->id_car_characteristic_value; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>