<?php
/* @var $this CharacteristicValueController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Characteristic Values',
);

$this->menu=array(
	array('label'=>'Create CharacteristicValue', 'url'=>array('create')),
	array('label'=>'Manage CharacteristicValue', 'url'=>array('admin')),
);
?>

<h1>Characteristic Values</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
