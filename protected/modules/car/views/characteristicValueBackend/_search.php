<?php
/* @var $this CharacteristicValueController */
/* @var $model CharacteristicValue */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_car_characteristic_value'); ?>
		<?php echo $form->textField($model,'id_car_characteristic_value'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'value'); ?>
		<?php echo $form->textField($model,'value',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unit'); ?>
		<?php echo $form->textField($model,'unit',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_car_characteristic'); ?>
		<?php echo $form->textField($model,'id_car_characteristic'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_car_modification'); ?>
		<?php echo $form->textField($model,'id_car_modification'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_visible'); ?>
		<?php echo $form->textField($model,'is_visible'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_car_type'); ?>
		<?php echo $form->textField($model,'id_car_type'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->