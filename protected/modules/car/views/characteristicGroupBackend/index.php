<?php
/* @var $this CharacteristicGroupController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Characteristic Groups',
);

$this->menu=array(
	array('label'=>'Create CharacteristicGroup', 'url'=>array('create')),
	array('label'=>'Manage CharacteristicGroup', 'url'=>array('admin')),
);
?>

<h1>Characteristic Groups</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
