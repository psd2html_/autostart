<?php
/* @var $this CharacteristicGroupController */
/* @var $data CharacteristicGroup */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_characteristic_group')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_car_characteristic_group), array('view', 'id'=>$data->id_car_characteristic_group)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />


</div>