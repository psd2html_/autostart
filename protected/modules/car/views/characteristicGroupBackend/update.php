<?php
/* @var $this CharacteristicGroupController */
/* @var $model CharacteristicGroup */

$this->breadcrumbs=array(
	'Characteristic Groups'=>array('index'),
	$model->name=>array('view','id'=>$model->id_car_characteristic_group),
	'Update',
);

$this->menu=array(
	array('label'=>'List CharacteristicGroup', 'url'=>array('index')),
	array('label'=>'Create CharacteristicGroup', 'url'=>array('create')),
	array('label'=>'View CharacteristicGroup', 'url'=>array('view', 'id'=>$model->id_car_characteristic_group)),
	array('label'=>'Manage CharacteristicGroup', 'url'=>array('admin')),
);
?>

<h1>Update CharacteristicGroup <?php echo $model->id_car_characteristic_group; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>