<?php
/* @var $this CharacteristicGroupController */
/* @var $model CharacteristicGroup */

$this->breadcrumbs=array(
	'Characteristic Groups'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List CharacteristicGroup', 'url'=>array('index')),
	array('label'=>'Create CharacteristicGroup', 'url'=>array('create')),
	array('label'=>'Update CharacteristicGroup', 'url'=>array('update', 'id'=>$model->id_car_characteristic_group)),
	array('label'=>'Delete CharacteristicGroup', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_car_characteristic_group),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CharacteristicGroup', 'url'=>array('admin')),
);
?>

<h1>View CharacteristicGroup #<?php echo $model->id_car_characteristic_group; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_car_characteristic_group',
		'name',
	),
)); ?>
