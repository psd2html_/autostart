<?php
/* @var $this CharacteristicGroupController */
/* @var $model CharacteristicGroup */

$this->breadcrumbs=array(
	'Characteristic Groups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CharacteristicGroup', 'url'=>array('index')),
	array('label'=>'Manage CharacteristicGroup', 'url'=>array('admin')),
);
?>

<h1>Create CharacteristicGroup</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>