<?php
/* @var $this ComplectationCharacteristicValueController */
/* @var $model ComplectationCharacteristicValue */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_car_characteristic_value'); ?>
		<?php echo $form->textField($model,'id_car_characteristic_value'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'value'); ?>
		<?php echo $form->textArea($model,'value',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'image'); ?>
		<?php echo $form->textField($model,'image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unit'); ?>
		<?php echo $form->textField($model,'unit',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_car_add_characteristic'); ?>
		<?php echo $form->textField($model,'id_car_add_characteristic'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_car_model'); ?>
		<?php echo $form->textField($model,'id_car_model'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_visible'); ?>
		<?php echo $form->textField($model,'is_visible'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_car_type'); ?>
		<?php echo $form->textField($model,'id_car_type'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->