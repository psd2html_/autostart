<?php
/* @var $this ComplectationCharacteristicValueController */
/* @var $model ComplectationCharacteristicValue */

$this->breadcrumbs=array(
	'Complectation Characteristic Values'=>array('index'),
	$model->id_car_characteristic_value=>array('view','id'=>$model->id_car_characteristic_value),
	'Update',
);

$this->menu=array(
	array('label'=>'List ComplectationCharacteristicValue', 'url'=>array('index')),
	array('label'=>'Create ComplectationCharacteristicValue', 'url'=>array('create')),
	array('label'=>'View ComplectationCharacteristicValue', 'url'=>array('view', 'id'=>$model->id_car_characteristic_value)),
	array('label'=>'Manage ComplectationCharacteristicValue', 'url'=>array('admin')),
);
?>

<h1>Update ComplectationCharacteristicValue <?php echo $model->id_car_characteristic_value; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>