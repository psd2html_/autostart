<?php
/* @var $this ComplectationCharacteristicValueController */
/* @var $model ComplectationCharacteristicValue */

$this->breadcrumbs=array(
	'Complectation Characteristic Values'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ComplectationCharacteristicValue', 'url'=>array('index')),
	array('label'=>'Manage ComplectationCharacteristicValue', 'url'=>array('admin')),
);
?>

<h1>Create ComplectationCharacteristicValue</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>