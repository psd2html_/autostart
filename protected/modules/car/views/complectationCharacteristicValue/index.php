<?php
/* @var $this ComplectationCharacteristicValueController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Complectation Characteristic Values',
);

$this->menu=array(
	array('label'=>'Create ComplectationCharacteristicValue', 'url'=>array('create')),
	array('label'=>'Manage ComplectationCharacteristicValue', 'url'=>array('admin')),
);
?>

<h1>Complectation Characteristic Values</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
