<?php
/* @var $this ComplectationCharacteristicValueController */
/* @var $model ComplectationCharacteristicValue */

$this->breadcrumbs=array(
	'Complectation Characteristic Values'=>array('index'),
	$model->id_car_characteristic_value,
);

$this->menu=array(
	array('label'=>'List ComplectationCharacteristicValue', 'url'=>array('index')),
	array('label'=>'Create ComplectationCharacteristicValue', 'url'=>array('create')),
	array('label'=>'Update ComplectationCharacteristicValue', 'url'=>array('update', 'id'=>$model->id_car_characteristic_value)),
	array('label'=>'Delete ComplectationCharacteristicValue', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_car_characteristic_value),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ComplectationCharacteristicValue', 'url'=>array('admin')),
);
?>

<h1>View ComplectationCharacteristicValue #<?php echo $model->id_car_characteristic_value; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_car_characteristic_value',
		'value',
		'image',
		'unit',
		'id_car_add_characteristic',
		'id_car_model',
		'is_visible',
		'id_car_type',
	),
)); ?>
