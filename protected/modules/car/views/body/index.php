<?php
/* @var $this BodyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bodies',
);

$this->menu=array(
	array('label'=>'Create Body', 'url'=>array('create')),
	array('label'=>'Manage Body', 'url'=>array('admin')),
);
?>

<h1>Bodies</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
