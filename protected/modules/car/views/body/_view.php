<?php
/* @var $this BodyController */
/* @var $data Body */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_body')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_car_body), array('view', 'id'=>$data->id_car_body)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />


</div>