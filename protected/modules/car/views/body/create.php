<?php
/* @var $this BodyController */
/* @var $model Body */

$this->breadcrumbs=array(
	'Bodies'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Body', 'url'=>array('index')),
	array('label'=>'Manage Body', 'url'=>array('admin')),
);
?>

<h1>Create Body</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>