<?php
/* @var $this BodyController */
/* @var $model Body */

$this->breadcrumbs=array(
	'Bodies'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Body', 'url'=>array('index')),
	array('label'=>'Create Body', 'url'=>array('create')),
	array('label'=>'Update Body', 'url'=>array('update', 'id'=>$model->id_car_body)),
	array('label'=>'Delete Body', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_car_body),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Body', 'url'=>array('admin')),
);
?>

<h1>View Body #<?php echo $model->id_car_body; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_car_body',
		'name',
	),
)); ?>
