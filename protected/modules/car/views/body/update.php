<?php
/* @var $this BodyController */
/* @var $model Body */

$this->breadcrumbs=array(
	'Bodies'=>array('index'),
	$model->name=>array('view','id'=>$model->id_car_body),
	'Update',
);

$this->menu=array(
	array('label'=>'List Body', 'url'=>array('index')),
	array('label'=>'Create Body', 'url'=>array('create')),
	array('label'=>'View Body', 'url'=>array('view', 'id'=>$model->id_car_body)),
	array('label'=>'Manage Body', 'url'=>array('admin')),
);
?>

<h1>Update Body <?php echo $model->id_car_body; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>