<?php
/* @var $this ModificationController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Modifications',
);

$this->menu=array(
	array('label'=>'Create Modification', 'url'=>array('create')),
	array('label'=>'Manage Modification', 'url'=>array('admin')),
);
?>

<h1>Modifications</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
