<?php
/* @var $this ModificationController */
/* @var $model Modification */

$this->breadcrumbs=array(
	'Modifications'=>array('index'),
	$model->name=>array('view','id'=>$model->id_car_modification),
	'Update',
);

$this->menu=array(
	array('label'=>'List Modification', 'url'=>array('index')),
	array('label'=>'Create Modification', 'url'=>array('create')),
	array('label'=>'View Modification', 'url'=>array('view', 'id'=>$model->id_car_modification)),
	array('label'=>'Manage Modification', 'url'=>array('admin')),
);
?>

<h1>Update Modification <?php echo $model->id_car_modification; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>