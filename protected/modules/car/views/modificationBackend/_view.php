<?php
/* @var $this ModificationController */
/* @var $data Modification */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_modification')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_car_modification), array('view', 'id'=>$data->id_car_modification)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_serie')); ?>:</b>
	<?php echo CHtml::encode($data->id_car_serie); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_model')); ?>:</b>
	<?php echo CHtml::encode($data->id_car_model); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_production_year')); ?>:</b>
	<?php echo CHtml::encode($data->start_production_year); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_production_year')); ?>:</b>
	<?php echo CHtml::encode($data->end_production_year); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_visible')); ?>:</b>
	<?php echo CHtml::encode($data->is_visible); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_type')); ?>:</b>
	<?php echo CHtml::encode($data->id_car_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price_min')); ?>:</b>
	<?php echo CHtml::encode($data->price_min); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price_max')); ?>:</b>
	<?php echo CHtml::encode($data->price_max); ?>
	<br />

	*/ ?>

</div>