<?php
/* @var $this ModificationController */
/* @var $model Modification */

$this->breadcrumbs=array(
	'Modifications'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Modification', 'url'=>array('index')),
	array('label'=>'Create Modification', 'url'=>array('create')),
	array('label'=>'Update Modification', 'url'=>array('update', 'id'=>$model->id_car_modification)),
	array('label'=>'Delete Modification', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_car_modification),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Modification', 'url'=>array('admin')),
);
?>

<h1>View Modification #<?php echo $model->id_car_modification; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_car_modification',
		'id_car_serie',
		'id_car_model',
		'name',
		'start_production_year',
		'end_production_year',
		'is_visible',
		'id_car_type',
		'price_min',
		'price_max',
	),
)); ?>
