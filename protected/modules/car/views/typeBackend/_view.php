<?php
/* @var $this TypeController */
/* @var $data Type */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_type')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_car_type), array('view', 'id'=>$data->id_car_type)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />


</div>