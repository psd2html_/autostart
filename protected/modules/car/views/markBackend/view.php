<?php
/* @var $this MarkController */
/* @var $model Mark */

$this->breadcrumbs=array(
	'Marks'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Mark', 'url'=>array('index')),
	array('label'=>'Create Mark', 'url'=>array('create')),
	array('label'=>'Update Mark', 'url'=>array('update', 'id'=>$model->id_car_mark)),
	array('label'=>'Delete Mark', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_car_mark),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Mark', 'url'=>array('admin')),
);
?>

<h1>View Mark #<?php echo $model->id_car_mark; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_car_mark',
		'name',
		'is_visible',
		'id_car_type',
		'name_rus',
	),
)); ?>
