<?php
/* @var $this MarkController */
/* @var $model Mark */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mark-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_visible'); ?>
		<?php echo $form->textField($model,'is_visible'); ?>
		<?php echo $form->error($model,'is_visible'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_car_type'); ?>
		<?php echo $form->textField($model,'id_car_type'); ?>
		<?php echo $form->error($model,'id_car_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name_rus'); ?>
		<?php echo $form->textField($model,'name_rus',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name_rus'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->