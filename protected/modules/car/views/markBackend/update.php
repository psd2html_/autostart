<?php
/* @var $this MarkController */
/* @var $model Mark */

$this->breadcrumbs=array(
	'Marks'=>array('index'),
	$model->name=>array('view','id'=>$model->id_car_mark),
	'Update',
);

$this->menu=array(
	array('label'=>'List Mark', 'url'=>array('index')),
	array('label'=>'Create Mark', 'url'=>array('create')),
	array('label'=>'View Mark', 'url'=>array('view', 'id'=>$model->id_car_mark)),
	array('label'=>'Manage Mark', 'url'=>array('admin')),
);
?>

<h1>Update Mark <?php echo $model->id_car_mark; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>