<?php
/* @var $this BodyCharController */
/* @var $model BodyChar */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_car_body'); ?>
		<?php echo $form->textField($model,'id_car_body'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_car_char'); ?>
		<?php echo $form->textField($model,'id_car_char'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->