<?php
/* @var $this BodyCharController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Body Chars',
);

$this->menu=array(
	array('label'=>'Create BodyChar', 'url'=>array('create')),
	array('label'=>'Manage BodyChar', 'url'=>array('admin')),
);
?>

<h1>Body Chars</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
