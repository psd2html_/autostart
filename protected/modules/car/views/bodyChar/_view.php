<?php
/* @var $this BodyCharController */
/* @var $data BodyChar */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_body')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_car_body), array('view', 'id'=>$data->id_car_body)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_car_char')); ?>:</b>
	<?php echo CHtml::encode($data->id_car_char); ?>
	<br />


</div>