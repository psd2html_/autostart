<?php
/* @var $this BodyCharController */
/* @var $model BodyChar */

$this->breadcrumbs=array(
	'Body Chars'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BodyChar', 'url'=>array('index')),
	array('label'=>'Manage BodyChar', 'url'=>array('admin')),
);
?>

<h1>Create BodyChar</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>