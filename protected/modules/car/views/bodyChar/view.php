<?php
/* @var $this BodyCharController */
/* @var $model BodyChar */

$this->breadcrumbs=array(
	'Body Chars'=>array('index'),
	$model->id_car_body,
);

$this->menu=array(
	array('label'=>'List BodyChar', 'url'=>array('index')),
	array('label'=>'Create BodyChar', 'url'=>array('create')),
	array('label'=>'Update BodyChar', 'url'=>array('update', 'id'=>$model->id_car_body)),
	array('label'=>'Delete BodyChar', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_car_body),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BodyChar', 'url'=>array('admin')),
);
?>

<h1>View BodyChar #<?php echo $model->id_car_body; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_car_body',
		'id_car_char',
	),
)); ?>
