<?php
/* @var $this BodyCharController */
/* @var $model BodyChar */

$this->breadcrumbs=array(
	'Body Chars'=>array('index'),
	$model->id_car_body=>array('view','id'=>$model->id_car_body),
	'Update',
);

$this->menu=array(
	array('label'=>'List BodyChar', 'url'=>array('index')),
	array('label'=>'Create BodyChar', 'url'=>array('create')),
	array('label'=>'View BodyChar', 'url'=>array('view', 'id'=>$model->id_car_body)),
	array('label'=>'Manage BodyChar', 'url'=>array('admin')),
);
?>

<h1>Update BodyChar <?php echo $model->id_car_body; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>