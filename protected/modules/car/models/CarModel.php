<?php

/**
 * This is the model class for table "car_model".
 *
 * The followings are the available columns in table 'car_model':
 * @property integer $id_car_model
 * @property integer $id_car_mark
 * @property string $name
 * @property integer $is_visible
 * @property integer $id_car_type
 * @property string $name_rus
 * @property string $icon
 * @property integer $is_error_ignore
 */
class CarModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'car_model';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_car_mark, name, id_car_type, alias', 'required'),
			array('id_car_mark, is_visible, id_car_type, is_error_ignore', 'numerical', 'integerOnly'=>true),
			array('name, name_rus, icon', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_car_model, id_car_mark, name, is_visible, id_car_type, name_rus, is_error_ignore, icon', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{  
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'type'=>array(self::BELONGS_TO, 'Type', 'id_car_type'),
            'mark'=>array(self::BELONGS_TO, 'Mark', 'id_car_mark'),
            'generationMods'=>array(self::HAS_MANY, 'GenerationModification', array('id_car_generation'=>'id_car_generation'),'through'=>'generation'),
            'generation'=>array(self::HAS_MANY, 'Generation', 'id_car_model', 'on'=>'generation.is_visible=1'),
            'complectation'=>array(self::HAS_MANY, 'Complectation', 'id_car_model', 'order'=>'name'),
            'mods'=>array(self::HAS_MANY, 'Modification', 'id_car_model', 'on'=>'mods.is_visible=1'),
		);
        
        
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_car_model' => 'ID',
			'id_car_mark' => 'Марка автомобиля',
			'name' => 'Название модели',
			'is_visible' => 'Видимость',
			'id_car_type' => 'Тип авто',
            'name_rus' => 'Название на русском',
			'icon' => 'Иконка для поиска',
			'is_error_ignore' => 'Игнорировать ошибки парсинга',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_car_model',$this->id_car_model);
		$criteria->compare('id_car_mark',$this->id_car_mark);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('is_visible',$this->is_visible);
		$criteria->compare('id_car_type',$this->id_car_type);
		$criteria->compare('name_rus',$this->name_rus,true);
		$criteria->compare('is_error_ignore',$this->is_error_ignore);
        $criteria->order = 'name';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CarModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    public function getMinPrice()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 'min(cast(car_add_characteristic_value.value as UNSIGNED)) as minPrice';
        $criteria->join = 'LEFT JOIN car_modification on car_modification.id_car_model=t.id_car_model
                        LEFT JOIN car_add_characteristic_value on car_add_characteristic_value.id_car_modification = car_modification.id_car_modification';
        $criteria->condition = 't.id_car_model=:id_car_model and car_add_characteristic_value.id_car_add_characteristic='.Modification::CHARACTERISTIC_MIN_PRICE;
        $criteria->params = array(':id_car_model'=>$this->id_car_model);
        
        $mods = CarModel::model()->find($criteria);
        return $mods->minPrice;
    }
    
    public function getGenerationList()
    {
        $res = array(); 
        $gens = $this->generation;    
        usort ( $gens , function($g1, $g2){if ($g1 == $g2) {return 0;} return ($g1->year_begin < $g2->year_begin) ? -1 : 1;} );
        foreach($gens as $gen)
        {
            $res[$gen->id_car_generation] = $gen->year_begin." - ".($gen->year_end?:'н.в.');
        }
        return $res;
    }     

    public function getGenerationListWithName()
    {
        $res = array(); 
        $gens = $this->generation;    
        usort ( $gens , function($g1, $g2){if ($g1 == $g2) {return 0;} return ($g1->year_begin < $g2->year_begin) ? -1 : 1;} );
        foreach($gens as $gen)
        {
            $res[$gen->id_car_generation] = $gen->name." (".$gen->year_begin." - ".($gen->year_end?:'н.в.').")";
        }
        return $res;
    }     

    public function getGenerationMods()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id';
        $criteria->with = array('generation','model','mark');
        $criteria->condition = 'model.id_car_model=:id_car_model';
        $criteria->params = array(':id_car_model'=>$this->id_car_model);
        $criteria->group = 'generation.year_begin, generation.year_end';
        $mods = GenerationModification::model()->findAll($criteria);
        
        $result = array();
        
        foreach($mods as $mod)
        {             
                $result[$mod->generation->id_car_generation]=array('data-modid'=>$mod->id,'data-mod'=>Yii::app()->createUrl('/car/catalog/single',array('id'=>$mod->id,'mark'=>$mod->mark->alias,'model'=>$mod->model->alias)),'data-char'=>Yii::app()->createUrl('/car/catalog/character',array('id'=>$mod->id,'mark'=>$mod->mark->alias,'model'=>$mod->model->alias)),'data-compl'=>0);
        }
        //var_dump($result);exit();
        return $result;
    }   
    
    public function deleteModel()
    {
        $id = $this->id_car_model;

        $query = "delete cv.* from `car_complectation_characteristic_value` as cv
            inner join `car_complectation` as c on c.id_car_complectation = cv.id_car_complectation
        where c.id_car_model=:id_car_model";
        
        $command = Yii::app()->db->createCommand($query);
        $command->execute(array(':id_car_model' => $id));


        $query = "delete cgm.* from `car_complectation_generation_modification` as cgm
            inner join `car_generation_modification` as gm on gm.id = cgm.id_car_generation_modification
            inner join `car_generation` as g on gm.id_car_generation = g.id_car_generation
        where g.id_car_model=:id_car_model";
        
        $command = Yii::app()->db->createCommand($query);
        $command->execute(array(':id_car_model' => $id));

        $query = "delete cgm.* from `car_complectation_generation_modification` as cgm
            inner join `car_generation_modification` as gm on gm.id = cgm.id_car_generation_modification
            inner join `car_modification` as m on gm.id_car_modification = m.id_car_modification
        where m.id_car_model=:id_car_model";
        
        Complectation::model()->deleteAll('id_car_model=:id_car_model',array(':id_car_model' => $id));

        $command = Yii::app()->db->createCommand($query);
        $command->execute(array(':id_car_model' => $id));

        $query = "delete gm.* from `car_generation_modification` as gm
            inner join `car_generation` as g on gm.id_car_generation = g.id_car_generation
        where g.id_car_model=:id_car_model";
        
        $command = Yii::app()->db->createCommand($query);
        $command->execute(array(':id_car_model' => $id));

        $query = "delete gm.* from `car_generation_modification` as gm
            inner join `car_modification` as m on gm.id_car_modification = m.id_car_modification
        where m.id_car_model=:id_car_model";
        
        $command = Yii::app()->db->createCommand($query);
        $command->execute(array(':id_car_model' => $id));

        $query = "delete gc.* from `car_generation_characteristic_value` as gc
            inner join `car_generation` as g on gc.id_car_generation = g.id_car_generation
        where g.id_car_model=:id_car_model";
        
        $command = Yii::app()->db->createCommand($query);
        $command->execute(array(':id_car_model' => $id));

        $query = "delete gs.* from `car_generation_similar` as gs
            inner join `car_generation` as g on gs.id_car_generation = g.id_car_generation
        where g.id_car_model=:id_car_model";
        
        $command = Yii::app()->db->createCommand($query);
        $command->execute(array(':id_car_model' => $id));

        $query = "delete gi.* from `car_generation_image` as gi
            inner join `car_generation` as g on gi.id_car_generation = g.id_car_generation
        where g.id_car_model=:id_car_model";
        
        $command = Yii::app()->db->createCommand($query);
        $command->execute(array(':id_car_model' => $id));


        
        Modification::model()->deleteAll('id_car_model=:id_car_model',array(':id_car_model' => $id));

        Generation::model()->deleteAll('id_car_model=:id_car_model',array(':id_car_model' => $id));

        $this->delete();
    
    }
}
