<?php

/**
 * This is the model class for table "car_generation".
 *
 * The followings are the available columns in table 'car_generation':
 * @property integer $id_car_generation
 * @property string $name
 * @property integer $id_car_model
 * @property string $year_begin
 * @property string $year_end
 * @property integer $is_visible
 * @property integer $id_car_type
 */
class Generation extends CActiveRecord
{

    public $colors;
    
    const CHARACTERISTIC_IMAGES_ID=11;
    const CHARACTERISTIC_HEADER_IMAGES_ID=12;
    const CHARACTERISTIC_COLORS_PARENT_ID=3;
    const CHARACTERISTIC_INTROTEXT_ID=9;
    const CHARACTERISTIC_DESCRIPTION_ID=10;
    const CHARACTERISTIC_REVIEW_ID=19;
    
   	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'car_generation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, id_car_model, id_car_type', 'required'),
			array('id_car_model, is_visible, id_car_type', 'numerical', 'integerOnly'=>true),
			array('name, year_begin, year_end', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_car_generation, name, id_car_model, year_begin, year_end, is_visible, id_car_type, mark, model', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'model'=>array(self::BELONGS_TO, 'CarModel', 'id_car_model'),
            'type'=>array(self::BELONGS_TO, 'Type', 'id_car_type'),
            'mark'=>array(self::BELONGS_TO, 'Mark', array('id_car_mark'=>'id_car_mark'),'through'=>'model'),
            'prev_images'=>array(self::HAS_MANY, 'GenerationCharacteristicValue', 'id_car_generation', 'on'=>'id_car_add_characteristic='.self::CHARACTERISTIC_IMAGES_ID),
            'head_images'=>array(self::HAS_MANY, 'GenerationCharacteristicValue', 'id_car_generation', 'on'=>'id_car_add_characteristic='.self::CHARACTERISTIC_HEADER_IMAGES_ID),
            'colorsChar'=>array(self::HAS_MANY,'GenerationCharacteristic','','on'=>'id_parent='.self::CHARACTERISTIC_COLORS_PARENT_ID),
            'gallery'=>array(self::HAS_MANY, 'GenerationImage', 'id_car_generation'),
            'modsRel'=>array(self::HAS_MANY, 'GenerationModification', array('id_car_generation'=>'id_car_generation')),        
            'modsRelFirst'=>array(self::HAS_ONE, 'GenerationModification', array('id_car_generation'=>'id_car_generation')),        
            'modsClear'=>array(self::HAS_MANY, 'Modification', array('id_car_modification'=>'id_car_modification'), 'through'=>'modsRel' ,'on'=>'modsClear.is_visible=1'),        
            'mods'=>array(self::HAS_MANY, 'Modification', array('id_car_modification'=>'id_car_modification'), 'through'=>'modsRel'),
            'privod'=>array(self::HAS_MANY, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'through'=>'mods','on'=>'privod.id_car_characteristic = '.Modification::CHARACTERISTIC_PRIVOD_ID),
            'fuel'=>array(self::HAS_MANY, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'through'=>'mods','on'=>'fuel.id_car_characteristic = '.Modification::CHARACTERISTIC_FUEL_ID),
            'similar'=>array(self::HAS_MANY,'GenerationSimilar', 'id_car_generation'),
            'introtext'=>array(self::HAS_ONE,'GenerationCharacteristicValue','id_car_generation','on'=>'introtext.id_car_add_characteristic='.self::CHARACTERISTIC_INTROTEXT_ID),
            'description'=>array(self::HAS_ONE,'GenerationCharacteristicValue','id_car_generation','on'=>'description.id_car_add_characteristic='.self::CHARACTERISTIC_DESCRIPTION_ID),
            'review'=>array(self::HAS_ONE,'GenerationCharacteristicValue','id_car_generation','on'=>'review.id_car_add_characteristic='.self::CHARACTERISTIC_REVIEW_ID),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_car_generation' => 'Id Car Generation',
			'name' => 'Название',
			'id_car_model' => 'Модель',
			'year_begin' => 'Год начала выпуска',
			'year_end' => 'Год окончания выпуска',
			'is_visible' => 'Видимость',
            'id_car_type' => 'Тип авто',
			'mark' => 'Марка авто',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id_car_generation',$this->id_car_generation);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('t.id_car_model',$this->id_car_model);
		$criteria->compare('t.year_begin',$this->year_begin,true);
		$criteria->compare('t.year_end',$this->year_end,true);
		$criteria->compare('t.is_visible',$this->is_visible);
		$criteria->compare('t.id_car_type',$this->id_car_type);

        $criteria->with = array('mark'); 
        $criteria->compare('model.name', $this->model, true); 

        $criteria->compare('mark.id_car_mark', $this->mark); 
        $criteria->order = 'mark.name, model.name, t.name';
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Generation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    public function loadChars()
    {

        foreach($this->getBodies() as $body)
        {
            $img = GenerationCharacteristicValue::model()->find('id_car_body=:id_car_body AND id_car_add_characteristic=:char_id AND id_car_generation=:id_car_generation',array(':char_id'=>self::CHARACTERISTIC_IMAGES_ID, ':id_car_generation'=>$this->id_car_generation,':id_car_body'=>$body->id_car_body));
            if(!$img)
            {
                $img = new GenerationCharacteristicValue();
                $img->id_car_generation = $this->id_car_generation;
                $img->id_car_type = $this->id_car_type;
                $img->is_visible = 1;
                $img->id_car_add_characteristic  = self::CHARACTERISTIC_IMAGES_ID;  
                $img->id_car_body  = $body->id_car_body;
                $img->save();  
            }
            $imgheader = GenerationCharacteristicValue::model()->find('id_car_body=:id_car_body AND id_car_add_characteristic=:char_id AND id_car_generation=:id_car_generation',array(':char_id'=>self::CHARACTERISTIC_HEADER_IMAGES_ID, ':id_car_generation'=>$this->id_car_generation,':id_car_body'=>$body->id_car_body));
            if(!$imgheader)
            {
                $imgheader = new GenerationCharacteristicValue();
                $imgheader->id_car_generation = $this->id_car_generation;
                $imgheader->id_car_type = $this->id_car_type;
                $imgheader->is_visible = 1;
                $imgheader->id_car_add_characteristic  = self::CHARACTERISTIC_HEADER_IMAGES_ID;  
                $imgheader->id_car_body  = $body->id_car_body;
                $imgheader->save();  
            }
                        
            //$imgcolors = GenerationCharacteristicValue::model()->find('id_car_body=:id_car_body AND id_car_add_characteristic=:char_id AND id_car_generation=:id_car_generation',array(':char_id'=>self::CHARACTERISTIC_HEADER_IMAGES_ID, ':id_car_generation'=>$this->id_car_generation,':id_car_body'=>$body->id_car_body));

            $chars = GenerationCharacteristic::model()->findAll('id_parent = '.self::CHARACTERISTIC_COLORS_PARENT_ID);
            foreach($chars as $charid => $char)
            {
                $val = GenerationCharacteristicValue::model()->find('id_car_body=:id_car_body AND id_car_add_characteristic=:char_id AND id_car_generation=:id_car_generation',array(':char_id'=>$char->id_car_characteristic, ':id_car_generation'=>$this->id_car_generation,':id_car_body'=>$body->id_car_body));
                if(!$val)
                {
                    $val = new GenerationCharacteristicValue();
                    $val->id_car_generation = $this->id_car_generation;
                    $val->id_car_type = $this->id_car_type;
                    $val->is_visible = 1;
                    $val->id_car_add_characteristic  = $char->id_car_characteristic;
                    $val->id_car_body  = $body->id_car_body;
                    $val->save();
                }
            }              
        }
        
        $this->colors = GenerationCharacteristicValue::model()->with('colorsChar')->findAll('t.id_car_generation=:id_car_generation and colorsChar.id_car_characteristic=t.id_car_add_characteristic and t.id_car_body<>0', array(':id_car_generation'=>$this->id_car_generation));
        
        
        if(!$this->introtext)
        {
            $this->introtext = new GenerationCharacteristicValue();
            $this->introtext->id_car_generation = $this->id_car_generation;
            $this->introtext->id_car_type = $this->id_car_type;
            $this->introtext->is_visible = 1;
            $this->introtext->id_car_add_characteristic  = 9;  
            $this->introtext->save();
        }
        
        if(!$this->description)
        {
            $this->description = new GenerationCharacteristicValue();
            $this->description->id_car_generation = $this->id_car_generation;
            $this->description->id_car_type = $this->id_car_type;
            $this->description->is_visible = 1;
            $this->description->id_car_add_characteristic  = 10;  
            $this->description->save();
        }

        if(!$this->review)
        {
            $this->review = new GenerationCharacteristicValue();
            $this->review->id_car_generation = $this->id_car_generation;
            $this->review->id_car_type = $this->id_car_type;
            $this->review->is_visible = 1;
            $this->review->id_car_add_characteristic  = 19;
            $this->review->save();
        }
    }

    public function getColors()
    {
        $arr = array();

        $criteria = new CDbCriteria;
        $criteria->select = '*';
        $criteria->with = array('colorsChar');
        $criteria->condition = "t.id_car_generation=:id_car_generation and colorsChar.id_car_characteristic=t.id_car_add_characteristic and t.id_car_body<>0";
        $criteria->params = array(':id_car_generation'=>$this->id_car_generation);
        $criteria->group = 'colorsChar.id_car_characteristic';
        $criteria->together = false;

        $arr = GenerationCharacteristicValue::model()->with('colorsChar')->findAll($criteria);

        return $arr;
    }

    public function getBodies()
    {
        $arr = array();
        
        foreach($this->mods as $mod)
        {
            if(!in_array($mod->body,$arr))
            {
                $arr[]=$mod->body;
            }
        }
        
        return $arr;
    }
    
    public function getBodyMods()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id';
        $criteria->with = array('mods','model','mark');
        $criteria->condition = 'model.id_car_model=:id_car_model and t.id_car_generation=:id_car_gen';
        $criteria->params = array(':id_car_model'=>$this->id_car_model,':id_car_gen'=>$this->id_car_generation);
        $criteria->group = 'mods.id_car_body';
        $mods = GenerationModification::model()->findAll($criteria);
        
        $result = array();
        
        foreach($mods as $mod)
        {             
                $result[$mod->mods->id_car_body]=array('data-modid'=>$mod->id,'data-mod'=>Yii::app()->createUrl('/car/catalog/single',array('id'=>$mod->id,'mark'=>$mod->mark->alias,'model'=>$mod->model->alias)),'data-char'=>Yii::app()->createUrl('/car/catalog/character',array('id'=>$mod->id,'mark'=>$mod->mark->alias,'model'=>$mod->model->alias)),'data-compl'=>0);
        }
        //var_dump($result);exit();
        return $result;        

    }    
    public function getMinPrice($body_id=null)
    {        
        $criteria = new CDbCriteria;
        
        $criteria->select = 'min(minprice.value) as minPrice';
        $criteria->condition = 't.id_car_generation = :id_car_generation';
        $criteria->params = array(':id_car_generation'=>$this->id_car_generation);
        
        if($body_id)
        {
            $criteria->with = array(
                'mods' => array(
                    'with'=>array('mainBody','minprice'),
                    'condition'=>'mainBody.id_car_body=:id_car_body',
                    'params'=>array(':id_car_body'=>$body_id)
                ),
            );
        }
        else
        {
            $criteria->with = array(
                'mods' => array(
                    'with'=>array('minprice'),
                ),
            );
        }
        $mods = Generation::model()->find($criteria);
        return $mods->minPrice?:0;
    }
    
    public function getVolume()
    {
        $result = array();
        
        foreach($this->mods as $mod)
        {
            if($mod->volume)
            {
                $val =  round($mod->volume->value/1000,1);
            }
            else
            {
                $val = 0;
                //var_dump($mod);exit();
            }
            if(!in_array($val, $result))
            {
                array_push($result,$val);
            }
        } 
        
        return $result;       
    }
    
    public function getTransmission()
    {
        $result = array();
        
        foreach($this->mods as $mod)
        {
            if($mod->transmission)
            {
                $val =  $mod->transmission->value=='Механическая'?'mechanics':'auto';
                $valname =  $mod->transmission->value=='Механическая'?'механика':'автомат';
            }
            else
            {
                $val =  'auto';
                $valname =  'автомат';
            }
            if(!in_array($val, $result))
            {
                $result[$val] = $valname;
            }
        } 
        
        return $result;       
       
    }

    public function getPrivod()
    {
        $result = array();

        foreach($this->privod as $privod)
        {
            $result[$privod->value] = $privod->value;
        }

        return $result;

    }

    public function getFuel()
    {
        $result = array();

        foreach($this->fuel as $privod)
        {
            $result[$privod->value] = $privod->value;
        }

        return $result;

    }

    public function getPrevImage($body)
    {
        $char = GenerationCharacteristicValue::model()->find('id_car_body=:id_car_body AND id_car_generation=:id_car_generation and id_car_add_characteristic=:id_car_add_characteristic',
            array(
                ':id_car_body'=>$body,
                ':id_car_generation'=>$this->id_car_generation,
                ':id_car_add_characteristic'=>self::CHARACTERISTIC_IMAGES_ID    
            ));
        return $char;
    }
    public function getHeadImage($body)
    {
        $char = GenerationCharacteristicValue::model()->find('id_car_body=:id_car_body AND id_car_generation=:id_car_generation and id_car_add_characteristic=:id_car_add_characteristic',
            array(
                ':id_car_body'=>$body,
                ':id_car_generation'=>$this->id_car_generation,
                ':id_car_add_characteristic'=>self::CHARACTERISTIC_HEADER_IMAGES_ID    
            ));
        return $char;
    }
    
    public static function getAddChars()
    {
        $arr = array(''=>'---');
        $groups = AddCharacteristic::model()->findAll('id_parent is null');
        
        foreach($groups as $group)
        {
            $arr[$group->name] = array();
            $chars = AddCharacteristic::model()->findAll('id_parent=:parent',array(':parent'=>$group->id_car_characteristic));
            foreach($chars as $char)
            {
                
                //array_push($arr,array('id'=>$char->id_car_characteristic,'text'=>$char->name,'group'=>$group->name));
                $arr[$group->name][$char->id_car_characteristic]=$char->name;
            }
        }
        
        return $arr;
    }
    public function getAddCharacteristics($params)
    {
        $result = array();
        foreach($params as $param)    
        {
            $char = GenerationImage::model()->find('id_car_generation=:id_gen and id_car_add_characteristic=:id_char',array(':id_gen'=>$this->id_car_generation,':id_char'=>$param->id_car_characteristic));
            if($char)
            {
                if(!empty($char->image))
                {
                    array_push($result,array('char_id'=>$char->id_car_add_characteristic,'char_val'=>CHtml::image("/upload/car/generation/200x150_".$char->image)));
                }
                else
                {
                    array_push($result,array('char_id'=>$param->id_car_characteristic,'char_val'=>CHtml::image("/images/nophoto.jpg")));
                }
            }
            else
            {
                if($param->parent)
                {
                    array_push($result,array('char_id'=>$param->id_car_characteristic,'char_val'=>CHtml::image("/images/nophoto.jpg")));
                }
                else
                {
                    array_push($result,array('char_id'=>$param->id_car_characteristic,'char_val'=>false));
                }
            }
        }
        return $result;   
    }
}
