<?php

/**
 * This is the model class for table "car_seo_page".
 *
 * The followings are the available columns in table 'car_seo_page':
 * @property integer $id
 * @property integer $id_car_model
 * @property integer $id_car_privod
 * @property integer $id_car_fuel
 * @property integer $group_id
 * @property integer $group_block_sort
 * @property string $create_time
 * @property string $update_time
 * @property integer $user_id
 * @property integer $change_user_id
 * @property string $title
 * @property string $slug
 * @property string $body
 * @property string $description
 */
class SeoPage extends CActiveRecord
{
    const CHARACTERISTIC_TRANSMISSION_ID=8;
    const CHARACTERISTIC_VOLUME_ID=1;
    const CHARACTERISTIC_PRIVOD_ID=7;
    const CHARACTERISTIC_FUEL_ID=117;
    const CHARACTERISTIC_POWER_ID=150;
    const CHARACTERISTIC_SPEED_ID=5;
    const CHARACTERISTIC_RASHOD_ID=6;

    public $updateTime;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'car_seo_page';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_car_mark, create_time, update_time, group_id', 'required'),
			array('id_car_model, id_car_mark, id_car_modification, group_block_sort, user_id, change_user_id, group_id', 'numerical', 'integerOnly'=>true),
			array('slug', 'length', 'max'=>150),
			array('title, description, id_car_fuel, id_car_privod, id_car_transmission', 'length', 'max'=>250),
            array('body', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, slug, mark, model, modification, group_id, body, description, id_car_mark, id_car_model, id_car_body', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'model'=>array(self::BELONGS_TO, 'CarModel', 'id_car_model'),
            //'mark'=>array(self::BELONGS_TO, 'Mark', array('id_car_mark'=>'id_car_mark'),'through'=>'model'),
            'mark'=>array(self::BELONGS_TO, 'Mark', 'id_car_mark'),
            'characteristics'=>array(self::HAS_MANY,'Characteristic',''),
            'characteristicValues'=>array(self::HAS_MANY,'CharacteristicValue',array('id_car_modification'=>'id_car_modification'),'with'=>'characteristic','order'=>'characteristic.id_parent'),
            'volume'=>array(self::HAS_ONE, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'on'=>'volume.id_car_characteristic = '.Modification::CHARACTERISTIC_VOLUME_ID),
            'transmission'=>array(self::HAS_ONE, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'on'=>'transmission.id_car_characteristic = '.Modification::CHARACTERISTIC_TRANSMISSION_ID),
            'fuel'=>array(self::HAS_ONE, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'on'=>'fuel.id_car_characteristic = '.Modification::CHARACTERISTIC_FUEL_ID),
            'privod'=>array(self::HAS_ONE, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'on'=>'privod.id_car_characteristic = '.Modification::CHARACTERISTIC_PRIVOD_ID),
            'mods'=>array(self::HAS_ONE, 'Modification', array('id_car_modification'=>'id_car_modification')),
            'carBody'=>array(self::HAS_ONE, 'Body', array('id_car_body'=>'id_car_body')),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'id_car_model' => 'Модель',
            'modification' => 'Модификация',
            'id_car_modification' => 'Модификация',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'user_id' => 'User',
			'change_user_id' => 'Change User',
			'title' => 'Title',
			'slug' => 'Slug',
			'body' => 'CЕО Текст',
			'description' => 'Description',
            'mark' => 'Марка авто',
            'group_id' => 'Группа',
            'id_car_fuel' => 'Тип двигателя',
            'id_car_privod' => 'Привод',
            'id_car_transmission' => 'Коробка',
            'group_block_sort' => 'Первый блок на странице',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('slug',$this->slug,true);

        if ($this->group_id)
        {
            $groupNames = Yii::app()->cars->getGroups();
            $group_id = array_search($this->group_id, $groupNames);
            $criteria->compare('group_id',$group_id);
        }

        $criteria->with = array('mark','model', 'carBody');
        $criteria->compare('mark.id_car_mark', $this->mark);
        //$criteria->compare('modification.name',$this->modification);
        $criteria->addSearchCondition('model.name',$this->model,true,'AND');
        $criteria->addSearchCondition('carBody.name',$this->id_car_body,true,'AND');

        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SeoPage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function loadChars()
    {
        $chars = $this->characteristics;

        foreach($chars as $char)
        {
            $val = CharacteristicValue::model()->find('id_car_modification='.$this->id_car_modification.' and id_car_characteristic='.$char->id_car_characteristic);
            if(!$val)
            {
                $val = new CharacteristicValue();
                $val->id_car_modification = $this->id_car_modification;
                //$val->id_car_type = $this->id_car_type;
                $val->is_visible = 1;
                $val->id_car_characteristic  = $char->id_car_characteristic;
                $val->save();
            }
        }
    }

}
