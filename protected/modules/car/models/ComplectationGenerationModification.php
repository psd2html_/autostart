<?php

/**
 * This is the model class for table "car_complectation_generation_modification".
 *
 * The followings are the available columns in table 'car_complectation_generation_modification':
 * @property integer $id_car_complectation_generation_modification
 * @property integer $id_car_generation_modification
 * @property integer $id_car_complectation
 * @property integer $price_min
 */
class ComplectationGenerationModification extends CActiveRecord
{
    public $minPriceSelect;
    public $complName;
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'car_complectation_generation_modification';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_car_generation_modification, id_car_complectation, price_min', 'required'),
			array('id_car_generation_modification, id_car_complectation, price_min', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_car_complectation_generation_modification, id_car_generation_modification, id_car_complectation, price_min', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'genModRel'=>array(self::HAS_ONE, 'GenerationModification', array('id'=>'id_car_generation_modification')),
            'generation'=>array(self::HAS_ONE, 'Generation', array('id_car_generation'=>'id_car_generation'), 'through'=>'genModRel'),
            'mods'=>array(self::HAS_ONE, 'Modification', array('id_car_modification'=>'id_car_modification'), 'through'=>'genModRel'),
            'body'=>array(self::HAS_ONE, 'Body', array('id_car_body'=>'id_car_body'), 'through'=>'mods'),
            'complectation'=>array(self::HAS_ONE,'Complectation',array('id_car_complectation'=>'id_car_complectation')),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_car_complectation_generation_modification' => 'Id Car Complectation Generation Modification',
			'id_car_generation_modification' => 'Id Car Generation Modification',
			'id_car_complectation' => 'Id Car Complectation',
			'price_min' => 'Price Min',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_car_complectation_generation_modification',$this->id_car_complectation_generation_modification);
		$criteria->compare('id_car_generation_modification',$this->id_car_generation_modification);
		$criteria->compare('id_car_complectation',$this->id_car_complectation);
		$criteria->compare('price_min',$this->price_min);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ComplectationGenerationModification the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    public static function getMinPrice()
    {
        $model = new ComplectationGenerationModification;
        $criteria=new CDbCriteria;
        $criteria->select='min(price_min) AS price_min';
        $row = $model->find($criteria);
        if($row)
        {
            return $row['price_min'];
        }
    }
    public static function getMaxPrice()
    {
        $model = new ComplectationGenerationModification;
        $criteria=new CDbCriteria;
        $criteria->select='max(price_min) AS price_min';
        $row = $model->find($criteria);
        if($row)
        {
            return $row['price_min'];
        }
    }
}
