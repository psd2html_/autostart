<?php

/**
 * This is the model class for table "car_mark".
 *
 * The followings are the available columns in table 'car_mark':
 * @property integer $id_car_mark
 * @property string $name
 * @property string $alias
 * @property integer $is_visible
 * @property integer $id_car_type
 * @property string $name_rus
 * @property string $icon
 * @property string $image
 * @property string $title
 * @property string $description
 * @property string $body
 */
class Mark extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'car_mark';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, id_car_type, alias', 'required'),
			array('is_visible, is_fav, id_car_type', 'numerical', 'integerOnly'=>true),
			array('name, name_rus, image, icon, alias, description, title', 'length', 'max'=>255),
            array('body', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_car_mark, name, is_visible, is_fav, id_car_type, name_rus, image, icon,alias, description, title, body', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'type'=>array(self::BELONGS_TO, 'Type', 'id_car_type'),
            'models'=>array(self::HAS_MANY,'CarModel','id_car_mark','on'=>'models.is_visible=1','order'=>'models.name'),
            'allmodels'=>array(self::HAS_MANY,'CarModel','id_car_mark'),
            'mods'=>array(self::HAS_MANY,'Modification',array('id_car_model'=>'id_car_model'), 'through'=>'models', 'on'=>'mods.is_visible=1')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_car_mark' => 'ID',
            'name' => 'Название марки',
			'alias' => 'Alias',
            'is_visible' => 'Видимость',
			'is_fav' => 'Популярная',
			'id_car_type' => 'Тип авто',
			'name_rus' => 'Назв. рус.',
            'image' => 'Изображение',
            'icon' => 'Шильд',
            'title' => 'Seo title',
            'description' => 'Seo description',
            'body' => 'Seo текст',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_car_mark',$this->id_car_mark);
        $criteria->compare('name',$this->name,true);
		$criteria->compare('alias',$this->alias,true);
        $criteria->compare('is_fav',$this->is_fav);
		$criteria->compare('is_visible',$this->is_visible);
		$criteria->compare('id_car_type',$this->id_car_type);
		$criteria->compare('name_rus',$this->name_rus,true);

        $criteria->order = 'name';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mark the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /*public function beforeDelete()
    {
        set_time_limit(0);
        if(isset($this->image))
            $this->image->delete();
        
        foreach($this->models as $model)
            $model->delete();
        
        return parent::beforeDelete();
        

    }*/    
    
}
