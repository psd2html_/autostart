<?php

/**
 * This is the model class for table "car_characteristic".
 *
 * The followings are the available columns in table 'car_characteristic':
 * @property integer $id_car_characteristic
 * @property string $name
 * @property integer $id_parent
 * @property integer $id_car_type
 */
class Characteristic extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'car_characteristic';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_car_type', 'required'),
			array('id_parent, id_car_type', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_car_characteristic, name, id_parent, id_car_type, parent', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'parent'=>array(self::BELONGS_TO, 'CharacteristicGroup', 'id_parent'),
            'type'=>array(self::BELONGS_TO, 'Type', 'id_car_type'),
            'children'=>array(self::HAS_MANY,'Characteristic', 'id_parent'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_car_characteristic' => 'id',
			'name' => 'Название',
			'id_parent' => 'Родитель',
			'id_car_type' => 'Тип авто',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id_car_characteristic',$this->id_car_characteristic);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('t.id_parent',$this->id_parent);
		$criteria->compare('t.id_car_type',$this->id_car_type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Characteristic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
