<?php

/**
 * This is the model class for table "car_modification".
 *
 * The followings are the available columns in table 'car_modification':
 * @property integer $id_car_modification
 * @property integer $id_car_model
 * @property string $name
 * @property integer $is_visible
 * @property integer $id_car_body
 */
class Modification extends CActiveRecord
{  
    const CHARACTERISTIC_TRANSMISSION_ID=8;
    const CHARACTERISTIC_VOLUME_ID=1;
    const CHARACTERISTIC_PRIVOD_ID=7;
    const CHARACTERISTIC_FUEL_ID=117;
    const CHARACTERISTIC_POWER_ID=150;
    const CHARACTERISTIC_SPEED_ID=5;
    const CHARACTERISTIC_RASHOD_ID=6;
    
   	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'car_modification';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_car_model, name, id_car_type, id_car_body', 'required'),
			array('id_car_model, is_visible, id_car_type, id_car_body', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_car_modification, id_car_model, name, is_visible, id_car_type, id_car_body, mark, model', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'model'=>array(self::BELONGS_TO, 'CarModel', 'id_car_model'),
            'type'=>array(self::BELONGS_TO, 'Type', 'id_car_type'),
            'mark'=>array(self::BELONGS_TO, 'Mark', array('id_car_mark'=>'id_car_mark'),'through'=>'model'),
            'body'=>array(self::HAS_ONE,'Body', array('id_car_body'=>'id_car_body')),
            'groups'=>array(self::HAS_MANY,'CharacteristicGroup',''),
            'generationRel'=>array(self::HAS_MANY,'GenerationModification',array('id_car_modification'=>'id_car_modification')),
            'complectationRel'=>array(self::HAS_ONE, 'ComplectationGenerationModification', array('id'=>'id_car_generation_modification'), 'through'=>'generationRel'),
            'complectationsRel'=>array(self::HAS_MANY, 'ComplectationGenerationModification', array('id'=>'id_car_generation_modification'), 'through'=>'generationRel'),
            'complectation'=>array(self::HAS_MANY, 'Complectation', array('id_car_complectation'=>'id_car_complectation'), 'through'=>'complectationRel'),
            'generations'=>array(self::HAS_MANY,'Generation',array('id_car_generation'=>'id_car_generation'),'through'=>'generationRel'),
            //'generation'=>array(self::HAS_ONE,'Generation',array('id_car_generation'=>'id_car_generation'),'through'=>'generationRel'),
            'generation'=>array(self::HAS_MANY,'Generation',array('id_car_generation'=>'id_car_generation'),'through'=>'generationRel'),
            'characteristics'=>array(self::HAS_MANY,'Characteristic',''),
            'characteristicValues'=>array(self::HAS_MANY,'CharacteristicValue',array('id_car_modification'=>'id_car_modification'),'with'=>'characteristic','order'=>'characteristic.id_parent'),
            'volume'=>array(self::HAS_ONE, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'on'=>'volume.id_car_characteristic = '.Modification::CHARACTERISTIC_VOLUME_ID),
            'transmission'=>array(self::HAS_ONE, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'on'=>'transmission.id_car_characteristic = '.Modification::CHARACTERISTIC_TRANSMISSION_ID),
            'fuel'=>array(self::HAS_ONE, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'on'=>'fuel.id_car_characteristic = '.Modification::CHARACTERISTIC_FUEL_ID),
            'privod'=>array(self::HAS_ONE, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'on'=>'privod.id_car_characteristic = '.Modification::CHARACTERISTIC_PRIVOD_ID),
            /*'characteristic'=>array(self::HAS_MANY,'AddCharacteristic','','on'=>'characteristic.id_parent is not null and characteristic.id_car_characteristic not in ('.self::CHARACTERISTIC_MIN_PRICE.','.self::CHARACTERISTIC_MAX_PRICE.')'),
            'characteristicValues'=>array(self::HAS_MANY,'AddCharacteristicValue','id_car_modification','on'=>'characteristicValues.id_car_add_characteristic not in ('.self::CHARACTERISTIC_MIN_PRICE.','.self::CHARACTERISTIC_MAX_PRICE.')'),
            'defaultCharacteristicValues'=>array(self::HAS_MANY,'CharacteristicValue','id_car_modification'),
            'complectation'=>array(self::HAS_MANY,'Complectation','id_car_modification','on'=>'complectation.is_visible=1'),*/
   		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_car_modification' => 'ID',
            'id_car_model' => 'Модель автомобиля',
			'name' => 'Название модификции',
			'is_visible' => 'Видимость',
			'id_car_type' => 'Тип авто',
            'id_car_body' => 'Тип кузова',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $this->is_visible = 1;

		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('t.is_visible',$this->is_visible);
        $criteria->compare('t.id_car_type',$this->id_car_type);
		$criteria->compare('t.id_car_body',$this->id_car_body);

        $criteria->with = array('mark','model'); 
        $criteria->compare('mark.id_car_mark', $this->mark); 
        $criteria->addSearchCondition('model.name',$this->model,true,'AND');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Modification the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    public function inGeneration($idGen)
    {
        $gen = GenerationModification::model()->find('id_car_generation=:gen_id and id_car_modification=:mod_id',
            array(':gen_id'=>$idGen,':mod_id'=>$this->id_car_modification));
            
        if($gen)
            return true;
            
        return false;
    }
    
    public function loadChars()
    {
        $chars = $this->characteristics;
        
        foreach($chars as $char)
        {
            $val = CharacteristicValue::model()->find('id_car_modification='.$this->id_car_modification.' and id_car_characteristic='.$char->id_car_characteristic);
            if(!$val)
            {
                $val = new CharacteristicValue();
                $val->id_car_modification = $this->id_car_modification;
                $val->id_car_type = $this->id_car_type;
                $val->is_visible = 1;
                $val->id_car_characteristic  = $char->id_car_characteristic;
                $val->save();
            }
        }
        //$this->addchars = array();
        
        //$res = array();
        //$chars = CHtml::listData(AddCharacteristic::model()->findAll('id_parent is not null'),'id_car_characteristic','name');
        //echo $this->id_car_modification; exit();
        /*foreach($this->characteristic as $char)
        {
            $val = AddCharacteristicValue::model()->find('id_car_modification='.$this->id_car_modification.' and id_car_add_characteristic='.$char->id_car_characteristic);
            if(!$val)
            {
                $val = new AddCharacteristicValue();
                $val->id_car_modification = $this->id_car_modification;
                $val->id_car_type = $this->id_car_type;
                $val->is_visible = 1;
                $val->id_car_add_characteristic  = $char->id_car_characteristic;
                $val->save();
            }
            //array_push($this->characteristic, $val);
        }
        foreach($this->defaultCharacteristic as $char)
        {
            $val = CharacteristicValue::model()->find('id_car_modification='.$this->id_car_modification.' and id_car_characteristic='.$char->id_car_characteristic);

            if(!$val)
            {
                $val = new CharacteristicValue();
                $val->id_car_modification = $this->id_car_modification;
                $val->id_car_type = $this->id_car_type;
                $val->value = '';
                $val->is_visible = 1;
                $val->id_car_characteristic  = $char->id_car_characteristic;
                $val->save();
            }
            //array_push($this->characteristic, $val);
        }
        
        if(!$this->minprice)
        {
            $val = new AddCharacteristicValue();
            $val->id_car_modification = $this->id_car_modification;
            $val->id_car_type = $this->id_car_type;
            $val->is_visible = 1;
            $val->id_car_add_characteristic  = self::CHARACTERISTIC_MIN_PRICE;
            $val->save();
            $this->minprice=$val;
            $this->save();
        }
        if(!$this->maxprice)
        {
            $val = new AddCharacteristicValue();
            $val->id_car_modification = $this->id_car_modification;
            $val->id_car_type = $this->id_car_type;
            $val->is_visible = 1;
            $val->id_car_add_characteristic  = self::CHARACTERISTIC_MAX_PRICE;
            $val->save();
            $this->maxprice=$val;
            $this->save();
        }*/
    }
    
        
    public function getGenerationMods()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id_car_modification, generation.id_car_generation as generation';
        $criteria->with = array('generation','model');
        $criteria->condition = 'generation.id_car_generation<>:id_car_generation and model.id_car_model=:id_car_model';
        $criteria->params = array(':id_car_generation'=>$this->generation->id_car_generation,':id_car_model'=>$this->model->id_car_model);
        $criteria->group = 'generation.year_begin';
        $mods = Modification::model()->findAll($criteria);
        
        $result = array();
        
        foreach($mods as $mod)
        {             
            if(count($mod->generation->mods)>0)
            {
                $result[$mod->generation->id_car_generation]=array('data-modid'=>$mod->generation->mods[0]->id_car_modification,'data-mod'=>Yii::app()->createUrl('/car/catalog/single',array('id'=>$mod->id_car_modification)),'data-char'=>Yii::app()->createUrl('/car/catalog/character',array('id'=>$mod->id_car_modification)),'data-compl'=>0);
            }
        }
        //var_dump($result);exit();
        return $result;
    }
    public function getModificationMods()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id_car_modification';
        $criteria->with = array('generation');
        $criteria->condition = 'generation.id_car_generation=:id_car_generation';
        $criteria->params = array(':id_car_generation'=>$this->generation->id_car_generation);
        $mods = Modification::model()->findAll($criteria);
        
        $result = array();
        
        foreach($mods as $mod)
        {
            $result[$mod->id_car_modification]=array('data-modid'=>$mod->id_car_modification,'data-mod'=>Yii::app()->createUrl('/car/catalog/single',array('id'=>$mod->id_car_modification)),'data-char'=>Yii::app()->createUrl('/car/catalog/character',array('id'=>$mod->id_car_modification)),'data-compl'=>0);
        }
        //var_dump($result);exit();
        return $result;
    }

    public function getModificationByBodyMods($bodyId)
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id_car_modification, t.name';
        $criteria->with = array('generation','mainBody');
        $criteria->condition = 'generation.id_car_generation=:id_car_generation and mainBody.id_car_body=:id_car_body';
        $criteria->params = array(':id_car_generation'=>$this->generation->id_car_generation,':id_car_body'=>$bodyId);
        
        return GenerationModification::model()->findAll($criteria);
    }
    public function getBodyMods()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id_car_modification, mainBody.id_car_body as id_car_body';
        $criteria->with = array('generation','mainBody');
        $criteria->condition = 'generation.id_car_generation=:id_car_generation and mainBody.id_car_body<>:id_car_body';
        $criteria->params = array(':id_car_generation'=>$this->generation->id_car_generation,':id_car_body'=>$this->mainBody->id_car_body);
        $criteria->group = 'mainBody.id_car_body, generation.year_begin';
        $mods = Modification::model()->findAll($criteria);
        
        $result = array();
        
        foreach($mods as $mod)
        {
            $result[$mod->mainBody->id_car_body]=array('data-modid'=>$mod->id_car_modification, 'data-mod'=>Yii::app()->createUrl('/car/catalog/single/',array('id'=>$mod->id_car_modification)), 'data-char'=>Yii::app()->createUrl('/car/catalog/character/',array('id'=>$mod->id_car_modification)));
        }
        return $result;
    }
    
    public function getComplMods()
    {
        foreach($this->complectation as $compl)
        {
            $result[$compl->id_car_complectation]=array('data-modid'=>$this->id_car_modification, 'data-mod'=>Yii::app()->createUrl('/car/catalog/single/',array('id'=>$this->id_car_modification)), 'data-char'=>Yii::app()->createUrl('/car/catalog/character/',array('id'=>$this->id_car_modification, 'compl'=>$compl->id_car_complectation)));
        }
        return $result;
    } 
    
    public function getImage()
    {
        foreach($this->generation->prev_images as $image)
        {
            if($image->id_car_body==$this->mainBody->id_car_body)
            {
                return $image;
            }
        }
        return false;
    }
    
    public function getCharacteristics($params)
    {
        $result = array();
        foreach($params as $param)    
        {
            $char = CharacteristicValue::model()->find('id_car_modification=:id_mod and id_car_characteristic=:id_char',array(':id_mod'=>$this->id_car_modification,':id_char'=>$param->id_car_characteristic));
            if($char)
            {
                array_push($result,array('char_id'=>$char->id_car_characteristic,'char_val'=>$char->value." ".$char->unit));
            }
            else
            {
                if($param->parent)
                {
                    array_push($result,array('char_id'=>$param->id_car_characteristic,'char_val'=>'-'));
                }
                else
                {
                    array_push($result,array('char_id'=>$param->id_car_characteristic,'char_val'=>false));
                }
            }
        }
        return $result;
    }
    public function getAddCharacteristics($params)
    {
        $result = array();
        foreach($params as $param)    
        {
            $char = AddCharacteristicValue::model()->find('id_car_modification=:id_mod and id_car_add_characteristic=:id_char',array(':id_mod'=>$this->id_car_modification,':id_char'=>$param->id_car_characteristic));
            if($char)
            {
                if(!empty($char->image))
                {
                    array_push($result,array('char_id'=>$char->id_car_add_characteristic,'char_val'=>CHtml::image("/upload/car/chars/200x150_".$char->image)));
                }
                else
                {
                    array_push($result,array('char_id'=>$param->id_car_characteristic,'char_val'=>CHtml::image("/images/nophoto.jpg")));
                }
            }
            else
            {
                if($param->parent)
                {
                    array_push($result,array('char_id'=>$param->id_car_characteristic,'char_val'=>CHtml::image("/images/nophoto.jpg")));
                }
                else
                {
                    array_push($result,array('char_id'=>$param->id_car_characteristic,'char_val'=>false));
                }
            }
        }
        return $result;
    }
    
    /*public function beforeDelete()
    {
        echo "Modification deleted";
        if(isset($this->minprice))
            $this->minprice->delete();
        if(isset($this->maxprice))
            $this->maxprice->delete();
        
        foreach($this->characteristicValues as $model)
            $model->delete();
        
        foreach($this->defaultCharacteristicValues as $model)
            $model->delete();
        
        foreach($this->complectation as $model)
            $model->delete();
        
        return parent::beforeDelete();
        

    }*/       
}
