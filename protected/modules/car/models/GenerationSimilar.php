<?php

/**
 * This is the model class for table "car_generation_similar".
 *
 * The followings are the available columns in table 'car_generation_similar':
 * @property integer $id_car_similar
 * @property integer $id_car_generation
 * @property integer $id_car_generation_similar
 */
class GenerationSimilar extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'car_generation_similar';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_car_generation, id_car_generation_similar', 'required'),
			array('id_car_generation, id_car_generation_similar', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_car_similar, id_car_generation, id_car_generation_similar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'generation'=>array(self::BELONGS_TO,'Generation','id_car_generation'),
            'mods'=>array(self::BELONGS_TO,'GenerationModification','id_car_generation_similar'),
            'body'=>array(self::BELONGS_TO,'Body','id_car_body')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_car_similar' => 'Id Car Similar',
			'id_car_generation' => 'Модификация Авто',
			'id_car_generation_similar' => 'Id Car Generation Similar',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_car_similar',$this->id_car_similar);
		$criteria->compare('id_car_generation',$this->id_car_generation);
		$criteria->compare('id_car_generation_similar',$this->id_car_generation_similar);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GenerationSimilar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
