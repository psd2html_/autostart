<?php

/**
 * This is the model class for table "car_generation_modification".
 *
 * The followings are the available columns in table 'car_generation_modification':
 * @property integer $id
 * @property integer $id_car_generation
 * @property integer $id_car_modification
 */
class GenerationModification extends CActiveRecord
{
    public $minPriceSelect;
    public $bodySelect;
    public $modName;
    public $markName;
    public $modelName;
    public $markID;
    public $modelID;
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'car_generation_modification';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_car_generation, id_car_modification', 'required'),
			array('id_car_generation, id_car_modification', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_car_generation, id_car_modification', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'generation'=>array(self::HAS_ONE, 'Generation', array('id_car_generation'=>'id_car_generation')),
            'model'=>array(self::HAS_ONE, 'CarModel', array('id_car_model'=>'id_car_model'),'through'=>'generation'),
            'mark'=>array(self::HAS_ONE, 'Mark', array('id_car_mark'=>'id_car_mark'),'through'=>'model'),
            'mods'=>array(self::HAS_ONE, 'Modification', array('id_car_modification'=>'id_car_modification')),
            'modsAll'=>array(self::HAS_MANY, 'Modification', array('id_car_modification'=>'id_car_modification')),
            'body'=>array(self::HAS_ONE, 'Body', array('id_car_body'=>'id_car_body'), 'through'=>'mods'),
            'complectationRel'=>array(self::HAS_MANY, 'ComplectationGenerationModification', array('id_car_generation_modification'=>'id')),
            'complectation'=>array(self::HAS_MANY, 'Complectation', array('id_car_complectation'=>'id_car_complectation'), 'through'=>'complectationRel'),
            'privod'=>array(self::HAS_ONE, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'through'=>'mods','on'=>'privod.id_car_characteristic = '.Modification::CHARACTERISTIC_PRIVOD_ID),
            'transmission'=>array(self::HAS_ONE, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'through'=>'mods','on'=>'transmission.id_car_characteristic = '.Modification::CHARACTERISTIC_TRANSMISSION_ID),
            'fuel'=>array(self::HAS_ONE, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'through'=>'mods','on'=>'fuel.id_car_characteristic = '.Modification::CHARACTERISTIC_FUEL_ID),
            'rashod'=>array(self::HAS_ONE, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'through'=>'mods','on'=>'rashod.id_car_characteristic = '.Modification::CHARACTERISTIC_RASHOD_ID),
            'speed'=>array(self::HAS_ONE, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'through'=>'mods','on'=>'speed.id_car_characteristic = '.Modification::CHARACTERISTIC_SPEED_ID),
            'power'=>array(self::HAS_ONE, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'through'=>'mods','on'=>'power.id_car_characteristic = '.Modification::CHARACTERISTIC_POWER_ID),
            'volume'=>array(self::HAS_ONE, 'CharacteristicValue', array('id_car_modification'=>'id_car_modification'),'on'=>'volume.id_car_characteristic = '.Modification::CHARACTERISTIC_VOLUME_ID),
        );
	}


    
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_car_generation' => 'Id Car Generation',
			'id_car_modification' => 'Id Car Modification',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_car_generation',$this->id_car_generation);
		$criteria->compare('id_car_modification',$this->id_car_modification);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GenerationModification the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    public function getMinPrice()
    {
        /*$gen = GenerationModification::model()->find(array(
                'select'=>'min(complectationRel.price_min) as minPriceSelect',
                'with'=>array('complectationRel','model'),
                'condition'=>'model.id_car_model=:model',
                'params'=>array(
                    ':model'=>$this->generation->id_car_model,
                )
            )
        );
        return($gen->minPriceSelect);   */
        $this->minPriceSelect = 100000000;
        
        foreach($this->complectationRel as $rel)
        {
            if($this->minPriceSelect>$rel->price_min)
            {
                $this->minPriceSelect=$rel->price_min;    
            }
        }
        return $this->minPriceSelect;
    } 

    public function getModificationByBodyMods($bodyId)
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id, mods.name as modName';
        $criteria->with = array('generation','mods');
        $criteria->condition = 'generation.id_car_generation=:id_car_generation and mods.id_car_body=:id_car_body';
        $criteria->params = array(':id_car_generation'=>$this->generation->id_car_generation,':id_car_body'=>$bodyId);
        
        return GenerationModification::model()->findAll($criteria);
    }
    
    public function getComplRelWithName()
    {

        foreach($this->complectationRel as $rel)
        {
            $arr[$rel->id_car_complectation_generation_modification]=$rel->complectation->name;
        }
        return $arr;
    }
    public function getModificationMods()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id';
        $criteria->with = array('generation');
        $criteria->condition = 'generation.id_car_generation=:id_car_generation';
        $criteria->params = array(':id_car_generation'=>$this->generation->id_car_generation);
        $mods = GenerationModification::model()->findAll($criteria);
        
        $result = array();
        
        foreach($mods as $mod)
        {
            $result[$mod->id]=array('data-modid'=>$mod->id,'data-mod'=>Yii::app()->createUrl('/car/catalog/single',array('id'=>$mod->id)),'data-char'=>Yii::app()->createUrl('/car/catalog/character',array('id'=>$mod->id)),'data-compl'=>0);
        }
        //var_dump($result);exit();
        return $result;
    }    
    public function getComplMods()
    {
        foreach($this->complectationRel as $compl)
        {
            $result[$compl->id_car_complectation_generation_modification]=array('data-modid'=>$this->id, 'data-mod'=>Yii::app()->createUrl('/car/catalog/single/',array('id'=>$this->id,'mark'=>$this->mark->alias,'model'=>$this->model->alias)), 'data-char'=>Yii::app()->createUrl('/car/catalog/character/',array('id'=>$this->id, 'compl'=>$compl->id_car_complectation_generation_modification,'mark'=>$this->mark->alias,'model'=>$this->model->alias)));
        }
        return $result;
    } 
    public function getComplList()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id_car_complectation_generation_modification, complectation.name as complName';
        $criteria->with = array('complectation');
        $criteria->condition = 't.id_car_generation_modification=:id_car_generation_compl';
        $criteria->params = array(':id_car_generation_compl'=>$this->id);
        $mods = ComplectationGenerationModification::model()->findAll($criteria);

        return $mods;

    } 
    
    public function getImage()
    {
        foreach($this->generation->prev_images as $image)
        {
            if($image->id_car_body==$this->mods->id_car_body)
            {
                return $image;
            }
        }
        return false;
    }    
}
