<?php

/**
 * This is the model class for table "car_complectation".
 *
 * The followings are the available columns in table 'car_complectation':
 * @property integer $id_car_complectation
 * @property integer $id_car_modification
 * @property string $name
 * @property integer $is_visible
 * @property integer $price_min
 * @property integer $price_max
 */
class Complectation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'car_complectation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('is_visible', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_car_complectation, name, is_visible, mark, model', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            /*'modification'=>array(self::BELONGS_TO, 'Modification', 'id_car_modification'),*/
            'model'=>array(self::BELONGS_TO, 'CarModel', array('id_car_model'=>'id_car_model')),
            'mark'=>array(self::BELONGS_TO, 'Mark', array('id_car_mark'=>'id_car_mark'),'through'=>'model'),
            'genModRel'=>array(self::HAS_MANY, 'ComplectationGenerationModification', array('id_car_complectation'=>'id_car_complectation')),
            'genMod'=>array(self::HAS_MANY,'GenerationModification',array('id_car_complectation'=>'id'),'through'=>'genModRel'),
            'generation'=>array(self::HAS_MANY,'Generation',array('id_car_generation'=>'id_car_generation'), 'through'=>'genMod'),
            'mods'=>array(self::HAS_MANY,'Modification',array('id_car_modification'=>'id_car_modification'), 'through'=>'genMod'),
            'characteristic'=>array(self::HAS_MANY,'ComplectationCharacteristic','','on'=>'characteristic.id_parent'),
            'characteristicValues'=>array(self::HAS_MANY, 'ComplectationCharacteristicValue', 'id_car_complectation'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_car_complectation' => 'Id Car Complectation',
			'id_car_modification' => 'Id Car Modification',
			'name' => 'Название комплектации',
			'is_visible' => 'Видимость',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id_car_complectation',$this->id_car_complectation);
		//$criteria->compare('t.id_car_modification',$this->id_car_modification);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('t.is_visible',$this->is_visible);
        
        $criteria->with = array('mark','model'); 
        $criteria->compare('mark.id_car_mark', $this->mark); 
        $criteria->addSearchCondition('model.name',$this->model,true,'AND');

        $criteria->order = 'mark.name, model.name, t.name';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Complectation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    public function loadChars()
    {
        foreach($this->characteristic as $char)
        {
            $val = ComplectationCharacteristicValue::model()->find('id_car_complectation='.$this->id_car_complectation.' and id_car_add_characteristic='.$char->id_car_characteristic);
            if(!$val)
            {
                $val = new ComplectationCharacteristicValue();
                $val->id_car_complectation = $this->id_car_complectation;
                $val->id_car_type = 1;
                $val->is_visible = 1;
                $val->id_car_add_characteristic  = $char->id_car_characteristic;
                $val->save();
            }
            //array_push($this->characteristic, $val);
        }
    }

    public function inGenerationMods($idGenMod)
    {
        $gen = ComplectationGenerationModification::model()->find('id_car_complectation=:compl_id and id_car_generation_modification=:genModId',
            array(':compl_id'=>$this->id_car_complectation,':genModId'=>$idGenMod));
            
        if($gen)
            return true;
            
        return false;
    }
    
    public function getComplCharacteristics($params)
    {
        $result = array();
        foreach($params as $param)    
        {
            $char = ComplectationCharacteristicValue::model()->find('id_car_complectation=:id_compl and id_car_add_characteristic=:id_char',array(':id_compl'=>$this->id_car_complectation,':id_char'=>$param->id_car_characteristic));
            if($char)
            {
                if($param->parent)
                {
                    array_push($result,array('char_id'=>$char->id_car_add_characteristic,'char_val'=>($char->value?"Да":"-"),'char_parent'=>$param->parent->id_car_characteristic));
                }
                else
                {
                    array_push($result,array('char_id'=>$param->id_car_characteristic,'char_val'=>false,'char_parent'=>false));
                }
            }
            else
            {
                if($param->parent)
                {
                    array_push($result,array('char_id'=>$param->id_car_characteristic,'char_val'=>'-','char_parent'=>$param->parent->id_car_characteristic));
                }
                else
                {
                    array_push($result,array('char_id'=>$param->id_car_characteristic,'char_val'=>false,'char_parent'=>false));
                }
            }
        }
        return $result;
    }
    /*public function beforeDelete()
    {
     
        foreach($this->characteristicValues as $model)
            $model->delete();

        return parent::beforeDelete();
        

    } */      
}
