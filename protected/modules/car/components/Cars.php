<?php 
class Cars extends CComponent
{
    public $importLog = '/var/www/html/autostart.sitemaket.ru/public_html/autostart/protected/massImportLog.txt';

    public $cell_mark=0;
    public $cell_model=1;
    public $cell_gen=2;
    public $cell_year_begin=3;
    public $cell_year_end=4;
    public $cell_body=5;
    public $cell_modelId=6;
    public $cell_mod=7;
    public $cell_img=7;
    public $cell_view=8;
    public $cell_complName=8;
    public $cell_complId=9;
    public $cell_complPrice=10;
    public $cell_complCharName=11;
    public $cell_complModName=12;
    public $cell_complModVal=13;
    
    public $rashod_name = 'Расход топлива, л город / трасса / смешанный';
    public $rashod_new_name = 'Расход топлива, л';
    public $power_name = 'Максимальная мощность,';
    public $power_new_name = 'Максимальная мощность, л.с.';

	public function init()
	{
	}

    public function translit($st)
    {
        $rus=array('А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я','а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',' ');
        
        $lat=array('a','b','v','g','d','e','e','gh','z','i','y','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sch','y','y','y','e','yu','ya','a','b','v','g','d','e','e','gh','z','i','y','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sch','y','y','y','e','yu','ya',' ');

        $st = str_replace($rus, $lat, $st);
        $st = preg_replace('~[^-a-zA-Z0-9_]+~u', '', $st);
        $st = strtolower($st);
        
        return $st;
    }
    public function getPrivods()
    {
        /*$crit = new CDbCriteria;
        $crit->distinct = true;
        $crit->select = "value";
        $crit->condition = "id_car_characteristic = ".Modification::CHARACTERISTIC_PRIVOD_ID;
        $crit->order = "value";
        $models = CharacteristicValue::model()->findAll($crit);*/
        $arr = array(
            'Передний'=>'Передний',
            'Задний'=>'Задний',
            'Полный'=>'Полный',
        );
        return $arr;
    }
    public function getTransmissions()
    {
        /*$crit = new CDbCriteria;
        $crit->distinct = true;
        $crit->select = "value";
        $crit->order = "value";
        $crit->condition = "id_car_characteristic = ".Modification::CHARACTERISTIC_TRANSMISSION_ID;
        $models = CharacteristicValue::model()->findAll($crit);*/
        
        $arr = array(
            'Механическая'=>'Механическая',
            'Автоматическая'=>'Автоматическая',
        );
        return $arr;
    }
    public function getFuelTypes()
    {
        /*$crit = new CDbCriteria;
        $crit->distinct = true;
        $crit->select = "value";
        $crit->order = "value";
        $crit->condition = "id_car_characteristic = ".Modification::CHARACTERISTIC_FUEL_ID;
        $models = CharacteristicValue::model()->findAll($crit);
        return CHtml::listData($models,'value','value');*/
        
        $arr = array(
            'Бензин'=>'Бензин',
            'Гибрид'=>'Гибрид',
            'Дизель'=>'Дизель',
            'Электро'=>'Электро',
        );
        return $arr;
        
    }

    public function getGroups()
    {
        $arr = array(
            '1'=>'A',
            '2'=>'B',
            '3'=>'C',
            '4'=>'D',
            '5'=>'E'
        );

        return $arr;
    }

    public function getGroupBlockSort()
    {
        $arr = array(
            '1'=>'Комплектации и цены',
            //'2'=>'Фото',
            '3'=>'Обзоры',
            '4'=>'Отзывы',
            //'5'=>'Похожие авто'
        );

        return $arr;
    }
        
    public function getUnits()
    {
        $crit = new CDbCriteria;
        $crit->distinct = true;
        $crit->select = "unit";
        $models = CharacteristicValue::model()->findAll($crit);
        return CHtml::listData($models,'unit','unit');
    }
    
    public function importModels($path)
    {
        $this->importLog =  Yii::getPathOfAlias('application').'/massImportLog.txt';
        $fileHandler=fopen($path,'r');
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Import start\r\n",FILE_APPEND);
        
        setlocale(LC_ALL, 'ru_RU.cp1251');
        if($fileHandler)
        {
            $line=fgetcsv($fileHandler,1000,';'); //пропускаем заголовок
            foreach($line as &$a) {
                $a = iconv ('windows-1251', 'utf-8', $a);
            }

            $complId = '';
            
            $car = new stdClass();
            while($line=fgetcsv($fileHandler,1000,';')){
                foreach($line as &$a) {
                    $a = str_replace( chr(194).chr(160),' ', iconv ('windows-1251', 'utf-8', trim($a)));
                    $a = str_replace('?','', $a);
                }
                if($complId != $line[$this->cell_complId])
                {
                    if($complId!='')
                    {
                        $this->saveComplectation($car);
                        file_put_contents($this->importLog,date('d.m.Y H:i:s').": New car saved (".$car->mark." ".$car->model.", ".$car->body.", ".$car->year_begin."-".(empty($car->year_end)?"...":$car->year_end).", ".$car->mod.", ".$car->compl.", ".$car->complPrice.")\r\n",FILE_APPEND);
                    }
                    $complId = $line[$this->cell_complId];
                    $car = new stdClass();
                    $car->mark = $line[$this->cell_mark];
                    $car->gen = $line[$this->cell_gen];
                    $car->model = $line[$this->cell_model];
                    $car->modelId = $line[$this->cell_modelId];
                    $car->year_begin = $line[$this->cell_year_begin];
                    $car->year_end = $line[$this->cell_year_end]=='н. в.'?'':$line[$this->cell_year_end];
                    $car->body = $line[$this->cell_body];
                    $car->mod = $line[$this->cell_mod];
                    $car->compl = $line[$this->cell_complName];
                    $car->complId = $line[$this->cell_complId];
                    $car->complPrice = $line[$this->cell_complPrice];
                    $car->complChars = array();
                    $car->modChars = array();
                }     
                if(!empty($line[$this->cell_complCharName]))
                {
                    array_push($car->complChars, $line[$this->cell_complCharName]);
                }           
                else if(!empty($line[$this->cell_complModName]))
                {
                    if($line[$this->cell_complModVal]=='автомат' || $line[$this->cell_complModVal]=='робот' || $line[$this->cell_complModVal]=='вариатор' )
                    {
                        $line[$this->cell_complModVal] = 'Автоматическая';    
                    }
                    if($line[$this->cell_complModVal]=='механика')
                    {
                        $line[$this->cell_complModVal] = 'Механическая';    
                    }
                    if($line[$this->cell_complModVal]=='передний')
                    {
                        $line[$this->cell_complModVal] = 'Передний';    
                    }
                    if($line[$this->cell_complModVal]=='задний')
                    {
                        $line[$this->cell_complModVal] = 'Задний';    
                    }
                    if($line[$this->cell_complModVal]=='полный')
                    {
                        $line[$this->cell_complModVal] = 'Полный';    
                    }
                    if($line[$this->cell_complModVal]=='бензин')
                    {
                        $line[$this->cell_complModVal] = 'Бензин';    
                    }
                    if($line[$this->cell_complModVal]=='гибрид')
                    {
                        $line[$this->cell_complModVal] = 'Гибрид';    
                    }
                    if($line[$this->cell_complModVal]=='дизель')
                    {
                        $line[$this->cell_complModVal] = 'Дизель';    
                    }
                    if($line[$this->cell_complModVal]=='электро')
                    {
                        $line[$this->cell_complModVal] = 'Электро';    
                    }
                    if($line[$this->cell_complModName]==$this->rashod_name)
                    {
                        $mv = explode('/',$line[$this->cell_complModVal]);
                        if(isset($mv[1]))
                        {
                            $rashod = trim($mv[1]);
                            array_push($car->modChars,array('name'=>$line[$this->cell_complModName],'val'=>$line[$this->cell_complModVal]));
                            array_push($car->modChars,array('name'=>$this->rashod_new_name,'val'=>$rashod));
                        }
                        else
                        {
                            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Rashod crash!!! (".var_export(explode('/',$line[$this->cell_complModVal]),true).")\r\n",FILE_APPEND);
                        }
                    }
                    else if($line[$this->cell_complModName]==$this->power_name)
                    {
                        $cm = explode('/',$line[$this->cell_complModVal]);
                        $power = trim($cm[0]);
                        array_push($car->modChars,array('name'=>$line[$this->cell_complModName],'val'=>$line[$this->cell_complModVal]));
                        array_push($car->modChars,array('name'=>$this->power_new_name,'val'=>$power));
                    }
                    else
                    {
                        array_push($car->modChars,array('name'=>$line[$this->cell_complModName],'val'=>$line[$this->cell_complModVal]));
                    }
                }
            }
            $this->saveComplectation($car);
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": New car saved (".$car->mark." ".$car->model.", ".$car->body.", ".$car->year_begin."-".(empty($car->year_end)?"...":$car->year_end).", ".$car->mod.", ".$car->compl.", ".$car->complPrice.")\r\n",FILE_APPEND);
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Import finished\r\n",FILE_APPEND);
        }        

    }
    
    public function updatePrices($path)
    {
        $this->importLog =  Yii::getPathOfAlias('application').'/massImportLog.txt';
        $fileHandler=fopen($path,'r');
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Update start\r\n",FILE_APPEND);
        
        if($fileHandler)
        {
            $line=fgetcsv($fileHandler,1000,';'); //пропускаем заголовок
            
            foreach($line as &$a) {
                $a = iconv ('windows-1251', 'utf-8', $a);
            }

            $car = new stdClass();
            while($line=fgetcsv($fileHandler,1000,';'))
            {
                foreach($line as &$a) {
                    $a = str_replace( chr(194).chr(160),' ', iconv ('windows-1251', 'utf-8', trim($a)));
                    $a = str_replace('?','', $a);
                }
                $car = new stdClass();

                $car->complId = $line[$this->cell_complId];
                $car->complPrice = $line[$this->cell_complPrice];
                
                $compl = ComplectationGenerationModification::model()->findByPk($car->complId);
                
                if(!$compl)
                {
                    file_put_contents($this->importLog,date('d.m.Y H:i:s').": Комплектация не найдена (".$car->complId.")\r\n",FILE_APPEND);
                }
                else
                {
                    file_put_contents($this->importLog,date('d.m.Y H:i:s').": Комплектация найдена (".$car->complId."). Цена ".$compl->price_min." => ".$car->complPrice."\r\n",FILE_APPEND);

                    $compl->price_min = $car->complPrice;
                    $compl->save();
                }
            }
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Update finished\r\n",FILE_APPEND);
        }        

    }
    
    public function importImagesModels($path,$genPath)
    {
        $this->importLog =  Yii::getPathOfAlias('application').'/massImportLog.txt';

        $fileHandler=fopen($path,'r');
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Import start\r\n",FILE_APPEND);
            setlocale(LC_ALL, 'ru_RU.cp1251');
        
        if($fileHandler)
        {
            $line=fgetcsv($fileHandler,1000,';'); //пропускаем заголовок
           
            foreach($line as &$a) {
                $a = iconv ('windows-1251', 'utf-8', $a);
            }

            $complId = '';
            
            
            
            $car = new stdClass();
            while($line=fgetcsv($fileHandler,1000,';'))
            {
                foreach($line as &$a) {
                    $a = str_replace( chr(194).chr(160),' ', iconv ('windows-1251', 'utf-8', trim($a)));
                    $a = str_replace('?','', $a);
                }
                $car = new stdClass();
                $car->mark = $line[$this->cell_mark];
                $car->model = $line[$this->cell_model];
                $car->modelId = $line[$this->cell_modelId];
                $car->year_begin = $line[$this->cell_year_begin];
                $car->gen = $line[$this->cell_gen];
                
                $gen_arr = explode('/',$line[$this->cell_gen]);
                
                $car->year_end = ($line[$this->cell_year_end]=='н. в.')?'':$line[$this->cell_year_end];

                if(count($gen_arr)>1)
                {
                    $car->year_end = 'н.в. ('.trim($gen_arr[1]).')';
                }
                
                $car->body = $line[$this->cell_body];
                $car->img = $line[$this->cell_img];
                $car->view = $line[$this->cell_view];
                    
                $this->saveImage($car,$genPath);
            }     
                
        }


    }
    
    public function saveImage($compl, $genPath)
    {
        //var_dump($compl);  

        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Марка - ".$compl->mark."\r\n",FILE_APPEND);
        
        $mark = Mark::model()->find('name=:name',array('name'=>$compl->mark));
        if($mark)
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Марка найдена (".$mark->id_car_mark.")\r\n",FILE_APPEND);
        }  
        else
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Марка не найдена.\r\n",FILE_APPEND);
            return false;
        }

        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Модель - ".$compl->model."\r\n",FILE_APPEND);
        $model = CarModel::model()->find('name=:name and id_car_mark=:mark_id',array('name'=>$compl->model,':mark_id'=>$mark->id_car_mark));
        
        if($model)
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Модель найдена (".$model->id_car_model.")\r\n",FILE_APPEND);
        }  
        else
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Модель не найдена.\r\n",FILE_APPEND);
            return false;
        }
        
        $gen_arr = explode('/',$compl->gen);
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Поколение - ".$compl->year_begin."-".$compl->year_end."\r\n",FILE_APPEND);
        $generation = Generation::model()->find('name=:name and id_car_model=:id_car_model and year_begin=:year_begin and year_end=:year_end',array(
            ':name'=>$mark->name." ".$model->name.(count($gen_arr)>1?(' ('.trim($gen_arr[1]).')'):''),    
            ':id_car_model'=>$model->id_car_model,    
            ':year_begin'=>$compl->year_begin,    
            ':year_end'=>$compl->year_end,    
        ));
        
        if($generation)
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Поколение найдено (".$generation->id_car_generation.")\r\n",FILE_APPEND);
        }
        else
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Поколение не найдено.\r\n",FILE_APPEND);
            return false;
        }
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Кузов - ".$compl->body."\r\n",FILE_APPEND);
        $body = Body::model()->find('name=:name',array(':name'=>$compl->body));
        if(!$body)
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Кузов не найден.\r\n",FILE_APPEND);
            return false;
        }
        else
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Кузов найден (".$body->id_car_body.")\r\n",FILE_APPEND);
        }        
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Ищем характеристику\r\n",FILE_APPEND);
        
        $character = AddCharacteristic::model()->find('name=:name',array(':name'=>$compl->view));
        if(!$character)
        {
            $character = new AddCharacteristic;
            $character->name = $compl->view;
            $character->id_car_type = 1;
            $character->id_parent = 1;
            $character->save();
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Добавили новую хар-ку - ".$character->name."\r\n",FILE_APPEND);
        }
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Ищем значение характеристики\r\n",FILE_APPEND);

        /*$charVal = GenerationCharacteristicValue::model()->findAll('id_car_add_characteristic=:char_id and id_car_generation=:gen_id and id_car_body=:id_car_body',array(':char_id'=>$character->id_car_characteristic,':gen_id'=>$generation->id_car_generation,':id_car_body'=>$body->id_car_body));
        
        if(!$charVal)
        {
            $charVal = new GenerationCharacteristicValue;
            $charVal->id_car_add_characteristic = $character->id_car_characteristic;
            $charVal->id_car_generation = $generation->id_car_generation;
            $charVal->id_car_body = $body->id_car_body;
            $charVal->id_car_type = 1;
            $charVal->is_visible = 1;
            $charVal->save();
                
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Не нашли хар-ки. Добавили новую. (".$charVal->id_car_characteristic_value.")\r\n",FILE_APPEND);
        }*/
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Забираем картинку\r\n",FILE_APPEND);
        $newname = uniqid().'.jpg';    
        $path = $genPath;
        

        $getImg = @file_get_contents($compl->img);
        
        if(!$getImg){
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": 404 - ".$compl->img." . Пропускаем.\r\n",FILE_APPEND);
            return false;
        }

        file_put_contents($path.$newname,$getImg);
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Сохранили как ".$newname."\r\n",FILE_APPEND);
       
        $genImg = new GenerationImage;
        $genImg->image = $newname;
        $genImg->id_car_body = $body->id_car_body;
        $genImg->id_car_generation = $generation->id_car_generation;
        $genImg->id_car_add_characteristic = $character->id_car_characteristic;
        $genImg->save();
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Добавили в галерею\r\n",FILE_APPEND);
        
        try
        {
            $thumb=@Yii::app()->phpThumb->create($path.$newname);
            if($thumb)
            {
                if(!@$thumb->resize(350,260))
                {
                    file_put_contents($this->importLog,date('d.m.Y H:i:s').": Ошибка - не смогли открыть картинку. Продолжаем\r\n",FILE_APPEND);
                    return false;
                }
                if(!@$thumb->save($path."350x260_".$newname))
                {
                    file_put_contents($this->importLog,date('d.m.Y H:i:s').": Ошибка - не смогли открыть картинку. Продолжаем\r\n",FILE_APPEND);
                    return false;
                }
                if(!@$thumb->resize(200,150))
                {
                    file_put_contents($this->importLog,date('d.m.Y H:i:s').": Ошибка - не смогли открыть картинку. Продолжаем\r\n",FILE_APPEND);
                    return false;
                }
                
                if(!@$thumb->save($path."200x150_".$newname))
                {
                    file_put_contents($this->importLog,date('d.m.Y H:i:s').": Ошибка - не смогли открыть картинку. Продолжаем\r\n",FILE_APPEND);
                    return false;
                }
                
                if(!@$thumb->resize(100,100))
                {
                    file_put_contents($this->importLog,date('d.m.Y H:i:s').": Ошибка - не смогли открыть картинку. Продолжаем\r\n",FILE_APPEND);
                    return false;
                }
                
                if(!@$thumb->save($path."100x100_".$newname))
                {
                    file_put_contents($this->importLog,date('d.m.Y H:i:s').": Ошибка - не смогли открыть картинку. Продолжаем\r\n",FILE_APPEND);
                    return false;
                }
                
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Добавили превью\r\n",FILE_APPEND);
            }
            else
            {
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Ошибка - не смогли открыть картинку. Продолжаем\r\n",FILE_APPEND);
                return false;
            }
        }
        catch(Exception $ex)
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Ошибка - ".$ex->getMessage().". Продолжаем\r\n",FILE_APPEND);
            return false;
        }        

        
        if($compl->view == '3/4 спереди')
        {
            $generation->loadChars();
            $headImg = $generation->getHeadImage($body->id_car_body);
            if($headImg)
            {
                $headImg->image = $newname;
                $headImg->save();
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Сохранили как картинку в шапку\r\n",FILE_APPEND);
            }
            $prevImg = $generation->getPrevImage($body->id_car_body);
            if($prevImg)
            {
                $prevImg->image = $newname;
                $prevImg->save();
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Сохранили как картинку для тумбы\r\n",FILE_APPEND);
            }
        }
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Закончили с картинкой\r\n",FILE_APPEND);
    }
    public function saveComplectation($compl)
    {
        //var_dump($compl);  
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Марка - ".$compl->mark."\r\n",FILE_APPEND);
        
        $mark = Mark::model()->find('name=:name',array('name'=>$compl->mark));
        if($mark)
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Марка найдена (".$mark->id_car_mark.") - обновляем видимость\r\n",FILE_APPEND);
            $mark->is_visible=1;
            $mark->save();
        }  
        else
        {
            $mark = new Mark;
            $mark->is_visible=1;
            $mark->name=$compl->mark;
            $mark->alias=$this->translit($compl->mark);
            $mark->id_car_type=1;
            $mark->name_rus=$compl->mark;
            $mark->is_fav=0;
            $mark->save();
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Марка не найдена. Сделали новую марку (".$mark->id_car_mark.")\r\n",FILE_APPEND);
        }

        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Модель - ".$compl->model."\r\n",FILE_APPEND);
        $model = CarModel::model()->find('name=:name and id_car_mark=:mark_id',array('name'=>$compl->model,':mark_id'=>$mark->id_car_mark));
        
        if($model)
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Модель найдена (".$model->id_car_model.") - обновляем видимость\r\n",FILE_APPEND);
            $model->is_visible=1;
            $model->save();
        }  
        else
        {
            $model = new CarModel;
            $model->id_car_mark = $mark->id_car_mark;
            $model->name = $compl->model;
            $model->alias = $this->translit($compl->model);
            $model->is_visible = 1;
            $model->name_rus = $compl->model;
            $model->id_car_type=1;
            $model->save();
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Модель не найдена. Сделали новую модель (".$model->id_car_model.")\r\n",FILE_APPEND);
        }
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Поколение - ".$compl->year_begin."-".$compl->year_end."\r\n",FILE_APPEND);
        
        $gen_arr = explode('/',$compl->gen);
        if(count($gen_arr)>1)
        {
            $compl->year_end = 'н.в. ('.trim($gen_arr[1]).')';    
        }
        
        $generation = Generation::model()->find('name=:name and id_car_model=:id_car_model and year_begin=:year_begin and year_end=:year_end',array(
            ':name'=>$mark->name." ".$model->name.(count($gen_arr)>1?(' ('.trim($gen_arr[1]).')'):''),    
            ':id_car_model'=>$model->id_car_model,    
            ':year_begin'=>$compl->year_begin,    
            ':year_end'=>$compl->year_end,    
        ));
        
        if($generation)
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Поколение найдено (".$generation->id_car_generation.") - обновляем видимость\r\n",FILE_APPEND);
            $generation->is_visible=1;
            $generation->save();
        }
        else
        {
            $generation = new Generation;
            $generation->name = $mark->name." ".$model->name.((count($gen_arr)>1)?(' ('.trim($gen_arr[1]).')'):'');
            $generation->id_car_model = $model->id_car_model;
            $generation->year_begin = $compl->year_begin;
            $generation->year_end = $compl->year_end;
            $generation->is_visible = 1;
            $generation->id_car_type=1;
            $generation->save();
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Поколение не найдено. Сделали новое поколение (".$generation->id_car_generation.")\r\n",FILE_APPEND);
        }
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Кузов - ".$compl->body."\r\n",FILE_APPEND);
        $body = Body::model()->find('name=:name',array(':name'=>$compl->body));
        if(!$body)
        {
            $body = new Body;
            $body->name = $compl->body;
            $body->save();
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Кузов не найден. Сделали новый кузов (".$body->id_car_body.")\r\n",FILE_APPEND);
        }
        else
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Кузов найден (".$body->id_car_body.")\r\n",FILE_APPEND);

        }        
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Проверяем все характеристики модификаций\r\n",FILE_APPEND);
        
        foreach($compl->modChars as $char)
        {
            $character = Characteristic::model()->find('name=:name',array(':name'=>$char['name']));
            if(!$character)
            {
                $character = new Characteristic;
                $character->name = $char['name'];
                $character->id_parent = 1;
                $character->id_car_type = 1;
                $character->save();
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Добавили новую хар-ку - ".$character->name."\r\n",FILE_APPEND);
            }
        }
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Проверяем все характеристики комплектаций\r\n",FILE_APPEND);
        foreach($compl->complChars as $char)
        {
            $complCharacter = ComplectationCharacteristic::model()->find('name=:name',array(':name'=>$char));
            if(!$complCharacter)
            {
                $complCharacter = new ComplectationCharacteristic;
                $complCharacter->name = $char;
                $complCharacter->id_parent = 1;
                $complCharacter->id_car_type = 1;
                $complCharacter->save();
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Добавили новую хар-ку - ".$complCharacter->name."\r\n",FILE_APPEND);
            }
        }
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Модификация - ".$compl->mod."\r\n",FILE_APPEND);
        $mod = Modification::model()->findAll('name=:name and id_car_model=:id_car_model and id_car_body=:id_car_body',array(':name'=>$compl->mod,':id_car_model'=>$model->id_car_model,':id_car_body'=>$body->id_car_body));
        
        if($mod)
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Нашли похожие модификации - (".var_export(CHtml::listData($mod,'id_car_modification','name'),true).")\r\n",FILE_APPEND);
            $modExists = false;
            foreach($mod as $m)
            {
                if($this->compareModChars($m, $compl->modChars))
                {
                    $modExists = true;
                    $mod = $m;
                    file_put_contents($this->importLog,date('d.m.Y H:i:s').": Нашли совпадающую модификацию (".$mod->id_car_modification.")\r\n",FILE_APPEND);
                    break;
                }
            }
            
            if(!$modExists)
            {
                $mod = new Modification;
                $mod->name = $compl->mod;
                $mod->id_car_model = $model->id_car_model;
                $mod->id_car_type = 1;
                $mod->id_car_body = $body->id_car_body;
                $mod->save();
                
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Не нашли совпадающей модификации - добавляем новую (".$mod->id_car_modification.")\r\n",FILE_APPEND);
                $this->addModChars($mod, $compl->modChars);
            }
        }
        else
        {
            $mod = new Modification;
            $mod->name = $compl->mod;
            $mod->id_car_model = $model->id_car_model;
            $mod->id_car_type = 1;
            $mod->id_car_body = $body->id_car_body;
            $mod->save();
            
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Не нашли совпадающей модификации - добавляем новую (".$mod->id_car_modification.")\r\n",FILE_APPEND);
            
            $this->addModChars($mod, $compl->modChars);
        }
        
        $genModRel = GenerationModification::model()->find('id_car_generation=:id_gen and id_car_modification=:id_mod',array(':id_gen'=>$generation->id_car_generation,':id_mod'=>$mod->id_car_modification));
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Ищем связку модификации и поколения (G: ".$generation->id_car_generation." - M: ".$mod->id_car_modification.")\r\n",FILE_APPEND);

        if(!$genModRel)
        {
            $genModRel = new GenerationModification;
            $genModRel->id_car_generation = $generation->id_car_generation;
            $genModRel->id_car_modification = $mod->id_car_modification;
            $genModRel->save();
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Связка не нашлась. Добавляем новую (".$genModRel->id.")\r\n",FILE_APPEND);
        }
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Ищем связку комплектации (".$compl->complId.")\r\n",FILE_APPEND);
        $complectationRel = ComplectationGenerationModification::model()->findByPk($compl->complId);
        if(!$complectationRel)
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Связка не нашлась. Тогда сперва найдем нужную комплектацию.\r\n",FILE_APPEND);
            
            $complectation = Complectation::model()->findAll('name=:name and id_car_model=:id_model',array(':name'=>$compl->compl, ':id_model'=>$model->id_car_model));
                        
            if($complectation)
            {
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Нашли похожие комплектации (".var_export(CHtml::listData($complectation,'id_car_complectation','name'),true).").\r\n",FILE_APPEND);
                
                $complExists = false;
                foreach($complectation as $c)
                {
                    if($this->compareComplChars($c, $compl->complChars))
                    {
                        $complExists = true;
                        $complectation = $c;
                        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Нашли совпадающую комплектацию (".$complectation->id_car_complectation.")\r\n",FILE_APPEND);
                        break;
                    }
                }
                
                if(!$complExists)
                {
                    $complectation = new Complectation;
                    
                    $complectation->name = $compl->compl;
                    $complectation->id_car_model = $model->id_car_model;
                    $complectation->is_visible = 1;
                    $complectation->save();
                    
                    file_put_contents($this->importLog,date('d.m.Y H:i:s').": Не нашли совпадающей комплектации - добавляем новую (".$complectation->id_car_complectation.")\r\n",FILE_APPEND);
                    
                    $this->addComplChars($complectation, $compl->complChars);
                }
              
            }
            else
            {
                $complectation = new Complectation;
                
                $complectation->name = $compl->compl;
                $complectation->id_car_model = $model->id_car_model;
                $complectation->is_visible = 1;
                $complectation->save();
                
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Не нашли совпадающей комплектации - добавляем новую (".$complectation->id_car_complectation.")\r\n",FILE_APPEND);
                $this->addComplChars($complectation, $compl->complChars);
            }
            
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Добавляем связку поколения-модификации-комплектации.\r\n",FILE_APPEND);

            $complectationRel = new ComplectationGenerationModification;
            $complectationRel->id_car_complectation_generation_modification = $compl->complId;
            $complectationRel->id_car_generation_modification = $genModRel->id;
            $complectationRel->id_car_complectation = $complectation->id_car_complectation;
            $complectationRel->price_min = $compl->complPrice;
            $complectationRel->save();
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Добавили связку (".$complectationRel->id_car_complectation_generation_modification.").\r\n",FILE_APPEND);
        }
        else
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Связка нашлась. Укажем сразу, какая это комплектация (".$complectationRel->complectation->id_car_complectation.").\r\n",FILE_APPEND);
            $complectation = $complectationRel->complectation;
        }
        
    }
    
    public function addModChars($mod,$chars)
    {
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Добавляем характеристики к модификации (".$mod->id_car_modification.")\r\n",FILE_APPEND);
        foreach($chars as $char)
        {
            $character = Characteristic::model()->find('name=:name',array(':name'=>$char['name']));
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Ищем характеристику (".$char['name'].")\r\n",FILE_APPEND);
            if($character)
            {
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Нашли характеристику (".$character->id_car_characteristic.")\r\n",FILE_APPEND);
                
                $val = new CharacteristicValue;
                $val->value = $char['val'];
                $val->unit = '';
                $val->id_car_modification = $mod->id_car_modification;
                $val->id_car_characteristic = $character->id_car_characteristic;
                $val->is_visible = 1;
                $val->id_car_type = 1;  
                $val->save();  
                          
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Добавили значение характеристики (".$val->id_car_characteristic_value.")\r\n",FILE_APPEND);
            }
            else
            {
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Не нашли характеристику. Что-то пошло не так :(((\r\n",FILE_APPEND);
            }
        }
    }
    public function addComplChars($compl,$chars)
    {
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Добавляем характеристики к комплектации (".$compl->id_car_complectation.")\r\n",FILE_APPEND);
        foreach($chars as $char)
        {
            $character = ComplectationCharacteristic::model()->find('name=:name',array(':name'=>$char));
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Ищем характеристику (".$char.")\r\n",FILE_APPEND);
            if($character)
            {
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Нашли характеристику (".$character->id_car_characteristic.")\r\n",FILE_APPEND);
                $val = new ComplectationCharacteristicValue;
                $val->value = 1;
                $val->unit = '';
                $val->id_car_complectation = $compl->id_car_complectation;
                $val->id_car_add_characteristic = $character->id_car_characteristic;
                $val->is_visible = 1;
                $val->id_car_type = 1;  
                $val->save();            
                
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Добавили значение характеристики (".$val->id_car_characteristic_value.")\r\n",FILE_APPEND);
            }
            else
            {
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Не нашли характеристику. Что-то пошло не так :(((\r\n",FILE_APPEND);
            }
        }
    }
    public function compareModChars($mod,$chars)
    {
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Сравниваем модификацию (".$mod->id_car_modification.")\r\n",FILE_APPEND);

        if(count($mod->characteristicValues)!=count($chars))
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Кол-во параметров не совпадает. Не та модификация\r\n",FILE_APPEND);
            return false;
        }
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Кол-во параметров совпадает. Сравниваем хар-ки\r\n",FILE_APPEND);
        foreach($chars as $char)
        {
            $character = Characteristic::model()->find('name=:name',array(':name'=>$char['name']));
            if(!$character)  
            {
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Не нашли хар-ку (".$char['name']."). Не та модификация.\r\n",FILE_APPEND);
                return false;
            }  
            
            $charValue = CharacteristicValue::model()->find('id_car_modification=:mod_id and id_car_characteristic=:char_id and value=:val',array(':mod_id'=>$mod->id_car_modification,':char_id'=>$character->id_car_characteristic,':val'=>$char['val']));
            
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Нашли хар-ку (".$char['name']."). Ищем значение хар-ки (".$character->id_car_characteristic." - ".$char['val'].").\r\n",FILE_APPEND);
            if(!$charValue)
            {
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Не нашли хар-ки с таким значением. Не та модификация.\r\n",FILE_APPEND);
                return false;
            }
        }
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Все хар-ки нашлись. Та модификация.\r\n",FILE_APPEND);
        return true;
    }
    public function compareComplChars($compl,$chars)
    {
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Сравниваем комплектацию (".$compl->id_car_complectation.")\r\n",FILE_APPEND);
        if(count($compl->characteristicValues)!=count($chars))
        {
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Кол-во параметров не совпадает. Не та комплектация\r\n",FILE_APPEND);
            return false;
        }
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Кол-во параметров совпадает. Сравниваем хар-ки\r\n",FILE_APPEND);
        foreach($chars as $char)
        {
            $character = ComplectationCharacteristic::model()->find('name=:name',array(':name'=>$char));
            if(!$character)  
            {
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Не нашли хар-ку (".$char."). Не та комплектация.\r\n",FILE_APPEND);
                return false;
            }  
            
            $charValue = ComplectationCharacteristicValue::model()->find('id_car_complectation=:compl_id and id_car_add_characteristic=:char_id',array(':compl_id'=>$compl->id_car_complectation,':char_id'=>$character->id_car_characteristic));
            
            file_put_contents($this->importLog,date('d.m.Y H:i:s').": Нашли хар-ку (".$char."). Ищем значение хар-ки (".$character->id_car_characteristic.").\r\n",FILE_APPEND);
            
            if(!$charValue)
            {
                file_put_contents($this->importLog,date('d.m.Y H:i:s').": Не нашли хар-ки с таким значением. Не та комплектация.\r\n",FILE_APPEND);
                return false;
            }
        }
        
        file_put_contents($this->importLog,date('d.m.Y H:i:s').": Все хар-ки нашлись. Та комплектация.\r\n",FILE_APPEND);
        return true;
    }
}
?>