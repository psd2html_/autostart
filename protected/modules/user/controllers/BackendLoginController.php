<?php

class BackendLoginController extends Controller
{
	public $defaultAction = 'login';

	/**
	 * Displays the login page
	 */
    public function actionLogin()
    {
        if (Yii::app()->user->isGuest) {
            $model=new UserLogin;
            // collect user input data
            if(isset($_POST['UserLogin']))
            {
                $model->attributes=$_POST['UserLogin'];
                // validate user input and redirect to previous page if valid
                if($model->validate()) {
                    $this->lastViset();
                    if (Yii::app()->getBaseUrl()."/index.php" === Yii::app()->user->returnBackendUrl)
                        $this->redirect(Yii::app()->controller->module->returnBackendUrl);
                    else
                        $this->redirect(Yii::app()->user->returnBackendUrl);
                }
            }
            // display the login form
            $this->render('/user/backendlogin',array('model'=>$model));
        } else
            $this->redirect(Yii::app()->controller->module->returnBackendUrl);
    }
    
	private function lastViset() {
		$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit_at = date('Y-m-d H:i:s');
		$lastVisit->save();
	}

}