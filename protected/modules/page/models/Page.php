<?php

/**
 * This is the model class for table "{{page_page}}".
 *
 * The followings are the available columns in table '{{page_page}}':
 * @property integer $id
 * @property integer $category_id
 * @property string $lang
 * @property integer $parent_id
 * @property string $create_time
 * @property string $update_time
 * @property integer $user_id
 * @property integer $change_user_id
 * @property string $title_short
 * @property string $title
 * @property string $slug
 * @property string $body
 * @property string $keywords
 * @property string $description
 * @property integer $status
 * @property integer $is_protected
 * @property integer $order
 * @property string $layout
 * @property string $view
 */
class Page extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{page_page}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('create_time, update_time, title_short, title, slug, body, keywords, description, status', 'required'),
			array('category_id, parent_id, user_id, change_user_id, status, is_protected', 'numerical', 'integerOnly'=>true),
			array('lang', 'length', 'max'=>2),
			array('title_short, slug', 'length', 'max'=>150),
			array('title, keywords, description, layout, view', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, category_id, lang, parent_id, create_time, update_time, user_id, change_user_id, title_short, title, slug, body, keywords, description, status, is_protected, layout, view', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
			'lang' => 'Lang',
			'parent_id' => 'Parent',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'user_id' => 'User',
			'change_user_id' => 'Change User',
			'title_short' => 'Title Short',
			'title' => 'Title',
			'slug' => 'Slug',
			'body' => 'Body',
			'keywords' => 'Keywords',
			'description' => 'Description',
			'status' => 'Status',
			'is_protected' => 'Is Protected',
			'layout' => 'Layout',
			'view' => 'View',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('lang',$this->lang,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('change_user_id',$this->change_user_id);
		$criteria->compare('title_short',$this->title_short,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('keywords',$this->keywords,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('is_protected',$this->is_protected);
		$criteria->compare('layout',$this->layout,true);
		$criteria->compare('view',$this->view,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Page the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
