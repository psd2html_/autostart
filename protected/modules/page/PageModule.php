<?php

class PageModule extends CWebModule
{
    public function init()
    {
        parent::init();

        $this->setImport(
            array(
                'application.modules.page.models.*',
                'application.modules.page.components.widgets.*',
            )
        );

    }
}
