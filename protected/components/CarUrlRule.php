<?php
                                    
class CarUrlRule extends CUrlManager 
{
    public function parseUrl($route, $params = array(), $ampersand = '&') {

        return str_replace('%5D',']',str_replace('%5B','[',parent::createUrl($route, $params, $ampersand)));
    }
    public function createUrl($route, $params = array(), $ampersand = '&') {

        return str_replace('%5D',']',str_replace('%5B','[',parent::createUrl($route, $params, $ampersand)));
    }
}
