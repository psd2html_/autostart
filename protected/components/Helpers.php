<?php

class Helpers
{
    //get site settings data
    public static function getSettings($name=null)
    {
        if ($name)
            $setting = Settings::model()->find('name=:name',array('name'=>$name));
            return ($setting) ? $setting->value : false;
    }
}

