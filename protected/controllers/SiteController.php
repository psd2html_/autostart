<?php

class SiteController extends Controller
{
    public $layout = '//layouts/main';
    public $keywords = 'официальные дилеры в москве, автосалоны, купить машину, новые автомобили, комплектации и цены';
    public $description = 'Новые машины у официальных дилеров Москвы на одной платформе. Удобный подбор марок и моделей по ценам, техническим характеристикам, фото и обзорам';
    public $title = 'Автомобили в наличии у официальных дилеров | Dilerity';

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        Yii::import('application.modules.car.models.Mark');

        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters()
    {
        return array(
            'rights'
        );
    }

    public function actionFeedback()
    {
        $errors = array();

        if(Yii::app()->request->isPostRequest)
        {
            $post['name']=Yii::app()->request->getPost('name');
            $post['email']=Yii::app()->request->getPost('email');
            $post['text']=Yii::app()->request->getPost('text');
            $post['subject']=Yii::app()->request->getPost('subject');

            $error = array();
            if(!$post['name'])
            {
                $error['name']='Поле должно быть заполнено';
            }
            if(!$post['email'])
            {
                $error['email']='Поле должно быть заполнено';
            }
            if(!$post['text'])
            {
                $error['text']='Поле должно быть заполнено';
            }
            if(!$post['subject'])
            {
                $error['subject']='Поле должно быть заполнено';
            }

            $message = count($error)>0?'Сообщение не отправлено':'Сообщение успешно отправлено';

            if(count($error)==0)
            {
                $headers = "From: info@autostart.ru\r\nReply-To: info@autostart.ru";
                mail(Helpers::getSettings('email'), "Сообщение с сайта autostart.ru", "Имя: ".$post['name']."\r\nEmail: ".$post['email']."\r\nТема: ".$post['subject']."\r\nСообщение: ".$post['text']."\r\n",$headers);
            }
            echo json_encode(array('message'=>$message,'errors'=>$error));
        }
        else
        {
            throw new CHttpException(404,'Страница не найдена');
        }

    }
    public function actionFeedbackDealer()
    {
        $errors = array();
        
        if(Yii::app()->request->isPostRequest)
        {
            $post['inputComplectation']=Yii::app()->request->getPost('inputComplectation');
            $post['inputName']=Yii::app()->request->getPost('inputName');
            $post['inputPhone']=Yii::app()->request->getPost('inputPhone');
            $post['inputAddress']=Yii::app()->request->getPost('inputAddress');
            $post['inputMail']=Yii::app()->request->getPost('inputMail');
            $post['inputComment']=Yii::app()->request->getPost('inputComment');
            
            $error = array();
            if(!$post['inputComplectation'])
            {
                $error['inputComplectation']='Поле должно быть заполнено';
            }
            if(!$post['inputName'])
            {
                $error['inputName']='Поле должно быть заполнено';
            }
            if(!$post['inputPhone'])
            {
                $error['inputPhone']='Поле должно быть заполнено';
            }
            if(!$post['inputAddress'])
            {
                $error['inputAddress']='Поле должно быть заполнено';
            }
            if(!$post['inputMail'])
            {
                $error['inputMail']='Поле должно быть заполнено';
            }
            if(!$post['inputComment'])
            {
                $error['inputComment']='Поле должно быть заполнено';
            }

            
            $message = count($error)>0?'Сообщение не отправлено':'Сообщение успешно отправлено';
            
            if(count($errors)==0)
            {
                $headers="From: info@autostart.ru\r\nReply-To: info@autostart.ru";
                mail(Helpers::getSettings('email'), "Сообщение с сайта autostart.ru",
                    "Имя: ".$post['inputName'].
                    "\r\nEmail: ".$post['inputMail'].
                    "\r\nТелефон: ".$post['inputPhone'].
                    "\r\nАдрес: ".$post['inputAddress'].
                    "\r\nКомплектация: ".Yii::app()->getBaseUrl(true).$post['inputComplectation']."\r\n".
                    "\r\nСообщение: ".$post['inputComment']."\r\n",$headers);
            }
            echo json_encode(array('message'=>$message,'errors'=>$error));
        }
        else
        {
             throw new CHttpException(404,'Страница не найдена');
        }
        
    }

    public function actionFeedbackPreferences()
    {
        $errors = array();

        if(Yii::app()->request->isPostRequest)
        {
            $post['inputComment']=Yii::app()->request->getPost('inputComment');
            $post['inputAddress']=Yii::app()->request->getPost('inputAddress');
            $post['inputAddressOther']=Yii::app()->request->getPost('inputAddressOther');
            $post['inputMail']=Yii::app()->request->getPost('inputMail');
            $post['inputPhone']=Yii::app()->request->getPost('inputPhone');
            $post['inputCarTrade']=Yii::app()->request->getPost('inputCarTrade');
            $post['inputCarCredit']=Yii::app()->request->getPost('inputCarCredit');

            $error = array();

            if(!$post['inputAddress'])
            {
                $error['inputAddress']='Выберите один из пунктов';
            } elseif ($post['inputAddress']==2 && !$post['inputAddressOther'])
            {
                $error['inputAddressOther']='Поле должно быть заполнено';
            }
            if(!$post['inputMail'])
            {
                $error['inputMail']='Поле должно быть заполнено';
            }

            $message = count($error)>0?'Сообщение не отправлено':'Сообщение успешно отправлено';

            if(count($error)==0)
            {
                if ($post['inputAddress']==2) {
                    $address = $post['inputAddressOther'];
                } else {
                    $address = 'Я из Москвы или области';
                }

                $to = Helpers::getSettings('email');
                $subject = "Сообщение с сайта autostart.ru";
                $headers = "From: info@autostart.ru\r\nReply-To: info@autostart.ru";

                mail($to, $subject,
                    "Требования к машине".
                    "\r\nСообщение: ".$post['inputComment']."\r\n".
                    "\r\nАдрес: ".$address.
                    "\r\nEmail: ".$post['inputMail'].
                    "\r\nТелефон: ".$post['inputPhone'].
                    "\r\n".$post['inputCarTrade'].
                    "\r\n".$post['inputCarCredit']."\r\n",$headers);
            }
            echo json_encode(array('message'=>$message,'errors'=>$error));
        }
        else
        {
            throw new CHttpException(404,'Страница не найдена');
        }

    }

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
        Yii::import('application.modules.car.models.Body');
        Yii::import('application.modules.car.models.Mark');
        
        $bodies = Body::model()->findAll(array('order'=>'id_car_body', 'condition'=>'id_car_body<>8'));
        $marks = Mark::model()->findAll(array('condition'=>'is_visible=1','order'=>'name'));
        $marksFav = Mark::model()->findAll(array('condition'=>'is_visible=1 and is_fav=1','order'=>'name'));
        
        $arr = array();
        $arr[0] = array();
        $arr[1] = array();
        $arr[2] = array();
        $arr[3] = array();
        
        $arrFav = array();
        $arrFav[0] = array();
        $arrFav[1] = array();
        $arrFav[2] = array();
        $arrFav[3] = array();
        
        $cnt = count($marks);
        $perCol = ceil($cnt/4);

        $i = 0;
        $col = 0;
        foreach($marks as $mark)
        {
            $arr[$col][] = $mark;
            $i++;
            if($i>=$perCol)
            {
                $i=0;
                $col++;
            }
        }
        
        $cnt = count($marksFav);
        $perCol = $cnt/4;
        //echo $cnt;
        //echo $perCol;
        $i = 0;
        $col = 0;
        foreach($marksFav as $mark)
        {
            if($mark->is_fav)
            {
                $arrFav[$col][] = $mark;
                $i++;
                if($i>=$perCol)
                {
                $i=0;
                    $col++;
                }
            }
        }

        $this->title = 'Онлайн сервис покупки новых машин - Гараж АМ | Купить авто от официального дилера (салона)';
        $this->description = 'Онлайн сервис покупки новых машин. Получите предложения на новый автомобиль до визита в автосалон. Бесплатно. Только  официальные дилеры.' ;

        $this->render('index',array('bodies'=>$bodies, 'marks'=>$arr, 'marksFav'=>$arrFav));
	}

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {

        if($error=Yii::app()->errorHandler->error)
        {
            //show errors for developer
            if ($_SERVER['HTTP_HOST'] == 'autostart.loc') {
                echo '<pre>';
                print_r ($error);
                die();
            }
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model=new ContactForm;
        if(isset($_POST['ContactForm']))
        {
            $model->attributes=$_POST['ContactForm'];
            if($model->validate())
            {
                $headers="From: {$model->email}\r\nReply-To: {$model->email}";
                mail(Helpers::getSettings('email'),$model->subject,$model->body,$headers);
                Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact',array('model'=>$model));
    }

    public function actionThank()
    {
        
        $this->render('thank');
    }


}
