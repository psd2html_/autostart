<?php
return array (
  'template' => 'default',
  'connectionId' => 'db',
  'tablePrefix' => '',
  'modelPath' => 'application.modules.settings.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '0',
  'commentsAsLabels' => '1',
);
