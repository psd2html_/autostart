<?php

class ImportCommand extends CConsoleCommand
{
    public function actionFullImport()
    {
        $model = Mark::model()->findAll();    

        $path = Yii::getPathOfAlias('application').Yii::app()->getModule('car')->importPath.'/CharTable_6.csv';
        
        $newModel = Yii::app()->cars->importModels($path);
        
        return true;
    }
    public function actionImageImport()
    {
        $model = Mark::model()->findAll();    

        $path = Yii::getPathOfAlias('application').Yii::app()->getModule('car')->importPath.'/PicutresTable_final.csv';
        $genPath = Yii::getPathOfAlias('application').Yii::app()->getModule('car')->assetsGenPath.'/';        
        $newModel = Yii::app()->cars->importImagesModels($path,$genPath);
        
        return true;
    }
}
