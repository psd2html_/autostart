<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name' => 'Dilerity |  Выбор и поиск новых автомобилей у официальных дилеров',
    'charset'=>'windows-1251',

	// preloading 'log' component
	'preload'=>array(
        'log',
        'booster',
    ),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.modules.rights.*',
        'application.modules.rights.components.*', 
        'application.modules.backend.*',
		'application.modules.zendsearch.models.*',
		'application.modules.settings.models.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'iyIegee5',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','93.153.230.53','80.70.228.66', '83.171.85.214','::1'),
		),
		
        'user'=>array(
            'tableUsers' => 'as_users',
            'tableProfiles' => 'as_profiles',
            'tableProfileFields' => 'as_profiles_fields',
            'returnUrl' => array('/backend/backend/index'),
        ),
        'rights'=>array(
            'layout' => '//layouts/inner_rights',
        ),
        'car'=>array(
            'debug' => false,
            'assetsBodyPath' => 'webroot.upload.car.body',    
            'assetsModelPath' => 'webroot.upload.car.model',    
            'assetsCharPath' => 'webroot.upload.car.chars',    
            'assetsGenPath' => 'webroot.upload.car.generation',    
            'assetsMarkPath' => 'webroot.upload.car.mark',    
            'importPath' => 'webroot.upload.import',    

            'urlBodyPath' => '/upload/car/body/',
            'urlModelPath' => '/upload/car/model/',
            'urlCharPath' => '/upload/car/chars/',
            'urlGenPath' => '/upload/car/generation/',
            'urlMarkPath' => '/upload/car/mark/',
            'defaultCategoryImage' => '/images/garage_bgg.png',
            'defaultCategoryText' => 'Каталог новых автомобилей, которые можно купить в России. Используйте фильтры, чтобы выбрать авто для себя',
            'colors' => array(
                '13'=>'ff0000',
                '14'=>'f0f0f0',
                '15'=>'000000',
                '16'=>'0000ff',
                '17'=>'aaaaaa',
            ),
        ),
        'page'=>array(),
        'settings'=>array(),
        'backend'=>array(
            'sidebar'=>array(
                array(
                    'label' => 'Каталог',
                    'itemOptions' => array('class' => 'nav-header')
                ),
                /*array(
                    'label' => 'Типы автомобилей',
                    'url' => '/backend/car/Type/index',
                ),*/
                array(
                    'label' => 'Марки автомобилей',
                    'url' => '/backend/car/Mark/index',
                ),
                array(
                    'label' => 'Модели автомобилей',
                    'url' => '/backend/car/CarModel/index',
                ),
                array(
                    'label' => 'Поколения автомобилей',
                    'url' => '/backend/car/Generation/index',
                ),
                array(
                    'label' => 'Модификации автомобилей',
                    'url' => '/backend/car/Modification/index',
                ),
                array(
                    'label' => 'Комплектации автомобилей',
                    'url' => '/backend/car/Complectation/index',
                ),
                array(
                    'label' => 'Загрузка моделей',
                    'url' => '/backend/car/CarModel/upload',
                ),
                array(
                    'label' => 'Справочники',
                    'itemOptions' => array('class' => 'nav-header')
                ),
                array(
                    'label' => 'Типы кузовов',
                    'url' => '/backend/car/Body/index',
                ),
                array(
                    'label' => 'Группы характеристик модификаций',
                    'url' => '/backend/car/CharacteristicGroup/index',
                ),
                array(
                    'label' => 'Характеристики модификаций',
                    'url' => '/backend/car/Characteristic/index',
                ),
                array(
                    'label' => 'Дополнительные характеристики (с изображением)',
                    'url' => '/backend/car/AddCharacteristic/index',
                ),
                array(
                    'label' => 'Характеристики поколений',
                    'url' => '/backend/car/generationCharacteristic/index',
                ),
                array(
                    'label' => 'Характеристики комплектаций',
                    'url' => '/backend/car/ComplectationCharacteristic/index',
                ),
                array(
                    'label' => 'Администрирование',
                    'itemOptions' => array('class' => 'nav-header')
                ),
                array(
                    'label' => 'Управление пользователями',
                    'url' => '/user/admin',
                ),
                array(
                    'label' => 'Управление правами доступа',
                    'url' => '/rights',
                ),

                array(
                    'label' => 'Управление страницами',
                    'url' => '/backend/page/page/index',
                ),
                array(
                    'label' => 'СЕО',
                    'itemOptions' => array('class' => 'nav-header')
                ),
                array(
                    'label' => 'Страницы',
                    'url' => '/backend/car/SeoPage/index',
                ),
                array(
                    'label' => 'Cгенерировать страницы',
                    'url' => '/backend/car/SeoPage/generate',
                ),
                array(
                    'label' => 'XML - генерировать',
                    'url' => '/backend/car/SeoPage/generatexml',
                ),
                array(
                    'label' => 'Настройки',
                    'itemOptions' => array('class' => 'nav-header')
                ),
                array(
                    'label' => 'Редактировать',
                    'url' => '/settings/Settings/index',
                ),
            ),
/*            'zendserch'=> [
        'class'        => 'application.modules.zendsearch.ZendSearchModule',
        'searchModels' => [
            'Product' => [
                'path'        => 'application.modules.store.models.Product',
                'module'      => 'store',
                'condition' => 't.status = 1',
                'titleColumn' => 'name',
                'linkColumn'  => 'slug',
                'linkPattern' => '/store/show/{slug}',
                'textColumns' => 'name,description,short_description',
            ],
        'News' => [
                'path'        => 'application.modules.news.models.News',
                'module'      => 'news',
                'condition' => '',
                'titleColumn' => 'title',
                'linkColumn'  => 'slug',
                'linkPattern' => '/news/news/show?title={slug}',
                'textColumns' => 'full_text,short_text,keywords,description',
            ],
            'Page' => [
                'module'      => 'page',
                'path'        => 'application.modules.page.models.Page',
                'condition' => '',
                'titleColumn' => 'title',
                'linkColumn'  => 'slug',
                'linkPattern' => '/page/page/show?slug={slug}',
                'textColumns' => 'body,title_short,keywords,description',
            ],
        ],
    ],
 */      ),
	),

	// application components
	'components'=>array(
        'assetManager' => array(
            'forceCopy' => true,
        ),
        'cars' => array(
            'class' => 'application.modules.car.components.Cars',
        ),

        'booster' => array(
            'class' => 'application.components.yiibooster.components.Booster',
            'enablePopover' => false,
        ),
        'phpThumb'=>array(
            'class'=>'application.components.ephpthumb.EPhpThumb',
            'options'=>array()
        ),
        'user'=>array(
                'class'=>'RWebUser',
                // enable cookie-based authentication
                'allowAutoLogin'=>true,
                'loginUrl'=>array('/user/login'),
        ),
        'authManager'=>array(
                'class'=>'RDbAuthManager',
                'connectionID'=>'db',
                'defaultRoles'=>array('Authenticated', 'Guest'),
        ),
        
		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
            /*'class' => 'application.modules.car.components.CarUrlRule',*/
			'urlFormat'=>'path',
            'showScriptName'=>false,
			'rules'=>require(dirname(__FILE__).'/rules.php'),
		),


		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>YII_DEBUG ? null : 'site/error',
		),

		'log'=>array(
            'class'=>'CLogRouter',
            'enabled' => YII_DEBUG,
            'routes'=>array(
                #...
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
                array(
                    'class'=>'application.components.yii-debug-toolbar.YiiDebugToolbarRoute',
                    /*'ipFilters'=>array('127.0.0.1','192.168.1.215','93.153.230.53','80.70.228.66','109.87.73.72'),*/
                ),
            ),           
		),

	),
    
    'theme'=>'autostart',
    'sourceLanguage'    =>'en',
    'language'          =>'ru',
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
        // Client's mail
		'adminEmail'=>'as@garage.am',
		'feedBackThemes'=>array('Предложение о сотрудничестве','Технический вопрос','Отзыв пользователя','Прочее'),
	),
);
