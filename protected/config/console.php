<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),

    'import'=>array(
        'application.modules.car.models.*',
    ),

	// application components
	'components'=>array(
        'cars' => array(
            'class' => 'application.modules.car.components.Cars',
        ),

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
        'phpThumb'=>array(
            'class'=>'application.components.ephpthumb.EPhpThumb',
            'options'=>array()
        ),

	),
    'modules'=>array(
        'car'=>array(
            'debug' => false,
            'assetsBodyPath' => 'webroot.upload.car.body',    
            'assetsModelPath' => 'webroot.upload.car.model',    
            'assetsCharPath' => 'webroot.upload.car.chars',    
            'assetsGenPath' => 'webroot.upload.car.generation',    
            'assetsMarkPath' => 'webroot.upload.car.mark',    
            'assetsGenPath' => '/../upload/car/generation',    
            'importPath' => '/../upload/import',    

            'urlBodyPath' => '/upload/car/body/',
            'urlModelPath' => '/upload/car/model/',
            'urlCharPath' => '/upload/car/chars/',
            'urlGenPath' => '/upload/car/generation/',
            'urlMarkPath' => '/upload/car/mark/',
            'defaultCategoryImage' => '/images/bgg.jpg',
            'defaultCategoryText' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit consectetur excepturi eveniet veniam qui, repellat iure dolores, maxime placeat iusto delectus quod. Aliquam eveniet quis alias tempore officiis adipisci vel nihil mollitia natus obcaecati unde cupiditate sint iure quo non in dolorem repudiandae, recusandae provident nostrum minus minima fuga molestiae eaque. Veniam reiciendis, assumenda nulla?',
            'colors' => array(
                '13'=>'ff0000',
                '14'=>'f0f0f0',
                '15'=>'000000',
                '16'=>'0000ff',
                '17'=>'aaaaaa',
            ),
        ),
    ),
);
