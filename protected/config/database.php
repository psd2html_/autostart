<?php
//
$host = '';
if(isset($_SERVER['HTTP_HOST']))
{
    $host = $_SERVER['HTTP_HOST'];
}

if ($host == 'autostart.artwebit.ru' OR $host == 'garage.artwebit.ru')
{
    return array(
        'connectionString' => 'mysql:host=localhost;dbname=garage_db',
        'emulatePrepare' => true,
        'username' => 'garage_user',
        'password' => 'nPQuyG79Em',
        'charset' => 'utf8',
        'tablePrefix' => 'as_',
        'enableProfiling' => defined('YII_DEBUG') && YII_DEBUG ? true : 0,
    );
}
elseif(($host == 'dilerity.ru') OR ($host == '54.191.13.137') OR ($host == 'www.dilerity.ru') OR ($host == 'garage.am') OR ($host == 'www.garage.am'))
{
    return array(
        'connectionString' => 'mysql:host=localhost;dbname=autostart_2',
        'emulatePrepare' => true,
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8',
        'tablePrefix' => 'as_',
        'enableProfiling' => defined('YII_DEBUG') && YII_DEBUG ? true : 0,
    );
}
// localhost
else
{
    return array(
        'connectionString' => 'mysql:host=localhost;dbname=auto',
        'emulatePrepare' => true,
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'tablePrefix' => 'as_',
        'enableProfiling' => defined('YII_DEBUG') && YII_DEBUG ? true : 0,
    );
}
