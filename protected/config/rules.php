<?php

// rules for urlManager
return array(
    'gii'=>'gii',
    'gii/<controller:\w+>'=>'gii/<controller>',
    'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',

    '/search' => 'zendsearch/search/search',
    '/backend' => '/backend/backend/index',
    '/backend/login' => '/user/account/login',

    '/backend/<module:\w+>/<controller:\w+>' => '/<module>/<controller>Backend/index',
    '/backend/<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '/<module>/<controller>Backend/<action>',
    '/backend/<module:\w+>/<controller:\w+>/<action:\w+>' => '/<module>/<controller>Backend/<action>',

    '/pages/<slug>' => 'page/page/show',
    'car' => 'car/catalog/search',
    //'car' => 'car/catalog',
    'car/catalog/ajaxmod' => 'car/catalog/ajaxmod',

    'car/catalog/livesearch' => 'car/catalog/livesearch',
    'car/livesearch' => 'car/catalog/livesearch',
    'car/livesearchin' => 'car/catalog/livesearchin',

    'car/<body:(sedan|hatchback|van|coupe|suv|cabriolet|minivan|pickup|wagon)>' => 'car/catalog/search',
    'car/<body:(sedan|hatchback|van|coupe|suv|cabriolet|minivan|pickup|wagon)>' => 'car/catalog',

    'car/do-600000-rub' => 'car/catalog/search/price-min/0/price-max/600000',
    'car/ot-600000-do-1000000-rub' => 'car/catalog/search/price-min/600000/price-max/1000000',
    'car/ot-1000000-do-1500000-rub' => 'car/catalog/search/price-min/1000000/price-max/1500000',
    'car/ot-1500000-do-2000000-rub' => 'car/catalog/search/price-min/1500000/price-max/2000000',
    'car/ot-2000000-rub' => 'car/catalog/search/price-min/2000000/price-max/90000000',

    'car/<price:(do-600000-rub|ot-600000-do-1000000-rub|ot-1000000-do-1500000-rub|ot-1500000-do-2000000-rub|ot-2000000-rub)>' => 'car/catalog/search',
    'car/<price:(do-600000-rub|ot-600000-do-1000000-rub|ot-1000000-do-1500000-rub|ot-1500000-do-2000000-rub|ot-2000000-rub)>' => 'car/catalog',
    'car/<mark:[\w-]+>/<slug:(2016|2017|2018|dealers|new|diesel|tests|review|ttx)>' => 'car/catalog/search',
    'car/<mark:[\w-]+>/<slug:(2016|2017|2018|dealers|new|diesel|tests|review|ttx)>' => 'car/catalog',

    'car/<mark:[\w-]+>/<model:[\w-]+>/<slug:\d+>' => 'car/catalog/agroupseopage',
    'car/<mark:[\w-]+>/<model:[\w-]+>/<slug:(2016|2017|2018|new)\/\d+>' => 'car/catalog/agroupseopage',

    'car/<mark:[\w-]+>/<model:[\w-]+>/<slug:(at|mt|diesel|awd)\/\d+>' => 'car/catalog/agroupseopage',
    'car/<mark:[\w-]+>/<model:[\w-]+>/<slug:[\d-]-[\d-]\/(at|mt|diesel)\/\d+>' => 'car/catalog/agroupseopage',

    'car/<mark:[\w-]+>/<model:[\w-]+>/<slug:(tests|review)\/\d+>' => 'car/catalog/agroupseopage',
    'car/<mark:[\w-]+>/<model:[\w-]+>/<slug:(2016|2017|2018|new)\/(tests|review)\/\d+>' => 'car/catalog/agroupseopage',

    'car/<mark:[\w-]+>/<model:[\w-]+>/<slug:ttx\/\d+>' => 'car/catalog/dgroupseopage',
    'car/<mark:[\w-]+>/<model:[\w-]+>/<slug:(2016|2017|2018|new)\/ttx\/\d+>' => 'car/catalog/dgroupseopage',

    'car/<mark:[\w-]+>/<model:[\w-]+>/<id:[\d-]+>' => 'car/catalog/single',
    'car/<mark:[\w-]+>/<model:[\w-]+>/<mod:[\d-]-[\d-]>/<id:\d+>' => 'car/catalog/character',
    'car/<mark:[\w-]+>/<model:[\w-]+>/options/<compl:\d+>' => 'car/catalog/character',

    'car/<mark:(?!catalog\b)\b\w+>/<model:(?!favorites|profile|dealers|ajaxmod|compare|getmodellist|getgenerationlist|getbodylist|getmodlist\b)\b\w+>' => 'car/catalog/single',
    'car/<mark:(?!favorites|profile|dealers|ajaxmod|compare|getmodellist|getgenerationlist|getbodylist|getmodlist\b)\b(\w+|\w+-\w+)>' => 'car/catalog',

    'car/favorites' => 'car/catalog/favorites',
    'car/dealers' => 'car/catalog/dealers',
    'car/ajaxmod' => 'car/catalog/ajaxmod',
    'car/profile' => 'car/catalog/profile',
    'car/search' => 'car/catalog/search',

    'car/single' => 'car/catalog/single',
    'car/character' => 'car/catalog/character',
    'car/compare' => 'car/catalog/compare',
    'car/catalog/getmodellist' => 'car/catalog/getmodellist',
    'car/getmodellist' => 'car/catalog/getmodellist',
    'car/getgenerationlist' => 'car/catalog/getgenerationlist',
    'car/getbodylist' => 'car/catalog/getbodylist',

    'car/getmodlist' => 'car/catalog/getmodlist',

    '<view:about>'=>'site/page',
    '<action:(contact|login|logout|home)>'=>'site/<action>',
    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
    '<view>'=>array('site/page'),

);