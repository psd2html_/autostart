<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<div class="assideMargin clearfix withoutHead errorPage">

	<div class="container">
		<h1>Ошибка <?php echo $code; ?></h1>
		<h2>Страницы не существует или она была удалена</h2>
		<div class="error">
			<?php echo CHtml::encode($message); ?>
		</div>

		<div class="errorNav">
		    <a href="/" class="btn btn-default pull-center">Вернуться назад</a>
		    <a href="/" class="btn btn-default pull-center">На главную</a>
		</div>

	</div>

</div>