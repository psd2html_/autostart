<?php
$this->title = 'Гараж АМ - Стать партнером';
$this->description = 'Онлайн сервис покупки новых машин. Получите предложения на новый автомобиль до визита в автосалон. Бесплатно. Только  официальные дилеры.' ;
?>

<div class="assideMargin clearfix withoutHead forDealers">

    <div class="container whiteSpace">
        <div class="row">
            <h1 class="title text-center">Партнерская программа</h1>
            <div class="presentation_player">
                <script async class="speakerdeck-embed" data-id="40c22082324243cdb06a495ff83ac648" data-ratio="1.44428772919605" src="//speakerdeck.com/assets/embed.js"></script>
            </div>
        </div>
        <div class="row">
            <section class="formPreferences hideCheckboxBorder">

                <div class="alert" id="alertfb"></div>
                <div class="callBack">
                    <h3>Обратная связь</h3>
                    <p>Если у вас есть вопросы, то используйте эту форму для связи с нами</p>
                    <?php
                    $form=$this->beginWidget(
                        'booster.widgets.TbActiveForm',
                        array(
                            'id'                      => 'feedback_form'
                        , 'type'                    => 'horizontal'
                        )
                    );
                    ?>

                    <div class="alert" id="alertfb" style="display:none;">
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-xs-3 control-label">Ваше имя:</label>
                        <div class="col-xs-9">
                            <input type="text" class="form-control input-lg" id="name" name="name" value="" placeholder="Введите имя">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-xs-3 control-label">E-mail:</label>
                        <div class="col-xs-9">
                            <input type="email" class="form-control input-lg" id="email" name="email" value="" placeholder="Введите email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-xs-3 control-label">Тема:</label>
                        <div class="col-xs-9">
                            <select name='subject' value='<?=Yii::app()->params['feedBackThemes'][0]?>' id="subject" class="form-control input-lg">
                                <?php
                                foreach(Yii::app()->params['feedBackThemes'] as $theme)
                                {
                                    echo "<option value='".$theme."'>".$theme."</option>";
                                }
                                ?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-xs-3 control-label">Сообщение:</label>
                        <div class="col-xs-9">
                            <textarea name='text' id="text" class="form-control input-lg"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-9 col-xs-offset-3">
                            <?=CHtml::Button('Отправить',array('class'=>'btn btn-success','id'=>'feedback')  )?>
                        </div>
                    </div>

                    <?php
                    $this->endWidget();
                    ?>
                </div>

            </section>
        </div>
    </div>
</div>