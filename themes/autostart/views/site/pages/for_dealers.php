<?php
$this->title = 'Гараж АМ - Стать партнером';
$this->description = 'Онлайн сервис покупки новых машин. Получите предложения на новый автомобиль до визита в автосалон. Бесплатно. Только  официальные дилеры.' ;
?>

<div class="assideMargin clearfix withoutHead forDealers">

    <div class="container whiteSpace">
        <div class="row">
            <h1 class="title text-center">Для дилеров</h1>
            <div class="presentation_player">
                <script async class="speakerdeck-embed" data-id="494b4dc3e8924174bf0befd1efa3d405" data-ratio="1.44428772919605" src="//speakerdeck.com/assets/embed.js"></script>
            </div>


           <!-- <div style="margin-bottom:5px">
                <strong>
                    <a href="//www.slideshare.net/secret/zg1rg6N26Gyc0Y"  title="Garage dealers deck" target="_blank">Garage dealers deck</a>
                </strong>
                from
                <strong>
                    <a href="//www.slideshare.net/AlexeiSidorov" target="_blank">Alexey Sidorov</a>
                </strong>
            </div>-->


            <!--<section class="howItWorksworks">
                <div class="item">
                    <div class="forText">
                        <h5>How does it work?</h5>
                        <p>You open your dealership’s doors to hundreds of thousands of qualified users ready to buy brand new cars. Our registered users contact you directly to buy cars; they can read your reviews and get all your contact and location details upfront.</p>
                        <p>You get quick, easy, direct sales, on your terms and we only charge upon a success basis &ndash; every dealer pays the same amount per sale.</p>
                    </div>
                    <div class="forImage">
                        <div class="home-highlight-icon illustration illustration-large illustration-laptop-blue"></div>
                    </div>
                </div>
                <div class="item">
                    <div class="forImage">
                        <div class="home-highlight-icon illustration illustration-large illustration-award-blue"></div>
                    </div>
                    <div class="forText">
                        <h5>Rewarding successful dealers</h5>
                        <p>Our system rewards successful dealers, and we don’t just mean those that sell lots of cars. Providing outstanding customer service also gets you more visibility on our site &ndash; you reach a bigger audience of car buyers if our system sees you’re giving users a great buying experience &ndash; in return, you get to sell more cars!</p>
                        <p>We reward dealers who give prompt and informative responses to customer enquiries, convert the users who contact them, and receive great reviews and ratings from our users.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="forText">
                        <h5>How do I join?</h5>
                        <p>Get in touch to sign up your dealership or group effortlessly, there are no upfront costs or any contracts to sign. You sell at the price you set, we don’t impose any terms. It only takes minutes for us to get you up and running.</p>
                        <p>If you’ve got any questions feel free to get in touch on <a href="tel: 442037807175">020 3780 7175</a> and we’ll be happy to talk it through!</p>
                    </div>
                    <div class="forImage">
                        <div class="home-highlight-icon illustration illustration-large illustration-poundbanknote-blue"></div>
                    </div>
                </div>
            </section>-->
        </div>
        <div class="row">
            <section class="formPreferences hideCheckboxBorder">

                <div class="alert" id="alertfb"></div>
                <div class="callBack">
                    <h3>Обратная связь</h3>
                    <p>Если у вас есть вопросы, то используйте эту форму для связи с нами</p>
                    <?php
                    $form=$this->beginWidget(
                        'booster.widgets.TbActiveForm',
                        array(
                            'id'                      => 'feedback_form'
                        , 'type'                    => 'horizontal'
                        )
                    );
                    ?>

                    <div class="alert" id="alertfb" style="display:none;">
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-xs-3 control-label">Ваше имя:</label>
                        <div class="col-xs-9">
                            <input type="text" class="form-control input-lg" id="name" name="name" value="" placeholder="Введите имя">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-xs-3 control-label">E-mail:</label>
                        <div class="col-xs-9">
                            <input type="email" class="form-control input-lg" id="email" name="email" value="" placeholder="Введите email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-xs-3 control-label">Тема:</label>
                        <div class="col-xs-9">
                            <select name='subject' value='<?=Yii::app()->params['feedBackThemes'][0]?>' id="subject" class="form-control input-lg">
                                <?php
                                foreach(Yii::app()->params['feedBackThemes'] as $theme)
                                {
                                    echo "<option value='".$theme."'>".$theme."</option>";
                                }
                                ?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-xs-3 control-label">Сообщение:</label>
                        <div class="col-xs-9">
                            <textarea name='text' id="text" class="form-control input-lg"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-9 col-xs-offset-3">
                            <?=CHtml::Button('Отправить',array('class'=>'btn btn-success','id'=>'feedback')  )?>
                        </div>
                    </div>

                    <?php
                    $this->endWidget();
                    ?>
                </div>

            </section>
        </div>
    </div>
</div>