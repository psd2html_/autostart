<?php
$this->title = 'Гараж АМ - Спасибо, что оставили заявку.';
$this->description = 'Онлайн сервис покупки новых машин. Получите предложения на новый автомобиль до визита в автосалон. Бесплатно. Только  официальные дилеры.' ;
?>

<div class="assideMargin clearfix withoutHead">

    <div class="container whiteSpace">
        <div class="row">
            <h3>Спасибо, что оставили заявку. Наш специалист скоро с вами свяжется.</h3>
            <div class="table-responsive">
                <table class='table table-bordered table-condensed'>
                    <tr>
                        <th><p>Номер заявки</p></th>
                        <td> <?php echo Yii::app()->session->get('order_id'); ?></td>
                    </tr>
                    <tr>
                        <th><p>Детали заявки</p></th>
                        <td> <?php echo Yii::app()->session->get('sku_page');?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>