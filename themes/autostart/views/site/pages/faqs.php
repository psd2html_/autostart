<div class="assideMargin clearfix withoutHead faqs">

    <div class="container whiteSpace">
        <div class="row">
            <h1>FAQs</h1>
        </div>

        <div class="row">
            <section>
                <div class="grid-container faqs-container">
                    <section class="faqsNavigation">
                        <ul>
                            <li>
                                <a href="#entry-49">What is carwow?</a>
                            </li>
                            <li>
                                <a href="#entry-50">Why use carwow over walking into your local dealership?</a>
                            </li>
                            <li>
                                <a href="#entry-2">Why do I need to sign up to see the offers?</a>
                            </li>
                        </ul>
                    </section>
                    <section class="faqs-entries">
                        <div style="border-top: 1px solid #ddd">
                            <div style="border-bottom: 1px solid #ddd; margin-top: 24px; padding-bottom: 24px" id="entry-49" class="faqs-entry">
                                <h3>What is carwow?</h3>
                                <p></p><p>carwow is the new way of buying cars in Britain. A better way. One where you get to see upfront offers from the best dealers in the UK, without the hassle or haggle.</p>

                                <p>It’s turned the traditional way of car buying on it’s head - no more awkward negotiating on the dealership forecourt, you tell us the car you want and we get dealers to compete over you.</p>

                                <p>We send you up to 5 offers within 24 hours from the best-rated dealers in Britain. Dealers offer their best price from the get-go, so there is no need to haggle.</p>

                                <p>Your contact information is never shared with dealers, so you won’t be hassled by any unwanted sales calls.</p>

                                <p>Once you’ve received your deals, you can compare them by price, location of the dealership, and reviews of the dealer from other buyers.</p>

                                <p>All that’s left is to message your favourite dealers anonymously through the dashboard, or give them a call to buy directly.</p>

                                <p>Best of all it’s completely free whether you buy a car or not!</p><p></p>
                            </div>
                            <div style="border-bottom: 1px solid #ddd; margin-top: 24px; padding-bottom: 24px" id="entry-50" class="faqs-entry">
                                <h3>Why use carwow over walking into your local dealership?</h3>
                                <p></p><p>Buying a new car should be exciting and enjoyable - so why does it usually feel like a chore?</p>

                                <p>At carwow, we know car buyers are sick of wasting hours travelling around dealerships, haggling over prices, and never knowing if you’re getting a good deal.</p>

                                <p>When you walk into your local dealer you deal with a salesperson on the showroom floor who is paid on commission, and the commission depends on the amount of profit they can get from you.</p>

                                <p>That’s where carwow comes in - we bypass this by working with senior managers who authorise big discounts through carwow. In return, we provide them with a large volume of sales which makes them very happy.</p>

                                <p>We’re committed to finding the best deals for our users, while providing an easy and enjoyable car-buying experience.</p><p></p>
                            </div>
                            <div style="border-bottom: 1px solid #ddd; margin-top: 24px; padding-bottom: 24px" id="entry-2" class="faqs-entry">
                                <h3>Why do I need to sign up to see the offers?</h3>
                                <p></p><p>We notify you by email when your offers come in. Being able to log in to our system allows you to message the dealer back and forth anonymously without being hassled.  We’ll also keep you updated about vital details like upcoming price rises on the car you’re looking at.</p>

                                <p>Dealers don’t want to publicly display all the details of their offers for the world to see, but they’re happy to offer carwow exclusive discounts privately to our high quality users who’ve signed up.</p><p></p>
                            </div>
                        </div>
                    </section>

                </div>
            </section>
        </div>

    </div>
</div>