<?php
$this->title = 'Гараж АМ - Как работает сервис';
$this->description = 'Онлайн сервис покупки новых машин. Получите предложения на новый автомобиль до визита в автосалон. Бесплатно. Только  официальные дилеры.' ;
?>

<div class="assideMargin clearfix withoutHead">

    <div class="container howItWorks whiteSpace">
        <div class="row">

            <h1 class="title">Как это работает</h1>

            <h3 class="subTitle">Выберите марку автомобиля и лучшие дилеры начнут делать вам предложения уже сегодня</h3>

                <div class="item first">
                    <div class="forImage center"></div>
                    <div class="forText">
                        <p>Выбирайте и сравнивайте автомобили, используя полный каталог автомобилей в
                            продаже, фильтры, сервис сравнения по характеристикам и фотографиям.
                            Если вам нужна помощь в подборе машины, позвоните нам или опишите ваши предпочтения <a href="#ModalPreferences" data-toggle="modal">тут</a></p>
                    </div>
                </div>
                <div class="item second">
                    <div class="forImage center"></div>
                    <div class="forText">
                        <p>Вы выбираете интересующую вас модель автомобиля,
                            оставляете заявку и получаете предложения от дилеров. Мы отправим вам до пяти лучших доступных
                            предложений. Мы не передаем ваши контактные данные. Вы не будете получать назойливые звонки от множества
                            дилерских центров</p>
                    </div>
                </div>
                <div class="item last">
                    <div class="forImage center"></div>
                    <div class="forText">
                        <p>Сравнивайте предложения по
                            стоимости, бонусам, местоположению и репутации дилера. Мы выдаем вам сертификат, гарантирующий
                            покупку автомобиля по указанным в предложении  условиям. Не забудьте
                            предъявить его при посещении дилерского центра</p>
                    </div>
                </div>

        </div>

       <!-- <div class="row">
            <div class="how-it-works-cta">
                <h3>Ready to experience better new car buying?</h3>
                <p>Join thousands of people choosing the new way to buy their next car</p>
                <a href="/" class="btn">Get started</a>
            </div>
        </div>

        <div class="row">
            <div class="how-it-works-more-info">
                <h3>Looking for more info?</h3>
                <p>Visit our <a href="/faqs">FAQs</a> to learn more about how carwow works</p>
            </div>
        </div>-->

    </div>

</div>