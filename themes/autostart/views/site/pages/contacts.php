<?php
$this->title = 'Гараж АМ - Контакты';
$this->description = 'Онлайн сервис покупки новых машин. Получите предложения на новый автомобиль до визита в автосалон. Бесплатно. Только  официальные дилеры.' ;
?>


<div class="assideMargin clearfix withoutHead">

    <div class="container whiteSpace">
        <div class="row">
            <h1>Контакты</h1><br />
        </div>
        <div class="row">
            <p><b>Адрес:</b> г. Москва, ул. Рощинская 2-я, д. 4, офис 503</p>
            <p><b>Телефон:</b> +7 495 178 09 57</p>
            <p><b>Email:</b> info@garage.am</p>
        </div><br />
        <div class="row">
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=hWubMwoGGTLnleuhb--KxHFq1_C_bbSb&amp;width=1125&amp;height=550&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
        </div>
    </div>
</div>