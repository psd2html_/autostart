<?php
$this->title = 'Гараж АМ - Дилеры на платформе';
$this->description = 'Онлайн сервис покупки новых машин. Получите предложения на новый автомобиль до визита в автосалон. Бесплатно. Только  официальные дилеры.' ;
?>

<div class="assideMargin clearfix withoutHead">

    <div class="container whiteSpace">

        <div class="carwowDealers">

            <div class="row">
                <h1 class="title">Дилеры в гараже</h1>
                <!--<h2 class="subTitle"> What you should know</h2>-->
            </div>

            <div class="row">
                <div class="item col-xs-12 col-sm-4 col-lg-4">
                    <div class="forImage">
                        <img alt="Illustration offers" src="https://www-assets.carwow.co.uk/assets/illustration-offers-d3ff44c36d98c0f7503c01b7f430374206ce422370912c6e3cdeaaa23cf59b33.svg">
                        <!--[if lte IE 8]>
                        <img src="https://www-assets.carwow.co.uk/images/illustration-offers.png" alt="Illustration offers" />
                        <![endif]-->

                    </div>
                    <h5 class="heading">Гараж не продает автомобили</h5>
                    <p class="forText">
                        Мы помогаем вам найти лучшие предложения от нашей сети проверенных
                        дилеров России, но покупка осуществляется непосредственно через автосалон
                    </p>
                </div>
                <div class="item col-xs-12 col-sm-4 col-lg-4">
                    <div class="forImage">
                        <img alt="Illustration dealership" src="https://www-assets.carwow.co.uk/assets/illustration-dealership-d77d9cca47d16521e8ebd6e285ac0085c4721c0fc59d10c96a290da0e58ccdd5.svg">
                        <!--[if lte IE 8]>
                        <img width="300" height="200" src="https://www-assets.carwow.co.uk/images/illustration-dealership.png" alt="Illustration dealership" />
                        <![endif]-->

                    </div>
                    <h5 class="heading">Только официальные дилеры</h5>
                    <p class="forText">
                        Дилеры представленные на нашей платформе сертифицированы
                        производителями автомобилей. Вы можете посетить шоурум каждого из них
                    </p>
                </div>
                <div class="item col-xs-12 col-sm-4 col-lg-4">
                    <div class="forImage">
                        <img alt="Illustration trophy" src="https://www-assets.carwow.co.uk/assets/illustration-trophy-e89a0836a1d5358f20e62e66bb2ca917e99daa87142beac6129e3b9c780d4c18.svg">
                        <!--[if lte IE 8]>
                        <img width="300" height="200" src="https://www-assets.carwow.co.uk/images/illustration-trophy.png" alt="Illustration trophy" />
                        <![endif]-->

                    </div>
                    <h5 class="heading">Безупречное качество</h5>
                    <p class="forText">
                        Мы следим за качеством обслуживания наших клиентов и не работаем с
                        неофициальными дилерами и торговыми площадками.
                    </p>
                </div>
            </div>
        </div>

        <div class="dealersDescription">
            <div class="row">
                <div class="centered">
                    <!--<h4 class="heading">carwow dealers</h4>-->
                    <div class="item">
                        <div class="forImage">
                            <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <g stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="10">
                                    <circle vector-effect="non-scaling-stroke" r="11" cy="12" cx="12"/>
                                    <path vector-effect="non-scaling-stroke" data-color="color-2" d="M18 13c0 3.3-2.7 6-6 6s-6-2.7-6-6h12zM18 9c0-1.1-.9-2-2-2s-2 .9-2 2M10 9c0-1.1-.9-2-2-2s-2 .9-2 2"/>
                                </g>
                            </svg>
                        </div>
                        <p class="forText">
                            Если вы не любите торговаться, но хотите получить выгодное предложение на покупку нового автомобиля – Garage.am поможет Вам
                        </p>
                    </div>
                    <div class="item">
                        <div class="forImage">
                            <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <g stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="10">
                                    <path vector-effect="non-scaling-stroke" d="M1 7h22v16H1z"/>
                                    <circle vector-effect="non-scaling-stroke" r="2" cy="15" cx="12"/>
                                    <path vector-effect="non-scaling-stroke" d="M17 10H7c0 1.7-1.3 3-3 3v4c1.7 0 3 1.3 3 3h10c0-1.7 1.3-3 3-3v-4c-1.7 0-3-1.3-3-3z"/>
                                    <path vector-effect="non-scaling-stroke" data-color="color-2" d="M4 4h16M8 1h8"/>
                                </g>
                            </svg>

                        </div>
                        <p class="forText">
                            Больше нет необходимости тратить время на посещение множества
                            автосалонов в поисках лучшего предложения. Вы можете быть уверены, что
                            получаете лучшую сделку на рынке перед посещением дилерского центра
                        </p>
                    </div>
                    <div class="item">
                        <div class="forImage">
                            <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <g stroke-linejoin="round" stroke-linecap="round" fill-rule="evenodd">
                                    <path vector-effect="non-scaling-stroke" d="M12 1C8.7 3.2 4.3 4.3 1 4.3 1 12 4.3 19.7 12 23c7.7-3.3 11-11 11-18.7-3.3 0-7.7-1.1-11-3.3z"/>
                                    <path vector-effect="non-scaling-stroke" d="M7.5 12l3 3 6-6"/>
                                </g>
                            </svg>

                        </div>
                        <p class="forText">
                            Иногда бывает трудно найти нужную модель в наличии. Получайте
                            предложения именно на те спецификации, которые интересны
                        </p>
                    </div>
                    <div class="item">
                        <div class="forImage">
                            <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <g stroke-linejoin="round" stroke-linecap="round" stroke-miterlimit="10">
                                    <path vector-effect="non-scaling-stroke" d="M12 1C9.2 1 7 3.2 7 6v3h10V6c0-2.8-2.2-5-5-5zM4 9h16v14H4z"/>
                                    <circle vector-effect="non-scaling-stroke" data-color="color-2" r="2" cy="15" cx="12"/>
                                    <path vector-effect="non-scaling-stroke" data-color="color-2" d="M12 17v2"/>
                                </g>
                            </svg>

                        </div>
                        <p class="forText">
                            После выбора предложения вы получаете гарантирующий сертификат,
                            обязывающий дилера продать автомобиль по указанным условиям
                        </p>
                    </div>
                    <div class="item">
                        <div class="forImage">
                            <svg class="inline-icon inline-icon--large inline-icon__outline--blue" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <g stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10">
                                    <path vector-effect="non-scaling-stroke" data-color="color-2" data-cap="butt" d="M11.3 16.5l5.4 5.4c1.4 1.4 3.6 1.4 5 0s1.4-3.6 0-5l-5-5m-6.9-1.8L6.5 6.8"/>
                                    <path vector-effect="non-scaling-stroke" data-color="color-2" d="M4.4 7.5l2.1-.7.7-2.1-2.8-2.8-2.8 2.8z"/>
                                    <path vector-effect="non-scaling-stroke" d="M22.4 4.8l-3 3-3.5-3.5 3-3c-.7-.3-1.4-.4-2.2-.3-2.8.2-5.1 2.7-5 5.6 0 .6.1 1.1.3 1.6l-9.6 8.5C1 18 .9 20.2 2.2 21.5c1.4 1.4 3.6 1.3 4.8-.2l8.5-9.6c.9.3 1.8.3 2.8.2 2-.4 3.6-1.9 4.2-3.9.3-1.1.3-2.2-.1-3.2z"/>
                                </g>
                            </svg>
                        </div>
                        <p class="forText">
                            Техническое обслуживание и гарантийный ремонт может осуществляться в
                            любом дилерском центре выбранной марки
                        </p>
                    </div>
                    <div class="item">
                        <div class="forImage">
                            <svg class="inline-icon inline-icon--large inline-icon__outline--blue" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <g stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10">
                                    <path vector-effect="non-scaling-stroke" data-color="color-2" data-cap="butt" d="M1.018 11.376c5.118 1.312 8.89 5.937 8.98 11.442m12.984-11.443c-5.118 1.313-8.89 5.938-8.98 11.443M1.75 8h20.5"/>
                                    <circle vector-effect="non-scaling-stroke" r="11" cy="12" cx="12"/>
                                    <circle vector-effect="non-scaling-stroke" r="2" cy="13" cx="12"/>
                                </g>
                            </svg>
                        </div>
                        <p class="forText">
                            Все дилеры продают автомобили непосредственно от производителя. Вы
                            можете получить предложения на автомобили, которые есть в наличии или
                            ожидают поставки в скором времени
                        </p>
                    </div>
                    <div class="item">
                        <div class="forImage">
                            <svg class="inline-icon inline-icon--large inline-icon__outline--blue" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <g stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10">
                                    <path vector-effect="non-scaling-stroke" data-cap="butt" d="M21 15.1c1.2 0 2-.7 2-2s-1-1.5-1-1.5"/>
                                    <path vector-effect="non-scaling-stroke" d="M15 9H9C7.8 6 5 7 5 7l1 2.9c-1.2.7-2.1 1.8-2.6 3.1H1v5h2.8c.7 1.2 1.8 2.2 3.2 2.7V23h3v-2h4v2h3v-2.3c2.3-.8 4-3 4-5.7 0-3.3-2.7-6-6-6z"/>
                                    <path vector-effect="non-scaling-stroke" data-color="color-2" d="M11 12h4"/>
                                    <circle vector-effect="non-scaling-stroke" data-color="color-2" r="2" cy="3" cx="13"/>
                                </g>
                            </svg>
                        </div>
                        <p class="forText">
                            Наш онлайн сервис абсолютно бесплатен для пользователей
                        </p>
                    </div>
                </div>
            </div>

        </div>



    </div>

</div>