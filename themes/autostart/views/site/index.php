<header class="indexHead">
    <!-- <div class="carsCount">752 новых авто</div>-->

    <div class="headerBanner">
        <div class="bannerBlock">
            <div class="bannerImg" style="background: url(images/2.jpg) center center no-repeat; background-size: cover;" alt=""></div>
            <div class="bannerCaption">
                <h1>Планируете покупать новую машину?</h1>
                <h2>Здесь вы сможете получить лучшую цену, просто заполнив заявку. Бесплатно</h2>
                <a href="#help" class="btn btn-primary btn-lg" data-scroll="true">Выберите автомобиль</a>
                <?=CHtml::link('Расширенный поиск',array('/car/catalog/search'),array('class'=>'btn btn-success btn-lg'))?>
            </div>
        </div>
    </div>

    <!--    <div id="topCarousel" class="carousel slide">
        <ol class="carousel-indicators">
            <li class="" data-nav-slide="1"></li>
            <li data-nav-slide="2" class="active"></li>
            <li data-nav-slide="3"></li>
        </ol>
        <div class="carousel-inner">

            <div class="item" data-nav-slide="2" style="position: absolute; left: 0px; top: 0px; width: 100%; display: block; z-index: 1000; opacity: 1;">
                <div class="slideChilde">
                    <div class="slideImg" style="background: url(images/1.jpg) center center no-repeat; background-size: cover;" alt=""></div>
                    <div class="slideCaption">
                        <h2 style="opacity: 1;">Большой выбор автомобилей</h2>
                        <a href="#help" class="btn btn-primary btn-lg" data-scroll="true">Выберите автомобиль</a>
                        <?/*=CHtml::link('Расширенный поиск',array('/car/catalog/search'),array('class'=>'btn btn-success btn-lg'))*/?>
                    </div>
                </div>
            </div>
            <div class="item" data-nav-slide="3" style="position: absolute; left: 0px; top: 0px; width: 100%; display: block; z-index: 999; opacity: 1;">
                <div class="slideChilde">
                    <div class="slideImg" style="background: url(images/2.jpg) center center no-repeat; background-size: cover;" alt=""></div>
                    <div class="slideCaption">
                        <h2 style="opacity: 0;">Продажа автомобилей от  дилеров</h2>
                        <a href="#help" class="btn btn-primary btn-lg" data-scroll="true">Выберите автомобиль</a>
                        <?/*=CHtml::link('Расширенный поиск',array('/car/catalog/search'),array('class'=>'btn btn-success btn-lg'))*/?>
                    </div>
                </div>
            </div>
        <div class="item" data-nav-slide="1" style="position: absolute; left: 0px; top: 0px; width: 100%; display: block; z-index: 998; opacity: 1;">
                <div class="slideChilde">
                    <div class="slideImg" style="background: url(images/3.jpg) center center no-repeat; background-size: cover;" alt=""></div>
                    <div class="slideCaption">
                        <h2 style="opacity: 1;">Ваш персональный выбор</h2>
                        <a href="" class="btn btn-primary btn-lg" data-scroll="true">Выберите автомобиль</a>
                        <?/*=CHtml::link('Расширенный поиск',array('/car/catalog/search'),array('class'=>'btn btn-success btn-lg'))*/?>
                    </div>
                </div>
            </div></div>
        <a class="carousel-control left" href="#topCarousel">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right" href="#topCarousel">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>-->
</header>

<div class="container algorithm" id="algorythm">
    <h2 class="singleTitleH2 text-center">Самый удобный способ купить новую машину</h2>
    <div class="whiteSpace">
        <div class="row inlineblock">
            <div class="col-xs-12 col-sm-12 col-lg-4 oneThreeImg" style="padding: 0px 3.2%;">
                <div class="forImage"></div>
                <span style="margin-left: -20px; margin-right: -20px; margin-top: 20px;">
                    <a href="#help" data-scroll="true">Выберите</a> модель автомобиля и оставьте нам заявку
                </span>
            </div>
            <div class="col-xs-12 col-sm-12 col-lg-4 oneThreeImg"  style="padding: 0px 7.9%;">
                <div class="forImage"></div>
                <span style="margin-left: -20px; margin-right: -20px; margin-top: 20px;">Мы пришлем до 5 предложений от официальных дилеров</span>
            </div>
            <div class="col-xs-12 col-sm-12 col-lg-4 oneThreeImg" style="padding: 0px 6.05%;">
                <div class="forImage"></div>
                <span style="margin-left: -20px; margin-right: -20px; margin-top: 20px;">Вам останется только выбрать наиболее привлекательное и купить автомобиль</span>
            </div>
        </div>
        <div class="alert" id="alertfb"></div>
        <div class="alert alert-info">Или <a href="#ModalPreferences" data-toggle="modal">расскажите нам</a> о ваших предпочтениях, и мы поможем с выбором автомобиля.</div>
    </div>
</div>

<div class="container" id="help">
    <h2 class="singleTitleH2 text-center">Помощь в  подборе  автомобиля</h2>
    <div class="whiteSpace">
        <ul class="avtoTabs">
            <li class="active"><a data-toggle="tab" href="#panel1" class="oneTab" aria-expanded="true">По марке</a></li>
            <li><a data-toggle="tab" href="#panel2" class="oneTab" aria-expanded="false">По кузову</a></li>
            <li><a data-toggle="tab" href="#panel3" class="oneTab" aria-expanded="false">По цене</a></li>
        </ul>
        <div class="tab-content">
            <div id="panel1" class="tab-pane fade in active">
                <div class="row">
                    <?php
                    $class = 'oneCatLink col-lg-12 col-md-12 col-sm-12 col-xs-12';
                    foreach($marks as $i=>$col)
                    {
                        ?>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <?php
                            foreach($col as $m)
                            {
                                echo CHtml::link($m->name,Yii::app()->createUrl('/car/catalog',array('mark'=>$m->alias)),array('class'=>$class,'data-other'=>'true'));
                            }
                            foreach($marksFav[$i] as $m)
                            {
                                echo CHtml::link($m->name,Yii::app()->createUrl('/car/catalog',array('mark'=>$m->alias)),array('class'=>$class,'data-other'=>'false'));
                            }
                            ?>
                        </div>
                        <?php

                    }


                    ?>

                </div>

                <div class="showBtn">
                    <div class="btn btn-default" data-btn="show">Показать все</div>
                </div>
            </div>
            <div id="panel2" class="tab-pane fade">
                <div class="row">
                    <?php
                    $i=0;
                    foreach($bodies as $body)
                    {
                        $i++;
                        ?>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                            <a href="<?=Yii::app()->createUrl('/car/catalog',array('body'=>$body->alias))?>" class="oneCarcase">
                                <?=CHtml::image(Yii::app()->getModule('car')->urlBodyPath.'/250x188_'.$body->image)?>
                                <span><?=$body->name?></span>
                            </a>
                        </div>
                        <?php
                    }
                    ?>

                </div>
            </div>
            <div id="panel3" class="tab-pane fade">

                <!-- <div class="priceRow">
                     <div class="onePriceCol">
                         <a class="onePrice" href="/car/do-600000-rub">0 <i class="rubl">o</i> - 600 000 <i class="rubl">o</i></a>
                         <a class="onePrice" href="/car/ot-600000-do-1000000-rub">600 000 <i class="rubl">o</i> - 1 000 000 <i class="rubl">o</i></a>
                         <a class="onePrice" href="car/ot-1000000-do-15000000-rub">1 000 000 <i class="rubl">o</i> - 1 500 000 <i class="rubl">o</i></a>
                     </div>
                     <div class="onePriceCol">
                         <a class="onePrice" href="car/ot-15000000-do-2000000-rub">1 500 000 <i class="rubl">o</i> - 2 000 000 <i class="rubl">o</i></a>
                         <a class="onePrice" href="car/ot-2000000-rub">более 2 000 000 <i class="rubl">o</i></a>
                     </div>
                 </div>-->

                <div class="priceRow">
                    <div class="onePriceCol">
                        <?=CHtml::link('0 <i class="rubl">o</i> - 600 000 <i class="rubl">o</i>',Yii::app()->createUrl('/car/catalog',array('price'=>'do-600000-rub')),array('class'=>'onePrice'))?>
                        <?=CHtml::link('600 000 <i class="rubl">o</i> - 1 000 000 <i class="rubl">o</i>',Yii::app()->createUrl('/car/catalog',array('price'=>'ot-600000-do-1000000-rub')),array('class'=>'onePrice'))?>
                        <?=CHtml::link('1 000 000 <i class="rubl">o</i> - 1 500 000 <i class="rubl">o</i>',Yii::app()->createUrl('/car/catalog',array('price'=>'ot-1000000-do-1500000-rub')),array('class'=>'onePrice'))?>
                    </div>
                    <div class="onePriceCol">
                        <?=CHtml::link('1 500 000 <i class="rubl">o</i> - 2 000 000 <i class="rubl">o</i>',Yii::app()->createUrl('/car/catalog',array('price'=>'ot-1500000-do-2000000-rub')),array('class'=>'onePrice'))?>
                        <?=CHtml::link('более 2 000 000 <i class="rubl">o</i>',Yii::app()->createUrl('/car/catalog',array('price'=>'ot-2000000-rub')),array('class'=>'onePrice'))?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container" id="help">
    <h2 class="singleTitleH2 text-center">Счастливые клиенты о нас</h2>
    <div class="whiteSpace">
        <div class="tab-content">
            <!-- Begin Repuso widget code -->
            <div data-repuso-grid="3902" data-website-id="0"></div>
            <script type="text/javascript" src="https://repuso.com/widgets/grid.js" async></script>
            <!-- End Repuso widget code -->
        </div>
    </div>
</div>

<div class="container" id="advantages">
    <div class="row">
        <div class="centered">
            <h3 class="heading">В чем плюсы покупки машины через онлайн сервис Гараж</h3>
            <div class="item">
                <div class="forImage col-xs-2">
                    <img src="/themes/autostart/web/img/advantage_03.png" title="" alt="">
                </div>
                <div class="forText col-xs-10">
                    <h4>Экономия времени при получении и сравнении предложений</h4>
                    <p>Нет необходимости посещать несколько дилерских центров в поисках лучшей цены. Мы переносим эту функцию в онлайн. Сравните предложения в вашем личном кабинете</p>
                </div>
            </div>
            <div class="item">
                <div class="forImage col-xs-2">
                    <img src="/themes/autostart/web/img/advantage_01.png" title="" alt="">
                </div>
                <div class="forText col-xs-10">
                    <h4>Прозрачный конкурентный процесс</h4>
                    <p>Наши дилеры предлагают максимально привлекательные условия. Вы сами выбираете предложения по цене, рейтингу и локации дилерского центра</p>
                </div>
            </div>
            <div class="item">
                <div class="forImage col-xs-2">
                    <img src="/themes/autostart/web/img/advantage_02.png" title="" alt="">
                </div>
                <div class="forText col-xs-10">
                    <h4>Возможность самостоятельного выбора</h4>
                    <p>Без предвзятости менеджера автосалона. Фильтры и сервис сравнения по характеристикам, опциям и картинкам. Обзоры ведущих автомобильных экспертов</p>
                </div>
            </div>
            <div class="item">
                <div class="forImage col-xs-3 col-sm-3 col-lg-2">
                    <img src="/themes/autostart/web/img/advantage_04.png" title="" alt="">
                </div>
                <div class="forText col-xs-9 col-sm-9 col-lg-10">
                    <h4>Единый информационный ресурс</h4>
                    <p>Актуальный каталог новых автомобилей в продаже. Полная информация по моделям – цены, характеристики, комплектации, фото, обзоры, отзывы и похожие авто</p>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="backMessage" id="backMessage">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div>
                    <!--<p>Разнообразный и богатый опыт новая модель организационной деятельности позволяет выполнять
                        важные задания по разработке новых предложений. Товарищи! реализация намеченных плановых
                        заданий в значительной степени обуславливает создание новых предложений.
                        Не следует, однако забывать, что рамки и место обучения кадров требуют от нас
                        анализа направлений прогрессивного развития.</p>-->
                </div>
                <!--<div class="callBack">
                    <h3>Обратная связь</h3>
                    <p>Если у вас есть вопросы, то используйте эту форму для связи с нами</p>
                    <?php
                /*                        $form=$this->beginWidget(
                                            'booster.widgets.TbActiveForm',
                                            array(
                                                'id'                      => 'feedback_form'
                                                , 'type'                    => 'horizontal'
                                            )
                                        );
                                    */?>

                    <div class="alert" id="alertfb" style="display:none;">
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-xs-3 control-label">Ваше имя:</label>
                        <div class="col-xs-9">
                            <input type="text" class="form-control input-lg" id="name" name="name" value="" placeholder="Введите имя">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-xs-3 control-label">E-mail:</label>
                        <div class="col-xs-9">
                            <input type="email" class="form-control input-lg" id="email" name="email" value="" placeholder="Введите email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-xs-3 control-label">Тема:</label>
                        <div class="col-xs-9">
                            <select name='subject' value='<?/*=Yii::app()->params['feedBackThemes'][0]*/?>' id="subject" class="form-control input-lg">
                                <?php
                /*                                    foreach(Yii::app()->params['feedBackThemes'] as $theme)
                                                    {
                                                        echo "<option value='".$theme."'>".$theme."</option>";
                                                    }
                                                */?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-xs-3 control-label">Сообщение:</label>
                        <div class="col-xs-9">
                            <textarea name='text' id="text" class="form-control input-lg"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-9 col-xs-offset-3">
                            <?/*=CHtml::Button('Отправить',array('class'=>'btn btn-success','id'=>'feedback')  )*/?>
                        </div>
                    </div>

                    <?php
                /*                    $this->endWidget();
                                    */?>
                </div>
-->
            </div>
        </div>
    </div>
</div>