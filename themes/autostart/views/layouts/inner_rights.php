<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

        <title>АвтоСтарт</title>
        <link href='https://fonts.googleapis.com/css?family=PT+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

        <?php
            $mainAssets = Yii::app()->assetManager->publish(Yii::app()->theme->basePath ."/web/");

            Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/style.css');
            Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/script.js');  
            Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/addstyle.css');
            
            /*Yii::app()->getClientScript()->registerCssFile('//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');*/
            //Yii::app()->getClientScript()->registerScriptFile('//code.jquery.com/jquery-1.10.2.js');  
            /*Yii::app()->getClientScript()->registerScriptFile('//code.jquery.com/ui/1.11.4/jquery-ui.js');  */
            //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/bootstrap.min.js');  
            //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/jquery.mxslider.min.js');  
        ?>
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class='clearfix'>
        <div class="assideMargin clearfix">
            <?php $this->renderPartial('//layouts/_header_inner'); ?>
            <div class="row">
                <?php $this->renderPartial('//layouts/_sidebar_inner'); ?>
                <div class="col-lg-9 col-md-9 col-sm-9 col-lg-12">
                    <div class="whiteSpace">
                        <?php $this->beginContent(Rights::module()->appLayout); ?>

<div id="rights" class="container">

    <div id="content">

        <?php if( $this->id!=='install' ): ?>

            <div id="menu">

                <?php $this->renderPartial('/_menu'); ?>

            </div>

        <?php endif; ?>

        <?php $this->renderPartial('/_flash'); ?>

        <?php echo $content; ?>

    </div><!-- content -->

</div>

<?php $this->endContent(); ?>
                    </div>
                </div>
            </div>            
        </div>
    </body>
</html>