<!-- BEGIN Google analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-87116844-1', 'auto');
    ga('send', 'pageview');
</script>
<!-- END Google analytics -->
<!-- REFERSION -->
<?php Yii::app()->request->requestUri;

$order = Yii::app()->session->get("order_id");
$sku = Yii::app()->session->get("sku");
$name = Yii::app()->session->get("first_name");
$email = Yii::app()->session->get("email");

if (Yii::app()->request->requestUri == "/thanks") {
    ?>
    <!-- REFERSION TRACKING: BEGIN -->
    <script src="//garageam.refersion.com/tracker/v3/pub_ce957296bdabe42d8856.js"></script>
    <script>
        _refersion(function(){

            _rfsn._addTrans({
                'order_id': '<?php echo $order;?>',
                'currency_code': 'USD'
            });

            _rfsn._addCustomer({
                'first_name': '<?php echo $name;?>',
                'email': '<?php echo $email;?>',
            });

            _rfsn._addItem({
                'sku': '<?php echo $sku;?>',
                'quantity': '1',
                'price': '0'
            });

            _rfsn._sendConversion();

        });
    </script>
    <!-- REFERSION TRACKING: END -->
<?php }else { ?>

    <!-- REFERSION TRACKING: BEGIN -->
    <script src="//garageam.refersion.com/tracker/v3/pub_ce957296bdabe42d8856.js"></script>
    <script>_refersion();</script>
    <!-- REFERSION TRACKING: END -->



<?php } ?>