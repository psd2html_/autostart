<footer>
    <div class="row">

    </div>
</footer>
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Оставить заявку</h4>
            </div>
            <form action=''>
                <div class="modal-body">
                    <input type="hidden" name="inputComplectation" id="inputComplectation" required="required">
                    <div class="form-group form-group-lg has-feedback">
                        <label for="inputName">Имя</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" name="inputName" class="form-control" id="inputName" required="required" placeholder="Введите имя" pattern="[А-Яа-яA-Za-zё]{2,}">
                        </div>
                        <span class="glyphicon form-control-feedback"></span>
                    </div>
                    <div class="form-group form-group-lg has-feedback">
                        <label for="inputPhone">Телефон</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                            <input type="tel" name="inputPhone" class="form-control" id="inputPhone" required="required" placeholder="Введите телефон">
                        </div>
                        <span class="glyphicon form-control-feedback"></span>
                    </div>
                    <div class="form-group form-group-lg has-feedback">
                        <label for="inputAddress">Адрес</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                            <input type="tel" name="inputAddress" class="form-control" id="inputAddress" required="required" placeholder="Введите адрес">
                        </div>
                        <span class="glyphicon form-control-feedback"></span>
                    </div>
                    <div class="form-group form-group-lg has-feedback">
                        <label for="inputMail">Email</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                            <input type="email" name="inputMail" required="required" class="form-control" id="inputMail" placeholder="Введите email">
                        </div>
                    </div>
                    <label for="inputComment">Комментарий</label>
                    <textarea class="form-control" id="inputComment" name="inputComment" placeholder="Введите комментарий"></textarea>
                </div>
                <div class="modal-footer text-center">
                    <a href="#"  class="btn btn-success" id='sendNow'>Отправить</a>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Оставить заявку</h4>
            </div>
            <form action=''>
                <div class="modal-body">
                    <input type="hidden" name="inputComplectation" id="inputComplectation" required="required">
                    <div class="form-group form-group-lg has-feedback">
                        <label for="inputName">Имя</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" name="inputName" class="form-control" id="inputName" required="required" placeholder="Введите имя" pattern="[А-Яа-яA-Za-zё]{2,}">
                        </div>
                        <span class="glyphicon form-control-feedback"></span>
                    </div>
                    <div class="form-group form-group-lg has-feedback">
                        <label for="inputPhone">Телефон</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                            <input type="tel" name="inputPhone" class="form-control" id="inputPhone" required="required" placeholder="Введите телефон">
                        </div>
                        <span class="glyphicon form-control-feedback"></span>
                    </div>
                    <div class="form-group form-group-lg has-feedback">
                        <label for="inputAddress">Адрес</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                            <input type="tel" name="inputAddress" class="form-control" id="inputAddress" required="required" placeholder="Введите адрес">
                        </div>
                        <span class="glyphicon form-control-feedback"></span>
                    </div>
                    <div class="form-group form-group-lg has-feedback">
                        <label for="inputMail">Email</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                            <input type="email" name="inputMail" required="required" class="form-control" id="inputMail" placeholder="Введите email">
                        </div>
                    </div>
                    <label for="inputComment">Комментарий</label>
                    <textarea class="form-control" id="inputComment" name="inputComment" placeholder="Введите комментарий"></textarea>
                </div>
                <div class="modal-footer text-center">
                    <a href="#"  class="btn btn-success" id='sendNow'>Отправить</a>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->renderPartial('//layouts/code_google'); ?>
<?php $this->renderPartial('//layouts/code_rfsn'); ?>
