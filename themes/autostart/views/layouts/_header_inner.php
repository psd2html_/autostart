<div class="topOffice">
    <div class="whiteSpace">
    
    <a href="<?=Yii::app()->getBaseUrl(true);?>" class="logo col-lg-2 col-md-2 col-sm-3 col-xs-3">
        <img class="inner-logo" src="/images/garage.png" alt="">
    </a>
        <div class="btn-md btn btn-primary pull-right"><?=CHtml::link('Выход', array('/user/logout'))?></div>
        <?=CHtml::link('Профиль',array('/user/profile'),array('class'=>'topOfficeInfo'))?>
        <span class='topOfficeInfo'>Добрый день, <?=CHtml::link(Yii::app()->user->name,array('/user/profile'))?></span>
    </div>
</div>
