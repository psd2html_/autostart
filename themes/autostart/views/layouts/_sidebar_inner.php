<div class="col-lg-3 col-md-3 col-sm-3 col-lg-12">
    <div class="whiteSpace">
        <?php
            $this->widget(
                'booster.widgets.TbMenu',
                array(
                    'type' => 'list',
                    'items' => Yii::app()->getModule('backend')->sidebar,
                )
            );
        ?>
    
    </div>
</div>
