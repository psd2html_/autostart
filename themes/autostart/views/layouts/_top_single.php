<?php
$modName = str_replace(".", "-", number_format(floatval($model->mods->name), 1));
?>
<div class="topLinks">
        <a href="#design" class="toFixed" data-scroll="true">Комплектации и цены</a>
        <a href="#photos" class="toFixed" data-scroll="true">Фото</a>
        <a href="#views" class="toFixed" data-scroll="true">Обзоры</a>
        <a href="#otherModels" class="toFixed" data-scroll="true">Похожие модели</a>
        <a href="#otherModels" class="toFixed" data-scroll="true">Похожие модели</a>
        <a href="#offerFromDealer" data-toggle="modal" class="toFixed"><strong>Получить предложения</strong></a>
        <?php // CHtml::link('Характеристики и комплектации',Yii::app()->createUrl('/car/catalog/character',array('mark' => $model->mark->alias, 'model' => $model->model->alias, 'mod' => $modName, 'id' => $model->id)), array('class'=>'toFixed'))?>

    </div>
</div>
    <?php
        $img = $model->generation->getHeadImage($model->mods->id_car_body);
        $src = Yii::app()->getModule('car')->defaultCategoryImage;
        if($img)
        {
            if(!empty($img->image))
            {
                $src = Yii::app()->getModule('car')->urlGenPath.$img->image;
            }
        }
    ?>

<header class="singleHead" style="visibility: visible; background: url(<?=$src?>) 50% 50% / cover no-repeat;">
    <div class="singleHeadTitle">
        <?=CHtml::link($model->mark->name,Yii::app()->createUrl('/car/catalog',array('mark'=>$model->mark->alias)),array('class'=>'headMark'))?>
        <h1><?=$model->model->name?></h1>

        <div class="headPrice" style="margin-bottom:10px;"><?=$model->mods->body->name?>, <?=$model->generation->year_begin?> - <?=($model->generation->year_end?$model->generation->year_end:"н.в.")?></div>
        <div class="headPrice">от <?=number_format((!empty($model->minPriceSelect))?$model->minPriceSelect:$model->getMinPrice(), 0, ',', ' ')?>  <i class="rubl">o</i></div>
    </div>
</header>