<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="keywords" content ="<?=(isset($this->keywords)?$this->keywords:'')?>"/>
	<meta name="description" content ="<?=(isset($this->description)?$this->description 
:'')?>"/>
 	<title><?=(isset($this->title)?$this->title:Yii::app()->name)?></title>	
<link href="/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />

      

        <?php
            $mainAssets = Yii::app()->assetManager->publish(Yii::app()->theme->basePath ."/web/");

            Yii::app()->getClientScript()->registerCssFile('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css');
            Yii::app()->getClientScript()->registerCssFile('https://fonts.googleapis.com/css?family=PT+Sans&subset=latin,cyrillic');
            Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/style.css');
            Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/addstyle.css');
            Yii::app()->getClientScript()->registerScriptFile('http://code.jquery.com/jquery-1.10.2.js',CClientScript::POS_HEAD);  
            Yii::app()->getClientScript()->registerScriptFile('http://code.jquery.com/ui/1.11.4/jquery-ui.js',CClientScript::POS_HEAD);  
            Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/jquery.cookie.js',CClientScript::POS_HEAD);  
            Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/script.js');  
            Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/addscript.js');  
            
            /*Yii::app()->getClientScript()->registerCssFile('//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');*/
            //Yii::app()->getClientScript()->registerScriptFile('//code.jquery.com/jquery-1.10.2.js');  
            /*Yii::app()->getClientScript()->registerScriptFile('//code.jquery.com/ui/1.11.4/jquery-ui.js');  */
            //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/bootstrap.min.js');  
            //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/jquery.mxslider.min.js');  
        ?>
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <?php $this->renderPartial('//layouts/code_mixpanel'); ?>
        
        <meta name="google-site-verification" content="Rc2Nimp-0B7sPvTykCFgBhlUFEego3d27TrOTFeFSAY" />

    </head>
    <body class='clearfix'>

        <?php $this->renderPartial('//layouts/code_yandex'); ?>
        <?php $this->renderPartial('//layouts/_header'); ?>
        
        <?php echo $content;?>
        <?php $this->renderPartial('//layouts/_footer'); ?>
        <script type="text/javascript" src="https://static.leaddyno.com/js"></script>
        <script>
            LeadDyno.key = "e3ead4a918650fe458ad60ea2bd74b7fd3322dd2";
            LeadDyno.recordVisit();
            LeadDyno.autoWatch();
        </script>
        <!-- END JLeaddyno -->

        <!-- BEGIN JIVOSITE CODE -->
        <script type='text/javascript'>
            (function(){ var widget_id = '6Qk902dRTf';var d=document;var w=window;function l(){ var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
        </script>
        <!-- END JIVOSITE CODE -->
    </body>
</html>
