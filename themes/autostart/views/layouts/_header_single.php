<div class="topPanel">
    <a href="<?=Yii::app()->getBaseUrl(true);?>" class="logo col-lg-2 col-md-2 col-sm-6 col-xs-6">
        <img class="white-logo" src="/images/garage-white.png" alt="">
        <img class="black-logo" src="/images/garage.png" alt="">
    </a>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6"></div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center">
        <form action="<?=Yii::app()->createUrl('/car/catalog/search')?>">
            <input type="text" autocomplete="off" id="livesearch" name="search" class="glyphicon glyphicon-search" placeholder="Поиск">
            <span aria-hidden="true" class="glyphicon glyphicon-search"></span>
            <ul id="search-list">
            </ul>
        </form>
    </div>

    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-center">
        <span class="phone">+7 495 178 09 57</span>
    </div>

    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-right topFav">
        <?php
        $favs = array();
        if(isset(Yii::app()->request->cookies['favorites']))
        {
            $cookie = Yii::app()->request->cookies['favorites']->value;
            $favs = json_decode($cookie);
        }
        ?>
        <!--<a href="<?/*=Yii::app()->createUrl("/car/catalog/favorites")*/?>"><span class="glyphicon glyphicon-heart-empty"></span> Избранное <span class="cnt-fav"><?/*=count($favs)*/?></span></a>-->
    </div>

