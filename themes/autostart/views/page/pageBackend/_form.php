<?php
/* @var $this PageController */
/* @var $model Page */
/* @var $form CActiveForm */
?>

<div class="form">

<?php     $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'verticalForm',
            'htmlOptions' => array('class' => 'well', 'enctype'=>'multipart/form-data'), // for inset effect
            
        )
    ); ?>

		<?php echo $form->textFieldGroup($model,'title_short',array('size'=>60,'maxlength'=>150)); ?>


		<?php echo $form->textFieldGroup($model,'title',array('size'=>60,'maxlength'=>250)); ?>



		<?php echo $form->textFieldGroup($model,'slug',array('size'=>60,'maxlength'=>150)); ?>



		<?php 
                echo $form->ckEditorGroup(
                    $model,
                    'body',
                    array(
                           'wrapperHtmlOptions' => array(
                            /* 'class' => 'col-sm-5', */
                        ),
                        'widgetOptions' => array(
                            'editorOptions' => array(
                                'fullpage' => 'js:true',
                                /* 'width' => '640', */
                                /* 'resize_maxWidth' => '640', */
                                /* 'resize_minWidth' => '320'*/
                            )
                        )
                    )
                );
            ?>


		<?php echo $form->textFieldGroup($model,'keywords',array('size'=>60,'maxlength'=>250)); ?>



		<?php echo $form->textAreaGroup($model,'description',array('size'=>60,'maxlength'=>250)); ?>


        <?php echo $form->dropDownListGroup(
            $model,
            'status',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'widgetOptions' => array(
                    'data' => array('0'=>'Нет','1'=>'Да'),
                    'htmlOptions' => array(),
                )
            )
        ); 

    $this->widget(
        'booster.widgets.TbButton',
        array('buttonType' => 'submit', 'label' => 'Сохранить', 'context' => 'success')
    );
    ?>
<?php $this->endWidget(); ?>

</div><!-- form -->