<?php
/* @var $this PageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pages',
);

$this->menu=array(
	array('label'=>'Create Page', 'url'=>array('create')),
	array('label'=>'Manage Page', 'url'=>array('admin')),
);
?>

<h1>Страницы</h1>

<?php 
    echo CHtml::link('Добавить',array('/backend/page/page/create'),array('class'=>'btn btn-primary pull-right'));
    
    $gridColumns = array(
        array('name'=>'id', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 60px')),
        array('name'=>'title', 'header'=>'Заголовок'),
        array('name'=>'slug', 'header'=>'Алиас'),
        array('name'=>'status', 'header'=>'Статус',             
            'value'=>'$data->status?"Да":"Нет"'
        ),
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{view} {update} {delete}',
        ),
    );
    $this->widget(
        'booster.widgets.TbGridView',
        array(
            'dataProvider' => $model->search(),
            'template' => "{items}{pager}",
            'columns' => $gridColumns,
            'filter' => $model,
        )
    ); 
