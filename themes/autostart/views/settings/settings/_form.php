<?php
/* @var $this SettingsController */
/* @var $model Settings */
/* @var $form CActiveForm */
?>

<div class="form">

<?php /*$form=$this->beginWidget('CActiveForm', array(
	'id'=>'settings-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); */?>

<?php     $form = $this->beginWidget(
	'booster.widgets.TbActiveForm',
	array(
		'id' => 'verticalForm',
		'htmlOptions' => array('class' => 'well ', 'enctype'=>'multipart/form-data'), // for inset effect

	)
); ?>

	<div class="row">

		<div class="col-xs-6">
			<?php echo $form->textFieldGroup($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		</div>

		<div class="col-xs-6">
			<?php echo $form->textFieldGroup($model,'value',array('size'=>60,'maxlength'=>255)); ?>
		</div>


<!--	<div class="row">
		<?php /*echo $form->labelEx($model,'name'); */?>
		<?php /*echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); */?>
		<?php /*echo $form->error($model,'name'); */?>
	</div>

	<div class="row">
		<?php /*echo $form->labelEx($model,'value'); */?>
		<?php /*echo $form->textField($model,'value',array('size'=>60,'maxlength'=>255)); */?>
		<?php /*echo $form->error($model,'value'); */?>
	</div>

	<div class="row buttons">
		<?php /*echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); */?>
	</div>-->

	<?php
	$this->widget(
		'booster.widgets.TbButton',
		array('buttonType' => 'submit', 'label' => 'Сохранить', 'context' => 'success')
	);
	?>
<?php $this->endWidget(); ?>

</div><!-- form -->