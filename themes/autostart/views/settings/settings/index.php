
<h1>Настройки</h1>

<?php
echo CHtml::link('Добавить',array('/settings/settings/create'),array('class'=>'btn btn-primary pull-right'));

$gridColumns = array(
	array('name'=>'id', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 60px')),
	array('name'=>'name', 'header'=>'Название'),
	array('name'=>'value', 'header'=>'Значение'),
	array(
		'class'=>'booster.widgets.TbButtonColumn',
		'template'=>'{view} {update} {delete}',
	),
);
$this->widget(
	'booster.widgets.TbGridView',
	array(
		'dataProvider' => $model->search(),
		'template' => "{items}{pager}",
		'columns' => $gridColumns,
		'filter' => $model,
	)
);

?>