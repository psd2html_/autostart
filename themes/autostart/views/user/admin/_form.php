<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>true,
    'type' => 'horizontal',
	'htmlOptions' => array('enctype'=>'multipart/form-data', 'class' => 'well'),
));


?>

	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary(array($model,$profile)); ?>

	<div class="row">         
		<?php echo $form->textFieldGroup($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->passwordFieldGroup($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->textFieldGroup($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->dropDownListGroup($model,'superuser',array('widgetOptions' => array(
                    'data' => User::itemAlias('AdminStatus'),
                    'htmlOptions' => array(),
                ))); ?>
	</div>

	<div class="row">
		<?php echo $form->dropDownListGroup($model,'status',array('widgetOptions' => array(
                    'data' => User::itemAlias('UserStatus'),
                    'htmlOptions' => array(),
                ))); ?>
	</div>
<?php 
		$profileFields=Profile::getFields();
		if ($profileFields) {
			foreach($profileFields as $field) {
			?>
	<div class="row">
		<?php 
		if ($widgetEdit = $field->widgetEdit($profile)) {
			echo $widgetEdit;
		} elseif ($field->range) {
			echo $form->dropDownListGroup($profile,$field->varname,array('widgetOptions' => array(
                    'data' => Profile::range($field->range),
                    'htmlOptions' => array(),
                )));
		} elseif ($field->field_type=="TEXT") {
			echo CHtml::textAreaGroup($profile,$field->varname,array('rows'=>6, 'cols'=>50));
		} else {
			echo $form->textFieldGroup($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
		}
		 ?>
	</div>
			<?php
			}
		}
?>
	<div class="row buttons">
		<?php     $this->widget(
        'booster.widgets.TbButton',
        array('buttonType' => 'submit', 'label' => 'Сохранить', 'context' => 'success')
    ); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->