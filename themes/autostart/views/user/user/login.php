<?php
    $this->layout='/layouts/main';

?>
<div class="singleForm">
    <div class="loginForm">
        <div class="whiteSpaceLogin">
            <h1 class="loginTitle">Вход</h4>
            <?php if(Yii::app()->user->hasFlash('loginMessage')): ?>
                <div class="success">
                    <?php echo Yii::app()->user->getFlash('loginMessage'); ?>
                </div>
            <?php endif; ?>

            <?php $form = $this->beginWidget(
                        'booster.widgets.TbActiveForm',
                        array(
                            'id'                     => 'category-form',
                            'enableAjaxValidation'   => false,
                            'enableClientValidation' => true,
                            'type'                   => 'horizontal',
                            'htmlOptions'            => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
                        )
                    ); ?>

                
                <?php echo $form->textFieldGroup($model, 'username', array('label'=>UserModule::t("username"))); ?>

                <?php echo $form->textFieldGroup($model, 'password'); ?>
                
                <div class="form-group form-group-lg">
                    <div class="col-xs-offset-3 col-xs-9">
                        <?php  $this->widget(
                        'booster.widgets.TbButton',
                        array('buttonType' => 'submit', 'label' => UserModule::t("Login"), 'htmlOptions' => array('class'=>'btn btn-success btnValid'))
                    ); ?>
                    </div>
                </div>
                
            <?php $this->endWidget(); ?>
            
        </div>
    </div>
</div>
