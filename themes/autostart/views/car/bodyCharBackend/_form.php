<?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'verticalForm',
            'htmlOptions' => array('class' => 'well'), // for inset effect
            
        )
    );
     
    echo $form->dropDownListGroup(
        $model,
        'id_car_body',
        array(
            'widgetOptions' => array(
                'data' => CHtml::listData(Body::model()->findAll(),'id_car_body', 'name'),
                'htmlOptions' => array(
                    'encode' => false,
                ),
            ),
        )
    );
    
    // создаем критерии выборки
    $criteria=new CDbCriteria();
    $criteria->distinct=true;
    $criteria->select='value';    
    $criteria->compare('id_car_characteristic',2);
    $criteria->order='value';


    echo $form->dropDownListGroup(
        $model,
        'id_car_char',
        array(
            'widgetOptions' => array(
                'data' => CHtml::listData(CharacteristicValue::model()->findAll($criteria),'value', 'value'),
                'htmlOptions' => array(
                    'encode' => false,
                ),
            ),
        )
    ); 

    $this->widget(
        'booster.widgets.TbButton',
        array('buttonType' => 'submit', 'label' => 'Сохранить', 'context' => 'success')
    );
    
    echo " ".CHtml::link('Отмена',array('/backend/car/bodyChar/index'),array('class'=>'btn btn-danger ')) ;
    $this->endWidget();
    unset($form);
?>