
<h1>Типы кузовов</h1>

<?php                                      
    echo CHtml::link('Добавить',array('/backend/car/bodyChar/create'),array('class'=>'btn btn-primary pull-right'));
    
    $gridColumns = array(
        array('name'=>'id_car_body', 'header'=>'Тип кузова','value'=>'$data->body->name',
            'filter' =>CHtml::activeDropDownList($model, 'id_car_body',  
                CHtml::listData(Body::model()->findall(), 'id_car_body', 'name'),
                array('class' => 'form-control',
                    'empty' => 'Все типы'
                ),
                array('class' => 'form-control'
                )
            ), 
        ),
        array('name'=>'id_car_char', 'header'=>'Тип кузова из характеристик'),
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{update} {delete}',
        ),
    );
    $this->widget(
        'booster.widgets.TbGridView',
        array(
            'dataProvider' => $model->search(),
            'template' => "{items}{pager}",
            'columns' => $gridColumns,
            'filter' => $model,
        )
    ); 
    
?>