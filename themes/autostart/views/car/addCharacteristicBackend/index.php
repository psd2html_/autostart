<h1>Дополнительные Характеристики</h1>
                                   
<?php                               
    echo CHtml::link('Добавить',array('/backend/car/addCharacteristic/create'),array('class'=>'btn btn-primary pull-right'));

    $gridColumns = array(
        array('name'=>'id_car_characteristic', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 60px')),
        array('name'=>'name', 'header'=>'Характеристика'),
        array('name'=>'parent', 'header'=>'Родительская категория', 'value'=>'$data->parent?$data->parent->name:""'),
        array('name'=>'id_car_type', 'header'=>'Тип авто','value'=>'$data->type->name',
            'filter'=>CHtml::activeDropDownList($model,'id_car_type',  
                CHtml::listData(Type::model()->findall(), 'id_car_type', 'name'),
                array(
                    'class' => 'form-control'
                )
            ),
        ),
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{update} {delete}',
        ),
    );
    



    
    $this->widget(
        'booster.widgets.TbGridView',
        array(
            'dataProvider' => $model->search(),
            'template' => "{items}{pager}",
            'columns' => $gridColumns,
            'filter' => $model,
        )
    ); 
    
?>