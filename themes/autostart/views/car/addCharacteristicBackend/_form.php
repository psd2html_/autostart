<?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'verticalForm',
            'htmlOptions' => array('class' => 'well'), // for inset effect
            
        )
    );                                        
    //id_car_characteristic, name, id_parent, id_car_type'
     
    echo $form->textFieldGroup($model, 'name');
    echo $form->dropDownListGroup(
        $model,
        'id_parent',
        array(
            'widgetOptions' => array(
                'data' => CHtml::listData(AddCharacteristic::model()->findAll(),'id_car_characteristic', 'name'),
                'htmlOptions' => array(
                    'encode' => false,
                    'empty' => ' --- ',
                ),
            ),
        )
    ); 
    echo $form->dropDownListGroup(
        $model,
        'id_car_type',
        array(
            'widgetOptions' => array(
                'data' => CHtml::listData(Type::model()->findAll(),'id_car_type', 'name'),
                'htmlOptions' => array(
                    'encode' => false,
                ),
            ),
        )
    ); 
    
    $this->widget(
        'booster.widgets.TbButton',
        array('buttonType' => 'submit', 'label' => 'Сохранить', 'context' => 'success')
    );
    
    echo " ".CHtml::link('Отмена',array('/backend/car/addCharacteristic/index'),array('class'=>'btn btn-danger ')) ;
    $this->endWidget();
    unset($form);
?>