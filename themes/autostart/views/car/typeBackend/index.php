
<h1>Типы автомобилей</h1>

<?php 
    $gridColumns = array(
        array('name'=>'id_car_type', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 60px')),
        array('name'=>'name', 'header'=>'Тип'),
        array(
            'template'=>' ',
            'class'=>'booster.widgets.TbButtonColumn',
        )
    );
    $this->widget(
        'booster.widgets.TbGridView',
        array(
            'dataProvider' => $dataProvider,
            'template' => "{items}",
            'columns' => $gridColumns,
        )
    ); 
    
?>