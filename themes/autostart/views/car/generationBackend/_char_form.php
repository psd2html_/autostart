<div role="tabpanel" class="tab-pane <?=$active?>" id="body-<?=$body->id_car_body?>">
<h3>Доп. Характеристики</h3>
    <table class="detail-view table table-striped table-condensed" id="yw0"><tbody>
<?php
            echo "<tr class='odd'><th colspan='2'>Похожие модели: ";
            $arr = array();

            foreach($model->similar as $sim)
            {
                if($body->id_car_body==$sim->id_car_body)
                {
                    if(isset($sim->id_car_generation_similar) && isset($sim->mods->generation->name))
                    
                    
                    
                        array_push($arr,array('id'=>$sim->id_car_generation_similar,'text'=>$sim->mods->generation->name." (".$sim->mods->generation->year_begin.($sim->mods->generation->year_end?(" - ".$sim->mods->generation->year_end):" - н.д.")."), ".$sim->mods->mods->body->name));
                }   
            }
            $this->widget(
                'booster.widgets.TbSelect2',
                array(
                    'asDropDownList' => false,
                    'name' => 'CarGeneration[similar]['.$body->id_car_body.']',
                    'htmlOptions' => array(
                        'multiple' => 'multiple',
                        'id'=>'select_'.$body->id_car_body,
                    ),
                    'options' => array(
                        'tags' => array(),
                        'width' => '70%',
                        'tokenSeparators' => array(',', ' '),
                        'minimumInputLength' => 2,
                        'initSelection' => 'js: function (element, callback, param) {
                            var data = $.parseJSON($(element).data("val"));
                            callback(data);
                        }',
                        'ajax' => array (
                            'url' => '/car/catalog/livesearchin',
                            'type'=>'post',
                            'data'=>'js: function(term,page) {

                                return {
                                    "s": term
                                };
                                
                            }',
                            'results' => 'js: function(data,page){ 
                                return {
                                    results: data
                                };
                            }',
                            
                          ) 
                    )
                )
            );
            
            Yii::app()->clientScript->registerScript('addVal'.$body->id_car_body, '
                $(document).ready(function(){
                    $("#select_'.$body->id_car_body.'").data("val",\''.json_encode($arr).'\');
                    setTimeout(function(){$("#select_'.$body->id_car_body.'").select2("val","test");},300);
                });
                
            ', CClientScript::POS_END);

            echo "</th></tr>";
            

            echo "<tr class='odd'><th colspan='2' style='text-align:center;'>Превью изображения</th></tr>";
            foreach($model->prev_images as $img)
            {
                if($body->id_car_body==$img->id_car_body)
                {
                    echo "<tr class='odd'><td>".($img->image?CHtml::image("/upload/car/generation/100x100_".$img->image,'',array("style"=>"float:left;")):"");
                    echo CHtml::fileField('CarGeneration[image]['.$img->id_car_characteristic_value.']', '', array('id'=>'image'))."</td></tr>";
                }   
            }
            echo "</tr>";

            
            echo "<tr class='even'><th colspan='2' style='text-align:center;'>Изображения для шапки страницы</th></tr>";
            foreach($model->head_images as $img)
            {
                if($body->id_car_body==$img->id_car_body)
                {
                    echo "<tr class='even'><td>".($img->image?CHtml::image("/upload/car/generation/100x100_".$img->image,'',array("style"=>"float:left;")):"");
                    echo CHtml::fileField('CarGeneration[image_head]['.$img->id_car_characteristic_value.']', '', array('id'=>'image_head'))."</td></tr>";
                }   
            }

            
            echo "<tr class='even'><th colspan='2' style='text-align:left;'>Цвета</th><td>";

            $tdclass = "odd";
            $parent = "";

            //var_dump($model->colorsChar);exit();
            foreach($model->colors as $color)
            {
                if($body->id_car_body==$color->id_car_body)
                {
                    echo "<tr class='".$tdclass."'><th>".$color->characteristic->name."</th><td>";
                    echo $color->image?CHtml::image("/upload/car/generation/100x100_".$color->image,'',array("style"=>"float:left;")):""; 
                    echo CHtml::fileField('CarGeneration[colors]['.$color->id_car_characteristic_value.']', '', array('id'=>'')); 
                    if($color->image)
                    {
                        echo CHtml::checkBox('CarGeneration[colorDelete]['.$color->id_car_characteristic_value.']', '', array('id'=>'delete_'.$color->id_car_characteristic_value)); 
                        echo ' '.CHtml::label('Удалить','delete_'.$color->id_car_characteristic_value);
                    }
                    echo "</td></tr>";
                }
            }
  
?>
</tbody></table>
<h3>Галерея</h3>

<?php
        echo "<div class='row'>";
        foreach($model->gallery as $key => $image)
        {
            if($image->body->id_car_body==$body->id_car_body)
            {
                echo "<div class='col-sm-2' style='min-width: 180px; height:140px;'>".CHtml::image("/upload/car/generation/100x100_".$image->image)."<br>";
                echo CHtml::dropDownList('CarGeneration[galleryChar]['.$image->id_car_generation_image.']',$image->id_car_add_characteristic, $charlist); 
                echo "<br>".CHtml::checkBox('CarGeneration[galleryDelete]['.$image->id_car_generation_image.']', '', array('id'=>'deleteGal_'.$key)); 
                echo ' '.CHtml::label('Удалить','deleteGal_'.$key)."</div>";
            }
        }
        echo "</div><div class='row'>";
        echo "<div class='col-sm-12'>Добавить изображения<br>";
        echo CHtml::fileField('CarGeneration[gallery]['.$body->id_car_body.'][]', '', array('id'=>'','multiple'=>'multiple',)); 

        //echo CHtml::link('Еще изображение','#',array('class'=>'btn btn-success','style'=>'margin-top:5px;', 'onclick'=>'event.preventDefault(); addGalBlock(this,'.$body->id_car_body.');')) ; 
        echo "</div>";

        echo "</div><br><br>";

    Yii::app()->clientScript->registerScript('addBlock', "
        function addGalBlock(elem, body)
        {
            $(elem).parent().before('<div class=\"col-sm-12\">Добавить изображение<br><input id=\"\" type=\"file\" value=\"\" name=\"CarGeneration[gallery]['+body+'][]\"></div>');
            return false;
        }
        
    ", CClientScript::POS_END);
?>

<h3>Модификации</h3>
<div class="row">
<?php
    foreach($model->modsRel as $mod)
    {
        if($body->id_car_body==$mod->mods->id_car_body)
        {
            echo "<div class='row'><div class='col-sm-12'><h4>".$mod->mods->name."</h4></div></div>";
            foreach($model->model->complectation as $compl)
            {
                    $c = ComplectationGenerationModification::model()->find('id_car_generation_modification=:id_gen and id_car_complectation=:id_compl',array(':id_gen'=>$mod->id,':id_compl'=>$compl->id_car_complectation));
                    
                if($c)
                {
                    echo "<div class='row form-group'>";
                    echo "<div class='col-sm-2 col-sm-offset-1'>";
                    echo CHtml::dropDownList('compl[]', $compl->id_car_complectation, CHtml::listData($model->model->complectation,'id_car_complectation','name'), array('class'=>'form-control', 'disabled'=>'disabled'));
                    echo "</div>";
                    echo "<div class='col-sm-4'>";
                    echo "<input class='form-control' style='width:80%; float:left;' name='compRel[".$mod->id."][".$compl->id_car_complectation."]' value='".($c?$c->price_min:'')."'>&nbsp;<span style='line-height: 35px;margin-left: 3px;'>руб</span>";
                    echo "</div>";
                echo "<div class='col-sm-5'>";
                echo CHtml::link($compl->name." (ComplID = ".$c->id_car_complectation_generation_modification.")", '/backend/car/complectation/update/'.$compl->id_car_complectation,array('style'=>'    line-height: 35px;'));
                echo "</div>";
                                    echo "</div>";
                }
            }
            
            /*foreach($mod->complectationRel as $compl)
            {
                echo "<div class='row form-group'>";
                echo "<div class='col-sm-2 col-sm-offset-1'>";
                echo CHtml::dropDownList('compl[]', $compl->complectation->id_car_complectation, CHtml::listData($model->model->complectation,'id_car_complectation','name'), array('class'=>'form-control', 'disabled'=>'disabled'));
                echo "</div>";
                echo "<div class='col-sm-2'>";
                echo "<input class='form-control' name='compRel[".$mod->id."][".$compl->id_car_complectation_generation_modification."]' value='".$compl->price_min."'>";
                echo "</div>";
                echo "</div>";
            }*/
        }
    }
?>
</div>
</div>