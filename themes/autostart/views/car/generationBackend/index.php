<h1>Поколения автомобилей</h1>
                                    
<?php                               
    echo CHtml::link('Добавить',array('/backend/car/generation/create'),array('class'=>'btn btn-primary pull-right'));
    $gridColumns = array(
        array('name'=>'id_car_generation', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 60px')),
        array('name'=>'name', 'header'=>'Поколение'),
        array('name'=>'mark', 'header'=>'Марка','value'=>'$data->model->mark->name',
            'filter' =>CHtml::activeDropDownList($model, 'mark',  
                CHtml::listData(Mark::model()->findall(array('order'=>'name')), 'id_car_mark', 'name'),
                array('class' => 'form-control',
                    'empty' => 'Все марки'
                )
            ),
        ),
        array('name'=>'model', 'header'=>'Модель','value'=>'$data->model->name'),
        array('name'=>'year_begin', 'header'=>'Начало производства'),
        array('name'=>'year_end', 'header'=>'Окончание производства'),
        array('name'=>'is_visible', 'header'=>'Видимость',             
            'value'=>'$data->is_visible?"Да":"Нет"',
            'filter' =>CHtml::activeDropDownList($model, 'is_visible',  
                array(
                    ''=>'Все',
                    '1'=>'Да',
                    '0'=>'Нет',
                ),
                array('class' => 'form-control'
                )
            ), 
        ),
        array('name'=>'id_car_type', 'header'=>'Тип авто','value'=>'$data->type->name',
            'filter'=>CHtml::activeDropDownList($model,'id_car_type',  
                CHtml::listData(Type::model()->findall(), 'id_car_type', 'name'),
                array(
                    'class' => 'form-control'
                )
            ),
        ),
        array(  
            'template'=>' {update} {delete}',
            'class'=>'booster.widgets.TbButtonColumn',
            
        )
    );
    



    
    $this->widget(
        'booster.widgets.TbGridView',
        array(
            'dataProvider' => $model->search(),
            'template' => "{items}{pager}",
            'columns' => $gridColumns,
            'filter' => $model,
        )
    ); 
    
?>