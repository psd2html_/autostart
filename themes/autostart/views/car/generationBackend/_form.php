<?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'verticalForm',
            'htmlOptions' => array('class' => 'well', 'enctype'=>'multipart/form-data'), // for inset effect
            
        )
    );
    ?>
<?php 

    echo $form->textFieldGroup($model, 'name');
    echo $form->checkboxGroup($model, 'is_visible');
    echo '<label class="control-label required">Марка <span class="required">*</span></label>';
    echo CHtml::dropDownList(
        'id_car_mark',
        !$model->isNewRecord?$model->mark->id_car_mark:'',
        CHtml::listData(Mark::model()->findAll(array('order'=>'name')),'id_car_mark', 'name'),
        array('class'=>'form-control', 'empty'=>'',
            'ajax'=>array(
                'type'=>'POST', //request type
                'url'=>CController::createUrl('/car/catalog/getmodellist',array('dropdown'=>1)), //url to call.
                'data'=>array(
                    'mark'=>'js:this.value',
                ),
                'update'=>'#Generation_id_car_model'))
    );

    echo $form->dropDownListGroup(
        $model,
        'id_car_model',
        array(
            'widgetOptions' => array(
                'data' => $model->isNewRecord?array():CHtml::listData(CarModel::model()->findAll(array('condition'=>'id_car_mark=:id_car_mark','params'=>array(':id_car_mark'=>$model->mark->id_car_mark),'order'=>'name')),'id_car_model', 'name'),
                'htmlOptions' => array(
                    'encode' => false,
                ),
            ),
        )
    );    
    echo $form->dropDownListGroup(
        $model,
        'id_car_type',
        array(
            'widgetOptions' => array(
                'data' => CHtml::listData(Type::model()->findAll(),'id_car_type', 'name'),
                'htmlOptions' => array(
                    'encode' => false,
                ),
            ),
        )
    );    
    echo $form->textFieldGroup($model, 'year_begin');
    echo $form->textFieldGroup($model, 'year_end');
 
    echo '<div class="form-group"><label class="control-label" for="introtext">Описание</label>';

    $this->widget(
        'booster.widgets.TbCKEditor',
        array(
            'name' => 'CarGeneration[introtext]',
            'value' => $model->introtext?$model->introtext->value:'',
        )
    );
    echo "</div>";
            
    echo '<div class="form-group"><label class="control-label" for="description">Обзоры</label>';

    $this->widget(
        'booster.widgets.TbCKEditor',
        array(
            'name' => 'CarGeneration[description]',
            'value' => $model->description?$model->description->value:'',
        )
    );
    echo "</div>";

    echo '<div class="form-group"><label class="control-label" for="review">Отзывы</label>';

    $this->widget(
        'booster.widgets.TbCKEditor',
        array(
            'name' => 'CarGeneration[review]',
            'value' => $model->review?$model->review->value:'',
        )
    );
    echo "</div>";

    
    $bodies = $model->getBodies();
    
    echo '<div><ul class="nav nav-tabs" role="tablist">';
    $active = 'active';
    foreach($bodies as $body)
    {
        echo '<li role="presentation" class="'.$active.'"><a href="#body-'.$body->id_car_body.'" aria-controls="body-'.$body->id_car_body.'" role="tab" data-toggle="tab">'.$body->name.'</a></li>';    
        $active = '';
    }
    echo '</ul><div class="tab-content">';

    $active = 'active';
    foreach($bodies as $body)
    {
        $this->renderPartial('_char_form',array('model'=>$model,'body'=>$body,'active'=>$active,'charlist'=>Generation::getAddChars())); 
        $active = '';
    }
    
    echo "</div></div>";
    $this->widget(
        'booster.widgets.TbButton',
        array('buttonType' => 'submit', 'label' => 'Сохранить', 'context' => 'success')
    );
    
    echo " ".CHtml::link('Отмена',array('/backend/car/generation/index'),array('class'=>'btn btn-danger ')) ;
    $this->endWidget();
    unset($form);
?>