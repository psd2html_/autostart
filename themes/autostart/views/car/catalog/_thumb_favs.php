<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">
    <div class="thumbnail oneCar">
        <span class="oneCarLike redLike glyphicon glyphicon-heart" data-modify="<?=$data->id_car_modification?>" data-complectation="0"></span>
        <?php
            $prev = false;
            if(!empty($data->generation->prev_images))
            {
                foreach($data->generation->prev_images as $img)
                {
                    if($img->id_car_body == $data->mainBody->id_car_body)
                    {
                        $prev = $img;
                    }
                }
            }
        ?>
        <?php
            if(isset($prev->image))
            {
        ?>
        <?=CHtml::image(!empty($prev->image)?Yii::app()->getModule('car')->urlGenPath.'350x260_'.$prev->image :Yii::app()->getModule('car')->defaultImage)?>
        <?php
            }
            else
            {
                echo  CHtml::image(Yii::app()->getModule('car')->defaultImage);
            }
        ?>
        <div class="caption">
            <h3><input type="checkbox" name="mods[]" value="<?=$data->id_car_modification?>"><a href="<?=Yii::app()->createUrl('/car/catalog/single',array('id'=>$data->id_car_modification,'mark'=>$data->mark->alias,'model'=>$data->model->alias))?>"><?=$data->mark->name." ".$data->model->name?></a></h3>
            <?php
                $body=$data->bodyName;
            ?>
            <p><?=$body?>, <?=$data->generation->year_begin.($data->generation->year_end?("-".$data->generation->year_end):"")?></p>
            <div class="catalogPrice">от <?=($data->minPriceSelect?number_format($data->minPriceSelect, 0, ',', ','):"0")?> руб.</div>
        </div>
    </div>
</div>


