<?php if (isset($model)) { ?>

<?php
$favs = array();
if(isset(Yii::app()->request->cookies['favorites']))
{
    $cookie = Yii::app()->request->cookies['favorites']->value;
    $favs = json_decode($cookie);
}
//var_dump($favs);
$obj = new stdClass();
$obj->modify = $model->id;
$obj->compl = 0;
//var_dump($obj);

$class = "";
if(in_array($obj,$favs))
{
    $class = " redLike";
}
?>

<div id="offerFromDealer" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="col-xs-12">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form role="form" action="" method="post">
                <div class="stepwizard">
                    <div class="stepwizard-row setup-panel">
                        <div class="stepwizard-step step-1 col-xs-6">
                            <a href="#step-1" type="button" class="btn btn-primary btn-circle"></a>
                            <p>Модель</p>
                        </div>
                        <!--<div class="stepwizard-step step-2 col-xs-4">
                            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
                            <p>Цвет</p>
                        </div>-->
                        <div class="stepwizard-step step-3 col-xs-6">
                            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
                            <p>Получить предложения</p>
                        </div>
                    </div>
                </div>
                <div class="row setup-content" id="step-1">
                    <div class="col-xs-12">
                        <h3 class="text-center">Выберите на какую <?php echo $model->mark->name; ?> <?php echo $model->model->name; ?> вы хотите получить предложения</h3>
                        <div class="form-group col-xs-12">
                            <div class="filterChar col-sm-6 col-xs-12">
                                <div class="singleFilterModal">
                                    <div class="singleFilterTitle">
                                        <strong>Объем двигателя:</strong>
                                    </div>
                                    <?php

                                    if (!empty($this->seoPage) &&  $this->seoPage->group_id==2) {
                                        foreach($model->generation->modsRel as $modRelVal)
                                        {
                                            $currentMod = $this->seoPage->car_volume_value;

                                        }
                                        $seoFuel = $this->seoPage->id_car_fuel;
                                        $seoPrivod = $this->seoPage->id_car_privod;

                                        if ($this->seoPage->id_car_transmission=='Механическая') {
                                            $currentTrans = 'mechanics';
                                        } else if ($this->seoPage->id_car_transmission=='Автоматическая') {
                                            $currentTrans = 'auto';
                                        } else {
                                            $currentTrans = '';
                                        }

                                    }

                                    ?>
                                    <div class="singleFilterContent">
                                        <?php
                                        foreach($model->generation->getVolume() as $volume)
                                        {
                                            echo '<label>';
                                            if (!empty($currentMod) && $currentMod==$volume) {
                                                echo '<input type="checkbox" name="volume" value="'.$volume.'" checked>';
                                            } else {
                                                echo '<input type="checkbox" name="volume" value="'.$volume.'">';
                                            }
                                            echo $volume;
                                            echo '</label>';
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="singleFilterModal">
                                    <div class="singleFilterTitle">
                                        <strong>Коробка передач:</strong>
                                    </div>
                                    <div class="singleFilterContent">
                                        <?php
                                        foreach($model->generation->getTransmission() as $key => $val)
                                        {
                                            echo '<label>';
                                            if (!empty($currentTrans) && $currentTrans==$key) {
                                                echo '<input type="checkbox" name="transmission" value="'.$key.'" checked>';
                                            } else {
                                                echo '<input type="checkbox" name="transmission" value="'.$key.'">';
                                            }
                                            echo $val;
                                            echo '</label>';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-6 col-xs-12">

                                <div>
                                    <strong>Цена</strong>
                                </div>

                                <div class="row">
                                    <div class="col-xs-6">
                                        <input id="downPrice" class="form-control input-sm" name="price-min" placeholder="от" value="<?php echo ComplectationGenerationModification::getMinPrice(); ?>" type="text">
                                    </div>
                                    <div class="col-xs-6">
                                        <input id="topPrice" class="form-control input-sm" name="price-max" placeholder="до" value="<?php echo ComplectationGenerationModification::getMaxPrice(); ?>" type="text">
                                    </div>
                                </div>

                                <div id="slider-range-modal" data-min="<?php echo ComplectationGenerationModification::getMinPrice(); ?>" data-max="<?php echo ComplectationGenerationModification::getMaxPrice(); ?>" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                                    <div class="ui-slider-range ui-widget-header ui-corner-all"></div>
                                </div>

                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="col-xs-6">
                                <strong>Модификации</strong>
                            </div>
                            <div class="col-xs-6">
                                <strong>Комплектации</strong>
                            </div>
                        </div>

                        <div class="form-group col-xs-12">
                            <div class="table-wrapper">

                                <?php foreach($model->generation->modsRel as $modRel) { ?>
                                    <?php if ($modRel->mods->id_car_body==$model->mods->id_car_body) { ?>
                                        <?php $mod = $modRel->mods; ?>
                                        <?php $modName = str_replace(".", "-", number_format(floatval($mod->name), 1)); ?>
                                        <div class="singleTable" data-fuel="<?=$mod->fuel->value?>" data-privod="<?=$mod->privod->value?>" data-new="true" data-volume="<?=round($mod->volume->value/1000,1)?>" data-transmission="<?=$mod->transmission->value=='Механическая'?'mechanics':'auto'?>">
                                            <div class="singleTableLeft">
                                                <b><?php echo $mod->name; ?></b>
                                                <span>
                                                        <?=$mod->fuel->value?>,
                                                    <?php if (isset($mod->privod) and !empty($mod->privod->value)) { echo $mod->privod->value . ' привод,'; } ?>
                                                    <?php if (isset($modRel->power) and !empty($modRel->power->value)) { echo $modRel->power->value . '  л.с.'; } ?>
                                                    </span>
                                            </div>
                                            <div class="singleTableRight">
                                                <?php
                                                foreach($modRel->complectationRel as $complRel)
                                                {
                                                    $obj = new stdClass();
                                                    $obj->modify = $modRel->id;
                                                    $obj->compl = $complRel->id_car_complectation_generation_modification;
                                                    $class = "";
                                                    if(in_array($obj,$favs))
                                                    {
                                                        $class = " active";
                                                    }
                                                    ?>
                                                    <div class="oneSTR compl" data-price="<?php echo $complRel->price_min; ?>">
                                                        <div class="STRLeft">
                                                            <input type="checkbox" name="compls[]" value="<?=$complRel->id_car_complectation_generation_modification?>">
                                                            <span><?php echo $complRel->complectation->name; ?></span>
                                                        </div>
                                                        <div class="STRRight">
                                                            от <strong><?=number_format($complRel->price_min, 0, ',', ' ')?></strong>  <i class="rubl">o</i>
                                                            <div class="singleStar <?=$class?>" data-modify="<?=$modRel->id?>" data-complectation="<?=$complRel->id_car_complectation_generation_modification?>"></div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    <?php } } ?>
                            </div>
                        </div>
                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Далее</button>
                    </div>
                </div>
                <div class="row setup-content text-center hideCheckboxBorder" id="step-2">
                    <div class="col-xs-12">
                        <h3 class="text-center">Какого бы цвета вы хотели свою <?php echo $model->mark->name; ?> <?php echo $model->model->name; ?></h3>

                        <div class="form-group form-group-md">
                            <ul class="colorsChar" style="list-style: none">
                                <?php foreach($model->generation->getColors() as $colors)  {  ?>
                                    <li class="colorsCharItem<?php echo $colors->colorsChar[0]['id_car_characteristic']; ?>">
                                        <input type="checkbox" id="color<?php echo $colors->colorsChar[0]['id_car_characteristic']; ?>" name="colorsChar[]" value="<?php echo $colors->colorsChar[0]['name']; ?>" />
                                        <label for="color<?php echo $colors->colorsChar[0]['id_car_characteristic']; ?>"></label>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="form-group form-group-md">
                            <div class="input-group" style="margin: 0 auto;">
                                <input type="text" name="inputColorCustom" class="form-control" value="" id="inputColorCustom" placeholder="или укажите другой цвет">
                            </div>
                        </div>
                        <div class="form-group form-group-md inputColorAny">
                            <div class="input-group" style="margin: 0 auto;">
                                     <span class="input-group-addon" style="width: auto;">
                                         <input type="checkbox" checked="checked" value="Любой цвет" name="inputColorAny" id="inputColorAny">
                                     </span>
                                <label for="inputColorAny" class="form-control" style="width: auto;">мне не принципиально</label>
                            </div>
                        </div>

                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Далее</button>
                    </div>
                </div>
                <div class="row setup-content formPreferences hideCheckboxBorder" id="step-3">
                    <div class="col-xs-12">
                        <h3 class="text-center">Готовы получить предложения на выбранную <?php echo $model->mark->name; ?> <?php echo $model->model->name; ?>?</h3>
                        <h4 class="text-center">Зарегистрируйтесь, чтобы сравнить лучшие предложения</h4>

                        <div class="modal-body clearfix">

                            <div class="col-xs-12">
                                <div class="col-md-6 col-xs-12">

                                    <div class="form-group form-group-md has-feedback">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <input type="radio" name="inputAddress" id="inputAddressMoscow" value="Я из Москвы или области" checked>
                                            </span>
                                            <label for="inputAddressMoscow" class="form-control">Я из Москвы или области</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-md has-feedback">
                                        <div class="addressOther input-group">
                                            <span class="input-group-addon">
                                                <input type="radio" name="inputAddress" value="2">
                                            </span>
                                            <input class="form-control" type="text" name="inputAddressOther" id="inputAddressOther" placeholder="...или введите свой регион" value="">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-lg has-feedback">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                            <input type="text" name="inputName" required="required" class="form-control" id="inputName" placeholder="Введите ФИО">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-lg has-feedback">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                            <input type="email" name="inputMail" required="required" class="form-control" id="inputMail" placeholder="Введите email">
                                        </div>
                                    </div>
                                    <!--<div class="form-group form-group-lg has-feedback">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                            <input type="password" name="inputPassword" required="required" class="form-control" id="inputPassword" placeholder="Введите пароль">
                                        </div>
                                        <span class="glyphicon form-control-feedback"></span>
                                    </div>-->
                                    <div class="form-group form-group-lg has-feedback">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                                            <input type="tel" name="inputPhone" required="required"  class="form-control" id="inputPhone" placeholder="Введите свой телефон">
                                        </div>
                                        <span class="glyphicon form-control-feedback"></span>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xs-12 inputComment">
                                    <label for="inputCarTrade" class="form-control">Другие пожeлания:</label>
                                    <textarea class="form-control" id="inputComment" name="inputComment" placeholder="Текст" rows="6"></textarea>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="form-group-md">
                                    <div class="input-group">
                                             <span class="input-group-addon">
                                                 <input type="checkbox" value="Есть автомобиль для трейд-ина" name="inputCarCredit" id="inputCarCredit">
                                             </span>
                                        <label for="inputCarCredit" class="form-control">Есть автомобиль для трейд-ина</label>
                                    </div>
                                </div>
                                <div class="form-group form-group-md">
                                    <div class="input-group">
                                             <span class="input-group-addon">
                                                 <input type="checkbox" value="Нужен кредит для покупки" name="inputCarTrade" id="inputCarTrade">
                                             </span>
                                        <label for="inputCarTrade" class="form-control">Нужен кредит для покупки</label>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <!-- <div class="modal-footer2 text-center">
                             <a href="#"  class="btn btn-success" id='sendNow1'>Готово</a>
                         </div>-->
                        <div class="text-center col-xs-12">
                            <a id="sendDealersOffer" class="btn btn-success btn-lg" type="submit">Готово</a>
                        </div>

                        <div class="text-center">
                            <span>Мы не передаем ваши контактные данные, пока вы сами этого не захотите</span>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php } ?>
