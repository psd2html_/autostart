<div class="col-lg-3 col-md-6 col-sm-12 col-xs-6">
    <div class="thumbnail oneCar">
        <?php
            $favs = array();
            if(isset(Yii::app()->request->cookies['favorites']))
            {
                $cookie = Yii::app()->request->cookies['favorites']->value;
                $favs = json_decode($cookie);
            }
            //var_dump($favs);
            $obj = new stdClass();
            $obj->modify = $data->id;
            $obj->compl = 0;
            //var_dump($obj);

            $class = "";
            if(in_array($obj,$favs))
            {
                $class = " redLike";
            }
        ?>
        <!--<span class="oneCarLike glyphicon glyphicon glyphicon-heart-empty<?/*=$class*/?>" data-modify="<?/*=$data->id*/?>" data-complectation="0"></span>-->
        <?php
            $prev = false;
            if(!empty($data->generation->prev_images))
            {
                foreach($data->generation->prev_images as $img)
                {
                    if($img->id_car_body == $data->bodySelect)
                    {
                        $prev = $img;
                    }
                }
            }
        ?>
        <div class="prev_img">
        <?php
            if(isset($prev->image))
            {
        ?>
        <a href="<?=Yii::app()->createUrl('/car/catalog/single',array('mark'=>$data->mark->alias,'model'=>$data->model->alias,'id'=>$data->id))?>">
        <?=CHtml::image(!empty($prev->image)?Yii::app()->getModule('car')->urlGenPath.'350x260_'.$prev->image :Yii::app()->getModule('car')->defaultImage)?>
        </a>
        <?php
            }
            else
            {
                echo  CHtml::image(Yii::app()->getModule('car')->defaultImage);
            }
        ?>
        </div>
        <div class="caption">
            <?php

                $body=Body::model()->findByPk($data->bodySelect);

          /*  echo '<pre>';
            print_r($data);
            die();*/

            ?>
            <h3><a href="<?=Yii::app()->createUrl('/car/catalog/single',array('mark'=>$data->mark->alias,'model'=>$data->model->alias,'id'=>$data->id))?>"><?=$data->generation->mark->name." ".$data->generation->model->name?></a></h3>
            <p><?=$body->name?>, <?=$data->generation->year_begin.($data->generation->year_end?("-".$data->generation->year_end):"")?></p>
            <div class="catalogPrice">от <?=number_format($data->minPriceSelect, 0, ',', ' ')?> <i class="rubl">o</i></div>
            <a model="<?= $data->id ?>" href="#" class="btn btn-info veryBigBtn get_modal" style="white-space:normal">Получить предложения</a>
        </div>
    </div>
</div>

<?php
//$model = GenerationModification::model()->findByPk($data->id);
if (isset($model)) { ?>

    <?php
    $favs = array();
    if(isset(Yii::app()->request->cookies['favorites']))
    {
        $cookie = Yii::app()->request->cookies['favorites']->value;
        $favs = json_decode($cookie);
    }
//var_dump($favs);
    $obj = new stdClass();
    $obj->modify = $model->id;
    $obj->compl = 0;
//var_dump($obj);

    $class = "";
    if(in_array($obj,$favs))
    {
        $class = " redLike";
    }
    ?>

<?php } ?>
