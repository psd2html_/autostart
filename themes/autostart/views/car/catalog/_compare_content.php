                        <div class="oneCompareModel" data-idx="<?=$idx?>" data-mod="<?=$mod->id_car_modification?>" data-compl="<?=$compl?$compl->id_car_complectation:0?>">
                            <div class="oneCompare onlyText">
                                <div class="removeCol"><span class="glyphicon glyphicon-remove"></span></div>
                                <a href="<?=Yii::app()->createUrl("/car/catalog/single",array("id"=>$mod->id,'mark'=>$mod->mark->alias,'model'=>$mod->model->alias))?>" class="compareTitleLink"><?=$mod->mark->name?> <?=$mod->model->name?></a>
                            </div>
                            <div class="oneCompare onlyText">
                                <?php
                                    echo CHtml::dropDownList('generation', $mod->generation->id_car_generation, $mod->model->getGenerationList(),
                                        array('class'=>'form-control input-sm','data-idx'=>$idx,'options'=>$mod->model->getGenerationMods(),
                                            'ajax'=>array(
                                                'type'=>'POST',
                                                'url'=>CController::createUrl('/car/catalog/ajaxmod'), 
                                                 
                                                'success' => 'function(htmlcode){
                                                           replaceElem(htmlcode)                                                                             
                                                        }',
                                                'data'=>array(
                                                    'mod'=>'js:getMod(this)',
                                                    'compl'=>'js:getCompl(this)',
                                                    'idx'=>'js:getIdx(this)'
                                                ),
                                            )
                                    ));
                                ?>
                            </div>
                            <div class="oneCompare onlyText">
                                <?php
                                    echo CHtml::dropDownList('body', $mod->mods->id_car_body, CHtml::listData($mod->generation->getBodies(),'id_car_body','name'),array('class'=>'form-control input-sm','data-idx'=>$idx,'options'=>$mod->generation->getBodyMods(),
                                            'ajax'=>array(
                                                'type'=>'POST',
                                                'url'=>CController::createUrl('/car/catalog/ajaxmod'), 
                                                 
                                                'success' => 'function(htmlcode){
                                                           replaceElem(htmlcode)                                                                             
                                                        }',
                                                'data'=>array(
                                                    'mod'=>'js:getMod(this)',
                                                    'compl'=>'js:getCompl(this)',
                                                    'idx'=>'js:getIdx(this)'
                                                ),
                                            )));
                                ?>
                            
                            </div>
                            <div class="oneCompare onlyText">
                                <?php
                                    echo CHtml::dropDownList('modify', $mod->id, CHtml::listData($mod->getModificationByBodyMods($mod->mods->id_car_body),'id','modName'),array('class'=>'form-control input-sm','data-idx'=>$idx,'options'=>$mod->getModificationMods(),
                                            'ajax'=>array(
                                                'type'=>'POST',
                                                'url'=>CController::createUrl('/car/catalog/ajaxmod'), 
                                                 
                                                'success' => 'function(htmlcode){
                                                           replaceElem(htmlcode)                                                                             
                                                        }',
                                                'data'=>array(
                                                    'mod'=>'js:getMod(this)',
                                                    'compl'=>'js:getCompl(this)',
                                                    'idx'=>'js:getIdx(this)'
                                                ),
                                            )));
                                ?>
                             </div>
                            <div class="oneBigCompare">
                                <?php
                                    if($mod->complectation)
                                    {
                                        echo CHtml::dropDownList('complectation', ($compl?$compl->id_car_complectation_generation_modification:0),$mod->getComplRelWithName(),array('class'=>'form-control input-sm','data-idx'=>$idx,'data-modid'=>$mod->id,
                                            'ajax'=>array(
                                                'type'=>'POST',
                                                'url'=>CController::createUrl('/car/catalog/ajaxmod'), 
                                                 
                                                'success' => 'function(htmlcode){
                                                           replaceElem(htmlcode)                                                                             
                                                        }',
                                                'data'=>array(
                                                    'mod'=>'js:getMod(this)',
                                                    'compl'=>'js:getCompl(this)',
                                                    'idx'=>'js:getIdx(this)'
                                                ),
                                            )));
                                    }
                                    else
                                    {
                                        echo CHtml::dropDownList('complectation', 0, CHtml::listData(array(),'id_car_complectation','name'),array('class'=>'form-control input-sm','data-idx'=>$idx,
                                            'ajax'=>array(
                                                'type'=>'POST',
                                                'url'=>CController::createUrl('/car/catalog/ajaxmod'), 
                                                 
                                                'success' => 'function(htmlcode){
                                                           replaceElem(htmlcode)                                                                             
                                                        }',
                                                'data'=>array(
                                                    'mod'=>'js:getMod(this)',
                                                    'compl'=>'js:getCompl(this)',
                                                    'idx'=>'js:getIdx(this)'
                                                ),
                                            )));
                                    }
                                ?>

                                <a href="#">
                                    <?php
                                        $img = $mod->getImage();
                                        if($img)
                                        {
                                            echo CHtml::image(!empty($img->image)?Yii::app()->getModule('car')->urlGenPath.'200x150_'.$img->image:Yii::app()->getModule('car')->defaultImage,'',array('class'=>'compareImg'));
                                        }
                                        else
                                        {
                                            echo CHtml::image(Yii::app()->getModule('car')->defaultImage,'',array('class'=>'compareImg'));
                                        }
                                    ?>
                                </a>
                            </div>
                            
                            <?php
                                foreach($mod->mods->getCharacteristics($params) as $char)
                                {
                                    if(!$char['char_val'])
                                    {
                                        echo '<div class="compareRoll toText" data-compare="'.$char['char_id'].'">&nbsp;</div>';
                                    }
                                    else
                                    {
                                        echo '<div class="oneCompare toText">'.$char['char_val'].'</div>';
                                    }
                                }
                            ?>
                            <?php
                               if(!$compl)
                                {

                                        if(count($mod->complectationRel)>0)
                                        {
                                            $compl = $mod->complectationRel[0];    
                                        }

                                }                                                            
                            if($compl)
                            {
                                $parent = "";
                                foreach($compl->complectation->getComplCharacteristics($complParams) as $char)
                                {
                                    if($char['char_parent']!=$parent)
                                    {
                                        $parent = $char['char_parent'];
                                        echo '<div class="compareRoll toText" data-compare="'.$char['char_parent'].'">&nbsp;</div>';
                                    }
                                    echo '<div class="oneCompare toText compl">'.$char['char_val'].'</div>';
                                }
                            }
                            else
                            {
                                $parent = "";
                                foreach($complParams as $char)
                                {
                                    if($char->parent->name!=$parent)
                                    {
                                        $parent = $char->parent->name;
                                        echo '<div class="compareRoll toText compl" data-compare="'.$char->parent->id_car_characteristic.'">&nbsp;</div>';
                                    }
                                    echo '<div class="oneCompare toText compl">-</div>';
                                }

                            }
                            ?>
                            <?php
                                foreach($mod->generation->getAddCharacteristics($addParams) as $char)
                                {
                                    if(!$char['char_val'])
                                    {
                                        echo '<div class="compareRoll toImg" data-compare="'.$char['char_id'].'">&nbsp;</div>';
                                    }
                                    else
                                    {
                                        echo '<div class="oneCompare toImg">'.$char['char_val'].'</div>';
                                    }
                                }
                            ?>
                           
                        </div>
