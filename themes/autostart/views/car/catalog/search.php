<?php $this->renderPartial('//layouts/_top_category',array('markImg'=>$markImg,'markmodels'=>$markmodels,'mods'=>$models,'mark'=>$mark)); ?>

<div class="assideMargin clearfix ">
                <?php $this->widget('application.modules.car.widgets.FilterWidget'); ?>


                <div class="catlogList whiteSpace">
                    <div class="sort">
                        Сортировать: <!-- <a href="#">по популярности</a> --> <a href="<?=Yii::app()->createUrl(Yii::app()->request->getPathInfo(),array_merge($_GET,array("pricesort"=>(isset($_GET['pricesort'])?($_GET['pricesort']=='ASC'?'DESC':'ASC'):'ASC'))))?>">по цене</a>
                        <!-- <span class="checkbox"><label><input type="checkbox"> Группировать по моделям</label></span> -->
                        <?php
                            $favs = array();
                            if(isset(Yii::app()->request->cookies['favorites']))
                            {
                                $cookie = Yii::app()->request->cookies['favorites']->value;
                                $favs = json_decode($cookie);
                            }
                            $compare_display = "none";
                            if(count($favs)>1)
                            {
                                $compare_display = "inline-block";
                            }
                        ?>
                        <a href="<?=Yii::app()->createUrl('/car/catalog/compare')?>" class="btn btn-primary btn-md goToCompare" style="display:<?=$compare_display?>;">Перейти к сравнению выбранных моделей</a>
                    </div>

                    <?php
                        echo CHtml::openTag('div', array('class' => 'row search-results'));
                        $this->renderPartial('_loop_search', array('models'=>$models));
                        echo CHtml::closeTag('div');
                    ?>

                    <?php if ($models->totalItemCount > $models->pagination->pageSize): ?>

                        <div style="text-align: center;"><?=CHtml::link('Показать еще','#',array('id'=>'showMore', 'class'=>'btn btn-default pull-center','style'=>'width:150px;'))?></div>

                        <script type="text/javascript">
                        /*<![CDATA[*/
                            (function($)
                            {
                                // скрываем стандартный навигатор
                                $('.pager').hide();

                                // запоминаем текущую страницу и их максимальное количество
                                var page = parseInt('<?php echo (int)Yii::app()->request->getParam('page', 1); ?>');
                                var pageCount = parseInt('<?php echo (int)$models->pagination->pageCount; ?>');

                                var loadingFlag = false;

                                $('#showMore').click(function()
                                {
                                    // защита от повторных нажатий
                                    if (!loadingFlag)
                                    {
                                        // выставляем блокировку
                                        loadingFlag = true;

                                        // отображаем анимацию загрузки
                                        $('#showMore').html("<img src='/images/ajax-loader.gif'>");

                                        $.ajax({
                                            type: 'post',
                                            url: window.location.href,
                                            data: {
                                                // передаём номер нужной страницы методом POST
                                                'page': page + 1,
                                                '<?php echo Yii::app()->request->csrfTokenName; ?>': '<?php echo Yii::app()->request->csrfToken; ?>'
                                            },
                                            success: function(data)
                                            {
                                                // увеличиваем номер текущей страницы и снимаем блокировку
                                                page++;
                                                loadingFlag = false;

                                                // прячем анимацию загрузки
                                                $('#showMore').html("Показать еще");

                                                // вставляем полученные записи после имеющихся в наш блок
                                                $('.search-results').append(data);

                                                $(".oneCarLike, .singleLike").click(function(){
                                                    $(this).toggleClass("redLike");

                                                    addToCookie($(this));

                                                    try {
                                                        favs = JSON.parse($.cookie('favorites'));
                                                    } catch (e) {
                                                        favs = [];
                                                    }
                                                    if(favs.length>=2 && $(".goToCompare").length) $(".goToCompare").css("display","inline-block");
                                                    else $(".goToCompare").css("display","none");

                                                    $(".cnt-fav").html(favs.length);
                                                });
                                                // если достигли максимальной страницы, то прячем кнопку
                                                if (page >= pageCount)
                                                    $('#showMore').hide();
                                            }
                                        });
                                    }
                                    return false;
                                })
                            })(jQuery);
                        /*]]>*/
                        </script>

                    <?php endif; ?>



                </div>

                <?php if (!empty($this->seoPage->body)) { ?>
                    <div class="container whiteSpace col-xs-12 seoTextBottom">
                        <div class="row">
                            <?php echo $this->seoPage->body; ?>
                        </div>
                    </div>
                <?php } else if ( !empty($mark->body) ) { ?>
                    <div class="container whiteSpace col-xs-12 seoTextBottom">
                        <div class="row">
                            <?php echo $mark->body; ?>
                        </div>
                    </div>
                <?php } ?>

            </div>
<a id="click_modal" href="#offerFromDealer" data-toggle="modal"></a>
<div id="modal_from_ajax"></div>