<div class="assideMargin clearfix withoutHead">
                <div class="favoriteTitle">Избранное</div>
                <div class="sortCompare">
                    <?php
                        $form = $this->beginWidget(
                            'booster.widgets.TbActiveForm',
                            array(
                                'id' => 'verticalForm',
                                'htmlOptions' => array('class' => ''), // for inset effect
                                'method' => 'get',
                                'action' => Yii::app()->createUrl('/car/catalog/compare')
                            )
                        );                      
                    
                        $favs = array();
                        if(isset(Yii::app()->request->cookies['favorites']))
                        {
                            $cookie = Yii::app()->request->cookies['favorites']->value;
                            $favs = json_decode($cookie);
                        }
                    ?>
                    <div class="favorInfo"><span class="cnt-fav"><?=count($favs)?></span> авто</div>
                    <?php
                        $this->widget(
                            'booster.widgets.TbButton',
                            array('buttonType' => 'submit', 'label' => 'Сравнить выбранные', 'htmlOptions'=> array('class' => '"btn btn-success btn-md pull-right', 'id'=>'filtered'))
                        );
                    ?>
                    <div class="selectAllAuto pull-right"><label><input type="checkbox">Выделить все</label></div>
                </div>
                    <?php
                    echo CHtml::openTag('div', array('class' => 'row'));
                    
                    foreach($models as $mod)
                    {
                        ?>

                            <div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">
                                <div class="thumbnail oneCar">
                                    <span class="oneCarLike redLike glyphicon glyphicon-heart-empty" data-modify="<?=$mod->id?>" data-complectation="0"></span>
                                    <?php
                                        $prev = false;
                                        if(!empty($mod->generation->prev_images))
                                        {
                                            foreach($mod->generation->prev_images as $img)
                                            {
                                                if($img->id_car_body == $mod->mods->id_car_body)
                                                {
                                                    $prev = $img;
                                                }
                                            }
                                        }
                                    ?>
                                    <?php
                                        if(isset($prev->image))
                                        {
                                    ?>
                                    <?=CHtml::image(!empty($prev->image)?Yii::app()->getModule('car')->urlGenPath.'350x260_'.$prev->image :Yii::app()->getModule('car')->defaultImage)?>
                                    <?php
                                        }
                                        else
                                        {
                                            echo  CHtml::image(Yii::app()->getModule('car')->defaultImage);
                                        }
                                    ?>
                                    <div class="caption">
                                        <h3><input type="checkbox" name="mods[]" value="<?=$mod->id?>"><a href="<?=Yii::app()->createUrl('/car/catalog/single',array('id'=>$mod->id,'mark'=>$mod->mark->alias,'model'=>$mod->model->alias))?>"><?=$mod->mark->name." ".$mod->model->name?></a></h3>

                                        <p><?=$mod->mods->body->name?>, <?=$mod->generation->year_begin.($mod->generation->year_end?("-".$mod->generation->year_end):"")?>, <?=$mod->mods->name?></p>
                                        <div class="catalogPrice">от <?=($mod->getMinPrice()?number_format($mod->getMinPrice(), 0, ',', ' '):"0")?>  <i class="rubl">o</i></div>
                                    </div>
                                </div>
                            </div>                        
                        <?php
                    }

                    foreach($compls as $compl)
                    {
                        ?>

                            <div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">
                                <div class="thumbnail oneCar">
                                    <span class="oneCarLike redLike glyphicon glyphicon-heart-empty" data-modify="<?=$compl->genModRel->id?>" data-complectation="<?=$compl->id_car_complectation_generation_modification?>"></span>
                                    <?php
                                        $prev = false;
                                        if(!empty($compl->generation->prev_images))
                                        {
                                            foreach($compl->generation->prev_images as $img)
                                            {
                                                if($img->id_car_body == $compl->mods->id_car_body)
                                                {
                                                    $prev = $img;
                                                }
                                            }
                                        }
                                    ?>
                                    <?php
                                        if(isset($prev->image))
                                        {
                                    ?>
                                    <?=CHtml::image(!empty($prev->image)?Yii::app()->getModule('car')->urlGenPath.'350x260_'.$prev->image :Yii::app()->getModule('car')->defaultImage)?>
                                    <?php
                                        }
                                        else
                                        {
                                            echo  CHtml::image(Yii::app()->getModule('car')->defaultImage);
                                        }
                                    ?>
                                    <div class="caption">
                                        <h3><input type="checkbox" name="compls[]" value="<?=$compl->id_car_complectation_generation_modification?>"><a href="<?=Yii::app()->createUrl('/car/catalog/single',array('id'=>$compl->genModRel->id,'mark'=>$compl->mods->mark->alias,'model'=>$compl->mods->model->alias))?>"><?=$compl->generation->mark->name." ".$compl->generation->model->name?></a></h3>

                                        <p><?=$compl->mods->body->name?>, <?=$compl->generation->year_begin.($compl->generation->year_end?("-".$compl->generation->year_end):"")?>, <?=$compl->mods->name." - ".$compl->complectation->name?></p>
                                        <div class="catalogPrice">от <?=($compl->price_min?number_format($compl->price_min, 0, ',', ' '):"0")?>  <i class="rubl">o</i></div>
                                    </div>
                                </div>
                            </div>                        
                        <?php
                    }

                    echo CHtml::closeTag('div');    
                    
                    $this->endWidget();
                    unset($form);                                        
                ?>
                

            </div>