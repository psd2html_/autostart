                    <div class="compareLeft">
                        <div class="oneCompare onlyText"></div>
                        <div class="oneCompare onlyText">Поколение</div>
                        <div class="oneCompare onlyText">Тип кузова</div>
                        <div class="oneCompare onlyText">Модификация</div>
                        <div class="oneBigCompare">Комплектация</div>
                    <?php
                        $parent = "";
                        foreach($params as $param)
                        {
                            if(!$param->parent)
                            {
                                echo '<div class="compareRoll toText" data-compare="'.$param->id_car_characteristic.'"><span class="glyphicon glyphicon-triangle-bottom"></span>'.$param->name.'</div>';
                            }
                            else
                            {
                                echo '<div class="oneCompare toText">'.$param->name.'</div>';
                            };
                        }
                    ?>
                    <?php
                        $parent = "";
                        foreach($complParams as $param)
                        {
                            if($param->parent->name!=$parent)
                            {
                                $parent = $param->parent->name;
                                echo '<div class="compareRoll toText" data-compare="'.$param->parent->id_car_characteristic.'"><span class="glyphicon glyphicon-triangle-bottom"></span>'.$parent.'</div>';
                            }
                            echo '<div class="oneCompare toText compl">'.$param->name.'</div>';
                        }
                    ?>
                    <?php
                        $parent = "";
                        foreach($addParams as $param)
                        {      
                            if(!$param->parent)
                            {
                                echo '<div class="compareRoll toImg" data-compare="'.$param->id_car_characteristic.'"><span class="glyphicon glyphicon-triangle-bottom"></span>'.$param->name.'</div>';
                            }
                            else
                            {
                                echo '<div class="oneCompare toImg">'.$param->name.'</div>';
                            };
                        }
                    ?>
                    </div>
