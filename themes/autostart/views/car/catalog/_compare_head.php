            <div class="oneCompareModel" data-idx="<?=$idx?>" data-mod="<?=$mod->id_car_modification?>" data-compl="<?=$compl?$compl->id_car_complectation:0?>">
                <div class="oneCompare onlyText">
                    <div class="removeCol">
                        <span class="glyphicon glyphicon-remove"></span>
                    </div>
                    <a href="<?=Yii::app()->createUrl("/car/catalog/single",array("id"=>$mod->id,'mark'=>$mod->mark->alias,'model'=>$mod->model->alias))?>" class="compareTitleLink"><?=$mod->mark->name?> <?=$mod->model->name?></a>
                </div>
            
                <div class="oneCompare onlyText">
                    <?php
                        echo CHtml::dropDownList('generation', $mod->generation->id_car_generation, $mod->model->getGenerationList(),array('class'=>'form-control input-sm','options'=>$mod->model->getGenerationMods(),
                                            'ajax'=>array(
                                                'type'=>'POST',
                                                'url'=>CController::createUrl('/car/catalog/ajaxmod'), 
                                                 
                                                'success' => 'function(htmlcode){
                                                           replaceElem(htmlcode)                                                                             
                                                        }',
                                                'data'=>array(
                                                    'mod'=>'js:getMod(this)',
                                                    'compl'=>'js:getCompl(this)',
                                                    'idx'=>'js:getIdx(this)'
                                                ),
                                            )));
                    ?>
                </div>
                <div class="oneCompare onlyText">
                    <?php
                        echo CHtml::dropDownList('body', $mod->mods->id_car_body, CHtml::listData($mod->generation->getBodies(),'id_car_body','name'),array('class'=>'form-control input-sm','options'=>$mod->generation->getBodyMods(),
                                            'ajax'=>array(
                                                'type'=>'POST',
                                                'url'=>CController::createUrl('/car/catalog/ajaxmod'), 
                                                 
                                                'success' => 'function(htmlcode){
                                                           replaceElem(htmlcode)                                                                             
                                                        }',
                                                'data'=>array(
                                                    'mod'=>'js:getMod(this)',
                                                    'compl'=>'js:getCompl(this)',
                                                    'idx'=>'js:getIdx(this)'
                                                ),
                                            )));
                    ?>
                
                </div>
                <div class="oneCompare onlyText">
                    <?php
                        echo CHtml::dropDownList('modify', $mod->id, CHtml::listData($mod->getModificationByBodyMods($mod->mods->id_car_body),'id','modName'),array('class'=>'form-control input-sm','options'=>$mod->getModificationMods(),
                                            'ajax'=>array(
                                                'type'=>'POST',
                                                'url'=>CController::createUrl('/car/catalog/ajaxmod'), 
                                                 
                                                'success' => 'function(htmlcode){
                                                           replaceElem(htmlcode)                                                                             
                                                        }',
                                                'data'=>array(
                                                    'mod'=>'js:getMod(this)',
                                                    'compl'=>'js:getCompl(this)',
                                                    'idx'=>'js:getIdx(this)'
                                                ),
                                            )));
                    ?>
                 </div>
                <div class="oneCompare onlyText">
                    <?php
                        if($mod->complectation)
                        {
                            echo CHtml::dropDownList('complectation', ($compl?$compl->id_car_complectation_generation_modification:0), $mod->getComplRelWithName(),array('class'=>'form-control input-sm','data-modid'=>$mod->id,
                                            'ajax'=>array(
                                                'type'=>'POST',
                                                'url'=>CController::createUrl('/car/catalog/ajaxmod'), 
                                                 
                                                'success' => 'function(htmlcode){
                                                           replaceElem(htmlcode)                                                                             
                                                        }',
                                                'data'=>array(
                                                    'mod'=>'js:getMod(this)',
                                                    'compl'=>'js:getCompl(this)',
                                                    'idx'=>'js:getIdx(this)'
                                                ),
                                            )));
                        }
                        else
                        {
                            echo CHtml::dropDownList('complectation', 0, CHtml::listData(array(),'id_car_complectation','name'),array('class'=>'form-control input-sm',
                                            'ajax'=>array(
                                                'type'=>'POST',
                                                'url'=>CController::createUrl('/car/catalog/ajaxmod'), 
                                                 
                                                'success' => 'function(htmlcode){
                                                           replaceElem(htmlcode)                                                                             
                                                        }',
                                                'data'=>array(
                                                    'mod'=>'js:getMod(this)',
                                                    'compl'=>'js:getCompl(this)',
                                                    'idx'=>'js:getIdx(this)'
                                                ),
                                            )));
                        }
                    ?>
                </div>
            </div>
