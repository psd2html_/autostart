<?php $this->renderPartial('//layouts/_top_single', array('model'=>$genMod)); ?>

    <div class="assideMargin clearfix">

        <?php if(!empty($genMod->generation->description->value)){?>
            <h2 class="singleTitleH2 text-center" id="views">Обзоры</h2>
            <div class="whiteSpace obzor">
                <a href="#" class="scrollUp"></a>
                <div class="inner-obzor">
                    <div class="obzor-content">
                        <?=$genMod->generation->description->value?>
                    </div>
                </div>
                <a href="#" class="scrollDown"></a>
            </div>
        <?php }?>

        <h2 class="singleTitleH2 text-center" id="design">Комплектации и цены</h2>
        <div class="row tableElem" >
            <div class="col-lg-6 col-md-5 firstSingleCell">
                <div class="whiteSpace">
                    <div class="singleTopInfo">
                        <div class="singleTitle"><?=$genMod->mark->name?> <?=$genMod->model->name?></div>

                        <?php
                        $favs = array();
                        if(isset(Yii::app()->request->cookies['favorites']))
                        {
                            $cookie = Yii::app()->request->cookies['favorites']->value;
                            $favs = json_decode($cookie);
                        }
                        //var_dump($favs);
                        $obj = new stdClass();
                        $obj->modify = $genMod->id;
                        $obj->compl = 0;
                        //var_dump($obj);

                        $class = "";
                        if(in_array($obj,$favs))
                        {
                            $class = " redLike";
                        }
                        ?>

                        <div class="singleLike <?=$class?>" data-modify="<?=$genMod->id?>" data-complectation="0"><span class="glyphicon glyphicon-heart"></span></div>

                        <div class="singleSelect">
                            <?php
                            echo CHtml::dropDownList('generation', $genMod->generation->id_car_generation, $genMod->model->getGenerationList(),array('class'=>'form-control input-sm','options'=>$genMod->model->getGenerationMods(), 'onchange'=>'event.preventDefault(); changeMod(this);'));
                            ?>
                        </div>
                        <div class="singleSelect">
                            <?php
                            echo CHtml::dropDownList('body', $genMod->mods->id_car_body, CHtml::listData($genMod->generation->getBodies(),'id_car_body','name'),array('class'=>'form-control input-sm','options'=>$genMod->generation->getBodyMods(), 'onchange'=>'event.preventDefault(); changeMod(this);'));
                            ?>
                        </div>
                    </div>
                    <div class="tab-content">
                        <?php
                        $colors = Yii::app()->getModule('car')->colors;
                        $active = 'in active';
                        foreach($genMod->generation->colors as $color)
                        {
                            if(!empty($color->image) && $color->id_car_body==$genMod->mods->id_car_body)
                            {
                                echo '<div id="color-'.$colors[$color->id_car_add_characteristic].'" class="tab-pane fade '.$active.'">
                                            <div class="row colorImg">
                                                <img src="'.Yii::app()->getModule('car')->urlGenPath.'890x350_'.$color->image.'" alt="">
                                            </div>
                                        </div>';
                                $active = '';
                            }
                        }
                        ?>

                    </div>
                    <ul class="avtoColorTabs">
                        <?php
                        $active = 'class="active"';
                        foreach($genMod->generation->colors as $color)
                        {
                            if(!empty($color->image) && $color->id_car_body==$genMod->mods->id_car_body)
                            {
                                echo '<li '.$active.'><a data-toggle="tab" href="#color-'.$colors[$color->id_car_add_characteristic].'" class="colorTab" aria-expanded="'.(empty($active)?'false':'true').'"><span style="background-color:#'.$colors[$color->id_car_add_characteristic].';">&nbsp;</span></a></li>';
                                $active = '';
                            }
                        }
                        ?>
                    </ul>
                    <?=$genMod->generation->introtext->value?>
                </div>
            </div>
            <div class="col-lg-6 col-md-7 secondSingleCell" style="max-height: 618px;">
                <div class="whiteSpace">
                    <div class="singleFilter">
                        <div class="singleFilterTitle">
                            <strong>Объем двигателя:</strong>
                        </div>
                        <?php

                        if (!empty($this->seoPage)) {
                            foreach($genMod->generation->modsRel as $modRelVal)
                            {
                                /* if ($this->seoPage->id_car_modification == $modRelVal->id_car_modification) {
                                     $currentMod = round($modRelVal->mods->volume->value/1000,1);
                                 }*/
                                $currentMod = $this->seoPage->car_volume_value;

                            }
                            $seoFuel = $this->seoPage->id_car_fuel;
                            $seoPrivod = $this->seoPage->id_car_privod;

                            if ($this->seoPage->id_car_transmission=='Механическая') {
                                $currentTrans = 'mechanics';
                            } else if ($this->seoPage->id_car_transmission=='Автоматическая') {
                                $currentTrans = 'auto';
                            } else {
                                $currentTrans = '';
                            }

                        }

                        ?>
                        <div class="singleFilterContent">
                            <?php
                            foreach($genMod->generation->getVolume() as $volume)
                            {
                                echo '<label>';
                                if (!empty($currentMod) && $currentMod==$volume) {
                                    echo '<input type="checkbox" name="volume" value="'.$volume.'" checked>';
                                } else {
                                    echo '<input type="checkbox" name="volume" value="'.$volume.'">';
                                }
                                echo $volume;
                                echo '</label>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="singleFilter">
                        <div class="singleFilterTitle">
                            <strong>Коробка передач:</strong>
                        </div>
                        <div class="singleFilterContent">
                            <?php
                            foreach($genMod->generation->getTransmission() as $key => $val)
                            {
                                echo '<label>';
                                if (!empty($currentTrans) && $currentTrans==$key) {
                                    echo '<input type="checkbox" name="transmission" value="'.$key.'" checked>';
                                } else {
                                    echo '<input type="checkbox" name="transmission" value="'.$key.'">';
                                }
                                echo $val;
                                echo '</label>';
                            }
                            ?>

                            <a href="<?=Yii::app()->createUrl("/car/catalog/compare")?>" class="btn btn-success btn-md unactive singleComplect">Сравнить выбранные</a>
                        </div>
                    </div>

                    <?php if (!empty($this->seoPage) &&  $this->seoPage->group_id==2) { ?>
                        <div class="singleFilter">
                            <div class="singleFilterTitle">
                                <strong>Тип двигателя:</strong>
                            </div>
                            <div class="singleFilterContent">
                                <?php
                                foreach($genMod->generation->getFuel() as $key => $val)
                                {
                                    //(!empty($this->seoPage->id_car_fuel)&&$this->seoPage->id_car_fuel==$val)?"checked":"";
                                    echo '<label>';
                                    if (!empty($seoFuel) && $seoFuel == $val) {
                                        echo '<input type="checkbox" name="fuel" value="'.$key.'" checked>';
                                    } else {
                                        echo '<input type="checkbox" name="fuel" value="'.$key.'">';
                                    }
                                    echo $val;
                                    echo '</label>';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="singleFilter">
                            <div class="singleFilterTitle">
                                <strong>Тип привода:</strong>
                            </div>
                            <div class="singleFilterContent">
                                <?php
                                foreach($genMod->generation->getPrivod() as $key => $val)
                                {
                                    echo '<label>';
                                    if (!empty($seoPrivod) && $seoPrivod == $val) {
                                        echo '<input type="checkbox" name="privod" value="'.$key.'" checked>';
                                    } else {
                                        echo '<input type="checkbox" name="privod" value="'.$key.'">';
                                    }
                                    echo $val;
                                    echo '</label>';
                                }
                                ?>
                            </div>
                        </div>
                    <?php } ?>

                    <!-- <div class="singleFilter">
                        <div class="singleFilterTitle">
                            <strong>Показать:</strong>
                        </div>
                        <div class="singleFilterContent">
                            <label><input type="radio" name='view' value='new'> в продаже новые</label>
                            <label><input type="radio" name='view' checked="checked"> все</label>
                            <a href="#" class="btn btn-success btn-md unactive singleComplect">Сравнить выбранные</a>
                        </div>
                    </div> -->

                    <div class="table-wrapper">
                        <?php foreach($genMod->generation->modsRel as $modRel) { ?>
                            <?php if ($modRel->mods->id_car_body==$genMod->mods->id_car_body) { ?>
                                <?php $mod = $modRel->mods; ?>
                                <?php $modName = str_replace(".", "-", number_format(floatval($mod->name), 1)); ?>
                                <div class="singleTable" data-fuel="<?=$mod->fuel->value?>" data-privod="<?=$mod->privod->value?>" data-new="true" data-volume="<?=round($mod->volume->value/1000,1)?>" data-transmission="<?=$mod->transmission->value=='Механическая'?'mechanics':'auto'?>">
                                    <div class="singleTableLeft">
                                        <?=CHtml::link($mod->name,Yii::app()->createUrl('/car/catalog/character',array('mark'=>$modRel->mark->alias, 'model'=>$modRel->model->alias, 'mod'=>$modName, 'id'=>$modRel->id)))?>
                                        <span>
                                            <?=$mod->fuel->value?>,
                                            <?php
                                            /*<?=round($mod->volume->value/1000,1)?> л., <?=$mod->transmission->value=='Механическая'?'механика':'автомат'?>,*/
                                            ?>
                                            <?php if (isset($mod->privod) and !empty($mod->privod->value)) { echo $mod->privod->value . ' привод,'; } ?>
                                            <?php if (isset($modRel->power) and !empty($modRel->power->value)) { echo $modRel->power->value . '  л.с.'; } ?>
                                        </span>
                                    </div>
                                    <div class="singleTableRight">
                                        <?php
                                        foreach($modRel->complectationRel as $complRel)
                                        {
                                            $obj = new stdClass();
                                            $obj->modify = $modRel->id;
                                            $obj->compl = $complRel->id_car_complectation_generation_modification;
                                            //var_dump($obj);
                                            //var_dump(in_array($obj,$favs));
                                            $class = "";
                                            if(in_array($obj,$favs))
                                            {
                                                $class = " active";
                                            }
                                            ?>
                                            <div class="oneSTR">
                                                <div class="STRLeft">
                                                    <!-- <input type="radio" name="single">-->
                                                    <?=CHtml::link($complRel->complectation->name,Yii::app()->createUrl('/car/catalog/character',array('mark'=>$modRel->mark->alias,'model'=>$modRel->model->alias,'compl'=>$complRel->id_car_complectation_generation_modification)))?>
                                                </div>
                                                <div class="STRRight">
                                                    от <strong><?=number_format($complRel->price_min, 0, ',', ' ')?></strong>  <i class="rubl">o</i>
                                                    <div class="singleStar <?=$class?>" data-modify="<?=$modRel->id?>" data-complectation="<?=$complRel->id_car_complectation_generation_modification?>"><span class="glyphicon glyphicon-heart"></span></div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <div id="alertfb" class="alert"></div>
                    <a href="#offerFromDealer" data-toggle="modal" class="btn btn-info veryBigBtn">Получить предложения от дилеров</a>
                </div>
            </div>
        </div>
        <?php if(count($genMod->generation->gallery)>0){?>
            <h2 class="singleTitleH2 text-center" id="photos">Фото</h2>
            <div class="whiteSpace clearfix">
                <div class="leftColPhoto">
                    <?php
                    $main_img = "";
                    $i=0;
                    $class = "";
                    foreach($genMod->generation->gallery as $image)
                    {
                        if($image->id_car_body==$genMod->mods->id_car_body)
                        {
                            if($i>7)
                            {
                                $class='class="hiddenPhoto"';
                            }
                            if(empty($main_img))
                            {
                                $main_img=$image;
                                continue;
                            }
                            echo '<img src="'.Yii::app()->getModule('car')->urlGenPath.$image->image.'" '.$class.' alt="">';
                            $i++;
                        }
                    }
                    ?>
                </div>
                <div class="indexColPhoto" style="background:url(<?=!empty($main_img)?(Yii::app()->getModule('car')->urlGenPath.$main_img->image):""?>) center center no-repeat; background-size: cover;">&nbsp;</div>
            </div>
        <?php }?>

        <?php if(!empty($genMod->generation->review->value)){?>
            <h2 class="singleTitleH2 text-center" id="review">Отзывы</h2>
            <div class="whiteSpace review">
                <a href="#" class="scrollUp"></a>
                <div class="inner-review">
                    <div class="review-content">
                        <?=$genMod->generation->review->value?>
                    </div>
                </div>
                <a href="#" class="scrollDown"></a>
            </div>
        <?php }?>



        <?php if(count($genMod->generation->similar)>0){?>

            <h2 class="text-center singleTitleH2" id="otherModels">Похожие модели</h2>
            <div class="whiteSpace">
                <div id="modelsCarousel" class="carousel slide" data-interval="5000" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="fourModelsCar">
                                <?php
                                $idx = 0;

                                foreach($genMod->generation->similar as $mod)
                                {
                                $gen = $mod->mods;
                                $prev = false;
                                if(!empty($mod->mods->generation->prev_images))
                                {
                                    foreach($mod->mods->generation->prev_images as $img)
                                    {
                                        if($img->id_car_body == $mod->mods->mods->id_car_body)
                                        {
                                            $prev = $img;
                                        }
                                    }
                                }
                                if($idx>3)
                                {
                                $idx = 0;
                                ?>
                            </div>
                        </div>
                        <div class="item">
                            <div class="fourModelsCar">
                                <?php
                                }
                                ?>
                                <?php
                                if($mod->mods->generation)
                                {


                                    ?>
                                    <div class="oneModelCar">
                                        <div class="thumbnail oneCar">
                                            <?=CHtml::image(!empty($prev->image)?Yii::app()->getModule('car')->urlGenPath.'350x260_'.$prev->image :Yii::app()->getModule('car')->defaultImage)?>

                                            <div class="caption">
                                                <h3><a href="<?=Yii::app()->createUrl('/car/catalog/single',array('id'=>$mod->mods->id,'mark'=>$mod->mods->mark->alias,'model'=>$mod->mods->model->alias))?>"><?=$mod->mods->mark->name." ".$mod->mods->model->name?></a></h3>
                                                <p><?=$mod->mods->mods->body->name?>, <?=$mod->mods->generation->year_begin.($mod->mods->generation->year_end?("-".$mod->mods->generation->year_end):"")?></p>
                                                <div class="catalogPrice">от <?=number_format($mod->mods->getMinPrice(), 0, ',', ' ')?> <i class="rubl">o</i></div>

                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $idx++;
                                }


                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <a class="leftModelSlide" href="#modelsCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="rightModelSlide" href="#modelsCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                    <ol class="carousel-indicators modelsControl">
                        <?php
                        $class = 'active';
                        for($i=0;$i<count($genMod->generation->similar)/4;$i++)
                        {
                            echo '<li data-target="#modelsCarousel" data-slide-to="'.$i.'" class="'.$class.'"></li>';
                            $class='';
                        }

                        ?>

                    </ol>
                </div>
            </div>
        <?php }?>

        <?php if (!empty($this->seoPage->body)) { ?>
            <div class="container whiteSpace col-xs-12 seoTextBottom">
                <div class="row">
                    <?php echo $this->seoPage->body; ?>
                </div>
            </div>
        <?php } ?>

    </div>

<?php $this->renderPartial('//layouts/_footer', array('model'=>$genMod)); ?>

<?php
Yii::app()->clientScript->registerScript('modChange', "
        function changeMod(elem)
        {
            var url = $(elem).find(\"option[value=\"+$(elem).val()+\"]\").data().mod;
            if(typeof url !== 'undefined')
            {
                window.location.href = url;
            }
        }

    ", CClientScript::POS_END);

?>