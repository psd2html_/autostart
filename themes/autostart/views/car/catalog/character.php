<?php $this->renderPartial('//layouts/_top_single', array('model'=>$genModRel, 'complRel'=>$complRel)); ?>
<div class="assideMargin">
    <div class="twoCollums clearfix">
        <div class="categoryRight">
            <div class="whiteSpace">
                <?php
                    $form = $this->beginWidget(
                        'booster.widgets.TbActiveForm',
                        array(
                            'id' => 'verticalForm',
                            'htmlOptions' => array('class' => ''), // for inset effect
                            'method' => 'get',
                            'action' => Yii::app()->createUrl('/car/catalog/compare')
                        )
                    );

                    foreach($genModRel->generation->modsRel as $modRel)
                    {
                        if($modRel->mods->id_car_body==$genModRel->mods->id_car_body)
                        {
                        ?>
                            <div class="categoryFTitle">
                                <?php
                                    /*<a href="<?=Yii::app()->createUrl('/car/catalog/character',array('id'=>$modRel->id,'mark'=>$modRel->mark->alias,'model'=>$modRel->model->alias))?>">
                                    <?=$modRel->mods->name?>
                                </a>*/
                                ?>
                                <strong><?=$modRel->mods->name?></strong>
                            </div>

                                <?php
                                    foreach($modRel->complectationRel as $complectation)
                                    {
                                ?>
                                        <div class="filterCategory">
                                            <input type="checkbox" name="compls[]" value="<?=$complectation->id_car_complectation_generation_modification?>">
                                            <a href="<?=Yii::app()->createUrl('/car/catalog/character',array('mark'=>$modRel->mark->alias, 'model'=>$modRel->model->alias, 'compl'=>$complectation->id_car_complectation_generation_modification))?>"><?=$complectation->complectation->name?>
                                                <span><?=number_format($complectation->price_min, 0, ',', ' ')?></span>
                                            </a>
                                        </div>
                                <?php
                                    }
                                ?>

                        <?php
                        }
                    }
                ?>
                <?php
                    $this->widget(
                        'booster.widgets.TbButton',
                        array('buttonType' => 'submit', 'label' => 'Сравнить выбранные', 'htmlOptions'=> array('class' => 'btn btn-primary btn-md compareComplect', 'id'=>'filtered'))
                    );

                    $this->endWidget();
                    unset($form);
                ?>
            </div>
        </div>
        <div class="categoryContent clearfix">
            <div class="whiteSpace">
                <div class="categoryTopInfo">
                    <div class="singleTitle"><a href="<?=Yii::app()->createUrl('car/catalog/single',array('id'=>$genModRel->id, 'mark'=>$genModRel->mark->alias,'model'=>$genModRel->model->alias))?>"><?=$genModRel->generation->mark->name." ".$genModRel->generation->model->name?></a></div>
                    <?php
                        $favs = array();
                        if(isset(Yii::app()->request->cookies['favorites']))
                        {
                            $cookie = Yii::app()->request->cookies['favorites']->value;
                            $favs = json_decode($cookie);
                        }
                        //var_dump($favs);
                        $obj = new stdClass();
                        $obj->modify = $genModRel->id;
                        $obj->compl = 0;
                        //var_dump($obj);

                        $class = "";
                        if(in_array($obj,$favs))
                        {
                            $class = " redLike";
                        }
                    ?>

                    <div class="singleLike <?=$class?>" data-modify="<?=$genModRel->id?>" data-complectation="0"><span class="glyphicon glyphicon-heart"></span></div>

                    <div class="singleSelect">
                        <?php
                            echo CHtml::dropDownList('generation', $genModRel->generation->id_car_generation, $genModRel->generation->model->getGenerationList(),array('class'=>'form-control input-sm','options'=>$genModRel->generation->model->getGenerationMods(), 'onchange'=>'event.preventDefault(); changeCompl(this);'));
                        ?>
                    </div>
                    <div class="singleSelect">
                        <?php
                            echo CHtml::dropDownList('body', $genModRel->mods->id_car_body, CHtml::listData($genModRel->generation->getBodies(),'id_car_body','name'),array('class'=>'form-control input-sm','options'=>$genModRel->generation->getBodyMods(), 'onchange'=>'event.preventDefault(); changeCompl(this);'));
                        ?>
                    </div>
                    <?php
                        if(count($complRel)>0 && false)
                        {?>
                            <div class="singleSelect">
                                <?php
                                    echo CHtml::dropDownList('compl', ($complRel?$complRel->id_car_complectation_generation_modification:""), CHtml::listData($genModRel->getComplList(),'id_car_complectation_generation_modification','complName'),array('class'=>'form-control input-sm','options'=>$genModRel->getComplMods(), 'onchange'=>'event.preventDefault(); changeCompl(this);'));
                                ?>
                            </div>
                    <?php
                        }
                    ?>
                    <div class="singleTitle" style="clear: none; margin-left: 15px;">
                        <?=number_format($complRel->price_min, 0, ',', ' ')?>  <i class="rubl">o</i>
                    </div>
                </div>
                <h2 class="categoryTitle"><?=$genModRel->generation->name.", ".$genModRel->mods->name.($complRel?(" - ".$complRel->complectation->name):"")?></h2>
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#category1">Характеристики</a></li>
                    <li><a data-toggle="tab" href="#category2">Комплектации</a></li>
                </ul>
                <div class="tab-content">
                    <div id="category1" class="tab-pane fade in active">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <?php
                                    $parent = "";
                                    $idx = 0;
                                    foreach($genModRel->mods->characteristicValues as $char)
                                    {
                                        if($idx>count($genModRel->mods->characteristicValues)/2 && $char->characteristic->parent->name!=$parent)
                                        {
                                            echo '</div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">';
                                        }
                                        if($char->characteristic->parent->name!=$parent)
                                        {
                                            $parent = $char->characteristic->parent->name;
                                            echo '<h3 class="charTitle">'.$parent.'</h3>';
                                        }
                                        echo '<div class="row">
                                                <div class="col-xs-7">'.$char->characteristic->name.'</div>
                                                <div class="col-xs-5">'.$char->value.(empty($char->unit)?"":(" ".$char->unit)).'</div>
                                            </div>';
                                        $idx++;
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div id="category2" class="tab-pane fade">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <?php
                                    $parent = "";
                                    $idx = 0;

                                    if($complRel)
                                    {
                                        foreach($complRel->complectation->characteristicValues as $char)
                                        {
                                            if($idx>count($complRel->complectation->characteristicValues)/2 && $char->characteristic->parent->name!=$parent)
                                            {
                                                echo '</div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">';
                                            }
                                            if($char->characteristic->parent->name!=$parent)
                                            {
                                                $parent = $char->characteristic->parent->name;
                                                echo '<h3 class="charTitle">'.$parent.'</h3>';
                                            }
                                            echo '<div class="row">
                                                    <div class="col-xs-7">'.$char->characteristic->name.'</div>
                                                    <div class="col-xs-5">'.($char->value?'Да':'-').'</div>
                                                </div>';
                                            $idx++;
                                        }
                                    }
                                    else
                                    {
                                        $chars = ComplectationCharacteristic::model()->findAll('id_parent is not null');
                                        foreach($chars as $char)
                                        {
                                            if($idx>count($chars)/2 && $char->parent->name!=$parent)
                                            {
                                                echo '</div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">';
                                            }
                                            if($char->parent->name!=$parent)
                                            {
                                                $parent = $char->parent->name;
                                                echo '<h3 class="charTitle">'.$parent.'</h3>';
                                            }
                                            echo '<div class="row">
                                                    <div class="col-xs-7">'.$char->name.'</div>
                                                    <div class="col-xs-5">-</div>
                                                </div>';
                                            $idx++;
                                        }
                                    }

                                ?>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if (!empty($this->seoText)) { ?>
            <div class="container whiteSpace col-xs-12 seoTextBottom">
                <div class="row">
                    <?php echo $this->seoText; ?>
                </div>
            </div>
        <?php } ?>

    </div>
    <!-- <h3 class="charTitle">Пакеты опций</h3>
    <div class="categoryOptions">
        <div class="checkbox">
            <label><input type="checkbox" value='12 350'>Keyless-GO (содержится в пакете "Luxury")</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value='33 109'>Assistants - 33 109 руб.<span class="packInfo">Штатная навигационная система. Камера заднего вида</span></label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value='39 731'>Luxury - 39 731 руб.<span class="packInfo">Штатная аудиосистема HI-FI. Запуск двигателя с кнопки. Система доступа без ключа.</span></label>
        </div>
    </div>
    <div class="categoryCounter" data-price="645 622">
        <div>645 622<span class='glyphicon glyphicon-ruble'></span><span class='countInfo'>общая стоимость</span></div>
    </div> -->

</div>

<?php //$this->renderPartial('//layouts/_footer', array('model'=>$genModRel)); ?>

<?php
    Yii::app()->clientScript->registerScript('modChange', "
        function changeCompl(elem)
        {
            var url = $(elem).find(\"option[value=\"+$(elem).val()+\"]\").data().char;
            if(typeof url !== 'undefined')
            {
                window.location.href = url;
            }
        }

    ", CClientScript::POS_END);

?>