 
    <div class="wrappTopCompare" style="display: none;">
        <div class="topLeftCompare">
            <div class="oneCompare onlyText"></div>
            <div class="oneCompare onlyText">Поколение</div>
            <div class="oneCompare onlyText">Тип кузова</div>
            <div class="oneCompare onlyText">Модификация</div>
            <div class="oneCompare onlyText">Комплектация</div>
        </div>
        
        <div class="topCompare">
        
        <?php
            $idx = 0;
            foreach($models as $model)
            {
                $mod = $model['mod'];
                $compl = $model['compl'];
               if(!$compl)
                {

                        if(count($mod->complectationRel)>0)
                        {
                            $compl = $mod->complectationRel[0];    
                        }

                }
               $this->renderPartial('_compare_head',array('mod'=>$mod,'compl'=>$compl,'idx'=>$idx)); 
                $idx++;
            }
        ?>
    </div>
</div>    
</div>
<div class="assideMargin clearfix withoutHead">
                <div class="favoriteTitle">Сравнение автомобилей</div>
                <p class="textDesc">Сравните автомобили по самому подробному списку характеристик, а также по фотографиям автомобиля в разных ракурсах.</p>
                <div class="sortCompare">
                    <div class="btn-group btn-group-md pull-left" role="group" aria-label="left">
                        <div class="btn btn-default active" data-on-img="false">По параметрам</div>
                        <div class="btn btn-default" data-on-img="true">По фотографиям</div>
                    </div>
                    <a href="#myModal" data-toggle="modal" class="btn btn-success btn-md pull-right">Добавить к сравнению</a>
                    <div class="onlyExclusive"><label><input type="checkbox">Только различающиеся параметры</label></div>
                </div>
                <div class="twoCollums compareWrapp">
                    <?php $this->renderPartial('_compare_params', array('params'=>$params,'addParams'=>$addParams,'complParams'=>$complParams)); ?>

                    <div class="compareContent">

                        <?php
                            $idx = 0;
                            foreach($models as $model)
                            {
                                $mod = $model['mod'];
                                $compl = $model['compl'];
                                
                               if(!$compl)
                                {

                                        if(count($mod->complectationRel)>0)
                                        {
                                            $compl = $mod->complectationRel[0];    
                                        }

                                }                                
                                $this->renderPartial('_compare_content',array('mod'=>$mod,'compl'=>$compl,'params'=>$params,'addParams'=>$addParams,'complParams'=>$complParams, 'idx'=>$idx)); 
                                $idx++;
                            }
                        ?>                    
                    </div>
                </div>
            </div>