
<h1>Марки автомобилей</h1>

<?php                               
    echo CHtml::link('Добавить',array('/backend/car/mark/create'),array('class'=>'btn btn-primary pull-right'));

    $gridColumns = array(
        array(
            'name'=>'icon', 
            'header'=>'',
            'type'=>'html',
            'value'=>'!empty($data->icon)?CHtml::image(Yii::app()->getModule(\'car\')->urlMarkPath."/100x100_".$data->icon,$data->name,array("style"=>"width:50px;")):""',
            'filter'=>'',
        ),
        array('name'=>'id_car_mark', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 60px')),
        array('name'=>'name', 'header'=>'Название марки'),
        array('name'=>'alias', 'header'=>'Alias'),
        array(
            'name'=>'is_visible', 
            'header'=>'Видимость', 
            'value'=>'$data->is_visible?"Да":"Нет"',
            'filter' =>CHtml::activeDropDownList($model, 'is_visible',  
                array(
                    ''=>'Все',
                    '1'=>'Да',
                    '0'=>'Нет',
                ),
                array('class' => 'form-control'
                )
            ), 
        ),
        array(
            'name'=>'is_fav', 
            'header'=>'Популярная', 
            'value'=>'$data->is_fav?"Да":"Нет"',
            'filter' =>CHtml::activeDropDownList($model, 'is_fav',  
                array(
                    ''=>'Все',
                    '1'=>'Да',
                    '0'=>'Нет',
                ),
                array('class' => 'form-control'
                )
            ), 
        ),
        array('name'=>'id_car_type', 'header'=>'Тип авто','value'=>'$data->type->name',
            'filter' =>CHtml::activeDropDownList($model,'id_car_type',  
                CHtml::listData(Type::model()->findall(), 'id_car_type', 'name'),
                array(
                    'class' => 'form-control'
                )
            ),
        ),
        array('name'=>'name_rus', 'header'=>'Назв. рус.'),
        array(
            'template'=>'{update} {delete}',
            'class'=>'booster.widgets.TbButtonColumn',
        )
    );
    $this->widget(
        'booster.widgets.TbGridView',
        array(
            'dataProvider' => $model->search(),
            'template' => "{items}{pager}",
            'columns' => $gridColumns,
            'filter' => $model,
        )
    ); 
    
?>