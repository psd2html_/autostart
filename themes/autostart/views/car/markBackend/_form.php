<?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'verticalForm',
            'htmlOptions' => array('class' => 'well', 'enctype'=>'multipart/form-data'), // for inset effect
            
        )
    );
    ?>
    
<?php 
    echo $form->textFieldGroup($model, 'name');
    echo $form->textFieldGroup($model, 'name_rus');
    echo $form->textFieldGroup($model, 'alias');
    echo $form->checkboxGroup($model, 'is_fav');
    echo $form->checkboxGroup($model, 'is_visible');
    echo $form->dropDownListGroup(
        $model,
        'id_car_type',
        array(
            'widgetOptions' => array(
                'data' => CHtml::listData(Type::model()->findAll(),'id_car_type', 'name'),
                'htmlOptions' => array(
                    'encode' => false,
                ),
            ),
        )
    );                      

    
?>

<?php echo $form->textFieldGroup($model,'title',array('size'=>60,'maxlength'=>250)); ?>

<?php echo $form->textAreaGroup($model,'description',array('size'=>60,'maxlength'=>250)); ?>

<?php
echo $form->ckEditorGroup(
    $model,
    'body',
    array(
        'wrapperHtmlOptions' => array(
            /* 'class' => 'col-sm-5', */
        ),
        'widgetOptions' => array(
            'editorOptions' => array(
                'fullpage' => 'js:true',
            )
        )
    )
);
?>

<h3>Доп. Характеристики</h3>
    
    <table class="detail-view table table-striped table-condensed" id="yw0"><tbody>
<?php       
            echo "<tr class='odd'><th>Изображение</th>";
            echo "<td>".(($model->image)?CHtml::image(Yii::app()->getModule('car')->urlMarkPath."/100x100_".$model->image,'',array("style"=>"float:right;")):"");
            echo CHtml::fileField('CarMark[image]', '', array('id'=>'image'))." (Изображение для шапки категории машин)</td></tr>";
            echo "<tr class='even'><th>Шильд</th>";
            echo "<td>".(($model->icon)?CHtml::image(Yii::app()->getModule('car')->urlMarkPath."/100x100_".$model->icon,'',array("style"=>"float:right;")):"");
            echo CHtml::fileField('CarMark[icon]', '', array('id'=>'icon'))." (Изображение для отображения в поиске)</td></tr>";
            echo "</table><br><br>";        
    
    $this->widget(
        'booster.widgets.TbButton',
        array('buttonType' => 'submit', 'label' => 'Сохранить', 'context' => 'success')
    );
    
    echo " ".CHtml::link('Отмена',array('/backend/car/mark/index'),array('class'=>'btn btn-danger ')) ;
    $this->endWidget();
    unset($form);
?>