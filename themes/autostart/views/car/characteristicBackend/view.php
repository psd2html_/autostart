
<h1>Характеристика <strong><?=$model->name?></strong></h1>

<?php                                      
    echo CHtml::link('Все характеристики',array('/backend/car/characteristic/index'),array('class'=>'btn btn-primary pull-right'));
    echo CHtml::link('Редактировать',array('/backend/car/characteristic/update/'.$model->id_car_characteristic),array('class'=>'btn btn-success pull-right'))."<br><br>";
                                   
    $this->widget(
        'booster.widgets.TbDetailView',
        array(
            'data' => array(
                'id_car_characteristic' => $model->id_car_characteristic,
                'name' => $model->name,
                'id_parent' => $model->parent->name,
                'id_car_type' => $model->type->name,
            ),
            'attributes' => array(
                array('name' => 'id_car_characteristic', 'label' => 'ID'),
                array('name' => 'name', 'label' => 'Характеристика'),
                array('name' => 'id_parent', 'label' => 'Группа','type'=>'raw'),
                array('name' => 'id_car_type', 'label' => 'Тип авто','type'=>'raw'),
                
            ),
        )
    );
    
    
?>