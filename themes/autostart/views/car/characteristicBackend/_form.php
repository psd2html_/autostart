<?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'verticalForm',
            'htmlOptions' => array('class' => 'well', 'enctype'=>'multipart/form-data'), // for inset effect
            
        )
    );
    ?>
    
<?php 

    echo $form->textFieldGroup($model, 'name');
    
    echo $form->dropDownListGroup(
        $model,
        'id_parent',
        array(
            'widgetOptions' => array(
                'data' => CHtml::listData(CharacteristicGroup::model()->findAll(array('order'=>'name')),'id_car_characteristic_group', 'name'),
                'htmlOptions' => array(
                    'encode' => false,
                ),
            ),
        )
    );

    echo $form->dropDownListGroup(
        $model,
        'id_car_type',
        array(
            'widgetOptions' => array(
                'data' => CHtml::listData(Type::model()->findAll(),'id_car_type', 'name'),
                'htmlOptions' => array(
                    'encode' => false,
                ),
            ),
        )
    );                      

    
    $this->widget(
        'booster.widgets.TbButton',
        array('buttonType' => 'submit', 'label' => 'Сохранить', 'context' => 'success')
    );
    
    echo " ".CHtml::link('Отмена',array('/backend/car/characteristic/index'),array('class'=>'btn btn-danger ')) ;
    $this->endWidget();
    unset($form);
?>