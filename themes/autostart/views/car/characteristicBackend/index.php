<h1>Характеристики модификаций</h1>                                   
<?php                               
    $gridColumns = array(
        array('name'=>'id_car_characteristic', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 60px')),
        array('name'=>'name', 'header'=>'Характеристика'),
        array('name'=>'id_parent', 'header'=>'Группа', 'value'=>'$data->parent->name',
            'filter'=>CHtml::activeDropDownList($model,'id_parent',  
                CHtml::listData(CharacteristicGroup::model()->findall(array('order'=>'name')), 'id_car_characteristic_group', 'name'),
                array(
                    'class' => 'form-control',
                    'empty' => 'Все группы',
                )
            ),
        ),
        array('name'=>'id_car_type', 'header'=>'Тип авто','value'=>'$data->type->name',
            'filter'=>CHtml::activeDropDownList($model,'id_car_type',  
                CHtml::listData(Type::model()->findall(array('order'=>'name')), 'id_car_type', 'name'),
                array(
                    'class' => 'form-control'
                )
            ),
        ),
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{view} {update} {delete}',
        ),
    );
    
    echo CHtml::link('Добавить',array('/backend/car/Characteristic/create'),array('class'=>'btn btn-primary pull-right'));

    
    $this->widget(
        'booster.widgets.TbGridView',
        array(
            'dataProvider' => $model->search(),
            'template' => "{items}{pager}",
            'columns' => $gridColumns,
            'filter' => $model,
        )
    ); 
    
?>