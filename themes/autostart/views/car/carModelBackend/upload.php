<h1>Импорт автомобилей</h1>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '" style="color: green; fon-weight: bold; margin:15px 0px;">' . $message . "</div>\n";
    }
?>            
<?php                               
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'verticalForm',
            'htmlOptions' => array('class' => 'well', 'enctype'=>'multipart/form-data'), // for inset effect
            'action' => Yii::app()->createUrl('car/carModelBackend/importModel')
        )
    );
    ?>
    <h3>Импорт моделей</h3>
<?php
    echo CHtml::fileField('importModel', '', array())." ";
    
    echo "<br>";
    $this->widget(
        'booster.widgets.TbButton',
        array('buttonType' => 'submit', 'label' => 'Загрузить', 'context' => 'success')
    );
    $this->endWidget();
?>

<?php                               
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'verticalForm',
            'htmlOptions' => array('class' => 'well', 'enctype'=>'multipart/form-data'), // for inset effect
            'action' => Yii::app()->createUrl('car/carModelBackend/importImagesModel')
        )
    );
    ?>
    <h3>Импорт изображений</h3>
<?php
    echo CHtml::fileField('importImages', '', array())." ";
    
    echo "<br>";
    $this->widget(
        'booster.widgets.TbButton',
        array('buttonType' => 'submit', 'label' => 'Загрузить', 'context' => 'success')
    );
    $this->endWidget();
?>

<?php                               
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'verticalForm',
            'htmlOptions' => array('class' => 'well', 'enctype'=>'multipart/form-data'), // for inset effect
            'action' => Yii::app()->createUrl('car/carModelBackend/updatePrices')
        )
    );
    ?>
    <h3>Обновление прайса</h3>
<?php
    echo CHtml::fileField('updatePrices', '', array())." ";
    
    echo "<br>";
    $this->widget(
        'booster.widgets.TbButton',
        array('buttonType' => 'submit', 'label' => 'Загрузить', 'context' => 'success')
    );
    $this->endWidget();
?>

