<?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'verticalForm',
            'htmlOptions' => array('class' => 'well', 'enctype'=>'multipart/form-data'), // for inset effect
            
        )
    );
    ?>
    
<?php 

    
    echo $form->dropDownListGroup(
        $model,
        'id_car_mark',
        array(
            'widgetOptions' => array(
                'data' => CHtml::listData(Mark::model()->findAll(array('order'=>'name')),'id_car_mark', 'name'),
                'htmlOptions' => array(
                    'encode' => false,
                ),
            ),
        )
    );
    echo $form->textFieldGroup($model, 'name');
    echo $form->textFieldGroup($model, 'name_rus');
    echo $form->textFieldGroup($model, 'alias');
    echo $form->checkboxGroup($model, 'is_visible');
    echo $form->dropDownListGroup(
        $model,
        'id_car_type',
        array(
            'widgetOptions' => array(
                'data' => CHtml::listData(Type::model()->findAll(),'id_car_type', 'name'),
                'htmlOptions' => array(
                    'encode' => false,
                ),
            ),
        )
    );                      
   
?>
<h3>Доп. Характеристики</h3>
    
    <table class="detail-view table table-striped table-condensed" id="yw0">
        <tbody>
<?php
            echo "<tr class='odd'><th>Иконка</th>";
            echo "<td>".(($model->icon && $model->icon)?CHtml::image(Yii::app()->getModule('car')->urlModelPath."/100x100_".$model->icon,'',array("style"=>"float:right;")):"");
            echo CHtml::fileField('CarModel[icon]', '', array('id'=>'icon'))." (изображение для отображения в поиске)</td></tr>";
?>
        </tbody>
    </table>

<?php

    
    $this->widget(
        'booster.widgets.TbButton',
        array('buttonType' => 'submit', 'label' => 'Сохранить', 'context' => 'success')
    );
    
    echo " ".CHtml::link('Отмена',array('/backend/car/carModel/index'),array('class'=>'btn btn-danger ')) ;
    $this->endWidget();
    unset($form);
?>