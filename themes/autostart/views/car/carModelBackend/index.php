<h1>Модели автомобилей</h1>
            
<?php                               
    echo CHtml::link('Добавить',array('/backend/car/carModel/create'),array('class'=>'btn btn-primary pull-right'));

    $gridColumns = array(
        array(
            'name'=>'icon', 
            'header'=>'',
            'type'=>'html',
            'value'=>'!empty($data->icon)?CHtml::image(Yii::app()->getModule(\'car\')->urlModelPath."/100x100_".$data->icon,$data->name,array("style"=>"width:50px;")):""',
            'filter'=>'',
        ),
        array('name'=>'id_car_model', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 60px')),
        array('name'=>'id_car_mark', 'header'=>'Марка','value'=>'$data->mark->name',
            'filter'=>CHtml::activeDropDownList($model,'id_car_mark',  
                CHtml::listData(Mark::model()->findall(array('order'=>'name')), 'id_car_mark', 'name'),
                array(
                    'empty' => 'Все марки',
                    'class' => 'form-control'
                )
            ),    
        ),
        array('name'=>'name', 'header'=>'Название модели'),
        array('name'=>'alias', 'header'=>'Alias'),
        array('name'=>'is_visible', 'header'=>'Видимость',             
            'value'=>'$data->is_visible?"Да":"Нет"',
            'filter' =>CHtml::activeDropDownList($model, 'is_visible',  
                array(
                    ''=>'Все',
                    '1'=>'Да',
                    '0'=>'Нет',
                ),
                array('class' => 'form-control'
                )
            ), 
        ),
        array('name'=>'id_car_type', 'header'=>'Тип авто','value'=>'$data->type->name',
            'filter'=>CHtml::activeDropDownList($model,'id_car_type',  
                CHtml::listData(Type::model()->findall(), 'id_car_type', 'name'),
                array(
                    'class' => 'form-control'
                )
            ),
        ),
        array('name'=>'name_rus', 'header'=>'Назв. рус.'),
        array(
            'template'=>'{model} {complectation} {update} {delete}',
            'class'=>'booster.widgets.TbButtonColumn',
            'buttons'=>array(
                'complectation' => array
                    (
                        'label'=>'Добавить комплектацию',
                        'icon'=>'th-list',
                        'url'=>'Yii::app()->createUrl("car/complectationBackend/create", array("carModelId"=>$data->id_car_model))',
                    ),
                'model' => array
                    (
                        'label'=>'Добавить модификацию',
                        'icon'=>'plus',
                        'url'=>'Yii::app()->createUrl("car/modificationBackend/create", array("carModelId"=>$data->id_car_model))',
                    ),
            ),            
        )
    );
    



    
    $this->widget(
        'booster.widgets.TbGridView',
        array(
            'dataProvider' => $model->search(),
            'template' => "{items}{pager}",
            'columns' => $gridColumns,
            'filter' => $model,
        )
    ); 
    
?>