
<h1>Типы кузовов</h1>

<?php                                      
    echo CHtml::link('Добавить',array('/backend/car/body/create'),array('class'=>'btn btn-primary pull-right'));
    
    $gridColumns = array(
        array('name'=>'id_car_body', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 60px')),
        array('name'=>'name', 'header'=>'Тип'),
        array('name'=>'alias', 'header'=>'Alias'),
        array('name'=>'image', 'header'=>'Изображение', 'type' => 'raw', 'value'=>'$data->image?CHtml::image(Yii::app()->getModule(\'car\')->urlBodyPath."/100x100_".$data->image):""'),
        array('name'=>'icon', 'header'=>'Иконка', 'type' => 'raw', 'htmlOptions'=>array('style'=>'width: 100px')),
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{view} {update} {delete}',
        ),
    );
    $this->widget(
        'booster.widgets.TbGridView',
        array(
            'dataProvider' => $model->search(),
            'template' => "{items}{pager}",
            'columns' => $gridColumns,
            'filter' => $model,
        )
    ); 
    
?>