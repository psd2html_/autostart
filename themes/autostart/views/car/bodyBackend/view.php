
<h1>Типы кузова <strong><?=$model->name?></strong></h1>

<?php                                      
    echo CHtml::link('Все типы',array('/backend/car/body/index'),array('class'=>'btn btn-primary pull-right'));
    echo CHtml::link('Редактировать',array('/backend/car/body/update/'.$model->id_car_body),array('class'=>'btn btn-success pull-right'))."<br><br>";
                                   
    $this->widget(
        'booster.widgets.TbDetailView',
        array(
            'data' => array(
                'id_car_body' => $model->id_car_body,
                'name' => $model->name,
                'alias' => $model->alias,
                'image' => $model->image?CHtml::image("/upload/car/body/100x100_".$model->image):"",
                'icon' => CHtml::tag('div', array('style'=>'width: 100px;'), $model->icon),
            ),
            'attributes' => array(
                array('name' => 'id_car_body', 'label' => 'ID'),
                array('name' => 'name', 'label' => 'Тип кузова'),
                array('name' => 'alias', 'label' => 'Alias'),
                array('name' => 'image', 'label' => 'Изображение','type'=>'raw'),
                array('name' => 'icon', 'label' => 'Иконка','type'=>'raw'),
                
            ),
        )
    );
    
    
?>