<?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'verticalForm',
            'htmlOptions' => array('class' => 'well', 'enctype'=>'multipart/form-data'), // for inset effect
            
        )
    );
     
    echo $form->textFieldGroup($model, 'name');
    echo $form->textFieldGroup($model, 'alias');
    echo $form->fileFieldGroup($model, 'image',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
        )
    );
    $path=Yii::getPathOfAlias(Yii::app()->getModule('car')->assetsBodyPath).'/';
    echo $model->image?CHtml::image("/upload/car/body/100x100_".$model->image):"";

    echo $form->textAreaGroup($model, 'icon');
    echo CHtml::tag('div', array('style'=>'width: 100px;'), $model->icon);
    $this->widget(
        'booster.widgets.TbButton',
        array('buttonType' => 'submit', 'label' => 'Сохранить', 'context' => 'success')
    );
    
    echo " ".CHtml::link('Отмена',array('/backend/car/body/index'),array('class'=>'btn btn-danger ')) ;
    $this->endWidget();
    unset($form);
?>