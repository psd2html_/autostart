<h1>Модификации автомобилей</h1> (новую модификацию можно добавить из списка <?=CHtml::link('моделей',Yii::app()->createUrl('/backend/car/carModel/'))?>)
                    
<?php                               
    $gridColumns = array(
        array(
            'class' => 'CCheckBoxColumn',
            // enable selection multiple checkboxes
            'selectableRows' => 2,
            'checkBoxHtmlOptions' => array(
                    // array where the ids will be collected
                    'name' => 'car_modification_ids[]',
            ),
            // id value from $data
            'value' => '$data->id_car_modification'
        ),
        array('name'=>'id_car_modification', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 60px')),
        array('name'=>'mark', 'header'=>'Марка','value'=>'$data->mark->name',
            'filter'=>CHtml::activeDropDownList($model,'mark',  
                CHtml::listData(Mark::model()->findall(array('order'=>'name')), 'id_car_mark', 'name'),
                array(
                    'empty' => 'Все марки',
                    'class' => 'form-control'
                )
            ),    
        ),
        array('name'=>'model', 'header'=>'Модель','value'=>'$data->model->name'),
        array('name'=>'name', 'header'=>'Модификация'),
        array('name'=>'is_visible', 'header'=>'Видимость',             
            'value'=>'$data->is_visible?"Да":"Нет"',
            'filter' =>CHtml::activeDropDownList($model, 'is_visible',  
                array(
                    ''=>'Все',
                    '1'=>'Да',
                    '0'=>'Нет',
                ),
                array('class' => 'form-control'
                )
            ), 
        ),
        array('name'=>'body', 'header'=>'Кузов','value'=>'$data->body->name',
            'filter' =>CHtml::activeDropDownList($model, 'id_car_body',  
                CHtml::listData(Body::model()->findall(array('order'=>'name')), 'id_car_body', 'name'),
                array(
                    'empty' => 'Все типы кузова',
                    'class' => 'form-control'
                )
            ), 
        ),
        array(
            'header'=>'
<button id="#del_all_modifications" onclick="delAll();" type="button" class="btn btn-danger btn-xs">
Удалить
</button>',
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{update} {delete}',

        ),
    );
    
    $this->widget(
        'booster.widgets.TbGridView',
        array(
            'dataProvider' => $model->search(),
            'template' => "{items}{pager}",
            'columns' => $gridColumns,
            'filter' => $model,
        )
    );
?>
<script>
    function delAll(){
        var idList    = $('input[type=checkbox]:checked').serialize();
        if(idList)
        {
            if(!confirm('Вы уверены, что хотите удалить данные элементы?')) return false;

            $.post('/backend/car/modification/deleteAll',idList,function(response){
                jQuery('#yw0').yiiGridView('update');
            });

        }
        return false;
    }
</script>