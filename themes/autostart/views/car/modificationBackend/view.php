<h1>Модификация <strong><?=$model->name?></strong></h1>
                    
<?php                               
    //echo CHtml::link('Редактировать',array('/backend/car/modification/update/'.$model->id_car_modification),array('class'=>'btn btn-success pull-right'));
    $this->widget(
        'booster.widgets.TbDetailView',
        array(
            'data' => array(
                'id_car_modification' => $model->id_car_modification,
                'id_car_type' => $model->type->name,
                'mark' => $model->mark->name,
                'model' => $model->model->name,
                'name' => $model->name,
                'start_production_year' => $model->start_production_year,
                'end_production_year' => $model->end_production_year,
                'is_visible' => $model->is_visible?'Да':'Нет',
                'price_min' => $model->price_min,
                'price_max' => $model->price_max,

            ),
            'attributes' => array(
                array('name' => 'id_car_modification', 'label' => 'ID'),
                array('name' => 'id_car_type', 'label' => 'Тип авто'),
                array('name' => 'mark', 'label' => 'Марка автомобиля'),
                array('name' => 'model', 'label' => 'Модель автомобиля'),
                array('name' => 'name', 'label' => 'Название модификции'),
                array('name' => 'start_production_year', 'label' => 'Год начала производства'),
                array('name' => 'end_production_year', 'label' => 'Год конца производства'),
                array('name' => 'is_visible', 'label' => 'Видимость'),
                array('name' => 'price_min', 'label' => 'Минимальная цена'),
                array('name' => 'price_max', 'label' => 'Максимальная цена'),
            ),
        )
    );
    
?>
<h3>Характеристики</h3>
<?php         
    $arr = array();                      
    foreach($model->defaultCharacteristicValues as $char)
    {
        $arr[$char->characteristic->parent->name][$char->characteristic->name] = $char->value.($char->unit?(' '.$char->unit):'');
    };
    
    $tdclass = "odd";
    $parent = "";

    foreach($arr as $name=>$group)
    {
        ?>
        <h4><?=$name?></h4>
        <?php
        $data = array();
        $labels = array();
        $i = 0;
        foreach($group as $key => $value)    
        {
            $data['key_'.$i]=$value;
            array_push($labels,array('name'=>'key_'.$i, 'label'=>$key));
            $i++;
        }
        $this->widget(
            'booster.widgets.TbDetailView',
            array(
                'data' => $data,
                'attributes' => $labels,
            )
        );

    }
    
?>

<?php
/*<h3>Доп. Характеристики</h3>
    <table class="detail-view table table-striped table-condensed" id="yw0"><tbody>

    $tdclass = "odd";
    $parent = "";
    foreach($model->characteristicValues as $char)
    {
        if($parent!=$char->characteristic->parent->name)
        {
            $parent=$char->characteristic->parent->name;
            echo "<tr><th colspan='2' style='text-align:left;'>".$parent."</th></tr>";    
        }
        echo "<tr class='".$tdclass."'><th>".$char->characteristic->name."</th><td>";
        echo $char->image?CHtml::image("/upload/car/chars/100x100_".$char->image,'',array("style"=>"float:right;")):""; 
        echo "</td></tr>";
        echo "</td></tr>";

        $tdclass=($tdclass=="odd"?"even":"odd");
            
    }   
</tbody></table>
    */
?>

