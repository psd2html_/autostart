<?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'verticalForm',
            'type'=> 'horizontal',
            'htmlOptions' => array('class' => 'well', 'enctype'=>'multipart/form-data'), // for inset effect
            
        )
    );
    ?>
    
                    
<?php 
    echo '<div class="form-group"><label class="control-label required col-sm-3">Марка:</label><div class="col-sm-9">';
    echo "<input type='text' class='form-control' value='".$carModel->mark->name."' disabled='disabled'></div></div>";
    
    echo '<div class="form-group"><label class="control-label required col-sm-3">Модель:</label><div class="col-sm-9">';
    echo "<input type='text' class='form-control' value='".$carModel->name."' disabled='disabled'></div></div>";

    echo '<div class="form-group"><label class="control-label required col-sm-3">Поколения:</label><div class="col-sm-9">';
    
    $sel = array();
    foreach($model->generations as $gen)
    {
        $sel[$gen->id_car_generation] = array('selected'=>'selected');
    }
    $this->widget(
        'booster.widgets.TbSelect2',
            array(
                'name' => 'generations',
                'data' => $carModel->getGenerationListWithName(),
                'options'=> array(
                    'width' => '100%',
                ),
                'htmlOptions' => array(
                    'multiple' => 'multiple',
                    'options'=> $sel,
                ),
            )
    ); 
    echo "</div></div>";    
 
    /*echo "<div style='display:none;'>";
    echo $form->textFieldGroup($model, 'id_car_generation',
        array(
            'widgetOptions'=>array(
                'htmlOptions'=>array('disabled'=>true)),
                'label'=>'Поколение ('.$generation->name.')'
            )
        );
    echo "</div>";*/
    
    echo $form->textFieldGroup($model, 'name');
    //echo $form->textFieldGroup($model, 'start_production_year');
    //echo $form->textFieldGroup($model, 'end_production_year');
    echo $form->checkboxGroup($model, 'is_visible');
    /*echo $form->textFieldGroup($model, 'price_min',
        array(
            'widgetOptions'=>array(
                'htmlOptions'=>array('disabled'=>true)),
            )
        );
    echo $form->textFieldGroup($model, 'price_max',
        array(
            'widgetOptions'=>array(
                'htmlOptions'=>array('disabled'=>true)),
            )
        );*/
    echo $form->dropDownListGroup(
        $model,
        'id_car_type',
        array(
            'widgetOptions' => array(
                'data' => CHtml::listData(Type::model()->findAll(),'id_car_type', 'name'),
                'htmlOptions' => array(
                    'encode' => false,
                ),
            ),
        )
    );  
    ?>
        <div class="form-group"><label class="col-sm-3 control-label required" for="Modification_id_car_type">Кузов <span class="required">*</span></label><div class="col-sm-9">
    <?php
                     
    echo $form->dropDownList($model,'id_car_body',CHtml::listData(Body::model()->findAll(),'id_car_body','name'),array('class'=>'form-control'))."</div></div>";
  
?>
<h3>Характеристики</h3>    
<?php
    if($model->isNewRecord)
    {
        echo "<div style='font-style:italic;'>Заполнение характеристик доступно после сохранения модификации<br><br></div>";
    }
    else
    {
?>    
    
    
    <table class="detail-view table table-striped table-condensed" id="yw0"><tbody>
<?php
    $class = 'odd';
    $parent = '';
    
    $charArr = array();
    
    foreach($model->characteristicValues as $char)
    {

        if (!isset($char->characteristic)) {
            continue;
        }
        $charArr[$char->characteristic->parent->id_car_characteristic_group][]=$char;
    }
    
    foreach($charArr as $charGroup)
    {
        foreach($charGroup as $char)
        {
            if($parent != $char->characteristic->parent->name)
            {
                $parent = $char->characteristic->parent->name;
                echo '<tr><th colspan="2" style="text-align:center">'.$char->characteristic->parent->name.'</th></tr>';
            }
            
            echo "<tr class='".$class."'><th>".$char->characteristic->name."</th><td>";
            
            if(in_array($char->characteristic->id_car_characteristic,array(Modification::CHARACTERISTIC_FUEL_ID, Modification::CHARACTERISTIC_PRIVOD_ID, Modification::CHARACTERISTIC_TRANSMISSION_ID)))
            {
                $data = array();
                if($char->characteristic->id_car_characteristic==Modification::CHARACTERISTIC_PRIVOD_ID)
                {
                    $data = Yii::app()->cars->getPrivods();
                }
                else if($char->characteristic->id_car_characteristic==Modification::CHARACTERISTIC_TRANSMISSION_ID)
                {
                    $data = Yii::app()->cars->getTransmissions();
                }
                else if($char->characteristic->id_car_characteristic==Modification::CHARACTERISTIC_FUEL_ID)
                {
                    $data = Yii::app()->cars->getFuelTypes(); 
                }
                echo CHtml::dropDownList('Modification[char]['.$char->id_car_characteristic_value.']',$char->value,$data,array('class'=>'form-control'));
            }
            else
            {
                echo "<div class='col-sm-8'>".CHtml::textField('Modification[char]['.$char->id_car_characteristic_value.']',$char->value,array('class'=>'form-control'));
                echo "</div><div class='col-sm-4'><label class='control-label col-sm-4'>в </label><div class='col-sm-8'>";
                echo CHtml::textField('Modification[charUnit]['.$char->id_car_characteristic_value.']',$char->unit,array('class'=>'form-control'));
            }
            echo "</div></div>";
            echo "<td></tr>";
            
            $class=($class=="odd"?"even":"odd");
        }
    }
?>
</tbody></table>
<?php
    }

    
    $this->widget(
        'booster.widgets.TbButton',
        array('buttonType' => 'submit', 'label' => 'Сохранить', 'context' => 'success')
    );
    
    echo " ".CHtml::link('Отмена',array('/backend/car/modification/index'),array('class'=>'btn btn-danger ')) ;
    $this->endWidget();
    unset($form);
?>