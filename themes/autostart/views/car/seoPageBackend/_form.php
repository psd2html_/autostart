<?php
/* @var $this SeoPageBackendController */
/* @var $model SeoPage */
/* @var $form CActiveForm */
?>

<div class="form">

	<?php     $form = $this->beginWidget(
		'booster.widgets.TbActiveForm',
		array(
			'id' => 'verticalForm',
			'htmlOptions' => array('class' => 'well ', 'enctype'=>'multipart/form-data'), // for inset effect

		)
	); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">

		<div class="col-xs-6">

			<div class="form-group">
                <?php echo $form->labelEx($model,'mark'); ?>

				<?php
				echo CHtml::dropDownList(
					'id_car_mark',
					!$model->isNewRecord?$model->mark->id_car_mark:'',
					CHtml::listData(Mark::model()->findAll(array('condition'=>'is_visible=:is_visible','params'=>array(':is_visible'=>1), 'order'=>'name')), 'id_car_mark', 'name'),
					array('class'=>'form-control', 'empty'=>'',
						'ajax'=>array(
							'type'=>'POST', //request type
							'url'=>CController::createUrl('/car/catalog/getmodellist',array('dropdown'=>1)), //url to call.
							'data'=>array(
								'mark'=>'js:this.value',
							),
							'success'=>'updateSeoPageFields',
							's')
					)
				);
				?>

			</div>

		</div>

		<div class="col-xs-6">
			<?php
			echo $form->dropDownListGroup(
				$model,
				'id_car_model',
				array('class'=>'form-group ',
					'widgetOptions' => array(
						'data' => $model->isNewRecord?array():CHtml::listData(CarModel::model()->findAll(array('condition'=>'id_car_mark=:id_car_mark', 'params'=>array(':id_car_mark'=>$model->mark->id_car_mark), 'order'=>'name')), 'id_car_model', 'name'),
						'htmlOptions' => array(
							'encode' => false,
							'empty'=>'',
							'ajax'=>array(
								'type'=>'POST', //request type
								'url'=>CController::createUrl('/car/catalog/getmodificationlist',array('dropdown'=>1)), //url to call.
								'data'=>array(
									'model'=>'js:this.value',
								),
								'update'=>'#SeoPage_id_car_modification',
								's')
						),
					),
				)
			);

			?>
		</div>

	</div>

	<div class="row">

		<div class="col-xs-6">
			<?php echo $form->textFieldGroup($model,'title',array('size'=>60,'maxlength'=>250)); ?>
		</div>


		<div class="col-xs-6">
			<div class="form-group">
				<label class="control-label" for="SeoPage_slug">Slug</label>
				<?php echo $form->textField($model,'slug',array('disabled'=>'disabled','size'=>60,'maxlength'=>150, 'class'=>'form-control')); ?>
			</div>
		</div>

	</div>

	<?php echo $form->textAreaGroup($model,'description',array('size'=>60,'maxlength'=>250)); ?>

	<?php
	echo $form->ckEditorGroup(
		$model,
		'body',
		array(
			'wrapperHtmlOptions' => array(
				/* 'class' => 'col-sm-5', */
			),
			'widgetOptions' => array(
				'editorOptions' => array(
					'fullpage' => 'js:true',
				)
			)
		)
	);
	?>

	<?php echo $form->dropDownListGroup(
		$model,
		'group_id',
		array(
			'wrapperHtmlOptions' => array(
				'class' => 'col-sm-5',
			),
			'widgetOptions' => array(
				'data' => Yii::app()->cars->getGroups(),
				'htmlOptions' => array(),
			)
		)
	);
	?>

	<div class="row BGroupBlock GroupBlock">

		<div class="col-xs-3">
			<?php
			echo $form->dropDownListGroup(
				$model,
				'id_car_modification',
				array('class'=>'form-group ',
					'widgetOptions' => array(
						'data' => $model->isNewRecord?array():CHtml::listData(Modification::model()->findAll(array(
							'with'      => array('model'),
							'condition' => 'model.id_car_model=:id_car_model',
							'params'    => array(':id_car_model'=>(isset($model->model->id_car_model))?$model->model->id_car_model:''),
							'order'     => 'id_car_modification'
							)
						),'id_car_modification', 'name'),
						'htmlOptions' => array(
							'encode' => false,
							'empty' => '',
							'disabled' => ($model->isNewRecord)?'disabled':''
						),
					),
				)
			);
			?>
		</div>

		<div class="col-xs-3">

			<?php echo $form->dropDownListGroup(
				$model,
				'id_car_privod',
				array(
					'widgetOptions' => array(
						'data' => Yii::app()->cars->getPrivods(),
						'htmlOptions' => array('empty' => '', 'disabled'=>($model->id_car_privod=='Полный' || $model->isNewRecord)?'disabled':''),
					)
				)
			);
			?>

		</div>

		<div class="col-xs-3">

			<?php echo $form->dropDownListGroup(
				$model,
				'id_car_fuel',
				array(
					'widgetOptions' => array(
						'data' => Yii::app()->cars->getFuelTypes(),
						'htmlOptions' => array('empty' => '',  'disabled'=>($model->id_car_fuel=='Дизель' || $model->isNewRecord)?'disabled':''),
					)
				)
			);
			?>

		</div>

		<div class="col-xs-3">

			<?php echo $form->dropDownListGroup(
				$model,
				'id_car_transmission',
				array(
					'widgetOptions' => array(
						'data' => Yii::app()->cars->getTransmissions(),
						'htmlOptions' => array('empty' => '', 'disabled'=>($model->isNewRecord)?'disabled':''),
					)
				)
			);
			?>

		</div>

	</div>

	<div class="row CGroupBlock GroupBlock">

		<div class="col-xs-12">
			<?php echo $form->dropDownListGroup(
				$model,
				'group_block_sort',
				array(
					'wrapperHtmlOptions' => array(
						'class' => 'col-sm-5',
					),
					'widgetOptions' => array(
						'data' => Yii::app()->cars->getGroupBlockSort(),
						'htmlOptions' => array(),
					)
				)
			);
			?>
		</div>

	</div>

	<?php
	$this->widget(
		'booster.widgets.TbButton',
		array('buttonType' => 'submit', 'label' => 'Сохранить', 'context' => 'success')
	);
	?>
	<?php $this->endWidget(); ?>

</div><!-- form -->