<?php
/* @var $this SeoPageBackendController */
/* @var $model SeoPage */

$this->breadcrumbs=array(
	'Seo Pages'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List SeoPage', 'url'=>array('index')),
	array('label'=>'Create SeoPage', 'url'=>array('create')),
	array('label'=>'Update SeoPage', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SeoPage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SeoPage', 'url'=>array('admin')),
);
?>

<h1>View SeoPage #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'mark.id_car_mark',
		'id_car_model',
		'create_time',
		'update_time',
		'title',
		'slug',
		'body',
		'description',
		array(
			'label' => 'Link',
			'type'  => 'raw',
			'value' => (isset($model->model))?'<a target="_blank" href="' . Yii::app()->createUrl('/car/catalog/single',array('mark'=>$model->mark->alias,'model'=>$model->model->alias)) . '/' . $model->slug . '">/car/' . $model->mark->alias .'/' . $model->model->alias . '/' .$model->slug . '</a>':'<a target="_blank" href="' . Yii::app()->createUrl('/car/catalog/single',array('mark'=>$model->mark->alias)) . '/' . $model->slug . '">/car/' . $model->mark->alias . '/' .$model->slug . '</a>',
		)
	)
)); ?>
