<?php
/* @var $this SeoPageBackendController */
/* @var $model SeoPage */

$this->breadcrumbs=array(
	'Seo Pages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SeoPage', 'url'=>array('index')),
	array('label'=>'Manage SeoPage', 'url'=>array('admin')),
);
?>

<h1>Create SeoPage</h1>

<?php $this->renderPartial('_form_generate', array('model'=>$model)); ?>