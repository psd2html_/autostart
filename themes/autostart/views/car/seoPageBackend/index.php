<?php
/* @var $this SeoPageBackendController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Seo Pages',
);

$this->menu=array(
	array('label'=>'Create SeoPage', 'url'=>array('create')),
	array('label'=>'Manage SeoPage', 'url'=>array('admin')),
);
?>



<h1 class="pull-left">Seo Pages</h1>

<div style="display:inline-block; line-height: 70px; margin-left: 1%;">
	<a href="<?php echo Yii::app()->createUrl('/backend/car/seoPage/deleteAll') ?>">Удалить все ссылки</a>
</div>

<?php
echo CHtml::link('Добавить',array('/backend/car/SeoPage/create'),array('class'=>'btn btn-primary pull-right'));

$gridColumns = array(
	array('name'=>'id', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 60px')),
	array('name'=>'mark', 'header'=>'Марка','value'=>'$data->mark->name',
		'filter'=>CHtml::activeDropDownList($model,'mark',
			CHtml::listData(Mark::model()->findall(array('order'=>'name')), 'id_car_mark', 'name'),
			array(
				'empty' => 'Все марки',
				'class' => 'form-control'
			)
		),
	),
	array(
		'name' => 'model',
		'header' => 'Модель',
		'value' => function ($data){
			if (isset($data->model)) {
				return $data->model->name;
			}
		}
	),
	array('name'=>'title', 'header'=>'Заголовок'),
	array('name'=>'slug', 'header'=>'Алиас'),
	array(
		'filter' => false,
		'sortable'=>false,
		'type'=>'raw',
		'header'=>'Link',
		'value'=> function ($data){
			if (isset($data->model)) {
				return '<a target="_blank" href="' . Yii::app()->createUrl('/car/catalog/single',array('mark'=>$data->mark->alias,'model'=>$data->model->alias)) . '/' . $data->slug . '">/car/' . $data->mark->alias. '/' . $data->model->alias . '/' .$data->slug . '</a>';
			} else {
				return '<a target="_blank" href="' . Yii::app()->createUrl('/car/catalog',array('mark'=>$data->mark->alias,'slug'=>$data->slug)) . '">/car/' . $data->mark->alias. '/' .$data->slug . '</a>';
			}
		}
	),
	array(
		'name'=>'id_car_body',
		'header'=>'Кузов',
		'value'=> function ($data){
			if (isset($data->carBody->name)) {
				return $data->carBody->name;
			}
		}
	),
	array(
		'name'=>'group_id',
		'header'=>'Группа',
		'value'=> function ($data){

			$groupNames = Yii::app()->cars->getGroups();

			return $groupNames[$data->group_id];
		}
	),
	array(
		'class'=>'booster.widgets.TbButtonColumn',
		'template'=>'{view} {update} {delete}',
	),
);
$this->widget(
	'booster.widgets.TbGridView',
	array(
		'dataProvider' => $model->search(),
		'template' => "{items}{pager}",
		'columns' => $gridColumns,
		'filter' => $model,
	)
);