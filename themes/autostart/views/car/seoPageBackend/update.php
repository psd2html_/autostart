<?php
/* @var $this SeoPageBackendController */
/* @var $model SeoPage */

$this->breadcrumbs=array(
	'Seo Pages'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SeoPage', 'url'=>array('index')),
	array('label'=>'Create SeoPage', 'url'=>array('create')),
	array('label'=>'View SeoPage', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SeoPage', 'url'=>array('admin')),
);
?>

<h1 class="pull-left">Update SeoPage <?php echo $model->id; ?></h1>
<?php if (!$model->isNewRecord) { ?>
	<div style="display:inline-block; line-height: 70px; margin-left: 1%;">

		<?php if (!isset($model->model)) { ?>
			<a target="_blank" href="<?php echo Yii::app()->createUrl('/car/catalog/single',array('mark'=>$model->mark->alias)) . '/' . $model->slug ?>"><?php echo Yii::app()->createUrl('/car/catalog/single',array('mark'=>$model->mark->alias)) . '/' . $model->slug ?></a>
		<?php } else { ?>
			<a target="_blank" href="<?php echo Yii::app()->createUrl('/car/catalog/single',array('mark'=>$model->mark->alias,'model'=>$model->model->alias)) . '/' . $model->slug ?>"><?php echo Yii::app()->createUrl('/car/catalog/single',array('mark'=>$model->mark->alias,'model'=>$model->model->alias)) . '/' . $model->slug ?></a>
		<?php } ?>

	</div>
<?php } ?>
<div class="clearfix"></div>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>