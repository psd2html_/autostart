<?php
/* @var $this SeoPageBackendController */
/* @var $model SeoPage */
/* @var $form CActiveForm */
?>

<div class="form">

	<?php     $form = $this->beginWidget(
		'booster.widgets.TbActiveForm',
		array(
			'id' => 'verticalForm',
			'htmlOptions' => array('class' => 'well ', 'enctype'=>'multipart/form-data'), // for inset effect

		)
	); ?>

	<div class="row">

		<div class="col-xs-6">

			<div class="form-group">
                <?php echo $form->labelEx($model,'mark'); ?>

				<?php
				echo CHtml::dropDownList(
					'id_car_mark',
					!$model->isNewRecord?$model->mark->id_car_mark:'',
					CHtml::listData(Mark::model()->findAll(array('condition'=>'is_visible=:is_visible','params'=>array(':is_visible'=>1), 'order'=>'name')),'id_car_mark', 'name'),
					array('class'=>'form-control', 'empty'=>'',
						'ajax'=>array(
							'type'=>'POST', //request type
							'url'=>CController::createUrl('/car/catalog/getmodellist',array('dropdown'=>1)), //url to call.
							'data'=>array(
								'mark'=>'js:this.value',
							),
							//'update'=>'#SeoPage_id_car_model',
							'success'=>'updateSeoPageFields',
							's')
						/*'ajax' => array( 'type'=>'POST', //request type
							//'url'=>CController::createUrl('/car/catalog/getmodellist',array('dropdown'=>1)), //url to call.
							'data'=>array(
								'mark'=>'js:this.value',
							),
							'success'=>'updateFields',
							'dataType' => 'json',
						)*/

					)
				);
				?>

			</div>

		</div>

		<div class="col-xs-6">
			<?php
			echo $form->dropDownListGroup(
				$model,
				'id_car_model',
				array('class'=>'form-group ',
					'widgetOptions' => array(
						'data' => $model->isNewRecord?array():CHtml::listData(CarModel::model()->findAll(array('condition'=>'id_car_mark=:id_car_mark','params'=>array(':id_car_mark'=>$model->mark->id_car_mark),'order'=>'name')),'id_car_model', 'name'),
						'htmlOptions' => array(
							'encode' => false,
	/*						'ajax'=>array(
								'type'=>'POST', //request type
								'url'=>CController::createUrl('/car/catalog/getmodificationlist',array('dropdown'=>1)), //url to call.
								'data'=>array(
									'model'=>'js:this.value',
								),
								'update'=>'#SeoPage_id_car_modification',
								//'success'=>'updateSeoPageFields',
								's'),*/
							 'empty'=>''

						),
					),
				)
			);

			?>
		</div>

	</div>

	<?php
	$this->widget(
		'booster.widgets.TbButton',
		array('buttonType' => 'submit', 'label' => 'Cгенерировать', 'context' => 'success')
	);
	?>
	<?php $this->endWidget(); ?>

</div><!-- form -->