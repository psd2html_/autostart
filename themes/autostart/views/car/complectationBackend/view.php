
<h1>Комплектация <strong><?=$model->name?></strong></h1>

<?php                                      
    echo CHtml::link('Все комплектации',array('/backend/car/complectation/index'),array('class'=>'btn btn-primary pull-right'));
    echo CHtml::link('Редактировать',array('/backend/car/complectation/update/'.$model->id_car_complectation),array('class'=>'btn btn-success pull-right'))."<br><br>";
                                   
    $this->widget(
        'booster.widgets.TbDetailView',
        array(
            'data' => array(
                'id_car_complectation' => $model->id_car_complectation,
                'id_car_modification' => $model->modification->mark->name." - ".$model->modification->model->name." - ".$model->modification->name." (#".$model->modification->id_car_modification.")",
                'name' => $model->name,
                'price_min' => $model->price_min,
                'price_max' => $model->price_max,
                'is_visible' => $model->is_visible?'Да':'Нет',
            ),
            'attributes' => array(
                array('name' => 'id_car_complectation', 'label' => 'ID'),
                array('name' => 'id_car_modification', 'label' => 'Модификация'),
                array('name' => 'name', 'label' => 'Комплектация'),
                array('name' => 'price_min', 'label' => 'Минимальная цена'),
                array('name' => 'price_max', 'label' => 'Максимальная цена'),
                array('name' => 'is_visible', 'label' => 'Видимость'),
                
            ),
        )
    );
    
    
?>

    <h3>Характеристики</h3>
    <table class="detail-view table table-striped table-condensed" id="yw0"><tbody>
    <?php
        
    $tdclass = "odd";
    $parent = "";
    
    foreach($model->characteristicValues as $char)
    {
        if($parent!=$char->characteristic->parent->name)
        {
            $parent=$char->characteristic->parent->name;
            echo "<tr><th colspan='2' style='text-align:left;'>".$parent."</th></tr>";    
        }
        echo "<tr class='".$tdclass."'><th>".$char->characteristic->name."</th><td>";

        echo $char->value==1?'Да':'-'; 
        echo "</td></tr>";

        $tdclass=($tdclass=="odd"?"even":"odd");
            
    }   
    ?>
    </tbody>
    </table>