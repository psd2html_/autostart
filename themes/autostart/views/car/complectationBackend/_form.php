<?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'verticalForm',
            'htmlOptions' => array('class' => 'well', 'enctype'=>'multipart/form-data'), // for inset effect
            
        )
    );

     
    //echo $form->activeHiddenField($model, 'id_car_modification');
    

    echo $form->textFieldGroup($model, 'name');
    
    echo '<div class="form-group"><label class="control-label required ">Модификации:</label>';
    
    $sel = array();
    foreach($model->genModRel as $gen)
    {
        $sel[$gen->id_car_generation_modification] = array('selected'=>'selected');
    }

    $vals = array();
    foreach($carModel->generationMods as $genMod)
    {
        $vals[$genMod->id] = $genMod->generation->name." (".$genMod->generation->year_begin." - ".$genMod->generation->year_end.") - ".$genMod->mods->name." - ".$genMod->mods->body->name;
    }
    
    
    $this->widget(
        'booster.widgets.TbSelect2',
            array(
                'name' => 'generationMods',
                'data' => $vals,
                'options'=> array(
                    'width' => '100%',
                ),
                'htmlOptions' => array(
                    'multiple' => 'multiple',
                    'options'=> $sel,
                ),
            )
    ); 
    echo "</div>";    
    
    
    if(!$model->isNewRecord)
    {
        echo '<div class="form-group"><label class="control-label required col-sm-4">Цены на модификации в комплектации <strong>'.$model->name.'</strong>:</label><div class="col-sm-8">';
        foreach($model->genModRel as $genMod)
        {
            echo '<div class="form-group"><div class="col-sm-6 text-right"  style="line-height:35px;">'.$genMod->generation->name." (".$genMod->generation->year_begin." - ".$genMod->generation->year_end.") - ".$genMod->mods->name." - ".$genMod->mods->body->name.'</div>';
            echo '<div class="col-sm-6"><input class="form-control" style="width:80%;float:left;" type="text" name="GenMod['.$genMod->id_car_complectation_generation_modification.']" value="'.$genMod->price_min.'">&nbsp;&nbsp;&nbsp;<span style="line-height:35px;">руб</span></div><div style="clear:both;"></div></div>';
        }
        echo '<br><br></div></div>';
    }
    
    echo $form->dropDownListGroup(
        $model,
        'is_visible',
        array(
            'widgetOptions' => array(
                'data' => array('1'=>'Да','0'=>'Нет'),
                'htmlOptions' => array(
                    'encode' => false,
                ),
            ),
        )
    );     
    ?>
    <h3>Характеристики</h3>
    <table class="detail-view table table-striped table-condensed" id="yw0"><tbody>
    <?php
        
    $tdclass = "odd";
    $parent = "";
    
    foreach($model->characteristicValues as $char)
    {
        if($parent!=$char->characteristic->parent->name)
        {
            $parent=$char->characteristic->parent->name;
            echo "<tr><th colspan='2' style='text-align:left;'>".$parent."</th></tr>";    
        }
        echo "<tr class='".$tdclass."'><th>".$char->characteristic->name."</th><td>";

        echo CHtml::CheckBox('Complectation[chars]['.$char->id_car_characteristic_value.']', $char->value); 
        echo "</td></tr>";

        $tdclass=($tdclass=="odd"?"even":"odd");
            
    }
    ?>
    </tbody>
    </table>
    <?php
    
    
    $this->widget(
        'booster.widgets.TbButton',
        array('buttonType' => 'submit', 'label' => 'Сохранить', 'context' => 'success')
    );
    
    echo " ".CHtml::link('Отмена',array('/backend/car/complectation/index'),array('class'=>'btn btn-danger ')) ;
    $this->endWidget();
    unset($form);
?>