
<h1>Комплектации автомобилей</h1> (новую комплектацию можно добавить из списка <?=CHtml::link('моделей',Yii::app()->createUrl('/backend/car/carModel/'))?>)

<?php                                      
    
    $gridColumns = array(
        array(
            'class' => 'CCheckBoxColumn',
            // enable selection multiple checkboxes
            'selectableRows' => 2,
            'checkBoxHtmlOptions' => array(
                // array where the ids will be collected
                'name' => 'car_complectation_ids[]',
            ),
            // id value from $data
            'value' => '$data->id_car_complectation'
        ),
        array('name'=>'id_car_complectation', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 60px')),
        array('name'=>'mark', 'header'=>'Марка','value'=>'$data->mark->name',
            'filter'=>CHtml::activeDropDownList($model,'mark',  
                CHtml::listData(Mark::model()->findall(array('order'=>'name')), 'id_car_mark', 'name'),
                array(
                    'empty' => 'Все марки',
                    'class' => 'form-control'
                )
            ),    
        ),
        array('name'=>'model', 'header'=>'Модель','value'=>'$data->model->name'),
        array('name'=>'name', 'header'=>'Комплектация'),
        array('name'=>'is_visible', 'header'=>'Видимость', 'value'=>'$data->is_visible?"Да":"Нет"'),
        array('header'=>'
<button id="#del_all_complectations" onclick="delAll();" type="button" class="btn btn-danger btn-xs">
Удалить
</button>',
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{update} {delete}',
        ),
    );
    $this->widget(
        'booster.widgets.TbGridView',
        array(
            'dataProvider' => $model->search(),
            'template' => "{items}{pager}",
            'columns' => $gridColumns,
            'filter' => $model,
        )
    ); 
    
?>
<script>
    function delAll(){
        var idList    = $('input[type=checkbox]:checked').serialize();
        if(idList)
        {
            if(!confirm('Вы уверены, что хотите удалить данные элементы?')) return false;

            $.post('/backend/car/complectation/deleteAll',idList,function(response){
                jQuery('#yw0').yiiGridView('update');

            });

        }
        return false;
    }
</script>
