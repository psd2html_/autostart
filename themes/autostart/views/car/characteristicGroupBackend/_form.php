<?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'verticalForm',
            'htmlOptions' => array('class' => 'well', 'enctype'=>'multipart/form-data'), // for inset effect
            
        )
    );
     
    echo $form->textFieldGroup($model, 'name');
    
    $this->widget(
        'booster.widgets.TbButton',
        array('buttonType' => 'submit', 'label' => 'Сохранить', 'context' => 'success')
    );    
    echo " ".CHtml::link('Отмена',array('/backend/car/characteristicGroup/index'),array('class'=>'btn btn-danger ')) ;
    $this->endWidget();
    unset($form);
?>