<h1>Группы характеристик для модификаций</h1>                                   
<?php                               
    $gridColumns = array(
        array('name'=>'id_car_characteristic_group', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 60px')),
        array('name'=>'name', 'header'=>'Группа характеристик'),
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{view} {update} {delete}',
        ),
    );
    
    echo CHtml::link('Добавить',array('/backend/car/CharacteristicGroup/create'),array('class'=>'btn btn-primary pull-right'));



    
    $this->widget(
        'booster.widgets.TbGridView',
        array(
            'dataProvider' => $dataProvider,
            'template' => "{items}{pager}",
            'columns' => $gridColumns,
        )
    ); 
    
?>
