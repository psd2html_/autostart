
<h1>Группа характеристик <strong><?=$model->name?></strong></h1>

<?php                                      
    echo CHtml::link('Все группы',array('/backend/car/characteristicGroup/index'),array('class'=>'btn btn-primary pull-right'));
    echo CHtml::link('Редактировать',array('/backend/car/characteristicGroup/update/'.$model->id_car_characteristic_group),array('class'=>'btn btn-success pull-right'))."<br><br>";
                                   
    $this->widget(
        'booster.widgets.TbDetailView',
        array(
            'data' => array(
                'id_car_characteristic_group' => $model->id_car_characteristic_group,
                'name' => $model->name,
            ),
            'attributes' => array(
                array('name' => 'id_car_characteristic_group', 'label' => 'ID'),
                array('name' => 'name', 'label' => 'Группа'),
            ),
        )
    );
    
    
?>