var priceFormat = function(price){
	var result = "";
	var priceLength = (price+"").length;
	for (var i = priceLength-1, k=0; i >= 0; i--) {
		if(k%3===0) result = (price+"").substring(i, i+1)+" "+result;
		else result = (price+"").substring(i, i+1) + result;
		k++;
	}
	return result;
}

function checkCompare()
{
    $(".complHidden").removeClass("complHidden");
    var i = 0,
        nowIndex,
        nowHtml="",
        flug = true;
        $(".compareLeft div.compl").each(function(){
            flug = false;
            nowIndex = $(".compareLeft div.compl").index($(this));
            
            $(".assideMargin .oneCompareModel").each(function(){
                if($(this).children("div.compl").eq(nowIndex).html()=='Да')
                {
                    flug = true;
                }
            }); 
            
            if(!flug)
            {
                $(".assideMargin .oneCompareModel").each(function(){
                    $(this).children("div.compl").eq(nowIndex).addClass('complHidden');                    
                });
                $(".compareLeft div.compl").eq(nowIndex).addClass('complHidden');                    
            }
        });
 
}

function updateSeoPageFields(data) {

	$("#SeoPage_id_car_model").html(data);
	var id_car_mark = $("#id_car_mark option:selected" ).val();
	var id_car_model = $("#SeoPage_id_car_model option:selected" ).val();

	if (id_car_model) {
		$.ajax({
			'type'   : 'POST',
			'url'    : '/car/catalog/getmodificationlist/dropdown/1',
			'data'   : {'model':id_car_model},
			'0'      : 's',
			'cache'  : false,
			'success': function(html){
				$("#SeoPage_id_car_modification").html(html);
			}
		})
	}


}

$(window).load(function(){
            checkCompare();            

	$(document).on("click", "[data-btn='show']", function(){
		$(this).attr("data-btn", "hide").text("Показать только популярные");
        $("[data-other='true']").css("display","block");
		$("[data-other='false']").css("display","none");
	});
	$(document).on("click", "[data-btn='hide']", function(){
		$(this).attr("data-btn", "show").text("Показать все марки");
        $("[data-other='true']").css("display","none");
		$("[data-other='false']").css("display","block");
	});
	$(document).on("click", "[data-scroll]", function(e){
		e.preventDefault();
		var topPanel = $(".topPanel");
		if($(".singleLinks").length) {
			$("body").animate({scrollTop: $($(this).attr("href")).offset().top-85+"px"},500);
		} else {
			$("body").animate({scrollTop: $($(this).attr("href")).offset().top-topPanel.outerHeight()+"px"},500);
		}
	});
	$(document).scroll(function(){
		var topScroll = $("body").scrollTop(),
			topPanel = $(".topPanel"),
			singleLinks = $(".singleLinks"),
			topLinks = $(".topLinks"),
			compare = $(".compareContent");
		if (topScroll>=$("header").height()-topPanel.outerHeight()) {
			topPanel.addClass("whitePanel");
			$("header").css("visibility", "hidden");
			if(topLinks.length){
				topPanel.addClass("bigTopPanel");
				topLinks.css("display", "block");
			}
		} else {
			topPanel.removeClass("whitePanel");
			$("header").css("visibility", "visible");
			if(topLinks.length){
				topPanel.removeClass("bigTopPanel");
				topLinks.css("display", "none");
			}
		}
		if (compare.length) {
			if (topScroll>=compare.offset().top-topPanel.outerHeight()) {
				if(!topPanel.children(".wrappTopCompare").length) {
					var endHtml = "";
					topPanel.append("<div class='wrappTopCompare'><div class='topLeftCompare'></div><div class='topCompare'></div></div>");
					var countCompare = 0;
					$(".compareLeft").children(".oneCompare").each(function(){
						countCompare++;
						$(".topLeftCompare").append($(this).get(0).outerHTML)
						if (countCompare>3) return false;
					})
					compare.find(".oneCompareModel").each(function(){
						var inEndHtml = "";
						countCompare = 0;
						endHtml += "<div class='oneCompareModel'>";
						$(this).children(".oneCompare").each(function(){
							countCompare++;
							inEndHtml += $(this).get(0).outerHTML;
							if (countCompare>3) return false;
						});
						endHtml += inEndHtml+"</div>";
					})
					topPanel.find(".topCompare").append(endHtml);
					$(".topCompare").scroll(function(){
						console.log(45)
						$(".compareContent").scrollLeft($(".topCompare").scrollLeft());
					});
				}
				topPanel.children(".wrappTopCompare").css({display:"block"});
				topPanel.css({minHeight:"295px"});
			}
			if (topScroll<compare.offset().top-topPanel.outerHeight()+235) {
				topPanel.children(".wrappTopCompare").css({display:"none"});
				topPanel.css({minHeight:"auto"});
			}
        } else {
            if (topScroll>=$("header").height()-topPanel.outerHeight() && $(window).width()>767) {
                topPanel.addClass("whitePanel");
                $("header").css("visibility", "hidden");
                if(topLinks.length){
                    topPanel.addClass("bigTopPanel");
                    topLinks.css("display", "block");
                }
            } else {
                topPanel.removeClass("whitePanel");
                $("header").css("visibility", "visible");
                if(topLinks.length){
                    topPanel.removeClass("bigTopPanel");
                    topLinks.css("display", "none");
                }
            }
        }

	});
	if($(".compareContent").length) $("body").scrollTop(0);
	$(".oneCarLike, .singleLike").click(function(){
		$(this).toggleClass("redLike");
        
        addToCookie($(this));
        
	});
	if ($("#slider-range").length) {
		$("#slider-range").slider({
			range: true,
			min: parseInt($("#slider-range").attr("data-min").replace(/[ ]/g, '')),
			max: parseInt($("#slider-range").attr("data-max").replace(/[ ]/g, '')),
			values: [parseInt($("#downPrice").val().replace(/[ ]/g, '')), parseInt($("#topPrice").val().replace(/[ ]/g, ''))],
			slide: function( event, ui ) {
				$("#downPrice").val(priceFormat(ui.values[0]));
				$("#topPrice").val(priceFormat(ui.values[1]));
			}
		});
	};
	$("[data-on-other]").click(function(){
		$(".active[data-on-other]").removeClass("active");
		$(this).addClass("active")
		if ($(this).attr("data-on-other")==="true") {
			$("[data-other='true']").css("display", "block");
		} else {
			$("[data-other='true']").css("display", "none");
		}
	});
	$("[data-on-img]").click(function(){
		$(".active[data-on-img]").removeClass("active");
		$(this).addClass("active")
		if ($(this).attr("data-on-img")==="true") {
			$(".toImg").css("display","block");
			$(".toText").css("display","none");
		} else {
			$(".toImg").css("display","none");
			$(".toText").css("display","block");
		}
	});
	$(".STRLeft input[type='checkbox']").change(function(e){
		e.preventDefault();
		if ($(".STRLeft input[type='checkbox']:checked").length>1) {
			$(".singleComplect").removeClass("unactive");
		} else {
			$(".singleComplect").addClass("unactive");
		}
	});
	$(document).on("click", ".unactive", function(e){
		e.preventDefault();
        
	});
	var lockAnimate = false;
	$(document).on("click", ".slideRight", function(){
		var lastElem = $(this).parent().children(".onePhotoNav").last(),
			firstElem = $(this).parent().children(".onePhotoNav").first(),
			thisWidth = firstElem.outerWidth()*5,
			self = $(this);
		if (!lockAnimate && lastElem.position().left+lastElem.outerWidth()>=$(this).parent().outerWidth()) {
			if (thisWidth>(lastElem.position().left+lastElem.outerWidth() - $(this).parent().outerWidth()))
				thisWidth = lastElem.position().left+lastElem.outerWidth() - $(this).parent().outerWidth()-15;
			lockAnimate = true;
			$(this).parent().children(".onePhotoNav").each(function(){
				$(this).animate({left:$(this).position().left - thisWidth + "px"}, 300, function(){
					lockAnimate = false;
					if (firstElem.position().left<0)
						self.parent().children(".slideLeft").css("display", "block");
					else
						self.parent().children(".slideLeft").css("display", "none");
					if (lastElem.position().left+lastElem.outerWidth()-15>$(this).parent().outerWidth())
						self.parent().children(".slideRight").css("display", "block");
					else
						self.parent().children(".slideRight").css("display", "none");
				});
			});
		};
	});
	$(document).on("click", ".slideLeft", function(){
		var lastElem = $(this).parent().children(".onePhotoNav").last(),
			firstElem = $(this).parent().children(".onePhotoNav").first(),
			thisWidth = firstElem.outerWidth()*5,
			self = $(this);
		if (!lockAnimate && firstElem.position().left<-5) {
			lockAnimate = true;
			if (firstElem.position().left + thisWidth > 0) thisWidth = -firstElem.position().left;
			$(this).parent().children(".onePhotoNav").each(function(){
				$(this).animate({left:$(this).position().left + thisWidth + "px"}, 300, function(){
					lockAnimate = false;
					if (firstElem.position().left<0)
						self.parent().children(".slideLeft").css("display", "block");
					else
						self.parent().children(".slideLeft").css("display", "none");
					if (lastElem.position().left+lastElem.outerWidth()>$(this).parent().outerWidth())
						self.parent().children(".slideRight").css("display", "block");
					else
						self.parent().children(".slideRight").css("display", "none");					
				});
			});
		}
	});
	$(".filterCategory input[type='checkbox']").change(function(e){
		if ($(".filterCategory input[type='checkbox']:checked").length>1) {
			$(".compareComplect").css("visibility", "visible");
		} else {
			$(".compareComplect").css("visibility", "hidden");
		}
	});
	$(".filterCategory a").click(function(e){
		//e.preventDefault();
		$(".filterCategory.active").removeClass("active");
		$(this).parent().addClass("active");
	});
	$(".categoryOptions input[type='checkbox']").change(function(){
		if($(".categoryOptions input[type='checkbox']:checked").length){
			var optionsPrice = 0;
			$(".categoryOptions input[type='checkbox']:checked").each(function(){
				optionsPrice += parseInt($(this).val().replace(/[ ]/g, ''));
			});
			$(".categoryCounter").html("<div>"+priceFormat($(".categoryCounter").attr("data-price").replace(/[ ]/g, ''))+"<span class='countInfo'>базовая комплектация</span></div><div>+</div><div>"+priceFormat(optionsPrice)+"<span class='countInfo'>выбранные опции</span></div><div>=</div><div>"+priceFormat(parseInt($(".categoryCounter").attr("data-price").replace(/[ ]/g, ''))+optionsPrice)+"<span class='glyphicon glyphicon-ruble'></span><span class='countInfo'>общая стоимость</span></div>");
		} else {
			$(".categoryCounter").html("<div>"+priceFormat($(".categoryCounter").attr("data-price").replace(/[ ]/g, ''))+" <span class='glyphicon glyphicon-ruble'></span><span class='countInfo'>общая стоимость</span></div>");
		}
	});
	$(document).on("click", ".compareRoll", function(){
		var up = true;
		if ($(this).next(".oneCompare").css("display") === "none") up = false;
		$(".compareRoll[data-compare='"+$(this).attr("data-compare")+"']").each(function(){
			var nextElem = $(this).next(".oneCompare");
			while(nextElem.length){
				if(up) {
					nextElem.css("display","none");
					$(this).addClass("topRollCompare");
				} else { 
					nextElem.css("display","block");
					$(this).removeClass("topRollCompare");
				}
				nextElem = nextElem.next(".oneCompare");
			}
		});
	});
	$(document).on("click", ".removeCol", function(){
		var remEl = $(this).parent().parent(".oneCompareModel"),
			eqElem = remEl.index();
		$(".compareContent").children().eq(eqElem).remove();
		$(".topCompare").children().eq(eqElem).remove();
        checkCompare();
	});
	$(".onlyExclusive input[type='checkbox']").change(function(){
		if ($(this).is(":checked")) {
			var i = 0,
				nowIndex,
				nowHtml="",
				flug = true;
			$(".assideMargin .oneCompareModel").first().children("div").each(function(){
				i++;
				if (i<=4) return 0;
				nowIndex = $(this).index(".assideMargin .oneCompareModel>div.toText");
				nowHtml = $(this).html();
				$(".assideMargin .oneCompareModel").each(function(){
					if(nowHtml !== $(this).children("div.toText").eq(nowIndex).html()) {
						flug = false;
					}
					console.log(nowHtml)
					console.log($(this).children("div.toText").eq(nowIndex).html())
				});
				if (flug) {
					$(".assideMargin .compareLeft").children("div.toText").eq(nowIndex).css("display","none");
					$(".assideMargin .oneCompareModel").each(function(){
						$(this).children("div.toText").eq(nowIndex).css("display","none");
					});
				};
				flug = true;
			});
		} else {
			$(".assideMargin .oneCompareModel>div,.assideMargin .compareLeft>div").css("display","block");
			$(".topRollCompare").removeClass("topRollCompare");
		}
        
	});
	$(".oneCar h3 input[type='checkbox']").change(function(){
		if($(".oneCar h3 input[type='checkbox']").length !== $(".oneCar h3 input[type='checkbox']:checked").length) {
			$(".selectAllAuto input[type='checkbox']").removeAttr("checked");
		} else {
			$(".selectAllAuto input[type='checkbox']").prop("checked", true);
		}
	});
	$(".selectAllAuto input[type='checkbox']").change(function(){
		if(!$(".selectAllAuto input[type='checkbox']:checked").length) {
			$(".oneCar h3 input[type='checkbox']").removeAttr("checked");
		} else {
			$(".oneCar h3 input[type='checkbox']").prop("checked", true);
		}
	});
	if ($("#topCarousel .carousel-inner").length) {
		var index = 1000,
			animateTime = 500;
		$("#topCarousel .carousel-inner").children(".item").each(function(){
			$(this).css({
				position: "absolute",
				left: 0,
				top: 0,
				width: "100%",
				zIndex: index,
				display: "block"
			});
			index--;
		});
		function topNextSlide(e){
			if(e) e.preventDefault();
			var dataSlide;
			if (!lockAnimate) {
				lockAnimate = true;
				$("#topCarousel .carousel-inner .item h2").css({opacity: 0});
				$("#topCarousel .carousel-inner").children(".item").first().find("h2").css({opacity: 1});
				$("#topCarousel .carousel-inner").children(".item").first().animate({opacity:"0"}, animateTime, function(){
					$("#topCarousel .carousel-inner").append($(this));
					index = 1000;
					$("#topCarousel .carousel-inner").children(".item").each(function(){
						$(this).css({
							zIndex: index,
							opacity: 1
						});
						index -=1;
					});
					dataSlide = $("#topCarousel .carousel-inner").children(".item").first().attr("data-nav-slide");
					$("#topCarousel .carousel-indicators").children(".active").removeClass("active");
					$("#topCarousel .carousel-indicators").children("li[data-nav-slide='"+dataSlide+"']").addClass("active");
					$("#topCarousel .carousel-inner").children(".item").first().find("h2").animate({opacity: 1},animateTime);
					lockAnimate = false;
				});
			};
		}
		function topPrevSlide(e){
			e.preventDefault();
			if (!lockAnimate) {
				lockAnimate = true;
				$("#topCarousel .carousel-inner .item h2").css({opacity: 0});
				$("#topCarousel .carousel-inner").children(".item").first().find("h2").css({opacity: 1});
				var lastSlide = $("#topCarousel .carousel-inner").children(".item").last(),
					dataSlide,
					firstSlide = $("#topCarousel .carousel-inner").children(".item").first();
				firstSlide.css("zIndex", "1001");
				lastSlide.css("zIndex", "1000");
				firstSlide.after(lastSlide);
				$("#topCarousel .carousel-inner").children(".item").first().animate({opacity:"0"}, animateTime, function(){
					$("#topCarousel .carousel-inner").append($(this));
					$("#topCarousel .carousel-inner").children(".item").first().after(firstSlide);
					index = 1000;
					$("#topCarousel .carousel-inner").children(".item").each(function(){
						$(this).css({
							zIndex: index,
							opacity: 1
						});
						index -=1;
					});
					dataSlide = $("#topCarousel .carousel-inner").children(".item").first().attr("data-nav-slide");
					$("#topCarousel .carousel-indicators").children(".active").removeClass("active");
					$("#topCarousel .carousel-indicators").children("li[data-nav-slide='"+dataSlide+"']").addClass("active");
					$("#topCarousel .carousel-inner").children(".item").first().find("h2").animate({opacity: 1},animateTime);
					lockAnimate = false;
				});
			};
		}
		function toSlide(e){
			e.preventDefault();
			var dataSlide = $(this).attr("data-nav-slide"),
				thisSlide = $("#topCarousel .carousel-inner").children(".item[data-nav-slide='"+dataSlide+"']"),
				firstSlide = $("#topCarousel .carousel-inner").children(".item").first();
			if ($(this).hasClass("active")) return false;
			if (!lockAnimate) {
				lockAnimate = true;
				$("#topCarousel .carousel-inner .item h2").css({opacity: 0});
				$("#topCarousel .carousel-inner").children(".item").first().find("h2").css({opacity: 1});
				$(this).parent().children(".active").removeClass("active");
				$(this).addClass("active");
				firstSlide.css("zIndex", "1001");
				index = 1000;
				$("#topCarousel .carousel-inner").children(".item").not(":first").each(function(){
					if ($(this).attr("data-nav-slide")===dataSlide) return false;
					$(this).css({
						zIndex: index,
						opacity: 0
					});
					index--;				
				});
				$("#topCarousel .carousel-inner").children(".item").first().animate({opacity:"0"}, animateTime, function(){
					index = 1000;
					$("#topCarousel .carousel-inner").children(".item").each(function(){
						if ($(this).attr("data-nav-slide")===dataSlide) return false;
						$("#topCarousel .carousel-inner").append($(this));
					});
					$("#topCarousel .carousel-inner").children(".item").each(function(){
						$(this).css({
							zIndex: index,
							opacity: 1
						});
						index -=1;
					});
					$("#topCarousel .carousel-inner").children(".item").first().find("h2").animate({opacity: 1},animateTime);
					lockAnimate = false;
				});
			};
		}
		$("#topCarousel .carousel-control.right").click(topNextSlide);
		$("#topCarousel .carousel-control.left").click(topPrevSlide);
		$("#topCarousel .carousel-indicators li").click(toSlide);
		setInterval(function(){topNextSlide()}, 5000);
	};
	$(".dealerScroll input[type='checkbox']").change(function(){
		if ($(".dealerScroll input[type='checkbox']:checked").length) $(".buyDealer").removeClass("unactive").attr("data-toggle", "modal");
		else $(".buyDealer").addClass("unactive").removeAttr("data-toggle");
	});
	$(".dealerOtherTitle span").click(function(){
		var thisParent = $(this).parents(".dealerOtherTitle").next(".dealerOther")
		if (thisParent.find("[data-dealer='hide']").length) {
			$(this).removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up")
			thisParent.find("[data-dealer='hide']").attr("data-dealer","show");
			thisParent.find(".dealerColor").css("display", "block");	
		} else if(thisParent.find("[data-dealer='show']").length) {
			$(this).removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down")
			thisParent.find("[data-dealer='show']").attr("data-dealer","hide");
			thisParent.find(".dealerColor").css("display", "none");
		}
	});

	$("#offerFromDealer button.close").on('click',function(){
		$("#offerFromDealer .singleTable").css("display", "block");
		$(".singleFilterModal input[type='checkbox']").prop("checked", false);
		singleFilterSelect();
	});

	$(".singleFilterModal input[type='checkbox']").on('change',function(){
		$("#offerFromDealer .singleTable").css("display", "block");
		singleFilterSelectModal ();
	});

	function singleFilterSelectModal () {
		$(".singleFilterModal input[type='checkbox']").each(function(){
			var inputName = $(this).attr("name"),
				inputValue = $(this).val();
			inputName = $(this).attr("name");
			inputValue = $(this).val();
			if (!$(this).is(":checked") && $(".singleFilterModal input[type='checkbox'][name='"+inputName+"']:checked").length) {
				$("#offerFromDealer .singleTable[data-"+inputName+"='"+inputValue+"']").css({
					display: "none"
				})
			};
		});
	}

	$(".singleFilter input[type='checkbox']").on('change',function(){
		$(".singleTable").css("display", "block");
		singleFilterSelect ();
	});

	singleFilterSelect ();

	function singleFilterSelect () {
		$(".singleFilter input[type='checkbox']").each(function(){
			var inputName = $(this).attr("name"),
				inputValue = $(this).val();
			inputName = $(this).attr("name");
			inputValue = $(this).val();
			if (!$(this).is(":checked") && $(".singleFilter input[type='checkbox'][name='"+inputName+"']:checked").length) {

				$(".singleTable[data-"+inputName+"='"+inputValue+"']").css({
					display: "none"
				})
			} else if ($(this).is(":checked")) {
				$(".singleFilterModal input[type='checkbox'][name='"+inputName+"'][value='"+inputValue+"']").prop("checked", true);
			} else if (!$(this).is(":checked")) {
				$(".singleFilterModal input[type='checkbox'][name='"+inputName+"'][value='"+inputValue+"']").prop("checked", false);
			} else {
				$(".singleFilterModal input[type='checkbox'][name='"+inputName+"'][value='"+inputValue+"']").prop("checked", false);
			}
		});
	}

	var elemsCache = "";
	$(".singleFilter input[name='view']").change(function(){
		if($(this).val()=="new") {
			$(".singleTable:not(*[data-new='true'])").css({
				"height": "0",
				border: "none",
				marginTop: "0",
				marginBottom: "0"
			});
		} else {
			$(".singleTable:not(*[data-new='true'])").css({
				"height": "auto",
				border: "border: 1px solid #ccc",
				marginTop: "5px",
				marginBottom: "15px"
			});
		}
	});
	$(".singleStar").click(function(){
		$(this).toggleClass("active");
		if ($(".singleStar.active").length>1) $(".singleComplect").removeClass("unactive");
		else $(".singleComplect").addClass("unactive");
        
        addToCookie($(this));

	});

	if ($("#slider-range-modal").length) {
		$("#slider-range-modal").slider({
			range: true,
			min: parseInt($("#slider-range-modal").attr("data-min").replace(/[ ]/g, '')),
			max: parseInt($("#slider-range-modal").attr("data-max").replace(/[ ]/g, '')),
			values: [parseInt($("#downPrice").val().replace(/[ ]/g, '')), parseInt($("#topPrice").val().replace(/[ ]/g, ''))],
			slide: function( event, ui ) {
				$("#downPrice").val(priceFormat(ui.values[0]));
				$("#topPrice").val(priceFormat(ui.values[1]));
			},
			change: function () {
				singleFilterSelectCompl();
			}
		});
	};

	$('input#downPrice, input#topPrice').keyup(function() {
		singleFilterSelectCompl();
	})

	function singleFilterSelectCompl()
	{
		var priceMin = parseInt($("input#downPrice").val().replace(/\s+/g, '')),
			priceMax = parseInt($("input#topPrice").val().replace(/\s+/g, ''));

		priceMin = isNaN(priceMin) ? 0 : priceMin;
		priceMax = isNaN(priceMax) ? 0 : priceMax;

		$("#offerFromDealer .compl.oneSTR").each(function() {
			var price = $(this).attr("data-price");
			if (price >= priceMin && price <= priceMax) {
				$(this).show();
			} else {
				$(this).hide();
			}

		});

	}
/*	$(document).on("change", ".STRLeft input[type='radio']", function(){
		if($(".STRLeft input[type='radio']:checked").length)
        { 
            $(".veryBigBtn").removeClass("unactive");
            $(".veryBigBtn").attr("data-toggle","modal" );
            $("#inputComplectation").val($(this).next().attr('href'));

        }
		else $(".veryBigBtn").addClass("unactive");
	});*/

	function singleSize(){
		if ($(".secondSingleCell").length) {
			if ($(window).width()>767) {
				if ($(".secondSingleCell").outerHeight()>$(".firstSingleCell").outerHeight()) {
					$(".firstSingleCell").css({
						height: $(".secondSingleCell").outerHeight()+"px"
					})
				} else {
					$(".secondSingleCell").css({
						height: $(".firstSingleCell").outerHeight()+"px"
					});
				}
			} else {
				$(".firstSingleCell,.secondSingleCell").css({
					height: "auto"
				})
			}
		}
		if ($(".rightDealers").length) {
			if ($(window).width()>767) {
				$(".leftDealers,.rightDealers").css("height", "auto")
				if ($(".rightDealers").outerHeight()>$(".leftDealers").outerHeight()) {
					$(".leftDealers").css("height", $(".rightDealers").outerHeight()+"px");
				} else {
					$(".rightDealers").css("height", $(".leftDealers").outerHeight()+"px");
					$(".leftDealerMap, .forDealerMap").css("height", $(".rightDealers .whiteSpace").height()-$(".dealerLeftTitle").height()-$(".buyDealerWrapper").height()-20+"px");
				}
			} else {
				$(".leftDealers,.rightDealers").css({
					height: "auto"
				})
			}
		}
		if ($(".profileTop").length) {
			$(".profileTop").css("height", "auto");
			var profileTop = 0;
			$(".profileTop").each(function(){
				if($(this).outerHeight()>profileTop) profileTop = $(this).outerHeight();
			});
			if(profileTop>0) $(".profileTop").css("height", profileTop+"px");
		};
	}
	$(window).resize(function(){
		singleSize();
	});
	singleSize();
	$(document).on("click", ".leftColPhoto img", function(){
		var str = "",
			self = $(this),
			firstImg = $(this).parent().next().css("backgroundImage");
		str = "<div class='onePhotoNav'><img src='"+firstImg.substring(0, firstImg.length-1).replace('url(','').replace(/"/g,"")+"' alt=''></div>"
		$("body").css("overflow", "hidden");
		$(this).parent().children("img").each(function(){
			if ($(this).is(self)) str += "<div class='onePhotoNav active'><img src='"+$(this).attr("src")+"' alt=''></div>";
			else str += "<div class='onePhotoNav'><img src='"+$(this).attr("src")+"' alt=''></div>";
		});
		$("body").append("<div class='popup'><div class='closePopup glyphicon glyphicon-remove'></div><div class='popupIndexPhoto'  style='background:url("+$(this).attr("src")+") 50% 50% / contain no-repeat;'><a class='carousel-control left' href='#'><span class='glyphicon glyphicon-chevron-left'></span></a><a class='carousel-control right' href='#'><span class='glyphicon glyphicon-chevron-right'></span></a></div><div class='photosNav'>"+str+"<div class='slideLeft'><span class='glyphicon glyphicon-chevron-left'>&nbsp;</span></div><div class='slideRight'><span class='glyphicon glyphicon-chevron-right'>&nbsp;</span></div></div></div></div>");

        $(document).keydown(function(e){
            if(e.keyCode==39) $(".popupIndexPhoto .carousel-control.right").click();
            else if(e.keyCode==37) $(".popupIndexPhoto .carousel-control.left").click();
        });
        $('.photosNav').mousewheel(function(event) {
            if (event.deltaY<0) $(".slideRight").click();
            else $(".slideLeft").click();
        });
		
        $(".popupIndexPhoto").css("height",$(".popup").outerHeight()-$(".photosNav").outerHeight()-parseInt($(".popupIndexPhoto").css("marginTop"))+"px");
		$(".photosNav").each(function(){
			var leftPos = 0;
			$(this).children(".onePhotoNav").each(function(){
				$(this).css({
					position: "absolute",
					left: leftPos + "px",
					top: 0
				});
				leftPos += $(this).outerWidth();
			});
			if (leftPos>$(this).parent().parent().outerWidth()) $(this).children(".slideRight").css("display", "block");
		});
		$(".photosNav .active").click();
	});
	function keyPressed(e) {
		var keyCode = e.which;
		if(e.which===27) $(".closePopup").click();
	}
	$(document).on("keydown", keyPressed);
	$(document).on("click", ".indexColPhoto", function(){
		var str = "",
			self = $(this),
			firstImg = $(this).css("backgroundImage"),
			thisSrc = firstImg.substring(0, firstImg.length-1).replace('url(','').replace(/"/g,"");
		str = "<div class='onePhotoNav active'><img src='"+thisSrc+"' alt=''></div>"
		$("body").css("overflow", "hidden");
		$(this).prev().children("img").each(function(){
			str += "<div class='onePhotoNav'><img src='"+$(this).attr("src")+"' alt=''></div>";
		});
		$("body").append("<div class='popup'><div class='closePopup glyphicon glyphicon-remove'></div><div class='popupIndexPhoto' style='background:url("+thisSrc+") center center no-repeat; background-size: contain;'><a class='carousel-control left' href='#'><span class='glyphicon glyphicon-chevron-left'></span></a><a class='carousel-control right' href='#'><span class='glyphicon glyphicon-chevron-right'></span></a></div><div class='photosNav'>"+str+"<div class='slideLeft'><span class='glyphicon glyphicon-chevron-left'>&nbsp;</span></div><div class='slideRight'><span class='glyphicon glyphicon-chevron-right'>&nbsp;</span></div></div></div></div>");

        $(document).keydown(function(e){
            if(e.keyCode==39) $(".popupIndexPhoto .carousel-control.right").click();
            else if(e.keyCode==37) $(".popupIndexPhoto .carousel-control.left").click();
        });
        $('.photosNav').mousewheel(function(event) {
            if (event.deltaY<0) $(".slideRight").click();
            else $(".slideLeft").click();
        });
		
        $(".popupIndexPhoto").css("height",$(".popup").outerHeight()-$(".photosNav").outerHeight()-parseInt($(".popupIndexPhoto").css("marginTop"))+"px");
		$(".photosNav").each(function(){
			var leftPos = 0;
			$(this).children(".onePhotoNav").each(function(){
				$(this).css({
					position: "absolute",
					left: leftPos + "px",
					top: 0
				});
				leftPos += $(this).outerWidth();
			});
			if (leftPos>$(this).parent().parent().outerWidth()) $(this).children(".slideRight").css("display", "block");
		});
		$(".photosNav .active").click();
	});
	$(document).on("click", ".popupIndexPhoto .left", function(e){
		e.preventDefault();
        if ($(".photosNav .active").prev(".onePhotoNav").length) $(".photosNav .active").prev().click();
	});
	$(document).on("click", ".popupIndexPhoto .right", function(e){
		e.preventDefault();
        if ($(".photosNav .active").next(".onePhotoNav").length) $(".photosNav .active").next().click();
	});
	$(document).on("click", ".closePopup", function(){
		$(".popup").remove();
		$("body").css("overflowY", "auto");
	});
	$(document).on("click", ".onePhotoNav", function(){
		var lastElem = $(this).parent().children(".onePhotoNav").last(),
			firstElem = $(this).parent().children(".onePhotoNav").first(),
			different,
			self = $(this);
		if(($(this).position().left+$(this).outerWidth()/2)>$(this).parent().outerWidth()/2 && !lockAnimate && lastElem.position().left+lastElem.outerWidth()>$(this).parent().outerWidth()){
			lockAnimate = true;
			different = ($(this).position().left+$(this).outerWidth()/2)-$(this).parent().outerWidth()/2;
			if (lastElem.position().left+lastElem.outerWidth() - different < $(this).parent().outerWidth())
				different = lastElem.position().left+lastElem.outerWidth() - $(this).parent().outerWidth()-15;
			$(this).parent().children(".onePhotoNav").each(function(){
				$(this).animate({
					left: $(this).position().left-different+"px"
				}, 500, function(){
					lockAnimate = false;
					if (firstElem.position().left<0)
						self.parent().children(".slideLeft").css("display", "block");
					else
						self.parent().children(".slideLeft").css("display", "none");
					if (lastElem.position().left+lastElem.outerWidth()-15>$(this).parent().outerWidth())
						self.parent().children(".slideRight").css("display", "block");
					else
						self.parent().children(".slideRight").css("display", "none");
				});
			})
		} else if(($(this).position().left+$(this).outerWidth()/2)<$(this).parent().outerWidth()/2 && !lockAnimate && firstElem.position().left<-5){
			lockAnimate = true;
			different = $(this).parent().outerWidth()/2-($(this).position().left+$(this).outerWidth()/2);
			if (firstElem.position().left + different > 0) different = -firstElem.position().left;
			$(this).parent().children(".onePhotoNav").each(function(){
				$(this).animate({
					left: different+$(this).position().left+"px"
				}, 500, function(){
					lockAnimate = false;
					if (firstElem.position().left<0)
						self.parent().children(".slideLeft").css("display", "block");
					else
						self.parent().children(".slideLeft").css("display", "none");
					if (lastElem.position().left+lastElem.outerWidth()-15>$(this).parent().outerWidth())
						self.parent().children(".slideRight").css("display", "block");
					else
						self.parent().children(".slideRight").css("display", "none");
				});
			})
		}
		$(this).parent().children(".active").removeClass("active")
		$(".popupIndexPhoto").css("backgroundImage", "url("+$(this).children().attr("src")+")").attr("data-counter", $(this).attr("data-counter"));
		$(this).addClass("active");
		$(".popupIndexPhoto .left").css("display", "block");
		$(".popupIndexPhoto .right").css("display", "block");
		if ($(this).index()===$(this).parent().children(".onePhotoNav").last().index()) $(".popupIndexPhoto .right").css("display", "none");
		if ($(this).index()===$(this).parent().children(".onePhotoNav").first().index()) $(".popupIndexPhoto .left").css("display", "none");
	});
	$(".btnValid").click(function() {
		var formValid = true;
		$(this).parents(".singleForm").find('input').each(function() {
			var formGroup = $(this).parents('.form-group'),
				glyphicon = formGroup.find('.form-control-feedback');
			if (this.checkValidity()) {
				formGroup.addClass('has-success').removeClass('has-error');
				glyphicon.addClass('glyphicon-ok').removeClass('glyphicon-remove');
			} else {
				formGroup.addClass('has-error').removeClass('has-success');
				glyphicon.addClass('glyphicon-remove').removeClass('glyphicon-ok');
				formValid = false;  
			}
		});
	});
	$('#buyNow').click(function(event) {
        event.preventDefault();
		var formValid = true;
		$(this).parents(".modal").find('input').each(function() {
            var formGroup = $(this).parents('.form-group'),
				glyphicon = formGroup.find('.form-control-feedback');
			if (this.checkValidity()) {
				formGroup.addClass('has-success').removeClass('has-error');
				glyphicon.addClass('glyphicon-ok').removeClass('glyphicon-remove');
			} else {
				formGroup.addClass('has-error').removeClass('has-success');
				glyphicon.addClass('glyphicon-remove').removeClass('glyphicon-ok');
				formValid = false;  
			}
		});
        
        if(formValid)
        {
            var mod = $("#complectSelect input").val();
            
            $.ajax({type: "POST",
                  url: "/car/catalog/ajaxmod",
                  data: {'mod':mod,'compl':0,'idx':$(".topCompare .oneCompareModel").length},
                  success: function(data){
                       addElem(data);
                       $("button.close").click();  
                       
                  }
            });
        }
	});
	$(document).on("click", ".sortCompare [data-toggle='modal']", function(){
		$("#markSelect .searchSelectTitle").click();
		$("#markSelect input").focus();
	});
	$(document).on("click", ".enable .searchSelectTitle,.enable .searchSelectTitle>div,.enable .searchSelectTitle>span", function(){
		var thisElem = $(this);
		if (!$(this).hasClass("searchSelectTitle")) thisElem = $(this).parents(".searchSelectTitle");
		$(".searchInput").css("display","none");
		thisElem.css("display", "none");
		$(".searchResText").remove();
		thisElem.parent().children(".searchInput").css("display", "block").find(".oneSearchResult").css("display","block");
	});
	$(document).on("keyup", ".searchInput input", function(){
		var thisVal = $.trim($(this).val()).toLowerCase(),
			flug = true;
		$(".searchResText").remove();
		if (thisVal.length) {
			$(this).next(".searchResults").children(".oneSearchResult").each(function(){
				if ($(this).text().toLowerCase().indexOf(thisVal)+1) $(this).css('display', 'block');
				else $(this).css('display', 'none');
			});
		} else{
			$(this).next(".searchResults").children(".oneSearchResult").css('display', 'block');
		}
		$(this).next(".searchResults").children(".oneSearchResult").each(function(){
			if ($(this).css("display")!=="none") flug = false;
		});
		if(flug){
			$(this).next(".searchResults").prepend('<div class="searchResText">Ничего не найдено...</div>')
		}
	});
	$(document).on("click",".oneSearchResult", function(e){
        
        $(this).parents('.searchSelect ').removeClass('has-error');
		$(this).parent().children(".active").removeClass("active");
		if ($(this).parent().parent().parent("#markSelect").length) {
            
            $.ajax({type: "POST",
                  url: "/car/catalog/getmodellist",
                  data: {'mark':$(this).data().id},
                  success: function(data){
                    var models = JSON.parse(data);
                    $("#modelSelect .searchResults .oneSearchResult").remove();  
                    $.map( models, function( val, i ) {
                        $("#modelSelect .searchResults").append('<div class="oneSearchResult" data-id="'+i+'">'+val+'</div>');
                          //console.log( val + ": " + i );
                    });
                  }
            });
            
            $("#modelSelect,#yearSelect,#carcaseSelect,#complectSelect").find("input").val("");
            
			$("#modelSelect,#yearSelect,#carcaseSelect,#complectSelect").find(".active").removeClass("active");
			$("#modelSelect").addClass("enable").find('.searchSelectTitle div').text("Выберите модель");
			$("#yearSelect").removeClass("enable").find('.searchSelectTitle div').text("Выберите модельный год");
			$("#carcaseSelect").removeClass("enable").find('.searchSelectTitle div').text("Выберите тип кузова");
			$("#complectSelect").removeClass("enable").find('.searchSelectTitle div').text("Выберите комплектацию");
		}
        if ($(this).parent().parent().parent("#modelSelect").length) {
            
            $("#yearSelect,#carcaseSelect,#complectSelect").find("input").val("");
            $.ajax({type: "POST",
                  url: "/car/catalog/getgenerationlist",
                  data: {'model':$(this).data().id},
                  success: function(data){
                    var models = JSON.parse(data);
                    $("#yearSelect .searchResults .oneSearchResult").remove();  
                    $.map( models, function( val, i ) {
                        $("#yearSelect .searchResults").append('<div class="oneSearchResult" data-id="'+i+'">'+val+'</div>');
                          //console.log( val + ": " + i );
                    });
                  }
            });

            
            $("#yearSelect").addClass("enable").find(".oneSearchResult").first().click();
            $("#carcaseSelect").removeClass("enable").find('.searchSelectTitle div').text("Выберите тип кузова");
            $("#complectSelect").removeClass("enable").find('.searchSelectTitle div').text("Выберите комплектацию");
        }
        if ($(this).parent().parent().parent("#yearSelect").length) {
            $("#carcaseSelect,#complectSelect").find("input").val("");
            
            $.ajax({type: "POST",
                  url: "/car/catalog/getbodylist",
                  data: {'gen':$(this).data().id},
                  success: function(data){
                    var models = JSON.parse(data);
                    $("#carcaseSelect .searchResults .oneSearchResult").remove();  
                    $.map( models, function( val, i ) {
                        $("#carcaseSelect .searchResults").append('<div class="oneSearchResult" data-id="'+i+'">'+val+'</div>');
                          //console.log( val + ": " + i );
                    });
                  }
            });

            $("#carcaseSelect").addClass("enable").find(".oneSearchResult").first().click();
            $("#complectSelect").removeClass("enable").find('.searchSelectTitle div').text("Выберите комплектацию");
        }
		if ($(this).parent().parent().parent("#carcaseSelect").length) {
            $("#complectSelect").find("input").val("");
            $.ajax({type: "POST",
                  url: "/car/catalog/getmodlist",
                  data: {'gen':$("#yearSelect .searchSelectTitle div").data().id,'body':$(this).data().id},
                  success: function(data){
                    var models = JSON.parse(data);
                    $("#complectSelect .searchResults .oneSearchResult").remove();  
                    $.map( models, function( val, i ) {
                        $("#complectSelect .searchResults").append('<div class="oneSearchResult" data-id="'+i+'">'+val+'</div>');
                          //console.log( val + ": " + i );
                    });
                  }
            });

			$("#complectSelect").addClass("enable").find(".oneSearchResult").first().click();
		}
        
        $(this).closest(".searchSelect").find("input").val($(this).data().id);
		$(this).addClass("active");
		$(this).parent().parent().css("display", "none");
		$(this).parent().parent().prev().css("display", "block").children("div").text($(this).text()).data('id',$(this).data().id);
	});
	$(document).on("click", ".addCompareModal *", function(e){
		if ($(this).hasClass("searchSelect") || $(this).parents(".searchSelect").length) {
			e.stopPropagation();
		} else {
			$(".searchInput").css("display","none");
			$(".searchSelectTitle").css("display","block");
		}
	});
    
    $("#filtered").click(function(){
        $("#downPrice").val($("#downPrice").val().replace(" ","").replace(" ",""));
        $("#topPrice").val($("#topPrice").val().replace(" ","").replace(" ",""));
    });

	$('input[name="inputAddressOther"]').focusin(function(){
		$('input#inputAddressMoscow').prop('checked',false);
		$('.addressOther input[name="inputAddress"]').prop('checked','checked');
	});

    if ($('input#inputAddressMoscow').is(':checked')) {
        $('.addressOther input[name="inputAddress"]').prop('checked',false);
    }

	$("#SeoPage_group_id").on('change', function() {

		$('.GroupBlock').hide();

		if ($(this).val() == '2'){
			$('.BGroupBlock').show();
		} else if ($(this).val() == '3') {
			$('.CGroupBlock').show();
		} else {
			$('.GroupBlock').hide();
		}
	});

	$('#SeoPage_group_id option').each(function() {
		if(this.selected ) {

			$('.GroupBlock').hide();

			if ($(this).val() == '2'){
				$('.BGroupBlock').show();
			} else if ($(this).val() == '3') {
				$('.CGroupBlock').show();
			} else {
				$('.GroupBlock').hide();
			}
		}

	});

/*************************************/
	$( ".obzor .obzor-content a" ).attr("target","_blank");

	var navListItems = $('div.setup-panel div a'),
		allNextBtn = $('.nextBtn');

	$('.setup-content').hide();

	$('#modal_from_ajax').on('click', 'div.setup-panel div a', function(e) {

		e.preventDefault();
		var $target = $($(this).attr('href')),
			$item = $(this);

		if (!$item.hasClass('disabled')) {
			navListItems.removeClass('btn-primary').addClass('btn-default');
			$item.addClass('btn-primary');
			$('.setup-content').hide();
			$target.show();
			$target.find('input:eq(0)').focus();
		}
	});

	/*allNextBtn.click(function(){
		var curStep = $(this).closest(".setup-content"),
			curStepBtn = curStep.attr("id"),
			nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
			curInputs = curStep.find(".compl input[type='checkbox']"),
			isValid = true;

			console.log(curStep);

		$(".form-group").removeClass("has-error");
		for(var i=0; i<curInputs.length; i++){
			if (!curInputs[i].validity.valid){
				isValid = false;
				$(curInputs[i]).closest(".form-group").addClass("has-error");
			}
		}

		if (isValid)
			nextStepWizard.removeAttr('disabled').trigger('click');
	});*/
	$("#modal_from_ajax").on('click', '#step-1 [type="button"]', function(){


		var isValid = false,
		    curStep = $(this).closest(".setup-content"),
			curStepBtn = curStep.attr("id"),
			nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a");

		$('.compl input').each(function() {
			if ($(this).is(':checked'))
				isValid = true;
		});

		if (isValid)
			nextStepWizard.removeAttr('disabled').trigger('click');

	});


	$("#modal_from_ajax").on('click', '#step-2 [type="button"]', function(){

		var isValid = false,
			curStep = $(this).closest(".setup-content"),
			curStepBtn = curStep.attr("id"),
			nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a");

		$('.colorsChar input').each(function() {
			if ($(this).is(':checked'))
				isValid = true;
		});

		if ($('#inputColorCustom').val().replace(/\s+/g, '') != '')
			isValid = true;

		if ($('#inputColorAny').is(':checked'))
			isValid = true;

		if (isValid)
			nextStepWizard.removeAttr('disabled').trigger('click');
	});

	$('div.setup-panel div a.btn-primary').trigger('click');


});
