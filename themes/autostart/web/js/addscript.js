$(document).ready(function(){

    function updateMod(data)
    {
        console.log(1);

    }

    if($(".inner-obzor").height()<$(".obzor-content").height())
    {
        $(".scrollDown").fadeIn();
    }

    $(".scrollDown").click(function(event){
        event.preventDefault();
        $(".inner-obzor").animate({scrollTop:$(".inner-obzor").scrollTop()+200}, '200',function(){
            if($(".inner-obzor").scrollTop()>0)
            {
                $(".scrollUp").fadeIn();
            }
            if($(".inner-obzor").scrollTop()>=($(".obzor-content").height() - $(".inner-obzor").height()) )
            {
                $(".scrollDown").fadeOut();
            }
        });

    });
    $(".scrollUp").click(function(event){
        event.preventDefault();
        $(".inner-obzor").animate({scrollTop:$(".inner-obzor").scrollTop()-200}, '200',function(){
            if($(".inner-obzor").scrollTop()==0)
            {
                $(".scrollUp").fadeOut();
            }
            if($(".inner-obzor").scrollTop()<($(".obzor-content").height() - $(".inner-obzor").height()) )
            {
                $(".scrollDown").fadeIn();
            }
        });

    });

    $("#feedback").click(function(){
        var form = $(this).closest("form");
        var vals = $(form).serialize();
        $.ajax({type: "POST",
              url: "/site/feedback",
              data: vals,
              success: function(data){
                var result =  JSON.parse(data);
                if(result.errors.length==0)
                {

                    $(form).find(".form-group").removeClass("has-error").find("input:not(.btn)").val('');
                    $(form).find(".form-group textarea").val('');

                    $("#alertfb").removeClass('alert-danger').html(result.message).slideDown();

                  $("#alertfb").addClass('alert-success').html(result.message);
                }
                else
                {
                    $("#alertfb").addClass('alert-danger').html(result.message).slideDown();

                    console.log(result.errors);
                    $(form).find(".form-group").removeClass("has-error");

                    $.map( result.errors, function( val, i ) {
                        $(form).find("#"+i).closest(".form-group").addClass("has-error");
                          //console.log( val + ": " + i );
                    });

                }
              },
              fail: function(){
                  $("#alertfb").addClass('alert-danger').html('Произошла ошибка при отправке формы. Попробуйте позднее');
              }
        });
        console.log(vals);
    });

    $("#sendNow").click(function(event){
        event.preventDefault();
        var form = $(this).closest("form");
        var vals = $(form).serialize();
        $.ajax({type: "POST",
              url: "/site/feedbackDealer",
              data: vals,
              success: function(data){
                var result =  JSON.parse(data);
                if(result.errors.length==0)
                {

                    $(form).find(".form-group").removeClass("has-error").find("input:not(.btn)").val('');
                    $(form).find(".form-group textarea").val('');

                    $("#alertfb").removeClass('alert-danger').html(result.message).slideDown();

                  $("#alertfb").addClass('alert-success').html(result.message);
                  $("#myModal input").val('');
                  $("#myModal textarea").html('');
                  $("#myModal .close").click();
                }
                else
                {
                    $("#alertfb").addClass('alert-danger').html(result.message).slideDown();

                    console.log(result.errors);
                    $(form).find(".form-group").removeClass("has-error");

                    $.map( result.errors, function( val, i ) {
                        $(form).find("#"+i).closest(".form-group").addClass("has-error");
                          //console.log( val + ": " + i );
                    });

                }
              },
              fail: function(){
                  $("#alertfb").addClass('alert-danger').html('Произошла ошибка при отправке формы. Попробуйте позднее');
              }
        });
        console.log(vals);
    });

    $("#livesearch").keyup(function(){
        var s = $(this).val();
        if(s.length>2)
        {
            $.ajax({type: "POST",
                  url: "/car/catalog/livesearch",
                  data: {'s':s},
                  success: function(data){
                      $("#search-list").focus();
                       var result =  JSON.parse(data);
                       if(result)
                       {
                           fillSearch(result);
                       }
                  }
            });
        }
        else
        {
            $("#search-list").html("");

        }
    });

    $("#sendPref").click(function(event){
        event.preventDefault();
        var form = $(this).closest("form");
        var vals = $(form).serialize();
        $.ajax({
            type: "POST",
            url: "/site/feedbackPreferences",
            data: vals,
            success: function(data){
                var result =  JSON.parse(data);
                if(result.errors.length==0)
                {

                    $(form).find(".form-group").removeClass("has-error");
                    $(form).find(".form-group textarea").val('');

                    $("#alertfb").removeClass('alert-danger').html(result.message).slideDown();

                    $("#alertfb").addClass('alert-success').html(result.message);
                    $("#ModalPreferences input[type='text'], #ModalPreferences input[type='checkbox']").val('');
                    $("#ModalPreferences textarea").html('');
                    $("#ModalPreferences .close").click();
                }
                else
                {
                    $("#alertfb").addClass('alert-danger').html(result.message).slideDown();

                    console.log(result.errors);
                    $(form).find(".form-group").removeClass("has-error");

                    $.map( result.errors, function( val, i ) {
                        $(form).find("#"+i).closest(".form-group").addClass("has-error");
                        //console.log( val + ": " + i );
                    });

                }
            },
            fail: function(){
                $("#alertfb").addClass('alert-danger').html('Произошла ошибка при отправке формы. Попробуйте позднее');
            }
        });
        console.log(vals);
    })

    $("#modal_from_ajax").on('click', "#sendDealersOffer", function(event){
        event.preventDefault();
        var form = $(this).closest("form");
        var vals = $(form).serialize();

        $.ajax({type: "POST",
            url: "/car/catalog/OfferFromDealer",
            data: vals,
            success: function(data){
                var result = JSON.parse(data);
                if(result.errors.length==0)
                {
                    $(form).find(".form-group").removeClass("has-error");
                    $(form).find(".form-group textarea").val('');

                    $("#alertfb").removeClass('alert-danger').html(result.message).slideDown();

                    $("#alertfb").addClass('alert-success').html(result.message);
                    $("#offerFromDealer input[type='text']").val('');
                    $("#offerFromDealer input[type='checkbox']").prop( "checked", false );
                    $("#offerFromDealer input[type='inputColorAny']").prop( "checked", true );
                    $("#offerFromDealer .stepwizard-row .stepwizard-step a").removeClass('btn-primary');
                    $("#offerFromDealer .stepwizard-row .stepwizard-step.step-1 a").addClass('btn-primary');
                    
                    //$(".setup-content").hide();
                    //$("#step-1").show();

                    window.location.href = "/thanks";
                }
                else
                {
                    $("#alertfb").addClass('alert-danger').html(result.message).slideDown();

                    console.log(result.errors);
                    $(form).find(".form-group").removeClass("has-error");

                    $.map( result.errors, function( val, i ) {
                        $(form).find("#"+i).closest(".form-group").addClass("has-error");
                        //console.log( val + ": " + i );
                    });

                }
            },
            fail: function(){
                $("#alertfb").addClass('alert-danger').html('Произошла ошибка при отправке формы. Попробуйте позднее');
            }
        });

    })

    $("body *").not("#search-list").click(function() {
        $('#search-list').slideUp();
    });

    $('.get_modal').click(function (e) {
        var id = $(this).attr('model');
        $.ajax({
            type: "POST",
            url: "/car/catalog/SingleModal",
            data: {
                id: id
            },
            dataType: 'html',
            success: function(data){
                //console.log(data);
                $('#modal_from_ajax').html(data);

                var allWells = $('.setup-content');
                allWells.hide();
                $('div.setup-panel div a.btn-primary').trigger('click');

                var link = $('#click_modal')[0];
                var linkEvent = null;
                if (document.createEvent) {
                    linkEvent = document.createEvent('MouseEvents');
                    linkEvent.initEvent('click', true, true);
                    link.dispatchEvent(linkEvent);
                }
                else if (document.createEventObject) {
                    linkEvent = document.createEventObject();
                    link.fireEvent('onclick', linkEvent);
                }

                //$('click_modal').trigger('click');
            },
            fail: function(){
                $("#alertfb").addClass('alert-danger').html('Произошла ошибка при отправке формы. Попробуйте позднее');
            }
        });
        e.preventDefault();
    });

});

function addToCookie(elem)
{
    var mod = $(elem).data().modify;
    var compl = $(elem).data().complectation;
    var favs = "";

    if(mod)
    {
        try {
            favs = JSON.parse($.cookie('favorites'));
        } catch (e) {
            favs = [];
        }

        var fav = {
            modify: mod,
            compl: compl
        }
        if($(elem).hasClass('redLike') || $(elem).hasClass('active'))
        {
            favs.push(fav);
        }
        else
        {
            for (var key in favs) {
                if (favs[key].modify == fav.modify && favs[key].compl == fav.compl ) {
                    favs.splice(key, 1);
                }
            }
        }

        $.cookie('favorites',JSON.stringify(favs), { path: '/' });
    }

    try {
        favs = JSON.parse($.cookie('favorites'));
    } catch (e) {
        favs = [];
    }
    if(favs.length>=2 && $(".goToCompare").length) $(".goToCompare").css("display","inline-block");
    else $(".goToCompare").css("display","none");

    $(".cnt-fav").html(favs.length);

}

function getMod(elem)
{
    var mod = $(elem).find("option:selected").data().modid?$(elem).find("option:selected").data().modid:$(elem).data().modid;

    if(!mod)
    {
        return $(elem).val();
    }
    return mod;
}
function getCompl(elem)
{
    return ($(elem).find("option:selected").data().compl || $(elem).find("option:selected").data().compl==0)?$(elem).find("option:selected").data().compl:$(elem).val();
}
function getIdx(elem)
{
    return $(elem).closest(".oneCompareModel").data().idx;
}

function replaceElem(code)
{
    var elem =  jQuery.parseJSON(code);
    $(".topCompare .oneCompareModel[data-idx="+elem.idx+"]").replaceWith(elem.head);
    $(".compareContent .oneCompareModel[data-idx="+elem.idx+"]").replaceWith(elem.content);
    checkCompare();
}

function addElem(code)
{
    var elem =  jQuery.parseJSON(code);
    $(".topCompare .oneCompareModel").last().after(elem.head);
    $(".compareContent .oneCompareModel").last().after(elem.content);
                       checkCompare();

}

function fillSearch(data)
{
    var i = 0;
    $("#search-list").html("");
    if(data.mark.length>0)
    {
        $("#search-list").append('<li class="header">Марка</li>');
        $.map( data.mark, function( val, i ) {
            if(i<10)
            {
                $("#search-list").append('<li><a class="name" href="'+val.url+'"><div class="img">'+val.icon+'</div><span>'+val.name+'</span></a></li>');
                i++;
            }
              //console.log( val + ": " + i );
        });
    }
    if(data.body.length>0)
    {
        $("#search-list").append('<li class="header">Кузов</li>');
        $.map( data.body, function( val, i ) {
            if(i<10)
            {
                $("#search-list").append('<li><a class="name" href="'+val.url+'"><div class="img svg">'+val.icon+'</div><span>'+val.name+'</span></a></li>');
              //console.log( val + ": " + i );
                i++;
            }
        });
    }
    if(data.model.length>0)
    {
        $("#search-list").append('<li class="header">Модель</li>');
        $.map( data.model, function( val, i ) {
            if(i<10)
            {
                $("#search-list").append('<li><a class="name" href="'+val.url+'"><div class="img">'+val.icon+'</div><span>'+val.name+'</span></a></li>');
              //console.log( val + ": " + i );
                i++;
            }
        });
    }
    $("#search-list").slideDown();
}
